<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="customtags" prefix="ctg" %>

<!DOCTYPE html>
<html>
<head>
<%@ include file="WEB-INF/jspf/messages/localization.jspf" %>
<title>${title_index_page}</title>
<link rel="stylesheet" href="<c:url value="/css/index.css"/>" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>

<body>
	<div id="header">
		<%@ include file="WEB-INF/jspf/buttons&fields/localizationButtons.jspf" %>
		<ctg:time/>
		<%@ include file="WEB-INF/jspf/buttons&fields/navigationButtons.jspf" %>
	</div>
	
	<div id="nav">
		<c:if test="${sessionScope.user == null}">
			<%@ include file="WEB-INF/jspf/buttons&fields/authorizationButtons&Fields.jspf" %>
		</c:if>
		
		<%--вывод сообщения о необходимости снова войти в систему--%>
		<c:if test="${requestScope.mustAuthorizeAgainFlag == 'true'}">
			<div id="error-message"><c:out value="${message_needs_authorization_again}" /></div> <br />
		</c:if>
		
		<c:if test="${sessionScope.user != null}">
			<c:out value="${message_auth_ok_info}, ${sessionScope.user.name} ${sessionScope.user.surname}" /> <br />
			<c:choose>
				<c:when test="${sessionScope.user.roleName == 'client'}">
					<%@ include file="WEB-INF/jspf/buttons&fields/clientOperationButtons&Fields.jspf" %> <br />
				</c:when>
				<c:when test="${sessionScope.user.roleName == 'administrator'}">
					<%@ include file="WEB-INF/jspf/buttons&fields/administratorOperationButtons&Fields.jspf" %> <br />
				</c:when>
			</c:choose>
			<%@ include file="WEB-INF/jspf/buttons&fields/logoutButton.jspf" %>
		</c:if>
	</div>
	
	<div id="section">
		<h1 style="text-align:center">${caption_hotel_information}</h1>
		<h4>${sessionScope.lastRequiredResource}</h4>
		
		<%@ include file="WEB-INF/jspf/content_pages/apartmentTypesTable.jspf" %>
		<%@ include file="WEB-INF/jspf/buttons&fields/checkOnDateButtons&Fields.jspf" %>
	</div>
	
	<div id="footer">
		<c:out value="${message_copyright}" />
	</div>
</body>
</html>