<%@ page language="java" contentType="text/html; charset=UTF-8"
	isErrorPage="true" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="customtags" prefix="ctg" %>

<!DOCTYPE html>
<html>
<head>
<%@ include file="../../WEB-INF/jspf/messages/localization.jspf" %>
<%@ include file="../../WEB-INF/jspf/content_pages/errorParameters.jspf" %>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>${title_error_page}</title>
<link rel="stylesheet" href="<c:url value="/css/index.css"/>" />
</head>

<body>		
	<div id="header">
		<ctg:time/>
	</div>
	
	<div id="section">
		<h1>${message_error_sorry}</h1>		
		
		<ctg:error-table headers="${headers}" parameters="${queueOfErrorParameters}" size="${queueOfErrorParameters.size()}">
			${queueOfMessages.poll()}
		</ctg:error-table>
		
		<br /><br />
		<form action="Controller" method="post">
			<input type="hidden" name="command" value="home" />
			<input type="hidden" name="resource-name" value="hotel_information" />
			<input type="submit" value="${message_goto_index_page}" />
		</form>
	</div>
	
	<div id="footer">
		<c:out value="${message_copyright}" />
	</div>
</body>
</html>