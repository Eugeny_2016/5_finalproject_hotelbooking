<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="customtags" prefix="ctg" %>

<!DOCTYPE html>
<html>
<head>
<%@ include file="../../WEB-INF/jspf/messages/localization.jspf" %>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>${title_after_closing_page}</title>
<link rel="stylesheet" href="<c:url value="/css/index.css"/>" />
</head>

<body>
		
	<div id="header">
		<ctg:time/>
	</div>
	
	<div id="section">
	<c:if test="${sessionScope.closingFlag == 'true'}">
		<h3>${message_booking_successful_closing}</h3>
	</c:if>
	
	<c:if test="${sessionScope.closingFlag == 'false'}">
		<h3>${message_booking_unsuccessful_closing}</h3>
	</c:if>
	
	<br /><br />
	<form action="../../Controller" method="post">
		<input type="hidden" name="command" value="home" />
		<input type="hidden" name="resource-name" value="hotel_information" />
		<input type="submit" value="${message_goto_index_page}" />
	</form>
	</div>
	
	<div id="footer">
		<c:out value="${message_copyright}" />
	</div>
</body>
</html>