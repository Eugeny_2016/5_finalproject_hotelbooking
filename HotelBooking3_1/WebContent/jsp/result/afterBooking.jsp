<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="customtags" prefix="ctg" %>

<!DOCTYPE html>
<html>
<head>
<%@ include file="../../WEB-INF/jspf/messages/localization.jspf" %>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>${title_after_booking_page}</title>
<link rel="stylesheet" href="<c:url value="/css/index.css"/>" />
</head>

<body>
		
	<div id="header">
		<ctg:time/>
	</div>

	<div id="section">
		<c:if test="${sessionScope.saveFlag == 'true'}">
			<c:out value="${message_successful_bookingForm_saving}" />
		</c:if>
				
		<c:if test="${sessionScope.denialReason != null}">
			<c:if test="${sessionScope.denialReason == 'No available apartments of required amount for client now'}">
				<h3>${message_no_available_apartments_of_amount}</h3>
			</c:if>
			<c:if test="${sessionScope.denialReason == 'No available apartments of required type for client now'}">
				<h3>${message_no_available_apartments_of_type}</h3>
			</c:if>
			<c:if test="${sessionScope.denialReason == 'No available apartments at all for client now'}">
				<h3>${message_no_available_apartments_at_all}</h3>
			</c:if>
			<c:if test="${sessionScope.denialReason == 'Booking form hasn&#039t been saved for some reason'}">
				<h3>${message_booking_not_saved_for_some_reason}</h3>
			</c:if>
			<a href="index.jsp"><button>${message_goto_index_page}</button></a>
		</c:if>
		
		<br /><br />
		<form action="../../Controller" method="post">
			<input type="hidden" name="command" value="home" />
			<input type="hidden" name="resource-name" value="hotel_information" />
			<input type="submit" value="${message_goto_index_page}" />
		</form>
	</div>
	
	<div id="footer">
		<c:out value="${message_copyright}" />
	</div>
</body>
</html>