<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="customtags" prefix="ctg" %>

<!DOCTYPE html>
<html>
<head>
<%@ include file="../../WEB-INF/jspf/messages/localization.jspf" %>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>${title_booking_page}</title>
<link rel="stylesheet" href="<c:url value="/css/index.css"/>" />
</head>

<body>
	<%--создание переменной со значением слова "ночь" в нужном падеже--%>	
	<c:choose>
		<c:when test="${sessionScope.nightCase == 'singular_nom'}">
			<c:set var="nightMessage" scope="page" value="${word_night_singular_nom_case}"></c:set>
		</c:when>
		<c:when test="${sessionScope.nightCase == 'singular_par'}">
			<c:set var="nightMessage" scope="page" value="${word_night_singular_par_case}"></c:set>
		</c:when>
		<c:when test="${sessionScope.nightCase == 'plural_par'}">
			<c:set var="nightMessage" scope="page" value="${word_night_plural_par_case}"></c:set>
		</c:when>
	</c:choose>
	
	<div id="header">
		<ctg:time/>
	</div>
	<div id="section">
		<h3><c:out value="${message_booking}" /></h3>
		<form action="Controller" method="post">
			
			<fieldset style="width:1185px;">
				<legend>${message_client_information_block}</legend><br />
				<c:out value="${message_name}" />
				<input type="text" name="name" value="${sessionScope.user.name}" size="25" readonly="readonly" />
				
				<c:out value="${message_surname}" />
				<input type="text" name="surname" value="${sessionScope.user.surname}" size="25" readonly="readonly" />
				
				<c:out value="${message_email}" />
				<input type="text" value="${sessionScope.user.email}" size="25" readonly="readonly"/> <br /><br />
			</fieldset>
			
			<br />
			<fieldset style="width:1185px;">
				<legend>${message_apartment_information_block}</legend><br />
				<c:out value="${message_apartment_type}" />
				<c:if test="${sessionScope.chosenApartmentType.name == 'standard_single'}">
					<input type="text" name="apartment-description" value="${apartment_type_standard_single}" size="60" readonly="readonly" />
				</c:if>
				<c:if test="${sessionScope.chosenApartmentType.name == 'standard_double'}">
					<input type="text" name="apartment-description" value="${ap_type_standard_double_12_bed}" size="60" readonly="readonly" />
				</c:if>
				<c:if test="${sessionScope.chosenApartmentType.name == 'executive_single'}">
					<input type="text" name="apartment-description" value="${apartment_type_represent_single}" size="60" readonly="readonly" />
				</c:if>
				<c:if test="${sessionScope.chosenApartmentType.name == 'executive_double'}">
					<input type="text" name="apartment-description" value="${ap_type_represent_double_12_bed}" size="60" readonly="readonly" />
				</c:if>
				<c:if test="${sessionScope.chosenApartmentType.name == 'family_1adult2kids'}">
					<input type="text" name="apartment-description" value="${apartment_type_family_1ad_2ch}" size="60" readonly="readonly" />
				</c:if>
				<c:if test="${sessionScope.chosenApartmentType.name == 'family_2adults2kids'}">
					<input type="text" name="apartment-description" value="${apartment_type_family_2ad_2ch}" size="60" readonly="readonly" />
				</c:if>
				
				<c:out value="${message_amount_of_chosen_apartments}" />
				<input type="text" name="amount-of-apartments" value="${sessionScope.amountOfApartments}" size="3" readonly="readonly" />
				
				<c:out value="${message_price_for} ${sessionScope.amountOfNights} ${nightMessage}" />
				<input type="text" name="apartment-total-price-as-string" value="${sessionScope.currencyFormat.format(sessionScope.apartmentTotalPrice*sessionScope.dollarCoefficient)}" size="15" readonly="readonly" /> <br /><br />
				
				<c:out value="${message_arrival_date}: " />
				<input type="text" name="full-entered-arrival-date" value="${sessionScope.fullEnteredArrivalDateAsString}" size="14" readonly="readonly" style="border: 0px;"/>
				<c:out value=" ${preposition_since} ${sessionScope.arrivalTimeSince}" /> <br />			
				
				<c:out value="${message_leaving_date}: " />
				<input type="text" name="full-entered-leaving-date" value="${sessionScope.fullEnteredLeavingDateAsString}" size="14" readonly="readonly" style="border: 0px;"/>
				<c:out value=" ${preposition_until} ${sessionScope.leavingTimeUntil}" /> <br />
				
				<c:out value="(${sessionScope.amountOfNights} ${nightMessage})" /> <br /><br />
				
				<c:out value="${message_bed_type}" />
				<select name="bed-type" required="required">
					<c:forEach var="bedType" items="${sessionScope.bedTypes}">
						<c:if test="${bedType.name == '1double'}">
							<option value="${bedType.description}">${bed_type_one_double}</option>
						</c:if>
						<c:if test="${bedType.name == '2single'}">
							<option value="${bedType.description}">${bed_type_two_single}</option>
						</c:if>
						<c:if test="${bedType.name == '1double1sofa'}">
							<option value="${bedType.description}">${bed_type_one_double_one_sofa}</option>
						</c:if>
						<c:if test="${bedType.name == '2single1sofa'}">
							<option value="${bedType.description}">${bed_type_two_single_one_sofa}</option>
						</c:if>
					</c:forEach>
				</select> <br /><br />
				
				<c:out value="${message_amount_of_guests}" />
				<select name="amount-of-guests" required="required">
					<c:forEach var="amount" begin="1" end="${sessionScope.apartmentGuestsMax}" step="1">
						<option value="${amount}" selected="selected">${amount}</option>
					</c:forEach>
				</select> <br /><br />
				
				<input type="checkbox" name="sms-approval-flag" value="true" />${message_booking_approval_sms}  <br />
			</fieldset>
			
			<br />
			<fieldset style="width:1185px;">
				<legend>${message_card_information_block}</legend>
				
				<c:out value="${message_card_type}" />
				<select name="card-type" required="required">
					<c:forEach var="type" items="${sessionScope.cardTypes}">
						<option value="${type.name}">${type.name}</option>
					</c:forEach>			
				</select>
				
				<c:out value="${message_card_number}" />
				<input type="text" name="card-number" placeholder="0000-1111-2222-3333" title="${message_card_number_format}" pattern="[0-9]{4}-[0-9]{4}-[0-9]{4}-[0-9]{4}" maxlength="19" size="25" required="required" /> <br /><br />
				
				<c:out value="${message_card_expiration_period}" />
				<select name="month-value" required="required">
					<c:forEach var="month" begin="${sessionScope.currentMonth}" end="12" step="1">
						<option value="${month}">${month}</option>
					</c:forEach>			
				</select>
				/
				<select name="year-value" required="required">
					<c:forEach var="year" begin="${sessionScope.currentYear}" end="${sessionScope.currentYear + 15}" step="1">
						<option value="${year}">${year}</option>
					</c:forEach>			
				</select>
			</fieldset>
			
			<c:if test="${requestScope.incorrectCardNumberFlag}">
				<div id="error-message"><c:out value="${message_card_number_incorrect_value}" /></div>
			</c:if> <br />
	
			
			<c:out value="${message_personal_preferences_block}" /><br />
			<textarea name="personal-prefs" rows="15" cols="70"></textarea> <br /><br />
			
			<c:set var="language" scope="page" value="${sessionScope.locale.getLanguage()}"/>
			<textarea name="terms-agreement-text" rows="15" cols="70" readonly="readonly">${sessionScope.mapBookingAgreement.get(language)}</textarea> <br /><br />
			<input type="checkbox" name="terms-agreement-flag" value="true" required="required" />${message_terms_agreement}  <br />
								
			<input type="hidden" name="command" value="confirm_booking" />
			<input type="submit" name="submit" value="${button_confirm_booking}" style="float: right;"/> <br />
		</form> <br />
		
		<form action="Controller" method="post">
			<input type="hidden" name="command" value="home" />
			<input type="hidden" name="resource-name" value="hotel_information" />
			<input type="submit" value="${message_goto_index_page}" />
		</form>
	</div>
	
	<div id="footer">
		<c:out value="${message_copyright}" />
	</div>
</body>
</html>