<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="customtags" prefix="ctg" %>

<!DOCTYPE html>
<html>
<head>
<%@ include file="../../WEB-INF/jspf/messages/localization.jspf" %>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>${title_bill_forming_page}</title>
<link rel="stylesheet" href="<c:url value="/css/index.css"/>" />
</head>

<body>
	<%--создание переменной со значением слова "ночь" в нужном падеже--%>	
	<c:choose>
		<c:when test="${requestScope.nightCase == 'singular_nom'}">
			<c:set var="nightMessage" scope="page" value="${word_night_singular_nom_case}"></c:set>
		</c:when>
		<c:when test="${requestScope.nightCase == 'singular_par'}">
			<c:set var="nightMessage" scope="page" value="${word_night_singular_par_case}"></c:set>
		</c:when>
		<c:when test="${requestScope.nightCase == 'plural_par'}">
			<c:set var="nightMessage" scope="page" value="${word_night_plural_par_case}"></c:set>
		</c:when>
	</c:choose>
	
	<div id="header">
		<ctg:time/>
	</div>
	<div id="section">
		<h3><c:out value="${message_bill_creation}" /></h3>
		<form action="Controller" method="post">
			
			<fieldset style="width:1185px;">
				<legend>${message_bill_information_block}</legend><br />
				<c:out value="${message_name}" />
				<input type="text" name="name" value="${requestScope.userOfBooking.name}" size="25" readonly="readonly" />
				
				<c:out value="${message_surname}" />
				<input type="text" name="surname" value="${requestScope.userOfBooking.surname}" size="25" readonly="readonly" /> <br /><br />
				
				<c:out value="${message_email}" />
				<input type="text" value="${requestScope.userOfBooking.email}" size="25" readonly="readonly"/> <br /><br />
				
				<c:out value="${message_mobile}" />
				<input type="text" value="${requestScope.userOfBooking.mobile}" size="15" readonly="readonly"/> <br /><br />
			
				<c:out value="${message_apartment_type}" />
				<c:if test="${requestScope.bookingFormInBill.apartmentType.name == 'standard_single'}">
					<input type="text" name="apartment-description" value="${apartment_type_standard_single}" size="60" readonly="readonly" />
				</c:if>
				<c:if test="${requestScope.bookingFormInBill.apartmentType.name == 'standard_double'}">
					<input type="text" name="apartment-description" value="${ap_type_standard_double_12_bed}" size="60" readonly="readonly" />
				</c:if>
				<c:if test="${requestScope.bookingFormInBill.apartmentType.name == 'executive_single'}">
					<input type="text" name="apartment-description" value="${apartment_type_represent_single}" size="60" readonly="readonly" />
				</c:if>
				<c:if test="${requestScope.bookingFormInBill.apartmentType.name == 'executive_double'}">
					<input type="text" name="apartment-description" value="${ap_type_represent_double_12_bed}" size="60" readonly="readonly" />
				</c:if>
				<c:if test="${requestScope.bookingFormInBill.apartmentType.name == 'family_1adult2kids'}">
					<input type="text" name="apartment-description" value="${apartment_type_family_1ad_2ch}" size="60" readonly="readonly" />
				</c:if>
				<c:if test="${requestScope.bookingFormInBill.apartmentType.name == 'family_2adults2kids'}">
					<input type="text" name="apartment-description" value="${apartment_type_family_2ad_2ch}" size="60" readonly="readonly" />
				</c:if>
				
				<br /><br />
				<c:out value="${message_amount_of_chosen_apartments}" />
				<input type="text" name="amount-of-apartments" value="${requestScope.bookingFormInBill.amountOfApartments}" size="3" readonly="readonly" /> <br /><br />
				
				<c:out value="${message_price_for} 1 ${word_night_singular_nom_case}" />
				<input type="text" name="apartment-one-night-price-as-string" value="${sessionScope.currencyFormat.format((requestScope.bookingFormInBill.totalPrice*sessionScope.dollarCoefficient)/(requestScope.bookingFormInBill.amountOfApartments*requestScope.amountOfNights))}" size="15" readonly="readonly" /> <br /><br />
				
				<c:out value="${message_arrival_date}: " />
				<input type="text" name="full-entered-arrival-date" value="${requestScope.fullArrivalDate}" size="14" readonly="readonly" style="border: 0px;"/> <br />		
				
				<c:out value="${message_leaving_date}: " />
				<input type="text" name="full-entered-leaving-date" value="${requestScope.fullLeavingDate}" size="14" readonly="readonly" style="border: 0px;"/> <br /><br />
				
				<c:out value="${message_nights_amount}" />
				<input type="text" name="amount-of-nights" value="${requestScope.amountOfNights} ${nightMessage}" size="10" readonly="readonly" /> <br /><br />
				
				<c:out value="${message_total_price}" />
				<input type="text" name="apartment-total-price-as-string" value="${sessionScope.currencyFormat.format(requestScope.bookingFormInBill.totalPrice*sessionScope.dollarCoefficient)}" size="15" readonly="readonly" /> <br /><br />
				
				<c:out value="${message_card_type}" />
				<input type="text" name="card-type" value="${requestScope.bookingFormInBill.cardType.name}" size="10" readonly="readonly" />
				
				<c:out value="${message_card_number}" />
				<input type="text" name="card-number" value="${requestScope.bookingFormInBill.cardNumber}" size="25" readonly="readonly" /> <br />
				
				<c:out value="${message_card_expiration_date}" />
				<input type="text" name="card-expire-date" value="${cardExpireDate}" size="25" readonly="readonly" /> <br /><br />
				
				<input type="text" name="today-date" value="${requestScope.todayDate}" size="23" readonly="readonly" style="border: 0px;border-bottom: 1px solid black;" />
				<input type="text" name="admin-signature" size="20" readonly="readonly" style="border: 0px;border-bottom: 1px solid black;" /> <br />
				<input type="text" name="date-word" value="${message_signature_date}" size="35" readonly="readonly" style="border: 0px;font-size: x-small;" />
				<input type="text" name="admin-signature-word" value="${message_admin_signature}" size="25" readonly="readonly" style="border: 0px;font-size: x-small;" /> <br /><br />
				
				<input type="text" name="today-date" value="${requestScope.todayDate}" size="23" readonly="readonly" style="border: 0px;border-bottom: 1px solid black;"/>
				<input type="text" name="client-signature" size="20" readonly="readonly" style="border: 0px;border-bottom: 1px solid black;"/> <br />
				<input type="text" name="date-word" value="${message_signature_date}" size="37" readonly="readonly" style="border: 0px;font-size: x-small;" />
				<input type="text" name="client-signature-word" value="${message_client_signature}" size="20" readonly="readonly" style="border: 0px;font-size: x-small;" /> <br />
				
			</fieldset>
			
			<input type="hidden" name="command" value="save_bill" />
			<input type="hidden" name="booking-id" value="${bookingFormInBill.id}" />
			<input type="hidden" name="total-price" value="${bookingFormInBill.totalPrice}" />
			<c:if test="${requestScope.alreadySavedBillFlag == 'true'}">
				<div style="float: right;"><c:out value="${message_bill_already_saved}" /></div><br />
				<input type="submit" name="submit" value="${button_name_save_bill}" disabled="disabled" style="float: right;" /> <br />
			</c:if>
			<c:if test="${requestScope.alreadySavedBillFlag == null}">
				<input type="submit" name="submit" value="${button_name_save_bill}" style="float: right;" /> <br />
			</c:if>
			
		</form> <br />
		
		<form action="Controller" method="post">
			<input type="hidden" name="command" value="home" />
			<input type="hidden" name="resource-name" value="hotel_information" />
			<input type="submit" value="${message_goto_index_page}" />
		</form>
	</div>
	
	<div id="footer">
		<c:out value="${message_copyright}" />
	</div>
</body>
</html>