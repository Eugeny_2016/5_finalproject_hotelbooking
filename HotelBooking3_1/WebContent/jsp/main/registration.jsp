<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="customtags" prefix="ctg" %>

<!DOCTYPE html>
<html>
<head>
<%@ include file="../../WEB-INF/jspf/messages/localization.jspf" %>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>${title_registration_page}</title>
<link rel="stylesheet" href="<c:url value="/css/index.css"/>" />
</head>

<body>
	
	<div id="header">
		<ctg:time/>
	</div>
	
	<div id="section">
		<h3>${message_registration}</h3>		
		<form action="Controller" method="post">
			
			<fieldset style="width:1180px;">
				<legend>${message_technical_information_block}</legend><br />
				<c:out value="${message_registration_login}" />
				<input type="text" name="registration-login" value="${sessionScope.regLogin}" pattern="([a-zA-Z]{1})([a-zA-Z_0-9\-]{3,14})" title="${message_login_format}" autofocus="autofocus" size="20" maxlength="15" required="required" /> <br /><br />
				<c:if test="${requestScope.userAlreadyExistsFlag == 'true'}">
					<div id="error-message"><c:out value="${message_authorization_user_already_exists_info}" /></div> <br />
				</c:if>
				<c:out value="${message_registration_password}" />
				<input type="password" name="registration-password" value="${sessionScope.regPassword}" pattern="([a-zA-Z_0-9\-]{3,20})" title="${message_password_format}" size="20" maxlength="20" required="required" >
				<c:out value="${message_reg_password_again}" />
				<input type="password" name="registration-password-again" value="${sessionScope.regPasswordAgain}" pattern="([a-zA-Z_0-9\-]{3,20})" title="${message_password_format}" size="20" maxlength="20" required="required" /> <br /><br />
				<c:out value="${message_question}" />
				<input type="text" name="question" value="${sessionScope.keyQuestion}"  pattern="([a-zA-Z\u0400-\u052F\u0020.]{4,})(.*)" title="${message_text_format}" size="80" maxlength="80" required="required" /> <br /><br />
				<c:out value="${message_answer}" />
				<input type="text" name="answer" value="${sessionScope.keyAnswer}" size="71" maxlength="71" required="required" /> <br />
			</fieldset>	<br />
			
			<fieldset style="width:1180px;">
				<legend>${message_general_information_block}</legend><br />
				<c:out value="${message_name}" />
				<input type="text" name="name" value="${sessionScope.regName}" pattern="([a-zA-Z\u0400-\u052F\u0020.]{4,})(.*)" title="${message_text_format}" size="40" maxlength="40" required="required" />
				<c:out value="${message_surname}" />
				<input type="text" name="surname" value="${sessionScope.regSurname}" pattern="([a-zA-Z\u0400-\u052F\u0020.]{4,})(.*)" title="${message_text_format}" size="40" maxlength="40" required="required" />
				<c:out value="${message_patronymic}" />
				<input type="text" name="patronymic" value="${sessionScope.regPatronymic}" pattern="([a-zA-Z\u0400-\u052F\u0020.]{4,})(.*)" title="${message_text_format}" size="40" maxlength="40" /> <br /><br />
				<c:out value="${message_country}" />
				<input type="text" name="country" value="${sessionScope.regCountry}" pattern="([a-zA-Z\u0400-\u052F\u0020.]{4,})(.*)" title="${message_text_format}" size="40" maxlength="40" required="required" />
				<c:out value="${message_address}" />
				<input type="text" name="address"value="${sessionScope.regAddress}"  pattern="([a-zA-Z\u0400-\u052F\u0020.]{4,})(.*)" title="${message_text_format}" size="51" maxlength="51" required="required" /> <br /><br />
				<c:out value="${message_email}" />
				<input type="text" name="email" value="${sessionScope.regEmail}" pattern="([a-z]{1})([^@?!.,:;'=|\\%$&#\s\-(&#34)(&#42)(&#43)(&#47)(&#40)(&#41)]*)@([^@?!.,:;'=|\\%$&#\s\-(&#34)(&#42)(&#43)(&#47)(&#40)(&#41)]{2,20})(\.[a-z]{2,3}){1,}" title="${message_email_format}" size="42" maxlength="42" required="required" /> <br /><br />
				<c:out value="${message_phone}" />
				<input type="text" name="phone" value="${sessionScope.regPhone}" pattern="([(&#43)]{1})([0-9]{3})\-([0-9]{2})\-([0-9]{7})" title="${message_phone_format}" size="15" maxlength="15" required="required" />
				<c:out value="${message_mobile}" />
				<input type="text" name="mobile" value="${sessionScope.regMobile}" pattern="([(&#43)]{1})([0-9]{3})\-([0-9]{2})\-([0-9]{7})" title="${message_phone_format}" size="15" maxlength="15" required="required" /> <br />
			</fieldset>
			
			<c:if test="${requestScope.incorrectLoginFlag == 'true'}">
				<div id="error-message"><c:out value="${message_login_incorrect_value}" /></div>
			</c:if>
			<c:if test="${requestScope.incorrectPasswordFlag == 'true'}">
				<div id="error-message"><c:out value="${message_password_incorrect_value}" /></div>
			</c:if>
			<c:if test="${requestScope.passwordsNotSameFlag == 'true'}">
				<div id="error-message"><c:out value="${message_passwords_not_same}" /></div>
			</c:if>
			<c:if test="${requestScope.incorrectEmailFlag == 'true'}">
				<div id="error-message"><c:out value="${message_email_incorrect_value}" /></div>
			</c:if>
			<c:if test="${requestScope.incorrectTextFieldsFlag == 'true'}">
				<div id="error-message"><c:out value="${message_text_fields_incorrect_value}" /></div>
			</c:if>
			<c:if test="${requestScope.incorrectPhoneMobileFlag == 'true'}">
				<div id="error-message"><c:out value="${message_phone_mobile_incorrect_value}" /></div>
			</c:if>
			
			<input type="hidden" name="command" value="confirm_registration" />
			<input type="submit" value="${button_confirm_registration}" style="float: right;"/>
		</form>
		
		<br /><br />
		<form action="Controller" method="post">
			<input type="hidden" name="command" value="home" />
			<input type="hidden" name="resource-name" value="hotel_information" />
			<input type="submit" value="${message_goto_index_page}" />
		</form>
	</div>
				
	<div id="footer">
		<c:out value="${message_copyright}" />
	</div>
</body>
</html>
