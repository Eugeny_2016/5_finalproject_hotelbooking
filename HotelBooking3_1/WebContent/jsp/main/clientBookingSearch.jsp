<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="customtags" prefix="ctg" %>

<!DOCTYPE html>
<html>
<head>
<%@ include file="../../WEB-INF/jspf/messages/localization.jspf" %>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>${title_client_booking_search_page}</title>
<link rel="stylesheet" href="<c:url value="/css/index.css"/>" />
</head>

<body>
	
	<div id="header">
		<ctg:time/>
	</div>
	
	<div id="section">
		<form action="Controller" method="post">
			
			<fieldset style="width:1180px;">
				<legend>${message_search_fields_block}</legend><br />
				<c:out value="${message_name}" />
				<input type="text" name="name" value="${sessionScope.name}" pattern="([a-zA-Z\u0400-\u052F]{4,})(.*)" title="${message_text_format}" size="40" maxlength="40" />
				<c:out value="${message_surname}" />
				<input type="text" name="surname" value="${sessionScope.surname}" pattern="([a-zA-Z\u0400-\u052F]{4,})(.*)" title="${message_text_format}" size="40" maxlength="40" /> <br /><br />
				<c:out value="${message_email}" />
				<input type="text" name="email" value="${sessionScope.email}" pattern="([a-z]{1})([^@?!.,:;'=|\\%$&#\s\-(&#34)(&#42)(&#43)(&#47)(&#40)(&#41)]*)@([^@?!.,:;'=|\\%$&#\s\-(&#34)(&#42)(&#43)(&#47)(&#40)(&#41)]{2,20})(\.[a-z]{2,3}){1,}" title="${message_email_format}" size="40" maxlength="40" /> <br /><br />
				<c:out value="${message_mobile}" />
				<input type="text" name="mobile" value="${sessionScope.mobile}" pattern="([(&#43)]{1})([0-9]{3})\-([0-9]{2})\-([0-9]{7})" title="${message_phone_format}" size="15" maxlength="15" /> <br />
				<br />
				<c:if test="${sessionScope.futureCommandName == 'create_bill'}">
					<c:if test="${sessionScope.forciblyClosedFlag == null}">
						<input type="checkbox" name="forcibly-closed-flag" value="true" />${message_forcibly_closed_flag}
					</c:if>
					<c:if test="${sessionScope.forciblyClosedFlag == 'false'}">
						<input type="checkbox" name="forcibly-closed-flag" value="true" />${message_forcibly_closed_flag}
					</c:if>
					<c:if test="${sessionScope.forciblyClosedFlag == 'true'}">
						<input type="checkbox" name="forcibly-closed-flag" checked="checked" value="${sessionScope.forciblyClosedFlag}" />${message_forcibly_closed_flag}
					</c:if>
				</c:if>
			</fieldset>
			
			<input type="hidden" name="command" value="search_for_bookings" />
			<input type="submit" value="${button_name_search_for_bookings}" style="float: right;"/>
		</form>
		
		<%--вывод сообщений об неправильном вводе--%>
		<c:if test="${requestScope.searchFieldsNotFilledFlag == 'true'}">
			<div id="error-message"><c:out value="${message_empty_search_fields}" /></div>
			<form action="Controller" method="post">
				<input type="hidden" name="command" value="home" />
				<input type="hidden" name="resource-name" value="hotel_information" />
				<input type="submit" value="${message_goto_index_page}" />
			</form>
		</c:if>
		<c:if test="${requestScope.incorrectTextFieldsFlag == 'true'}">
			<div id="error-message"><c:out value="${message_text_fields_incorrect_value}" /></div>
			<form action="Controller" method="post">
				<input type="hidden" name="command" value="home" />
				<input type="hidden" name="resource-name" value="hotel_information" />
				<input type="submit" value="${message_goto_index_page}" />
			</form>
		</c:if>
		<c:if test="${requestScope.incorrectEmailFlag == 'true'}">
			<div id="error-message"><c:out value="${message_email_incorrect_value}" /></div>
			<form action="Controller" method="post">
				<input type="hidden" name="command" value="home" />
				<input type="hidden" name="resource-name" value="hotel_information" />
				<input type="submit" value="${message_goto_index_page}" />
			</form>
		</c:if>
		<c:if test="${requestScope.incorrectMobileFlag == 'true'}">
			<div id="error-message"><c:out value="${message_phone_mobile_incorrect_value}" /></div>
			<form action="Controller" method="post">
				<input type="hidden" name="command" value="home" />
				<input type="hidden" name="resource-name" value="hotel_information" />
				<input type="submit" value="${message_goto_index_page}" />
			</form>
		</c:if>
					
		<%--вывод сообщений об отсутствии заявок по бронированию--%>
		<c:if test="${requestScope.NoBookingsFoundFlag == 'true'}">
			<c:out value="${message_no_bookings_found_of_client}" /> <br /><br />
			<form action="Controller" method="post">
				<input type="hidden" name="command" value="home" />
				<input type="hidden" name="resource-name" value="hotel_information" />
				<input type="submit" value="${message_goto_index_page}" />
			</form>
		</c:if>
		<c:if test="${requestScope.NoTemporaryBookingsFoundFlag == 'true'}">
			<c:out value="${message_no_temp_bookings_found_of_client}" /> <br /><br />
			<c:set var="map" value="${sessionScope.bookingMapToConfirm = null}"></c:set>
			<form action="Controller" method="post">
				<input type="hidden" name="command" value="home" />
				<input type="hidden" name="resource-name" value="hotel_information" />
				<input type="submit" value="${message_goto_index_page}" />
			</form>
		</c:if>
		<c:if test="${requestScope.NoConfirmedBookingsFoundFlag == 'true'}">
			<c:out value="${message_no_conf_bookings_found_of_client}" /> <br /><br />
			<c:set var="map" value="${sessionScope.bookingMapToClose = null}"></c:set>
			<form action="Controller" method="post">
				<input type="hidden" name="command" value="home" />
				<input type="hidden" name="resource-name" value="hotel_information" />
				<input type="submit" value="${message_goto_index_page}" />
			</form>
		</c:if>
		<c:if test="${requestScope.NoClosedBookingsFoundFlag == 'true'}">
			<c:out value="${message_no_closed_bookings_found_of_client}" /> <br /><br />
			<c:set var="map" value="${sessionScope.bookingMapToCreateBill = null}"></c:set>
			<form action="Controller" method="post">
				<input type="hidden" name="command" value="home" />
				<input type="hidden" name="resource-name" value="hotel_information" />
				<input type="submit" value="${message_goto_index_page}" />
			</form>
		</c:if>
		<c:if test="${requestScope.NoForciblyClosedBookingsFoundFlag == 'true'}">
			<c:out value="${message_no_forcibly_closed_bookings_found}" /> <br /><br />
			<form action="Controller" method="post">
				<input type="hidden" name="command" value="home" />
				<input type="hidden" name="resource-name" value="hotel_information" />
				<input type="submit" value="${message_goto_index_page}" />
			</form>
		</c:if>
		
		<%--определение команды, с которой пришёл запрос--%>	
		<c:choose>
		<c:when test="${sessionScope.futureCommandName == 'view_all_bookings_of_client' && requestScope.bookingMapToView != null}">
			<div id="section">
				<h3>${message_all_bookings_of_client} ${requestScope.userId}</h3>
				<table>
					<tr>
						<th>№</th>
						<th>${message_arrival_date}</th>
						<th>${message_leaving_date}</th>
						<th>${message_amount_of_nights}</th>
						<th>${message_apartment_type}</th>
						<th>${message_amount_of_chosen_apartments}</th>
						<th>${message_amount_of_guests}</th>
						<th>${message_total_price}</th>
						<th>${message_booking_status}</th>
					</tr>
					<c:set var="i" scope="page" value="0"></c:set>
					<c:forEach var="entry" items="${requestScope.bookingMapToView}">
					<tr>
						<c:set var="i" value="${i+1}"></c:set>
						<td>${i}.</td>
						<td>${entry.value.poll()}</td>
						<td>${entry.value.poll()}</td>
						<c:set var="nightWord" scope="page" value="${entry.value.poll()}"></c:set>
						<c:set var="nightsAmount" scope="page" value="${entry.value.poll()}"></c:set>
						<td>
							<c:choose>
								<c:when test="${nightWord == 'singular_nom'}">
									<c:out value="${nightsAmount} ${word_night_singular_nom_case}" />
								</c:when>
								<c:when test="${nightWord == 'singular_par'}">
									<c:out value="${nightsAmount} ${word_night_singular_par_case}" />
								</c:when>
								<c:when test="${nightWord == 'plural_par'}">
									<c:out value="${nightsAmount} ${word_night_plural_par_case}" />
								</c:when>
							</c:choose>
						</td>
						<td>
							<c:choose>
								<c:when test="${entry.key.apartmentType.name == 'standard_single'}">
									<c:out value="${apartment_type_standard_single}" />
								</c:when>
								<c:when test="${entry.key.apartmentType.name == 'standard_double'}">
									<c:out value="${ap_type_standard_double_12_bed}" />
								</c:when>
								<c:when test="${entry.key.apartmentType.name == 'executive_single'}">
									<c:out value="${apartment_type_represent_single}" />
								</c:when>
								<c:when test="${entry.key.apartmentType.name == 'executive_double'}">
									<c:out value="${ap_type_represent_double_12_bed}" />
								</c:when>
								<c:when test="${entry.key.apartmentType.name == 'family_1adult2kids'}">
									<c:out value="${apartment_type_family_1ad_2ch}" />
								</c:when>
								<c:when test="${entry.key.apartmentType.name == 'family_2adults2kids'}">
									<c:out value="${apartment_type_family_2ad_2ch}" />
								</c:when>
							</c:choose>
						</td>
						<td>${entry.key.amountOfApartments}</td>
						<td>${entry.key.amountOfGuests}</td>
						<td>${currencyFormat.format(entry.key.totalPrice*sessionScope.dollarCoefficient)}</td>
						<td>
							<c:choose>
								<c:when test="${entry.key.status == 'temporary'}">
									<c:out value="${status_name_temporary}" />
								</c:when>
								<c:when test="${entry.key.status == 'confirmed'}">
									<c:out value="${status_name_confirmed}" />
								</c:when>
								<c:when test="${entry.key.status == 'canceled'}">
									<c:out value="${status_name_canceled}" />
								</c:when>
								<c:when test="${entry.key.status == 'closed'}">
									<c:out value="${status_name_closed}" />
								</c:when>
							</c:choose>
						</td>
					</tr>
					</c:forEach>
				</table> <br />
				<form action="Controller" method="post">
					<input type="hidden" name="command" value="home" />
					<input type="hidden" name="resource-name" value="hotel_information" />
					<input type="submit" value="${message_goto_index_page}" />
				</form>
			</div>
		</c:when>
		
		<c:when test="${sessionScope.futureCommandName == 'confirm_status_booking' && sessionScope.bookingMapToConfirm != null}">
			<div id="section">
				<h3>${message_all_bookings_of_client} ${requestScope.userId}</h3>
				<form action="Controller" method="post">
					<table>
						<tr>
							<th>№</th>
							<th>${message_arrival_date}</th>
							<th>${message_leaving_date}</th>
							<th>${message_amount_of_nights}</th>
							<th>${message_apartment_type}</th>
							<th>${message_amount_of_chosen_apartments}</th>
							<th>${message_amount_of_guests}</th>
							<th>${message_total_price}</th>
							<th>${message_booking_status}</th>
							<th> </th>
						</tr>
						<c:set var="i" scope="page" value="0"></c:set>
						<c:forEach var="entry" items="${sessionScope.bookingMapToConfirm}">
						<tr>
							<c:set var="i" value="${i+1}"></c:set>
							<td>${i}.</td>
							<%--извлекаем из очереди данные для вывода--%>
							<c:set var="dateFrom" scope="page" value="${entry.value.poll()}"></c:set>
							<c:set var="dateUntil" scope="page" value="${entry.value.poll()}"></c:set>
							<c:set var="nightWord" scope="page" value="${entry.value.poll()}"></c:set>
							<c:set var="nightsAmount" scope="page" value="${entry.value.poll()}"></c:set>
							<td>${dateFrom}</td>
							<td>${dateUntil}</td>
							<td>
								<c:choose>
									<c:when test="${nightWord == 'singular_nom'}">
										<c:out value="${nightsAmount} ${word_night_singular_nom_case}" />
									</c:when>
									<c:when test="${nightWord == 'singular_par'}">
										<c:out value="${nightsAmount} ${word_night_singular_par_case}" />
									</c:when>
									<c:when test="${nightWord == 'plural_par'}">
										<c:out value="${nightsAmount} ${word_night_plural_par_case}" />
									</c:when>
								</c:choose>
							</td>
							<td>
								<c:choose>
									<c:when test="${entry.key.apartmentType.name == 'standard_single'}">
										<c:out value="${apartment_type_standard_single}" />
									</c:when>
									<c:when test="${entry.key.apartmentType.name == 'standard_double'}">
										<c:out value="${ap_type_standard_double_12_bed}" />
									</c:when>
									<c:when test="${entry.key.apartmentType.name == 'executive_single'}">
										<c:out value="${apartment_type_represent_single}" />
									</c:when>
									<c:when test="${entry.key.apartmentType.name == 'executive_double'}">
										<c:out value="${ap_type_represent_double_12_bed}" />
									</c:when>
									<c:when test="${entry.key.apartmentType.name == 'family_1adult2kids'}">
										<c:out value="${apartment_type_family_1ad_2ch}" />
									</c:when>
									<c:when test="${entry.key.apartmentType.name == 'family_2adults2kids'}">
										<c:out value="${apartment_type_family_2ad_2ch}" />
									</c:when>
								</c:choose>
							</td>
							<td>${entry.key.amountOfApartments}</td>
							<td>${entry.key.amountOfGuests}</td>
							<td>${currencyFormat.format(entry.key.totalPrice*sessionScope.dollarCoefficient)}</td>
							<td>
								<c:choose>
									<c:when test="${entry.key.status == 'temporary'}">
										<c:out value="${status_name_temporary}" />
									</c:when>
									<c:when test="${entry.key.status == 'confirmed'}">
										<c:out value="${status_name_confirmed}" />
									</c:when>
									<c:when test="${entry.key.status == 'canceled'}">
										<c:out value="${status_name_canceled}" />
									</c:when>
									<c:when test="${entry.key.status == 'closed'}">
										<c:out value="${status_name_closed}" />
									</c:when>
								</c:choose>
							</td>
							<td>
								<input type="checkbox" name="chosen-bookigs" value="${entry.key.id}" />
							</td>
						</tr>
						<%--возвращаем обратно в очередь данные для вывода--%>
						<c:set var="dateFrom" scope="page" value="${entry.value.offer(dateFrom)}" ></c:set>
						<c:set var="dateUntil" scope="page" value="${entry.value.offer(dateUntil)}" ></c:set>
						<c:set var="nightWord" scope="page" value="${entry.value.offer(nightWord)}" ></c:set>
						<c:set var="nightsAmount" scope="page" value="${entry.value.offer(nightsAmount)}" ></c:set>
						</c:forEach>
					</table>
									
					<c:if test="${requestScope.noBookingsChosenFlag == 'true'}">
						<div id="error-message"><c:out value="${message_no_bookings_chosen}" /></div> <br />
					</c:if>
					
					<br />
					<input type="hidden" name="command" value="confirm_status_booking" />
					<input type="submit" name="submit" value="${button_name_confirm_status_booking}" style="float: right;"/>
				</form> <br />
							
				<form action="Controller" method="post">
					<input type="hidden" name="command" value="home" />
					<input type="hidden" name="resource-name" value="hotel_information" />
					<input type="submit" value="${message_goto_index_page}" />
				</form>
			</div>
		</c:when>
		
		<c:when test="${sessionScope.futureCommandName == 'close_status_booking' && sessionScope.bookingMapToClose != null}">
			<div id="section">
				<h3>${message_all_bookings_of_client} ${requestScope.userId}</h3>
				<form action="Controller" method="post">
					<table>
						<tr>
							<th>№</th>
							<th>${message_arrival_date}</th>
							<th>${message_leaving_date}</th>
							<th>${message_amount_of_nights}</th>
							<th>${message_apartment_type}</th>
							<th>${message_amount_of_chosen_apartments}</th>
							<th>${message_amount_of_guests}</th>
							<th>${message_total_price}</th>
							<th>${message_booking_status}</th>
							<th> </th>
						</tr>
						<c:set var="i" scope="page" value="0"></c:set>
						<c:forEach var="entry" items="${sessionScope.bookingMapToClose}">
						<tr>
							<c:set var="i" value="${i+1}"></c:set>
							<td>${i}.</td>
							<%--извлекаем из очереди данные для вывода--%>
							<c:set var="dateFrom" scope="page" value="${entry.value.poll()}"></c:set>
							<c:set var="dateUntil" scope="page" value="${entry.value.poll()}"></c:set>
							<c:set var="nightWord" scope="page" value="${entry.value.poll()}"></c:set>
							<c:set var="nightsAmount" scope="page" value="${entry.value.poll()}"></c:set>
							<td>${dateFrom}</td>
							<td>${dateUntil}</td>
							<td>
								<c:choose>
									<c:when test="${nightWord == 'singular_nom'}">
										<c:out value="${nightsAmount} ${word_night_singular_nom_case}" />
									</c:when>
									<c:when test="${nightWord == 'singular_par'}">
										<c:out value="${nightsAmount} ${word_night_singular_par_case}" />
									</c:when>
									<c:when test="${nightWord == 'plural_par'}">
										<c:out value="${nightsAmount} ${word_night_plural_par_case}" />
									</c:when>
								</c:choose>
							</td>
							<td>
								<c:choose>
									<c:when test="${entry.key.apartmentType.name == 'standard_single'}">
										<c:out value="${apartment_type_standard_single}" />
									</c:when>
									<c:when test="${entry.key.apartmentType.name == 'standard_double'}">
										<c:out value="${ap_type_standard_double_12_bed}" />
									</c:when>
									<c:when test="${entry.key.apartmentType.name == 'executive_single'}">
										<c:out value="${apartment_type_represent_single}" />
									</c:when>
									<c:when test="${entry.key.apartmentType.name == 'executive_double'}">
										<c:out value="${ap_type_represent_double_12_bed}" />
									</c:when>
									<c:when test="${entry.key.apartmentType.name == 'family_1adult2kids'}">
										<c:out value="${apartment_type_family_1ad_2ch}" />
									</c:when>
									<c:when test="${entry.key.apartmentType.name == 'family_2adults2kids'}">
										<c:out value="${apartment_type_family_2ad_2ch}" />
									</c:when>
								</c:choose>
							</td>
							<td>${entry.key.amountOfApartments}</td>
							<td>${entry.key.amountOfGuests}</td>
							<td>${currencyFormat.format(entry.key.totalPrice*sessionScope.dollarCoefficient)}</td>
							<td>
								<c:choose>
									<c:when test="${entry.key.status == 'temporary'}">
										<c:out value="${status_name_temporary}" />
									</c:when>
									<c:when test="${entry.key.status == 'confirmed'}">
										<c:out value="${status_name_confirmed}" />
									</c:when>
									<c:when test="${entry.key.status == 'canceled'}">
										<c:out value="${status_name_canceled}" />
									</c:when>
									<c:when test="${entry.key.status == 'closed'}">
										<c:out value="${status_name_closed}" />
									</c:when>
								</c:choose>
							</td>
							<td>
								<input type="checkbox" name="chosen-bookigs" value="${entry.key.id}" />
							</td>
						</tr>
						<%--возвращаем обратно в очередь данные для вывода--%>
						<c:set var="dateFrom" scope="page" value="${entry.value.offer(dateFrom)}" ></c:set>
						<c:set var="dateUntil" scope="page" value="${entry.value.offer(dateUntil)}" ></c:set>
						<c:set var="nightWord" scope="page" value="${entry.value.offer(nightWord)}" ></c:set>
						<c:set var="nightsAmount" scope="page" value="${entry.value.offer(nightsAmount)}" ></c:set>
						</c:forEach>
					</table>
									
					<c:if test="${requestScope.noBookingsChosenFlag == 'true'}">
						<div id="error-message"><c:out value="${message_no_bookings_chosen}" /></div> <br />
					</c:if>
					
					<br />
					<input type="hidden" name="command" value="close_status_booking" />
					<input type="submit" name="submit" value="${button_name_close_status_booking}" style="float: right;"/>
				</form> <br />
							
				<form action="Controller" method="post">
					<input type="hidden" name="command" value="home" />
					<input type="hidden" name="resource-name" value="hotel_information" />
					<input type="submit" value="${message_goto_index_page}" />
				</form>
			</div>
		</c:when>
		
		<c:when test="${sessionScope.futureCommandName == 'create_bill' && sessionScope.bookingMapToCreateBill != null}">
			<div id="section">
				<h3>${message_all_bookings_of_client} ${requestScope.userId}</h3>
				<form action="Controller" method="post">
					<table>
						<tr>
							<th>№</th>
							<th>${message_arrival_date}</th>
							<th>${message_leaving_date}</th>
							<th>${message_amount_of_nights}</th>
							<th>${message_apartment_type}</th>
							<th>${message_amount_of_chosen_apartments}</th>
							<th>${message_amount_of_guests}</th>
							<th>${message_total_price}</th>
							<th>${message_booking_status}</th>
							<c:if test="${sessionScope.forciblyClosedFlag == 'true'}">
								<th>${message_booking_forcibly_closed}</th>
							</c:if>
							<th> </th>
						</tr>
						<c:set var="i" scope="page" value="0"></c:set>
						<c:forEach var="entry" items="${sessionScope.bookingMapToCreateBill}">
						<tr>
							<c:set var="i" value="${i+1}"></c:set>
							<td>${i}.</td>
							<%--извлекаем из очереди данные для вывода--%>
							<c:set var="dateFrom" scope="page" value="${entry.value.poll()}"></c:set>
							<c:set var="dateUntil" scope="page" value="${entry.value.poll()}"></c:set>
							<c:set var="nightWord" scope="page" value="${entry.value.poll()}"></c:set>
							<c:set var="nightsAmount" scope="page" value="${entry.value.poll()}"></c:set>
							<td>${dateFrom}</td>
							<td>${dateUntil}</td>
							<td>
								<c:choose>
									<c:when test="${nightWord == 'singular_nom'}">
										<c:out value="${nightsAmount} ${word_night_singular_nom_case}" />
									</c:when>
									<c:when test="${nightWord == 'singular_par'}">
										<c:out value="${nightsAmount} ${word_night_singular_par_case}" />
									</c:when>
									<c:when test="${nightWord == 'plural_par'}">
										<c:out value="${nightsAmount} ${word_night_plural_par_case}" />
									</c:when>
								</c:choose>
							</td>
							<td>
								<c:choose>
									<c:when test="${entry.key.apartmentType.name == 'standard_single'}">
										<c:out value="${apartment_type_standard_single}" />
									</c:when>
									<c:when test="${entry.key.apartmentType.name == 'standard_double'}">
										<c:out value="${ap_type_standard_double_12_bed}" />
									</c:when>
									<c:when test="${entry.key.apartmentType.name == 'executive_single'}">
										<c:out value="${apartment_type_represent_single}" />
									</c:when>
									<c:when test="${entry.key.apartmentType.name == 'executive_double'}">
										<c:out value="${ap_type_represent_double_12_bed}" />
									</c:when>
									<c:when test="${entry.key.apartmentType.name == 'family_1adult2kids'}">
										<c:out value="${apartment_type_family_1ad_2ch}" />
									</c:when>
									<c:when test="${entry.key.apartmentType.name == 'family_2adults2kids'}">
										<c:out value="${apartment_type_family_2ad_2ch}" />
									</c:when>
								</c:choose>
							</td>
							<td>${entry.key.amountOfApartments}</td>
							<td>${entry.key.amountOfGuests}</td>
							<td>${currencyFormat.format(entry.key.totalPrice*sessionScope.dollarCoefficient)}</td>
							<td>
								<c:choose>
									<c:when test="${entry.key.status == 'temporary'}">
										<c:out value="${status_name_temporary}" />
									</c:when>
									<c:when test="${entry.key.status == 'confirmed'}">
										<c:out value="${status_name_confirmed}" />
									</c:when>
									<c:when test="${entry.key.status == 'canceled'}">
										<c:out value="${status_name_canceled}" />
									</c:when>
									<c:when test="${entry.key.status == 'closed'}">
										<c:out value="${status_name_closed}" />
									</c:when>
								</c:choose>
							</td>
							<c:if test="${sessionScope.forciblyClosedFlag == 'true'}">
								<td><input type="checkbox" checked="checked" disabled="disabled" /></td>
							</c:if>
							<td>
								<input type="radio" name="chosen-booking" value="${entry.key.id}" />
							</td>
						</tr>
						<%--возвращаем обратно в очередь данные для вывода--%>
						<c:set var="dateFrom" scope="page" value="${entry.value.offer(dateFrom)}" ></c:set>
						<c:set var="dateUntil" scope="page" value="${entry.value.offer(dateUntil)}" ></c:set>
						<c:set var="nightWord" scope="page" value="${entry.value.offer(nightWord)}" ></c:set>
						<c:set var="nightsAmount" scope="page" value="${entry.value.offer(nightsAmount)}" ></c:set>
						</c:forEach>
					</table>
									
					<c:if test="${requestScope.noBookingsChosenFlag == 'true'}">
						<div id="error-message"><c:out value="${message_no_bookings_chosen}" /></div> <br />
					</c:if>
					
					<br />
					<input type="hidden" name="command" value="create_bill" />
					<input type="submit" name="submit" value="${button_name_create_bill}" style="float: right;"/>
				</form> <br />
							
				<form action="Controller" method="post">
					<input type="hidden" name="command" value="home" />
					<input type="hidden" name="resource-name" value="hotel_information" />
					<input type="submit" value="${message_goto_index_page}" />
				</form>
			</div>
		</c:when>
		</c:choose>
	</div>
	
	<br />			
	<div id="footer">
		<c:out value="${message_copyright}" />
	</div>
</body>
</html>
