<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="customtags" prefix="ctg" %>

<!DOCTYPE html>
<html>
<head>
<%@ include file="../../WEB-INF/jspf/messages/localization.jspf" %>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>${title_unconfirmed_bookings_page}</title>
<link rel="stylesheet" href="<c:url value="/css/index.css"/>" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
</head>

<body>
		
	<div id="header">
		<ctg:time/>
	</div>
		
	<%--вывод сообщений об отсутствии заявок по бронированию--%>
	<c:if test="${requestScope.NoUnconfirmedBookingsFoundFlag == 'true'}">
		<c:out value="${message_no_unconfirmed_bookings_found}" /> <br /><br />
		<form action="Controller" method="post">
			<input type="hidden" name="command" value="home" />
			<input type="hidden" name="resource-name" value="hotel_information" />
			<input type="submit" value="${message_goto_index_page}" />
		</form>
	</c:if>
			
	<c:if test="${requestScope.NoUnconfirmedBookingsFoundFlag != 'true'}">
	<%--определение возможности принудительного закрытия неподтверждённых бронирований--%>	
	<c:choose>
		<c:when test="${requestScope.OnlyViewFlag == 'true'}">
			<div id="section">
				<h3>${message_unconfirmed_bookings}</h3>			
				<table>
					<tr>
						<th>№</th>
						<th>${message_booking_creation_date}</th>
						<th>${message_arrival_date}</th>
						<th>${message_leaving_date}</th>
						<th>${message_amount_of_nights}</th>
						<th>${message_apartment_type}</th>
						<th>${message_amount_of_chosen_apartments}</th>
						<th>${message_amount_of_guests}</th>
						<th>${message_total_price}</th>
						<th>${message_booking_status}</th>
					</tr>
					<c:set var="i" scope="page" value="0"></c:set>
					<c:forEach var="entry" items="${sessionScope.unconfirmedBookingMap}">
					<tr>
						<c:set var="i" value="${i+1}"></c:set>
						<td>${i}.</td>
						<%--извлекаем из очереди данные для вывода--%>
						<c:set var="creationDate" scope="page" value="${entry.value.poll()}"></c:set>
						<c:set var="dateFrom" scope="page" value="${entry.value.poll()}"></c:set>
						<c:set var="dateUntil" scope="page" value="${entry.value.poll()}"></c:set>
						<c:set var="nightWord" scope="page" value="${entry.value.poll()}"></c:set>
						<c:set var="nightsAmount" scope="page" value="${entry.value.poll()}"></c:set>
						<td>
							<c:choose>
								<c:when test="${creationDate == 'today'}">
									<c:out value="${word_today}" />
								</c:when>
								<c:otherwise>
									<c:out value="${creationDate}" />
								</c:otherwise>
							</c:choose>
						</td>
						<td>${dateFrom}</td>
						<td>${dateUntil}</td>
						<td>
							<c:choose>
								<c:when test="${nightWord == 'singular_nom'}">
									<c:out value="${nightsAmount} ${word_night_singular_nom_case}" />
								</c:when>
								<c:when test="${nightWord == 'singular_par'}">
									<c:out value="${nightsAmount} ${word_night_singular_par_case}" />
								</c:when>
								<c:when test="${nightWord == 'plural_par'}">
									<c:out value="${nightsAmount} ${word_night_plural_par_case}" />
								</c:when>
							</c:choose>
						</td>
						<td>
							<c:choose>
								<c:when test="${entry.key.apartmentType.name == 'standard_single'}">
									<c:out value="${apartment_type_standard_single}" />
								</c:when>
								<c:when test="${entry.key.apartmentType.name == 'standard_double'}">
									<c:out value="${ap_type_standard_double_12_bed}" />
								</c:when>
								<c:when test="${entry.key.apartmentType.name == 'executive_single'}">
									<c:out value="${apartment_type_represent_single}" />
								</c:when>
								<c:when test="${entry.key.apartmentType.name == 'executive_double'}">
									<c:out value="${ap_type_represent_double_12_bed}" />
								</c:when>
								<c:when test="${entry.key.apartmentType.name == 'family_1adult2kids'}">
									<c:out value="${apartment_type_family_1ad_2ch}" />
								</c:when>
								<c:when test="${entry.key.apartmentType.name == 'family_2adults2kids'}">
									<c:out value="${apartment_type_family_2ad_2ch}" />
								</c:when>
							</c:choose>
						</td>
						<td>${entry.key.amountOfApartments}</td>
						<td>${entry.key.amountOfGuests}</td>
						<td>${currencyFormat.format(entry.key.totalPrice*sessionScope.dollarCoefficient)}</td>
						<td>
							<c:choose>
								<c:when test="${entry.key.status == 'temporary'}">
									<c:out value="${status_name_temporary}" />
								</c:when>
								<c:when test="${entry.key.status == 'confirmed'}">
									<c:out value="${status_name_confirmed}" />
								</c:when>
								<c:when test="${entry.key.status == 'canceled'}">
									<c:out value="${status_name_canceled}" />
								</c:when>
								<c:when test="${entry.key.status == 'closed'}">
									<c:out value="${status_name_closed}" />
								</c:when>
							</c:choose>
						</td>
						<%--возвращаем обратно в очередь данные для вывода--%>
						<c:set var="creationDate" scope="page" value="${entry.value.offer(creationDate)}" ></c:set>
						<c:set var="dateFrom" scope="page" value="${entry.value.offer(dateFrom)}" ></c:set>
						<c:set var="dateUntil" scope="page" value="${entry.value.offer(dateUntil)}" ></c:set>
						<c:set var="nightWord" scope="page" value="${entry.value.offer(nightWord)}" ></c:set>
						<c:set var="nightsAmount" scope="page" value="${entry.value.offer(nightsAmount)}" ></c:set>
					</tr>
					</c:forEach>
				</table> <br />
				<form action="Controller" method="post">
					<input type="hidden" name="command" value="home" />
					<input type="hidden" name="resource-name" value="hotel_information" />
					<input type="submit" value="${message_goto_index_page}" />
				</form>
			</div>			
		</c:when>
		
		<c:when test="${requestScope.CanCloseFlag == 'true'}">
			<div id="section">
				<h3>${message_unconfirmed_bookings}</h3>
				<form action="Controller" method="post">
					<table>
						<tr>
							<th>№</th>
							<th>${message_booking_creation_date}</th>
							<th>${message_arrival_date}</th>
							<th>${message_leaving_date}</th>
							<th>${message_amount_of_nights}</th>
							<th>${message_apartment_type}</th>
							<th>${message_amount_of_chosen_apartments}</th>
							<th>${message_amount_of_guests}</th>
							<th>${message_total_price}</th>
							<th>${message_booking_status}</th>
							<th> </th>
						</tr>
						<c:set var="i" scope="page" value="0"></c:set>
						<c:forEach var="entry" items="${sessionScope.unconfirmedBookingMap}">
						<tr>
							<c:set var="i" value="${i+1}"></c:set>
							<td>${i}.</td>
							<%--извлекаем из очереди данные для вывода--%>
							<c:set var="creationDate" scope="page" value="${entry.value.poll()}"></c:set>
							<c:set var="dateFrom" scope="page" value="${entry.value.poll()}"></c:set>
							<c:set var="dateUntil" scope="page" value="${entry.value.poll()}"></c:set>
							<c:set var="nightWord" scope="page" value="${entry.value.poll()}"></c:set>
							<c:set var="nightsAmount" scope="page" value="${entry.value.poll()}"></c:set>
							<td>
								<c:choose>
									<c:when test="${creationDate == 'today'}">
										<c:out value="${word_today}" />
									</c:when>
									<c:otherwise>
										<c:out value="${creationDate}" />
									</c:otherwise>
								</c:choose>
							</td>
							<td>${dateFrom}</td>
							<td>${dateUntil}</td>
							<td>
								<c:choose>
									<c:when test="${nightWord == 'singular_nom'}">
										<c:out value="${nightsAmount} ${word_night_singular_nom_case}" />
									</c:when>
									<c:when test="${nightWord == 'singular_par'}">
										<c:out value="${nightsAmount} ${word_night_singular_par_case}" />
									</c:when>
									<c:when test="${nightWord == 'plural_par'}">
										<c:out value="${nightsAmount} ${word_night_plural_par_case}" />
									</c:when>
								</c:choose>
							</td>
							<td>
								<c:choose>
									<c:when test="${entry.key.apartmentType.name == 'standard_single'}">
										<c:out value="${apartment_type_standard_single}" />
									</c:when>
									<c:when test="${entry.key.apartmentType.name == 'standard_double'}">
										<c:out value="${ap_type_standard_double_12_bed}" />
									</c:when>
									<c:when test="${entry.key.apartmentType.name == 'executive_single'}">
										<c:out value="${apartment_type_represent_single}" />
									</c:when>
									<c:when test="${entry.key.apartmentType.name == 'executive_double'}">
										<c:out value="${ap_type_represent_double_12_bed}" />
									</c:when>
									<c:when test="${entry.key.apartmentType.name == 'family_1adult2kids'}">
										<c:out value="${apartment_type_family_1ad_2ch}" />
									</c:when>
									<c:when test="${entry.key.apartmentType.name == 'family_2adults2kids'}">
										<c:out value="${apartment_type_family_2ad_2ch}" />
									</c:when>
								</c:choose>
							</td>
							<td>${entry.key.amountOfApartments}</td>
							<td>${entry.key.amountOfGuests}</td>
							<td>${currencyFormat.format(entry.key.totalPrice*sessionScope.dollarCoefficient)}</td>
							<td>
								<c:choose>
									<c:when test="${entry.key.status == 'temporary'}">
										<c:out value="${status_name_temporary}" />
									</c:when>
									<c:when test="${entry.key.status == 'confirmed'}">
										<c:out value="${status_name_confirmed}" />
									</c:when>
									<c:when test="${entry.key.status == 'canceled'}">
										<c:out value="${status_name_canceled}" />
									</c:when>
									<c:when test="${entry.key.status == 'closed'}">
										<c:out value="${status_name_closed}" />
									</c:when>
								</c:choose>
							</td>
							<td>
								<input type="checkbox" name="chosen-bookings" value="${entry.key.id}" />
							</td>
						</tr>
						<%--возвращаем обратно в очередь данные для вывода--%>
						<c:set var="creationDate" scope="page" value="${entry.value.offer(creationDate)}" ></c:set>
						<c:set var="dateFrom" scope="page" value="${entry.value.offer(dateFrom)}" ></c:set>
						<c:set var="dateUntil" scope="page" value="${entry.value.offer(dateUntil)}" ></c:set>
						<c:set var="nightWord" scope="page" value="${entry.value.offer(nightWord)}" ></c:set>
						<c:set var="nightsAmount" scope="page" value="${entry.value.offer(nightsAmount)}" ></c:set>
						</c:forEach>
					</table>
									
					<c:if test="${requestScope.noBookingsChosenFlag == 'true'}">
						<div id="error-message"><c:out value="${message_no_bookings_chosen}" /></div> <br />
					</c:if>
					
					<input type="hidden" name="command" value="forcibly_close_bookings" />
					<input type="submit" name="submit" value="${button_name_forcibly_close_booking}" style="float: right;"/>
				</form> <br />
							
				<form action="Controller" method="post">
					<input type="hidden" name="command" value="home" />
					<input type="hidden" name="resource-name" value="hotel_information" />
					<input type="submit" value="${message_goto_index_page}" />
				</form>
			</div>
		</c:when>		
	</c:choose>
	</c:if>
		
	<div id="footer">
		<c:out value="${message_copyright}" />
	</div>
</body>
</html>