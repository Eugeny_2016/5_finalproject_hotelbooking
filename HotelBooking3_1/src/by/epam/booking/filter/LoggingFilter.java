package by.epam.booking.filter;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import by.epam.booking.command.CommandName;
import by.epam.booking.command.JspPageName;

// TODO: Auto-generated Javadoc
/**
 * The Class LoggingFilter.
 */
public class LoggingFilter implements Filter {
	
	/** The Constant LOG. */
	private final static Logger LOG = Logger.getLogger(LoggingFilter.class);
	
	/** The Constant COMMAND_REQUEST_PARAMETER. */
	private static final String COMMAND_REQUEST_PARAMETER = "command";
	
	/** The Constant LOGIN_REQUEST_PARAMETER. */
	private static final String LOGIN_REQUEST_PARAMETER = "login";
	
	/** The Constant PASSWORD_REQUEST_PARAMETER. */
	private static final String PASSWORD_REQUEST_PARAMETER = "password";
	
	/* (non-Javadoc)
	 * @see javax.servlet.Filter#init(javax.servlet.FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		LOG.info("LoggingFilter has been initialized");
    }
 
    /* (non-Javadoc)
     * @see javax.servlet.Filter#doFilter(javax.servlet.ServletRequest, javax.servlet.ServletResponse, javax.servlet.FilterChain)
     */
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
    	HttpServletRequest req = (HttpServletRequest) request;
        String command = req.getParameter(COMMAND_REQUEST_PARAMETER);
        
        if (command != null) {
		    if (CommandName.AUTHORIZATION.toString().toLowerCase().equals(command)) {
		   		String login = req.getParameter(LOGIN_REQUEST_PARAMETER);
		        String password = req.getParameter(PASSWORD_REQUEST_PARAMETER); 
		   		
		        Pattern pattern = Pattern.compile("([a-zA-Z_0-9\\.\\-]){3,}");
		        Matcher matcher1 = pattern.matcher(login);
		        Matcher matcher2 = pattern.matcher(password);
					        
		        if (!matcher1.find() || !matcher2.find()) {
					LOG.info("LoggingFilter found some problems in input");
		        	request.setAttribute("pageFromLoggingFilter", JspPageName.INDEX_PAGE); // чтобы команда по авторизации проверила этот атрибут запроса, и т.к. пользователь ввёл данные на русском, перенаправила его на страницу авторизации
				} else {
					LOG.info("Everything is good in LoggingFilter!");
				}
				LOG.info("LoggingFilter has done it's work!");
			}
        }
		chain.doFilter(request, response);  // эта строка должна всегда срабатывать, если есть несколько фильтров (нельзя в if убирать)
    }
 
    /* (non-Javadoc)
     * @see javax.servlet.Filter#destroy()
     */
    public void destroy() {
    	
    }
}