package by.epam.booking.filter;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import by.epam.booking.command.CommandName;
import by.epam.booking.command.JspPageName;

/**
 * The Class ConfirmBookingFilter.
 */
public class ConfirmBookingFilter implements Filter {
	
	/** The Constant LOG. */
	private final static Logger LOG = Logger.getLogger(ConfirmBookingFilter.class);
	
	/** The Constant COMMAND_REQUEST_PARAMETER. */
	private static final String COMMAND_REQUEST_PARAMETER = "command";
	
	/** The Constant CARD_NUMBER_PARAMETER. */
	private static final String CARD_NUMBER_PARAMETER = "card-number";
		
	/** The Constant CARD_NUMBER_PATTERN. */
	private static final String CARD_NUMBER_PATTERN = "[0-9]{4}-[0-9]{4}-[0-9]{4}-[0-9]{4}";
	
    
    /* (non-Javadoc)
     * @see javax.servlet.Filter#init(javax.servlet.FilterConfig)
     */
    public void init(FilterConfig fConfig) throws ServletException {
		LOG.info("ConfirmBookingFilter has been initialized");
    }
 
	/* (non-Javadoc)
	 * @see javax.servlet.Filter#doFilter(javax.servlet.ServletRequest, javax.servlet.ServletResponse, javax.servlet.FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
    	HttpServletRequest req = (HttpServletRequest) request;
        String command = req.getParameter(COMMAND_REQUEST_PARAMETER);
        if (command != null) {
	        if (CommandName.CONFIRM_BOOKING.toString().toLowerCase().equals(command)) {
	       		String cardNumber = req.getParameter(CARD_NUMBER_PARAMETER);
	       		LOG.debug(cardNumber);
	       		Pattern patternCardNumber = Pattern.compile(CARD_NUMBER_PATTERN);
	       		Matcher matcherCardNumber = patternCardNumber.matcher(cardNumber);
		        
		        if (!matcherCardNumber.find()) {
	    			LOG.info("Incorrect value for card number field");
		        	request.setAttribute("pageFromConfirmBookingFilter", JspPageName.BOOKING_PAGE);
		        	request.setAttribute("incorrectCardNumberFlag", "true");
	    		}
				LOG.info("ConfirmBookingFilter has done it's work!");
	    	}
        }
        chain.doFilter(request, response);
    }
 
    /* (non-Javadoc)
     * @see javax.servlet.Filter#destroy()
     */
    public void destroy() {
    	
    }
}
