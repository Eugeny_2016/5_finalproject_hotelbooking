package by.epam.booking.filter;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import by.epam.booking.command.CommandName;
import by.epam.booking.command.JspPageName;

/**
 * The Class ConfirmRegistrationFilter.
 */
public class ConfirmRegistrationFilter implements Filter {
	
	/** The Constant LOG. */
	private final static Logger LOG = Logger.getLogger(ConfirmRegistrationFilter.class);
	
	/** The Constant COMMAND_REQUEST_PARAMETER. */
	private static final String COMMAND_REQUEST_PARAMETER = "command";
	
	/** The Constant LOGIN_REQUEST_PARAMETER. */
	private static final String LOGIN_REQUEST_PARAMETER = "registration-login";
	
	/** The Constant PASSWORD_REQUEST_PARAMETER. */
	private static final String PASSWORD_REQUEST_PARAMETER = "registration-password";
	
	/** The Constant PASSWORD_AGAIN_REQUEST_PARAMETER. */
	private static final String PASSWORD_AGAIN_REQUEST_PARAMETER = "registration-password-again";
	
	/** The Constant QUESTION_REQUEST_PARAMETER. */
	private static final String QUESTION_REQUEST_PARAMETER = "question";
	
	/** The Constant ANSWER_REQUEST_PARAMETER. */
	private static final String ANSWER_REQUEST_PARAMETER = "answer";
	
	/** The Constant EMAIL_REQUEST_PARAMETER. */
	private static final String EMAIL_REQUEST_PARAMETER = "email";
	
	/** The Constant NAME_REQUEST_PARAMETER. */
	private static final String NAME_REQUEST_PARAMETER = "name";
	
	/** The Constant SURNAME_REQUEST_PARAMETER. */
	private static final String SURNAME_REQUEST_PARAMETER = "surname";
	
	/** The Constant PATRONYMIC_REQUEST_PARAMETER. */
	private static final String PATRONYMIC_REQUEST_PARAMETER = "patronymic";
	
	/** The Constant COUNTRY_REQUEST_PARAMETER. */
	private static final String COUNTRY_REQUEST_PARAMETER = "country";
	
	/** The Constant ADDRESS_REQUEST_PARAMETER. */
	private static final String ADDRESS_REQUEST_PARAMETER = "address";
	
	/** The Constant PHONE_REQUEST_PARAMETER. */
	private static final String PHONE_REQUEST_PARAMETER = "phone";
	
	/** The Constant MOBILE_REQUEST_PARAMETER. */
	private static final String MOBILE_REQUEST_PARAMETER = "mobile";
		
	/** The Constant LOGIN_PATTERN. */
	private static final String LOGIN_PATTERN = "([a-zA-Z]{1})([a-zA-Z_0-9-]{3,14})";	
	
	/** The Constant PASSWORD_PATTERN. */
	private static final String PASSWORD_PATTERN = "([a-zA-Z_0-9-]{3,20})";	
	
	/** The Constant EMAIL_PATTERN. */
	private static final String EMAIL_PATTERN = "([a-z]{1})([^@?!.,:;\'=|\\%$&#\\s-\"(&#42)(&#43)(&#47)(&#40)(&#41)]*)@([^@?!.,:;\'=|\\%$&#\\s-\"(&#42)(&#43)(&#47)(&#40)(&#41)]{2,20})(\\.[a-z]{2,3}){1,}";
	
	/** The Constant TEXT_PATTERN. */
	private static final String TEXT_PATTERN = "([a-zA-Z\u0400-\u052F\u0020.]{4,})(.*)";
	
	/** The Constant PHONE_PATTERN. */
	private static final String PHONE_PATTERN = "([\u002B]{1})([0-9]{3})-([0-9]{2})-([0-9]{7})";
	
    
    /* (non-Javadoc)
     * @see javax.servlet.Filter#init(javax.servlet.FilterConfig)
     */
    public void init(FilterConfig fConfig) throws ServletException {
		LOG.info("ConfirmRegistrationFilter has been initialized");
    }
 
	/* (non-Javadoc)
	 * @see javax.servlet.Filter#doFilter(javax.servlet.ServletRequest, javax.servlet.ServletResponse, javax.servlet.FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
    	HttpServletRequest req = (HttpServletRequest) request;
        String command = req.getParameter(COMMAND_REQUEST_PARAMETER);
        if (command != null) {
	        if (CommandName.CONFIRM_REGISTRATION.toString().toLowerCase().equals(command)) {
	       		String regLogin = req.getParameter(LOGIN_REQUEST_PARAMETER);
	       		String regPassword = req.getParameter(PASSWORD_REQUEST_PARAMETER); 
	       		String regPasswordAgain = req.getParameter(PASSWORD_AGAIN_REQUEST_PARAMETER);
	       		String keyQuestion = req.getParameter(QUESTION_REQUEST_PARAMETER);
	       		String keyAnswer = req.getParameter(ANSWER_REQUEST_PARAMETER);
	       		String regEmail = req.getParameter(EMAIL_REQUEST_PARAMETER); 
	       		String regName = req.getParameter(NAME_REQUEST_PARAMETER); 
	       		String regSurname = req.getParameter(SURNAME_REQUEST_PARAMETER); 
	       		String regPatronymic = req.getParameter(PATRONYMIC_REQUEST_PARAMETER); 
	       		String regCountry = req.getParameter(COUNTRY_REQUEST_PARAMETER); 
	       		String regAddress = req.getParameter(ADDRESS_REQUEST_PARAMETER); 
	       		String regPhone = req.getParameter(PHONE_REQUEST_PARAMETER); 
	       		String regMobile = req.getParameter(MOBILE_REQUEST_PARAMETER); 
	       		
	       		Pattern patternLoginPassword = Pattern.compile(LOGIN_PATTERN);
	       		Pattern patternPassword = Pattern.compile(PASSWORD_PATTERN);
	       		Pattern patternEmail = Pattern.compile(EMAIL_PATTERN);
	       		Pattern patternText = Pattern.compile(TEXT_PATTERN);
	       		Pattern patternPhone = Pattern.compile(PHONE_PATTERN);
	            
	       		Matcher matcherLogin = patternLoginPassword.matcher(regLogin);
		        Matcher matcherPassword = patternPassword.matcher(regPassword);
		        Matcher matcherQuestion = patternText.matcher(keyQuestion);
		        Matcher matcherEmail = patternEmail.matcher(regEmail);
		        Matcher matcherName = patternText.matcher(regName);
		        Matcher matcherSurname = patternText.matcher(regSurname);
		        Matcher matcherCountry = patternText.matcher(regCountry);
		        Matcher matcherAddress = patternText.matcher(regAddress);
		        Matcher matcherPhone = patternPhone.matcher(regPhone);
		        Matcher matcherMobile = patternPhone.matcher(regMobile);
		        
		        if (!matcherLogin.find()) {
		        	LOG.info("Incorrect value for login");
		        	request.setAttribute("pageFromConfirmRegFilter", JspPageName.REGISTRATION_PAGE);
		        	request.setAttribute("incorrectLoginFlag", "true");
		        } else if (!matcherPassword.find()) {
		        	LOG.info("Incorrect value for password");
		        	request.setAttribute("pageFromConfirmRegFilter", JspPageName.REGISTRATION_PAGE);
		        	request.setAttribute("incorrectPasswordFlag", "true");
		        } else if (!regPassword.equals(regPasswordAgain)) {
		        	LOG.info("Password and repeated password are not the same");
		        	request.setAttribute("pageFromConfirmRegFilter", JspPageName.REGISTRATION_PAGE);
		        	request.setAttribute("passwordsNotSameFlag", "true");
		        } else if(!matcherQuestion.find() || !matcherName.find() || !matcherSurname.find() || 
		        		!matcherCountry.find() || !matcherAddress.find()) {
		        	LOG.info("Values for some of the text fields are incorrect");
		        	request.setAttribute("pageFromConfirmRegFilter", JspPageName.REGISTRATION_PAGE);
		        	request.setAttribute("incorrectTextFieldsFlag", "true");
		        } else if (!matcherEmail.find()) {
		        	LOG.info("Incorrect value for email field");
		        	request.setAttribute("pageFromConfirmRegFilter", JspPageName.REGISTRATION_PAGE);
		        	request.setAttribute("incorrectEmailFlag", "true");
		        } else if (!matcherPhone.find() || !matcherMobile.find()) {
		        	LOG.info("Values for phone/mobile fields are incorrect");
		        	request.setAttribute("pageFromConfirmRegFilter", JspPageName.REGISTRATION_PAGE);
		        	request.setAttribute("incorrectPhoneMobileFlag", "true");
		        }
		        
		        req.getSession().setAttribute("regLogin", regLogin);
		        req.getSession().setAttribute("regPassword", regPassword);
		        req.getSession().setAttribute("regPasswordAgain", regPasswordAgain);
		        req.getSession().setAttribute("keyQuestion", keyQuestion);
		        req.getSession().setAttribute("keyAnswer", keyAnswer);
		        req.getSession().setAttribute("regEmail", regEmail);
		        req.getSession().setAttribute("regName", regName);
		        req.getSession().setAttribute("regSurname", regSurname);
		        req.getSession().setAttribute("regPatronymic", regPatronymic);
		        req.getSession().setAttribute("regCountry", regCountry);
		        req.getSession().setAttribute("regAddress", regAddress);
		        req.getSession().setAttribute("regPhone", regPhone);
		        req.getSession().setAttribute("regMobile", regMobile);		        
		        
				LOG.info("ConfirmRegistrationFilter has done it's work!");
	    	}
        }
        chain.doFilter(request, response);
    }
 
    /* (non-Javadoc)
     * @see javax.servlet.Filter#destroy()
     */
    public void destroy() {}
}
