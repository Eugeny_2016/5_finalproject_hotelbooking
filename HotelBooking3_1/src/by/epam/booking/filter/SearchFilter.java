package by.epam.booking.filter;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import by.epam.booking.command.CommandName;
import by.epam.booking.command.JspPageName;

/**
 * The Class SearchFilter.
 */
public class SearchFilter implements Filter {
	
	/** The Constant LOG. */
	private final static Logger LOG = Logger.getLogger(SearchFilter.class);
	
	/** The Constant COMMAND_REQUEST_PARAMETER. */
	private static final String COMMAND_REQUEST_PARAMETER = "command";
	
	/** The Constant NAME_REQUEST_PARAMETER. */
	private static final String NAME_REQUEST_PARAMETER = "name";
	
	/** The Constant SURNAME_REQUEST_PARAMETER. */
	private static final String SURNAME_REQUEST_PARAMETER = "surname";
	
	/** The Constant EMAIL_REQUEST_PARAMETER. */
	private static final String EMAIL_REQUEST_PARAMETER = "email";
	
	/** The Constant MOBILE_REQUEST_PARAMETER. */
	private static final String MOBILE_REQUEST_PARAMETER = "mobile";
	
	/** The Constant FORCIBLE_CLOSE_FLAG_REQUEST_PARAMETER. */
	private static final String FORCIBLE_CLOSE_FLAG_REQUEST_PARAMETER = "forcibly-closed-flag";
		
	/** The Constant TEXT_PATTERN. */
	private static final String TEXT_PATTERN = "([a-zA-Z\u0400-\u052F]{4,})(.*)";
	
	/** The Constant EMAIL_PATTERN. */
	private static final String EMAIL_PATTERN = "([a-z]{1})([^@?!.,:;\'=|\\%$&#\\s-\"(&#42)(&#43)(&#47)(&#40)(&#41)]*)@([^@?!.,:;\'=|\\%$&#\\s-\"(&#42)(&#43)(&#47)(&#40)(&#41)]{2,20})(\\.[a-z]{2,3}){1,}";
	
	/** The Constant PHONE_PATTERN. */
	private static final String PHONE_PATTERN = "([\u002B]{1})([0-9]{3})-([0-9]{2})-([0-9]{7})";
	
    
    /* (non-Javadoc)
     * @see javax.servlet.Filter#init(javax.servlet.FilterConfig)
     */
    public void init(FilterConfig fConfig) throws ServletException {
		LOG.info("SearchFilter has been initialized");
    }
 
	/* (non-Javadoc)
	 * @see javax.servlet.Filter#doFilter(javax.servlet.ServletRequest, javax.servlet.ServletResponse, javax.servlet.FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
    	HttpServletRequest req = (HttpServletRequest) request;
        String command = req.getParameter(COMMAND_REQUEST_PARAMETER);
        if (command != null) {
	        if (CommandName.SEARCH_FOR_BOOKINGS.toString().toLowerCase().equals(command)) {
	        	String name = req.getParameter(NAME_REQUEST_PARAMETER); 
	       		String surname = req.getParameter(SURNAME_REQUEST_PARAMETER);
	       		String email = req.getParameter(EMAIL_REQUEST_PARAMETER); 
	       		String mobile = req.getParameter(MOBILE_REQUEST_PARAMETER);
	       		boolean forciblyClosedFlag = Boolean.parseBoolean((req.getParameter(FORCIBLE_CLOSE_FLAG_REQUEST_PARAMETER)));
	       		LOG.debug("forcibly closed flag is: " + forciblyClosedFlag);
	       		
	       		if (name.isEmpty() && surname.isEmpty() && email.isEmpty() && mobile.isEmpty() && forciblyClosedFlag == false) {
	       			request.setAttribute("pageFromSearchFilter", JspPageName.SEARCH_PAGE);
	       			request.setAttribute("searchFieldsNotFilledFlag", "true");
	       			req.getSession().setAttribute("name", null);
			        req.getSession().setAttribute("surname", null);
			        req.getSession().setAttribute("email", null);
			        req.getSession().setAttribute("mobile", null);
			        req.getSession().setAttribute("forciblyClosedFlag", null);
	       		} else {	       		
		       		Pattern patternText = Pattern.compile(TEXT_PATTERN);
		       		Pattern patternEmail = Pattern.compile(EMAIL_PATTERN);
		       		Pattern patternPhone = Pattern.compile(PHONE_PATTERN);
	
			        Matcher matcherName = patternText.matcher(name);
			        Matcher matcherSurname = patternText.matcher(surname);
			        Matcher matcherEmail = patternEmail.matcher(email);
			        Matcher matcherMobile = patternPhone.matcher(mobile);
			        
			        if(!matcherName.find() && !name.isEmpty()) {
			        	LOG.info("Incorrect value for name field");
			        	request.setAttribute("pageFromSearchFilter", JspPageName.SEARCH_PAGE);
			        	request.setAttribute("incorrectTextFieldsFlag", "true");
			        } else if (!matcherSurname.find() && !surname.isEmpty()) {
			        	LOG.info("Incorrect value for surname field");
			        	request.setAttribute("pageFromSearchFilter", JspPageName.SEARCH_PAGE);
			        	request.setAttribute("incorrectTextFieldsFlag", "true");
			        } else if (!matcherEmail.find() && !email.isEmpty()) {
			        	LOG.info("Incorrect value for email field");
			        	request.setAttribute("pageFromSearchFilter", JspPageName.SEARCH_PAGE);
			        	request.setAttribute("incorrectEmailFlag", "true");
			        } else if (!matcherMobile.find() && !mobile.isEmpty()) {
			        	LOG.info("Incorrect value for mobile field");
			        	request.setAttribute("pageFromSearchFilter", JspPageName.SEARCH_PAGE);
			        	request.setAttribute("incorrectMobileFlag", "true");
			        }
			        
			        req.getSession().setAttribute("name", name);
			        req.getSession().setAttribute("surname", surname);
			        req.getSession().setAttribute("email", email);
			        req.getSession().setAttribute("mobile", mobile);
			        req.getSession().setAttribute("forciblyClosedFlag", forciblyClosedFlag);
	       		}   
				LOG.info("SearchFilter has done it's work!");
	    	}
        }
        chain.doFilter(request, response);
    }
 
    /* (non-Javadoc)
     * @see javax.servlet.Filter#destroy()
     */
    public void destroy() {}
}
