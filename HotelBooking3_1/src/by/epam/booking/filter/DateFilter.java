package by.epam.booking.filter;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import by.epam.booking.command.CommandName;
import by.epam.booking.command.JspPageName;

/**
 * The Class DateFilter.
 */
public class DateFilter implements Filter {
	
	/** The Constant LOG. */
	private final static Logger LOG = Logger.getLogger(DateFilter.class);
	
	/** The Constant COMMAND_REQUEST_PARAMETER. */
	private static final String COMMAND_REQUEST_PARAMETER = "command";
	
	/** The Constant ARRIVAL_DATE_REQUEST_PARAMETER. */
	private static final String ARRIVAL_DATE_REQUEST_PARAMETER = "arrival-date";
	
	/** The Constant LEAVING_DATE_REQUEST_PARAMETER. */
	private static final String LEAVING_DATE_REQUEST_PARAMETER = "leaving-date";
	
	/** The Constant PM1400_TIME_MILLISECONDS_AMOUNT. */
	private static final int PM1400_TIME_MILLISECONDS_AMOUNT = 50_400_000;
	
	/** The Constant AM1200_TIME_MILLISECONDS_AMOUNT. */
	private static final int AM1200_TIME_MILLISECONDS_AMOUNT = 43_200_000;
	
	/* (non-Javadoc)
	 * @see javax.servlet.Filter#init(javax.servlet.FilterConfig)
	 */
	@Override
	public void init(FilterConfig fConfig) throws ServletException {
		LOG.info("DateFilter has been initialized");
    }
 
    /* (non-Javadoc)
     * @see javax.servlet.Filter#doFilter(javax.servlet.ServletRequest, javax.servlet.ServletResponse, javax.servlet.FilterChain)
     */
    @Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
    	Date dateFrom = null;
    	Date dateUntil = null;
    	Date dateToday = null;
    	String arrivalDate = null;
        String leavingDate = null;
        String todayDate = null;
        String command = null;
        
    	SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
       	
        HttpServletRequest req = (HttpServletRequest) request;
        command = req.getParameter(COMMAND_REQUEST_PARAMETER);
        
        if (command != null) {
	        if (CommandName.CHECK_ON_DATE.toString().toLowerCase().equals(command)) {
	        	todayDate = req.getSession().getAttribute("minArrivalDateAsString").toString();
	        	arrivalDate = req.getParameter(ARRIVAL_DATE_REQUEST_PARAMETER);
	            leavingDate = req.getParameter(LEAVING_DATE_REQUEST_PARAMETER);
	           
	            if(arrivalDate.isEmpty() || leavingDate.isEmpty()) {
            		LOG.info("Dates are empty");
            		req.setAttribute("pageFromDateFilter", JspPageName.INDEX_PAGE);
                	req.setAttribute("emptyDateValuesFlag", "true");
            	} else {	
		            formatter.setLenient(false);
		            try {
		            	dateFrom = formatter.parse(arrivalDate);
		            	dateUntil = formatter.parse(leavingDate);
		            	dateToday = formatter.parse(todayDate);
		            	if ( (dateFrom.equals(dateToday) || dateFrom.after(dateToday)) && (dateUntil.after(dateFrom)) ) {
		            		req.getSession(false).setAttribute("enteredArrivalDateAsString", arrivalDate);
		        			req.getSession(false).setAttribute("enteredLeavingDateAsString", leavingDate);
		            		req.getSession(false).setAttribute("enteredArrivalDate", new Date(dateFrom.getTime() + PM1400_TIME_MILLISECONDS_AMOUNT));
		        			req.getSession(false).setAttribute("enteredLeavingDate", new Date(dateUntil.getTime() + AM1200_TIME_MILLISECONDS_AMOUNT));
		            		LOG.info("Everything is good in DateFilter!");
		            	} else {
		            		LOG.info("Arrival date or leaving date have incorrect values");
		                	req.setAttribute("pageFromDateFilter", JspPageName.INDEX_PAGE);
		                	req.setAttribute("incorrectDateValuesFlag", "true");
		            	}
		            } catch (ParseException ex) {
		            	LOG.info("Arrival date or leaving date are in the invalid format");
		            	request.setAttribute("pageFromDateFilter", JspPageName.INDEX_PAGE);
		            	request.setAttribute("incorrectDateFormatFlag", "true");
		            }
            	}
	           	LOG.info("DateFilter has done it's work!");
	    	}
        }
		chain.doFilter(request, response);
    }

	/* (non-Javadoc)
	 * @see javax.servlet.Filter#destroy()
	 */
	@Override
	public void destroy() {
		
	}
}