package by.epam.booking.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

/**
 * The Class RedirectSecurityFilter.
 */
public class RedirectSecurityFilter implements Filter {
	
	/** The Constant LOG. */
	private final static Logger LOG = Logger.getLogger(RedirectSecurityFilter.class);
	
	/** The index path. */
	private String indexPath;
	
	/* (non-Javadoc)
	 * @see javax.servlet.Filter#init(javax.servlet.FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		indexPath = fConfig.getInitParameter("indexPath");
		LOG.info("RedirectSecurityFilter has been initialized. indexPath=" + indexPath);
	}
	
	/* (non-Javadoc)
	 * @see javax.servlet.Filter#destroy()
	 */
	public void destroy() {		
	}

	/* (non-Javadoc)
	 * @see javax.servlet.Filter#doFilter(javax.servlet.ServletRequest, javax.servlet.ServletResponse, javax.servlet.FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest httpRequest = (HttpServletRequest) request;
		HttpServletResponse httpResponse = (HttpServletResponse) response;
		
		httpResponse.sendRedirect(httpRequest.getContextPath() + indexPath);
		
		httpRequest.getSession(false).setAttribute("bookingMapToView", null);								// обнуляем все важные атрибуты в сессии
		httpRequest.getSession(false).setAttribute("bookingMapToCancel", null);
		httpRequest.getSession(false).setAttribute("bookingMapToClose", null);
		httpRequest.getSession(false).setAttribute("bookingMapToConfirm", null);
		httpRequest.getSession(false).setAttribute("bookingMapToCancel", null);
		httpRequest.getSession(false).setAttribute("bookingMapToConfirm", null);
		httpRequest.getSession(false).setAttribute("bookingMapToClose", null);
		httpRequest.getSession(false).setAttribute("bookingMapToView", null);
		httpRequest.getSession(false).setAttribute("bookingMapToCreateBill", null);
		httpRequest.getSession(false).setAttribute("forciblyClosedFlag", null);
		
		chain.doFilter(request, response);
	}
}