package by.epam.booking.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import by.epam.booking.command.Command;
import by.epam.booking.command.CommandException;
import by.epam.booking.command.CommandMatcher;
import by.epam.booking.command.CommandName;
import by.epam.booking.command.CommandType;
import by.epam.booking.exception.BookingException;

/**
 * The Class Controller.
 */
public class Controller extends HttpServlet {
	
	/** The Constant LOG. */
	private static final Logger LOG = Logger.getLogger(Controller.class);
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The Constant COMMAND_REQUEST_PARAMETER. */
	private static final String COMMAND_REQUEST_PARAMETER = "command";

	/**
	 * Instantiates a new controller.
	 */
	public Controller() {
		super();
	}

	/* (non-Javadoc)
	 * @see javax.servlet.http.HttpServlet#doGet(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	/* (non-Javadoc)
	 * @see javax.servlet.http.HttpServlet#doPost(javax.servlet.http.HttpServletRequest, javax.servlet.http.HttpServletResponse)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}
	
	/**
	 * Processes request. Get's the command instance from command matcher and invokes execute method on it.
	 * After command execution process ends chooses whether to forward or sendRedirect to the resulting page. 
	 *
	 * @param request the request
	 * @param response the response
	 * @throws ServletException the servlet exception
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	private void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String page = null;
		Command command = null;
		CommandMatcher commandMatcher = CommandMatcher.getInstance();
		String commandName = request.getParameter(COMMAND_REQUEST_PARAMETER);
		LOG.debug(commandName);		
		
		if (commandName != null) {
			command = commandMatcher.getCommand(CommandName.valueOf(commandName.toUpperCase()));
			try {
				page = command.execute(request);
			} catch (CommandException ex) {
				throw new BookingException("An error occured in executing command. ", ex);
			}
			if (CommandType.WITHOUT_DB_CHANGING.equals(command.getCommandType())) {											// если тип команды подразумевает только получение данных из БД  
				LOG.debug("Forwarding to: " + page);
				request.getRequestDispatcher(page).forward(request, response);												// переходим на указанную страницу старым запросом
			} else {																										// иначе, если тип команды подразумевает изменение БД
				LOG.debug("Redirecting to: " + page);
				response.sendRedirect(page);																				// переходим на указанную страницу новым запросом
			}
		} else {
			throw new BookingException("There is no such command in application");
		}
	}
}
