package by.epam.booking.listener;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import org.apache.log4j.Logger;

import by.epam.booking.command.JspPageName;
import by.epam.booking.service.ServiceException;
import by.epam.booking.service.VisitorServiceGroup;

/**
 * The listener interface for receiving sessionContext events.
 * The class that is interested in processing a sessionContext
 * event implements this interface, and the object created
 * with that class is registered with a component using the
 * component's {@code addSessionContextListener} method. When
 * the sessionContext event occurs, that object's appropriate
 * method is invoked.
 *
 * @see SessionContextEvent
 */
public class SessionContextListener implements HttpSessionListener {
	
	/** The Constant LOG. */
	private final static Logger LOG = Logger.getLogger(SessionContextListener.class);
	
	/** The Constant HOTEL_INFORMATION_RESOURCE_NAME. */
	private static final String HOTEL_INFORMATION_RESOURCE_NAME = "hotel_information";
	
	/** The Constant BOOKING_AGREEMENT_RESOURCE_NAME. */
	private static final String BOOKING_AGREEMENT_RESOURCE_NAME = "booking_agreement";
	
	/** The Constant COUNTRY_DEFAULT_NAME. */
	private static final String COUNTRY_DEFAULT_NAME = "BY";
	
	/** The Constant CURRENCY_DEFAULT_COEFFICIENT. */
	private static final float CURRENCY_DEFAULT_COEFFICIENT = 1.0F;
	
	/** The Constant DAY_MILLISECONDS_AMOUNT. */
	private static final int DAY_MILLISECONDS_AMOUNT = 86_400_000;
	
	/* (non-Javadoc)
	 * @see javax.servlet.http.HttpSessionListener#sessionCreated(javax.servlet.http.HttpSessionEvent)
	 */
	@Override
	public void sessionCreated(HttpSessionEvent sessionEvent) {
		Locale locale = new Locale("ru", "Ru");
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		
		Calendar today = Calendar.getInstance();
    	today.set(Calendar.HOUR_OF_DAY, 0);
    	today.set(Calendar.MINUTE, 0);
    	today.set(Calendar.SECOND, 0);
    	today.set(Calendar.MILLISECOND, 0);
    	Date todayDate = today.getTime();
    	Date tomorrowDate = new Date(today.getTimeInMillis() + DAY_MILLISECONDS_AMOUNT);
    	
    	Calendar maxArrival = Calendar.getInstance();
    	maxArrival.set(Calendar.MONTH, 11);
    	maxArrival.set(Calendar.DAY_OF_MONTH, 30);
    	Date maxArrivalDate = maxArrival.getTime();
    	
    	Calendar maxLeaving = Calendar.getInstance();
    	maxLeaving.set(Calendar.MONTH, 11);
    	maxLeaving.set(Calendar.DAY_OF_MONTH, 31);
    	Date maxLeavingDate = maxLeaving.getTime();
    	
    	String resourceHotel = null;
    	String language = locale.getLanguage();
    	try {																														// получаем информацию об отеле на языке локали по умолчанию
			resourceHotel = VisitorServiceGroup.getInstance().getResource(HOTEL_INFORMATION_RESOURCE_NAME, language);
		} catch (ServiceException ex) {
			LOG.error("Error in obtaining hotel information resource");
		}
    	
    	Map<String, String> mapBookingAgreement = null;
    	Map<String, String> mapBookingAgreementTemp = new HashMap<String, String>();
    	try {																														// получаем информацию о соглашении по бронированию на доступных языках
    		mapBookingAgreement = VisitorServiceGroup.getInstance().getResource(BOOKING_AGREEMENT_RESOURCE_NAME);
    		for(Map.Entry<String, String> entry : mapBookingAgreement.entrySet()) {
    			String value = entry.getValue().replaceAll("(<br />){2}", "\n\n");													// заменяем все HTML-переходы на обычные
    			mapBookingAgreementTemp.put(entry.getKey(), value);
    		}
    		mapBookingAgreement.clear();
    		mapBookingAgreement.putAll(mapBookingAgreementTemp);
		} catch (ServiceException ex) {
			LOG.error("Error in obtaining booking agreement resource");
		}
    	
    	String countryName = "RU".equals(locale.getCountry()) ? COUNTRY_DEFAULT_NAME : locale.getCountry();    	
    	float dollarCoefficient = 0.0F;
    	try {																														// получаем курс доллара
    		dollarCoefficient = VisitorServiceGroup.getInstance().getDollarCoefficient(countryName);
			if (dollarCoefficient == 0 ) {
				dollarCoefficient = CURRENCY_DEFAULT_COEFFICIENT;
				LOG.info("Currency coefficient hasn't been found. Set to default: " + dollarCoefficient);
			}
		} catch (ServiceException ex) {
			LOG.error("Error in obtaining dollar coefficient information");
		}
    	
    	List<Double> apartmentPricesInDouble = null;
    	List<String> apartmentPricesInString = new ArrayList<String>();
    	NumberFormat currencyFormat = null;
    	try {																														// получаем список цен на все типы номеров
    		apartmentPricesInDouble = VisitorServiceGroup.getInstance().getApartmentPrices();
			if (apartmentPricesInDouble.isEmpty()) {
				LOG.info("No apartment prices found");
			} else {
				currencyFormat = NumberFormat.getCurrencyInstance(locale);
				for (int i = 0; i < apartmentPricesInDouble.size(); i++) {
					String price = currencyFormat.format(apartmentPricesInDouble.get(i) * dollarCoefficient);
					apartmentPricesInString.add(price);
				}
			}
		} catch (ServiceException ex) {
			LOG.error("Error in obtaining apartment prices");
		}
    	
    	sessionEvent.getSession().setAttribute("locale", locale);
    	sessionEvent.getSession().setAttribute("minArrivalDateAsString", formatter.format(todayDate));
    	sessionEvent.getSession().setAttribute("maxArrivalDateAsString", formatter.format(maxArrivalDate));
    	sessionEvent.getSession().setAttribute("minLeavingDateAsString", formatter.format(tomorrowDate));
    	sessionEvent.getSession().setAttribute("maxLeavingDateAsString", formatter.format(maxLeavingDate));
    	sessionEvent.getSession().setAttribute("mapBookingAgreement", mapBookingAgreement);
    	sessionEvent.getSession().setAttribute("dollarCoefficient", dollarCoefficient);
    	sessionEvent.getSession().setAttribute("currencyFormat", currencyFormat);
    	sessionEvent.getSession().setAttribute("apartmentPricesInDouble", apartmentPricesInDouble);
    	sessionEvent.getSession().setAttribute("apartmentPricesInString", apartmentPricesInString);
    	sessionEvent.getSession().setAttribute("lastRequiredResource", resourceHotel);
    	sessionEvent.getSession().setAttribute("lastAccessedPage", JspPageName.INDEX_PAGE);
    	sessionEvent.getSession().setAttribute("lastRequiredResourceName", HOTEL_INFORMATION_RESOURCE_NAME);
    	    	
    	LOG.info("Locale has been defined: " + locale);
    	LOG.info("Dollar coeffcient: " + dollarCoefficient);
    	LOG.info("All necessary dates have been defined");		
	}

	/* (non-Javadoc)
	 * @see javax.servlet.http.HttpSessionListener#sessionDestroyed(javax.servlet.http.HttpSessionEvent)
	 */
	@Override
	public void sessionDestroyed(HttpSessionEvent sessionEvent) {
		
	}

}