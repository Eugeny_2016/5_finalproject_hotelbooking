package by.epam.booking.listener;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.log4j.Logger;

import by.epam.booking.command.CommandMatcher;
import by.epam.booking.pool.ConnectionPool;
import by.epam.booking.pool.ConnectionPoolException;

/**
 * The listener interface for receiving projectContext events.
 * The class that is interested in processing a projectContext
 * event implements this interface, and the object created
 * with that class is registered with a component using the
 * component's {@code addProjectContextListener} method. When
 * the projectContext event occurs, that object's appropriate
 * method is invoked.
 * This listener invokes connection pool initialization method and 
 * it also initializes command matcher's table.
 *
 * @see ProjectContextEvent
 */
public class ProjectContextListener implements ServletContextListener {
	
	/** The Constant LOG. */
	private final static Logger LOG = Logger.getLogger(ProjectContextListener.class);
	
	/** The Constant AMOUNT_OF_POOL_INITIALIZING_ATTEMPTS. */
	private static final int AMOUNT_OF_POOL_INITIALIZING_ATTEMPTS = 3;
		
	/**
	 * Instantiates a new project context listener.
	 */
	public ProjectContextListener() {}

	/* (non-Javadoc)
	 * @see javax.servlet.ServletContextListener#contextInitialized(javax.servlet.ServletContextEvent)
	 */
	@Override
	public void contextInitialized(ServletContextEvent contextEvent) {
		initializePool();
		initializeCommandMatcher();
	}
	
	/* (non-Javadoc)
	 * @see javax.servlet.ServletContextListener#contextDestroyed(javax.servlet.ServletContextEvent)
	 */
	@Override
	public void contextDestroyed(ServletContextEvent contextEvent) {
		closePool();
	}
	
	/**
	 * Initialize command matcher.
	 */
	private void initializeCommandMatcher() {
		CommandMatcher.getInstance();
		LOG.info("All command objects have been initialized successfully!");
	}
	
	/**
	 * Initialize pool.
	 */
	private void initializePool() {
		ConnectionPool pool = null;
		int attemptsCounter = AMOUNT_OF_POOL_INITIALIZING_ATTEMPTS;
		try {
			pool = ConnectionPool.getInstance();
			if (pool != null) {
				
				pool.initPoolData();
				while (!pool.isMinimallyFilled() && attemptsCounter !=0) {													// 3 раза пытаемся хотя бы минимально наполнить пул соединений 
					LOG.info("Pool isn't filled enough! Pool size: " + pool.getPoolSize() + ". Trying to fill it at least to a minimum size");
					pool.initPoolData();
					attemptsCounter--;
				}
				
				if (pool.isMinimallyFilled()) {
					LOG.info("Connection pool has been initialized successfully! Pool size: " + pool.getPoolSize());
				} else {																									// если пул так и не наполнился минимально хотя бы, то выбрасываем ошибку времени выполнения
					LOG.fatal("Error in initializing the connection pool! Pool isn't filled enough: "+ pool.getPoolSize());
					throw new RuntimeException("Connection pool isn't filled enough!");
				}
				
			} else {
				LOG.fatal("Connection pool returned with null-value");
				throw new RuntimeException("Connection pool hasn't even been created!");
			}
		} catch (ConnectionPoolException ex) {
			LOG.fatal("Error in initializing the connection pool!");
			throw new RuntimeException("Connection pool hasn't been initialized!");
		}
	}
	
	/**
	 * Close pool.
	 */
	private void closePool() {
		ConnectionPool pool = ConnectionPool.getInstance();
		if (pool != null) {
			pool.closeConnectionQueues();
			LOG.info("All queues of connections have been destroyed!");
		}		
	}
}
