package by.epam.booking.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.locks.ReentrantLock;
import java.util.Map.Entry;

import org.apache.log4j.Logger;

import by.epam.booking.command.impl.CommandDenialReasonName;
import by.epam.booking.dao.ClientDAO;
import by.epam.booking.dao.DAOException;
import by.epam.booking.domain.ApartmentType;
import by.epam.booking.domain.BedType;
import by.epam.booking.domain.BookingForm;
import by.epam.booking.domain.CardType;
import by.epam.booking.domain.Service;
import by.epam.booking.pool.ConnectionPool;

/**
 * The Class SQLClientDAO.
 */
public class SQLClientDAO extends SQLUserDAO implements ClientDAO {
	
	/** The Constant LOG. */
	private static final Logger LOG = Logger.getLogger(SQLClientDAO.class);
	
	/** The Constant INSTANCE. */
	private static final SQLClientDAO INSTANCE = new SQLClientDAO();
	
	/** The Constant LOCK. */
	private static final ReentrantLock LOCK = new ReentrantLock();
		
	/** The Constant INTERSECTING_BOOKING_FORMS_SQL_CHECK. */
	private static final String INTERSECTING_BOOKING_FORMS_SQL_CHECK = "SELECT APARTMENT.id, APARTMENT.number, BOOKING_FORM.arrival_date, BOOKING_FORM.leaving_date "
																	 + "FROM hotel.APARTMENT "
																	 + "LEFT JOIN (hotel.APARTMENT_BUSYNESS INNER JOIN hotel.BOOKING_FORM "
																	 + "ON APARTMENT_BUSYNESS.booking_form_id = BOOKING_FORM.id) "
																	 + "ON APARTMENT.id = APARTMENT_BUSYNESS.apartment_id "
																	 + "WHERE (BOOKING_FORM.arrival_date = ? OR BOOKING_FORM.leaving_date = ? OR (BOOKING_FORM.arrival_date < ? AND BOOKING_FORM.leaving_date > ?));";

	/** The Constant ALL_APARTMENTS_SQL_SELECT. */
	private static final String ALL_APARTMENTS_SQL_SELECT = "SELECT id, number, apartment_type_id "
			  											  + "FROM hotel.APARTMENT;";
	
	/** The Constant APARTMENT_TYPE_INFORMATION_SQL_SELECT. */
	private static final String APARTMENT_TYPE_INFORMATION_SQL_SELECT = "SELECT id, name, description, square, amount_of_rooms, amount_of_guests_max, price_in_dollars, "
																	  + "discount_price_in_dollars, discount_price_date_from, discount_price_date_until, service_set_id "
																	  + "FROM hotel.APARTMENT_TYPE "
																	  + "WHERE id = ?;";
	
	/** The Constant ALL_SERVICES_OF_SPECIFIC_SERVICE_SET_SQL_SELECT. */
	private static final String ALL_SERVICES_OF_SPECIFIC_SERVICE_SET_SQL_SELECT = "SELECT SERVICE.id, SERVICE.name, SERVICE.description "
																			    + "FROM hotel.SERVICE_SET "
																			    + "INNER JOIN (hotel.SERVICE_MATCHER INNER JOIN hotel.SERVICE "
																			    + "ON SERVICE_MATCHER.service_id = SERVICE.id) "
																			    + "ON SERVICE_SET.id = SERVICE_MATCHER.service_set_id "
																			    + "WHERE SERVICE_SET.id = ?;";
	
	/** The Constant CARD_TYPES_INFORMATION_SQL_SELECT. */
	private static final String CARD_TYPES_INFORMATION_SQL_SELECT = "SELECT id, name, description "
																  + "FROM hotel.CARD_TYPE;";
	
	/** The Constant CARD_TYPE_INFORMATION_SQL_SELECT. */
	private static final String CARD_TYPE_INFORMATION_SQL_SELECT = "SELECT id, name, description "
															 	 + "FROM hotel.CARD_TYPE "
															 	 + "WHERE id = ?;";
	
	/** The Constant BED_TYPES_INFORMATION_SQL_SELECT. */
	private static final String BED_TYPES_INFORMATION_SQL_SELECT = "SELECT id, name, description, amount_of_persons "
																+ "FROM hotel.BED_TYPE";
	
	/** The Constant BED_TYPE_INFORMATION_SQL_SELECT. */
	private static final String BED_TYPE_INFORMATION_SQL_SELECT = "SELECT id, name, description, amount_of_persons "
																 + "FROM hotel.BED_TYPE "
																 + "WHERE id = ?;";
	
	/** The Constant BOOKING_FORM_INFORMATION_SQL_INSERT. */
	private static final String BOOKING_FORM_INFORMATION_SQL_INSERT = "INSERT INTO hotel.BOOKING_FORM (user_id, arrival_date, leaving_date, apartment_type_id, amount_of_guests, "
																	+ "amount_of_apartments, total_price, bed_type_id, personal_preferences, card_type_id, card_number, card_expire_date, "
																	+ "booking_aproval_sms, booking_creation_date, status, forcibly_closed_flag) "
																	+ "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);";
	
	/** The Constant BOOKING_FORM_ID_SQL_SELECT. */
	private static final String BOOKING_FORM_ID_SQL_SELECT = "SELECT id "
														   + "FROM hotel.BOOKING_FORM "
														   + "WHERE booking_creation_date = ?;";
	
	/** The Constant BOOKING_FORMS_OF_CLIENT_SQL_SELECT. */
	private static final String BOOKING_FORMS_OF_CLIENT_SQL_SELECT = "SELECT id, user_id, arrival_date, leaving_date, apartment_type_id, amount_of_guests, amount_of_apartments, total_price, bed_type_id, "
																   + "personal_preferences, card_type_id, card_number, card_expire_date, booking_aproval_sms, booking_creation_date, status, forcibly_closed_flag "
																   + "FROM hotel.BOOKING_FORM "
																   + "WHERE user_id = ?;";
	
	/** The Constant TEMPORARY_BOOKING_FORMS_OF_CLIENTSQL_SELECT. */
	private static final String TEMPORARY_BOOKING_FORMS_OF_CLIENTSQL_SELECT = "SELECT id, user_id, arrival_date, leaving_date, apartment_type_id, amount_of_guests, amount_of_apartments, total_price, bed_type_id, "
																			+ "personal_preferences, card_type_id, card_number, card_expire_date, booking_aproval_sms, booking_creation_date, status, forcibly_closed_flag "
																		    + "FROM hotel.BOOKING_FORM "
																		    + "WHERE user_id = ? AND status = ?;";
	
	/** The Constant APARTMENT_BUSYNESS_TABLE_SQL_INSERT. */
	private static final String APARTMENT_BUSYNESS_TABLE_SQL_INSERT = "INSERT INTO hotel.APARTMENT_BUSYNESS (booking_form_id, apartment_id) "
																	+ "VALUES (?,?);";
	
	/** The Constant BOOKING_FORM_CANCELING_SQL_UPDATE. */
	private static final String BOOKING_FORM_CANCELING_SQL_UPDATE = "UPDATE hotel.BOOKING_FORM "
																  + "SET status = ? "
																  + "WHERE id = ?;";
	
	/** The Constant APARTMENT_BUSYNESS_TABLE_SQL_DELETE. */
	private static final String APARTMENT_BUSYNESS_TABLE_SQL_DELETE = "DELETE FROM hotel.APARTMENT_BUSYNESS "
																	+ "WHERE booking_form_id = ?;";
	
	/**
	 * Instantiates a new SQL client dao.
	 */
	protected SQLClientDAO() {}
	
	/**
	 * Gets the single instance of SQLClientDAO.
	 *
	 * @return single instance of SQLClientDAO
	 */
	public static SQLClientDAO getInstance() {
		return INSTANCE;
	}
	
	/* (non-Javadoc)
	 * @see by.epam.booking.dao.ClientDAO#selectApartmentOnDate(long, long)
	 */
	@Override
	public Map<ApartmentType, List<Integer>> selectApartmentOnDate(long dateFrom, long dateUntil) throws DAOException {
		Connection connection = null;
		Map<ApartmentType, List<Integer>> resultingMap = null;
		
		LOCK.lock();
		try {
			connection = ConnectionPool.getInstance().takeConnection();
			resultingMap = check(dateFrom, dateUntil, connection);
		} catch (InterruptedException ex) {
			throw new DAOException("Error in obtaining connection. ", ex);			
		} catch (SQLException ex) {
			throw new DAOException("SQLException in DAO class.", ex);
		} finally {
			releaseConnection(connection);
			LOCK.unlock();
		}
		return resultingMap;
	}
	
	/* (non-Javadoc)
	 * @see by.epam.booking.dao.ClientDAO#saveBookingForm(long, long, by.epam.booking.domain.BookingForm)
	 */
	@SuppressWarnings("resource")
	@Override
	public Map<Boolean, CommandDenialReasonName> saveBookingForm(long dateFrom, long dateUntil, BookingForm bookingForm) throws DAOException {
		Connection connection = null;																								// данный метод сохраняет заявку по бронированию в БД
		PreparedStatement pstBookSaving = null;
		PreparedStatement pstBookingId = null;
		PreparedStatement pstBusynessTable = null;
		ResultSet rsBookingId = null;
		List<Integer> availableApartmentsOfType = new ArrayList<Integer>();
		List<Integer> apartmentsForClient = new ArrayList<Integer>();
		Map<ApartmentType, List<Integer>> resultingMap = null;
		HashMap<Boolean, CommandDenialReasonName> map = new HashMap<Boolean, CommandDenialReasonName>();
		int countBookSaving = 0;
		int countBusynessTable = 0;
		
		LOCK.lock();																												// блокируем все другие дейстивя, которым нужно обратиться к таблице apartment_busyness 
		try {
			connection = ConnectionPool.getInstance().takeConnection();
			resultingMap = check(dateFrom, dateUntil, connection);
			if (!resultingMap.isEmpty()) {																							// если номера ещё остались пока клиент оформлял заявку
				for (ApartmentType apartmentType : resultingMap.keySet()) {
					if (apartmentType.getId() == bookingForm.getApartmentType().getId()) {											// если из оставшихся номеров есть номера нужного типа
						availableApartmentsOfType.addAll(resultingMap.get(apartmentType));
					}
				}
			} else {																												// если номеров вообще не осталось на данный момент
				map.put(false, CommandDenialReasonName.NO_APARTMENTS_AT_ALL);														// возвращаем отказ в сохранении заявки по бронированию
				return map;
			}
			
			if (!availableApartmentsOfType.isEmpty()) {																				// если список доступных номеров нужного типа для пользователя не пустой
				if (availableApartmentsOfType.size() >= bookingForm.getAmountOfApartments()) {										// если в этом списке есть столько же номеров, сколько хочет забронировать клиент
					for (int i = 0; i < bookingForm.getAmountOfApartments(); i++) {
						apartmentsForClient.add(availableApartmentsOfType.get(i));
					}
				} else {																											// иначе возвращаем отказ в сохранении заявки по бронированию
					map.put(false, CommandDenialReasonName.NO_APARTMENTS_OF_REQUIRED_AMOUNT);
					return map;
				}
			} else {																												// иначе возвращаем отказ в сохранении заявки по бронированию
				map.put(false, CommandDenialReasonName.NO_APARTMENTS_OF_TYPE);
				return map;
			}
			
			connection.setAutoCommit(false);																						// убираем autocommit чтобы закоммитить всю транзакцию целиком
			connection.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);												// запрещаем грязное, неповторяющееся, фантомное чтения
			pstBookSaving = connection.prepareStatement(BOOKING_FORM_INFORMATION_SQL_INSERT);										// пытаемся сохранить новую заявку по бронированию
			pstBookSaving.setInt(1, bookingForm.getUserId());
			pstBookSaving.setLong(2, bookingForm.getArrivalDate());
			pstBookSaving.setLong(3, bookingForm.getLeavingDate());
			pstBookSaving.setInt(4, bookingForm.getApartmentType().getId());
			pstBookSaving.setInt(5, bookingForm.getAmountOfGuests());
			pstBookSaving.setInt(6, bookingForm.getAmountOfApartments());
			pstBookSaving.setDouble(7, bookingForm.getTotalPrice());
			pstBookSaving.setInt(8, bookingForm.getBedType().getId());
			pstBookSaving.setString(9, bookingForm.getPersonalPreferences());
			pstBookSaving.setInt(10, bookingForm.getCardType().getId());
			pstBookSaving.setString(11, bookingForm.getCardNumber());
			pstBookSaving.setLong(12, bookingForm.getCardExpireDate());
			pstBookSaving.setBoolean(13, bookingForm.getBookingAprovalSms());
			pstBookSaving.setLong(14, bookingForm.getBookingCreationDate());
			pstBookSaving.setString(15, bookingForm.getStatus());
			pstBookSaving.setBoolean(16, bookingForm.getForciblyClosedFlag());
			
			countBookSaving = pstBookSaving.executeUpdate();
			if (countBookSaving != 1) {																								// если сохранить заявку по какой-то причине не удалось
				connection.rollback();
				throw new SQLException("Some problems occured in saving booking form");
			}
			
			pstBookingId = connection.prepareStatement(BOOKING_FORM_ID_SQL_SELECT);													// получаем id только что сохранённой заявки
			pstBookingId.setLong(1, bookingForm.getBookingCreationDate());
			rsBookingId = pstBookingId.executeQuery();
			if (rsBookingId.next()) {																								// если получить id удалось
				pstBusynessTable = connection.prepareStatement(APARTMENT_BUSYNESS_TABLE_SQL_INSERT);
				for (int i = 0; i < apartmentsForClient.size(); i++) {
					pstBusynessTable.setInt(1, rsBookingId.getInt("id"));
					pstBusynessTable.setInt(2, apartmentsForClient.get(i));
					countBusynessTable = pstBusynessTable.executeUpdate();															// для каждого номера вставляем в таблицу занятости номеров новые записи с одинаковым booking id
					if (countBusynessTable != 1) {																					// если сохранить запись по занятости номера по какой-то причине не удалось
						connection.rollback();
						throw new SQLException("Some problems occured in saving apartment busyness");
					}
				}
				connection.commit();
				map.put(true, CommandDenialReasonName.NONE);
			} else {
				throw new SQLException("Some problems occured in obtaining booking form id");
			}
		} catch (InterruptedException ex) {
			throw new DAOException("Error in obtaining connection. ", ex);			
		} catch (SQLException ex) {
			throw new DAOException("SQLException in DAO class.", ex);
		} finally {
			closeStatement(pstBookSaving);
			closeStatement(pstBookingId);
			closeStatement(pstBusynessTable);
			releaseConnection(connection);
			LOCK.unlock();
		}
		return map;
	}
	
	/* (non-Javadoc)
	 * @see by.epam.booking.dao.ClientDAO#selectAllBookingForms(int)
	 */
	@Override
	public List<BookingForm> selectAllBookingForms(int userId) throws DAOException {
		Connection connection = null;
		PreparedStatement pstBookingForm = null;
		List<BookingForm> listOfClientBookings = new ArrayList<BookingForm>();
		
		try {
			connection = ConnectionPool.getInstance().takeConnection();
			pstBookingForm = connection.prepareStatement(BOOKING_FORMS_OF_CLIENT_SQL_SELECT);
			pstBookingForm.setInt(1, userId);
			listOfClientBookings = selectBookingForms(pstBookingForm, connection);
		
		} catch(InterruptedException ex) {
			throw new DAOException("Error in obtaining connection. ", ex);			
		} catch (SQLException ex) {
			throw new DAOException("SQLException in DAO class. ", ex);
		} finally {
			closeStatement(pstBookingForm);
			releaseConnection(connection);
		}	
		return listOfClientBookings;
	}
	
	/* (non-Javadoc)
	 * @see by.epam.booking.dao.ClientDAO#selectAllTemporaryBookingForms(int, java.lang.String)
	 */
	@Override
	public List<BookingForm> selectAllTemporaryBookingForms(int userId, String status) throws DAOException {
		Connection connection = null;
		PreparedStatement pstBookingForm = null;
		
		List<BookingForm> listOfClientBookings = new ArrayList<BookingForm>();
		
		try {
			connection = ConnectionPool.getInstance().takeConnection();
			pstBookingForm = connection.prepareStatement(TEMPORARY_BOOKING_FORMS_OF_CLIENTSQL_SELECT);
			pstBookingForm.setInt(1, userId);
			pstBookingForm.setString(2, status);
			listOfClientBookings = selectBookingForms(pstBookingForm, connection);
			
		} catch(InterruptedException ex) {
			throw new DAOException("Error in obtaining connection. ", ex);			
		} catch (SQLException ex) {
			throw new DAOException("SQLException in DAO class. ", ex);
		} finally {
			closeStatement(pstBookingForm);
			releaseConnection(connection);
		}	
		return listOfClientBookings;
	}	
	
	/* (non-Javadoc)
	 * @see by.epam.booking.dao.ClientDAO#cancelBookingForm(int[], java.lang.String)
	 */
	@Override
	public boolean cancelBookingForm(int[] bookingId, String status) throws DAOException {
		Connection connection = null;
		PreparedStatement pstCancelBooking = null;
		PreparedStatement pstTableApartmentBusyness = null;
		int countCancelBooking = 0;
		int countTableApartmentBusyness = 0;
		
		LOCK.lock();																												// блокируем все другие дейстивя, которым нужно обратиться к таблице apartment_busyness
		try {
			connection = ConnectionPool.getInstance().takeConnection();
			connection.setAutoCommit(false);
			connection.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
			pstCancelBooking = connection.prepareStatement(BOOKING_FORM_CANCELING_SQL_UPDATE);
			pstTableApartmentBusyness = connection.prepareStatement(APARTMENT_BUSYNESS_TABLE_SQL_DELETE);
			
			for (int id : bookingId) {
				pstCancelBooking.setString(1, status);
				pstCancelBooking.setInt(2, id);
				countCancelBooking = pstCancelBooking.executeUpdate();
				if (countCancelBooking != 1) {
					connection.rollback();
					return false;
				}
			}
			
			for (int id : bookingId) {
				pstTableApartmentBusyness.setInt(1, id);
				countTableApartmentBusyness = pstTableApartmentBusyness.executeUpdate();
				if (countTableApartmentBusyness == 0) {
					connection.rollback();
					return false;
				}
			}
			connection.commit();			
		} catch (InterruptedException ex) {
			throw new DAOException("Error in obtaining connection. ", ex);			
		} catch (SQLException ex) {
			throw new DAOException("SQLException in DAO class.", ex);
		} finally {
			closeStatement(pstCancelBooking);
			closeStatement(pstTableApartmentBusyness);
			releaseConnection(connection);
			LOCK.unlock();
		}	
		return true;
	}
	
	/* (non-Javadoc)
	 * @see by.epam.booking.dao.ClientDAO#selectApartmentTypeById(int)
	 */
	@Override
	public ApartmentType selectApartmentTypeById(int typeId) throws DAOException {
		Connection connection = null;
		PreparedStatement pstApartmentTypeInf = null;
		PreparedStatement pstServicesOfSet = null;
		ResultSet rsApartmentTypeInf = null;
		ResultSet rsServicesOfSet = null;
		ApartmentType apartmentType = null;
		Set<Service> setOfServices = new HashSet<Service>();
		
		try {
			connection = ConnectionPool.getInstance().takeConnection();
			pstApartmentTypeInf = connection.prepareStatement(APARTMENT_TYPE_INFORMATION_SQL_SELECT);
			pstApartmentTypeInf.setInt(1, typeId);
			rsApartmentTypeInf = pstApartmentTypeInf.executeQuery();
			
			if (rsApartmentTypeInf.next()) {
				pstServicesOfSet = connection.prepareStatement(ALL_SERVICES_OF_SPECIFIC_SERVICE_SET_SQL_SELECT);
				pstServicesOfSet.setInt(1, rsApartmentTypeInf.getInt("service_set_id"));
				rsServicesOfSet = pstServicesOfSet.executeQuery();
				while(rsServicesOfSet.next()) {
					Service service = new Service(rsServicesOfSet.getInt("id"), rsServicesOfSet.getString("name"), rsServicesOfSet.getString("description"));
					setOfServices.add(service);
				}
				apartmentType = new ApartmentType(rsApartmentTypeInf.getInt("id"), rsApartmentTypeInf.getString("name"), rsApartmentTypeInf.getString("description"), 
						  rsApartmentTypeInf.getInt("square"), rsApartmentTypeInf.getInt("amount_of_rooms"), rsApartmentTypeInf.getInt("amount_of_guests_max"), 
						  rsApartmentTypeInf.getDouble("price_in_dollars"), rsApartmentTypeInf.getDouble("discount_price_in_dollars"), 
						  rsApartmentTypeInf.getLong("discount_price_date_from"), rsApartmentTypeInf.getLong("discount_price_date_until"), setOfServices);
			}
		} catch(InterruptedException ex) {
			throw new DAOException("Error in obtaining connection. ", ex);			
		} catch (SQLException ex) {
			throw new DAOException("SQLException in DAO class. ", ex);
		} finally {
			closeStatement(pstApartmentTypeInf);
			closeStatement(pstServicesOfSet);
			releaseConnection(connection);
		}	
		return apartmentType;
	}
	
	/* (non-Javadoc)
	 * @see by.epam.booking.dao.ClientDAO#selectAllCardTypes()
	 */
	@Override
	public Set<CardType> selectAllCardTypes() throws DAOException {
		Connection connection = null;
		Statement st = null;
		ResultSet rs = null;
		Set<CardType> setOfCardTypes = new HashSet<CardType>();
		
		try {
			connection = ConnectionPool.getInstance().takeConnection();
			st = connection.createStatement();
			rs = st.executeQuery(CARD_TYPES_INFORMATION_SQL_SELECT);
			
			while (rs.next()) {
				setOfCardTypes.add(new CardType(rs.getInt("id"), rs.getString("name"), rs.getString("description")));
			}
		} catch(InterruptedException ex) {
			throw new DAOException("Error in obtaining connection. ", ex);			
		} catch (SQLException ex) {
			throw new DAOException("SQLException in DAO class. ", ex);
		} finally {
			closeStatement(st);
			releaseConnection(connection);
		}
		return setOfCardTypes;
	}
	
	/* (non-Javadoc)
	 * @see by.epam.booking.dao.ClientDAO#selectAllBedTypes()
	 */
	@Override
	public Set<BedType> selectAllBedTypes() throws DAOException {
		Connection connection = null;
		Statement st = null;
		ResultSet rs = null;
		Set<BedType> setOfCardTypes = new HashSet<BedType>();
		
		try {
			connection = ConnectionPool.getInstance().takeConnection();
			st = connection.createStatement();
			rs = st.executeQuery(BED_TYPES_INFORMATION_SQL_SELECT);
			
			while (rs.next()) {
				setOfCardTypes.add(new BedType(rs.getInt("id"), rs.getString("name"), rs.getString("description"), rs.getInt("amount_of_persons")));
			}
		} catch(InterruptedException ex) {
			throw new DAOException("Error in obtaining connection. ", ex);			
		} catch (SQLException ex) {
			throw new DAOException("SQLException in DAO class. ", ex);
		} finally {
			closeStatement(st);
			releaseConnection(connection);
		}
		return setOfCardTypes;
	}
	
	/**
	 * Check.
	 *
	 * @param dateFrom the date from
	 * @param dateUntil the date until
	 * @param connection the connection
	 * @return the map
	 * @throws DAOException the DAO exception
	 * @throws SQLException the SQL exception
	 */
	private Map<ApartmentType, List<Integer>> check(long dateFrom, long dateUntil, Connection connection) throws DAOException, SQLException {		// метод поиска доступных номеров и типов
		Statement stApartments = null;
		PreparedStatement pstIntersectBookig = null;
		ResultSet rsIntersectBookig = null;
		ResultSet rsApartments = null;
		Set<Integer> setOfBusyApartmentId = new HashSet<Integer>();
		Set<Integer> setOfBusyApartmentNumber = new HashSet<Integer>();
		Set<Integer> setOfAvailableApartmentTypeId = new HashSet<Integer>();
		ArrayList<Integer> listOfAvailableApartmentIdOfType = new ArrayList<Integer>();
		Map<Integer, Integer> mapOfAvailableApartmentIdAndType = new HashMap<Integer, Integer>();
		Map<ApartmentType, List<Integer>> resultingMap = new HashMap<ApartmentType, List<Integer>>();
		
		try {
			pstIntersectBookig = connection.prepareStatement(INTERSECTING_BOOKING_FORMS_SQL_CHECK);
			pstIntersectBookig.setLong(1, dateFrom);
			pstIntersectBookig.setLong(2, dateUntil);
			pstIntersectBookig.setLong(3, dateUntil);
			pstIntersectBookig.setLong(4, dateFrom);
			rsIntersectBookig = pstIntersectBookig.executeQuery();												// получаем все заявки, которые пересекаются с датами, введёнными пользователем
			while (rsIntersectBookig.next()) {
				setOfBusyApartmentId.add(new Integer(rsIntersectBookig.getInt("id")));							// сохраняем id всех номеров, которые точно НЕ доступны для бронирования в указанный клиентом срок
				setOfBusyApartmentNumber.add(new Integer(rsIntersectBookig.getInt("number")));					// сохраняем № всех номеров, которые точно НЕ доступны для бронирования в указанный клиентом срок
			}
			LOG.debug("Set of busy apartments: " + setOfBusyApartmentNumber);
			
			stApartments = connection.createStatement();
			rsApartments = stApartments.executeQuery(ALL_APARTMENTS_SQL_SELECT);								// получаем все номера, которые есть в отеле
			while (rsApartments.next()) {
				if (!setOfBusyApartmentId.contains(rsApartments.getInt("id"))) {
					mapOfAvailableApartmentIdAndType.put(rsApartments.getInt("id"), rsApartments.getInt("apartment_type_id"));		// сохраняем все номера (с их типами), которые точно ДОСТУПНЫ для бронирования в указанный клиентом срок
				}
			}
			setOfAvailableApartmentTypeId.addAll(mapOfAvailableApartmentIdAndType.values());					// добавляем все типы доступных номеров в множество (без дубликатов ибо HashSet!) 
			LOG.debug("Set of available apartment type ids: " + setOfAvailableApartmentTypeId);
			
			for (Integer apartmentTypeId : setOfAvailableApartmentTypeId) {										// берём id каждого из типов номеров и работаем с ним
				LOG.debug("Current apartmentTypeId: " + apartmentTypeId);
				ApartmentType apartmentType = selectApartmentTypeById(apartmentTypeId);
				
				LOG.debug("Current apartment type: " + apartmentType);
				for (Entry<Integer, Integer> entry : mapOfAvailableApartmentIdAndType.entrySet()) {				// наполняем список номеров с текущим типом
					if (entry.getValue() ==  apartmentTypeId) {
						listOfAvailableApartmentIdOfType.add(entry.getKey());
					}
				}
				LOG.debug("List of available apartments of type: " + listOfAvailableApartmentIdOfType);
				resultingMap.put(apartmentType, new ArrayList<Integer>(listOfAvailableApartmentIdOfType));		// вставляем в результирующую таблицу пару "доступный тип номера - список доступных номеров этого типа"
				LOG.debug("resulting map: " + resultingMap);													// причём вставлять нужно новый объект для списка номеров, иначе это будет всегда ссылка на последний список,а потом и вовсе null 
				
				listOfAvailableApartmentIdOfType.clear();														// очищаем список номеров для следующего наполнения				
			}
		} finally {
			closeStatement(pstIntersectBookig);
			closeStatement(stApartments);
		}
		
		return resultingMap;
	}
	
	/**
	 * Select booking forms.
	 *
	 * @param pstBookingForm the pst booking form
	 * @param connection the connection
	 * @return the list
	 * @throws SQLException the SQL exception
	 * @throws DAOException the DAO exception
	 */
	private List<BookingForm> selectBookingForms(PreparedStatement pstBookingForm, Connection connection) throws SQLException, DAOException {
		PreparedStatement pstBedType = null;
		PreparedStatement pstCardType = null;
		ResultSet rsBookingForm = null;
		ResultSet rsBedType = null;
		ResultSet rsCardType = null;
		ApartmentType apartmentType = null;
		BedType bedType = null;
		CardType cardType = null;
		List<BookingForm> listOfClientBookings = new ArrayList<BookingForm>();
		
		rsBookingForm = pstBookingForm.executeQuery();
		
		try {
			while (rsBookingForm.next()) {																					// для каждой найденной заявки получаем данные по типу номера
				apartmentType = selectApartmentTypeById(rsBookingForm.getInt("apartment_type_id"));
				LOG.debug(rsBookingForm.getInt("id"));
								
				pstBedType = connection.prepareStatement(BED_TYPE_INFORMATION_SQL_SELECT);									// для каждой найденной заявки получаем данные по типу кроватей
				pstBedType.setInt(1, rsBookingForm.getInt("bed_type_id"));
				rsBedType = pstBedType.executeQuery();
				if (rsBedType.next()) {
					bedType = new BedType(rsBedType.getInt("id"), rsBedType.getString("name"), rsBedType.getString("description"), rsBedType.getInt("amount_of_persons")); 
				} else {
					throw new SQLException("Bed type with required id doesn't exist");
				}
				
				pstCardType = connection.prepareStatement(CARD_TYPE_INFORMATION_SQL_SELECT);								// для каждой найденной заявки получаем данные по типу карты оплаты
				pstCardType.setInt(1, rsBookingForm.getInt("card_type_id"));
				rsCardType = pstCardType.executeQuery();
				if (rsCardType.next()) {
					cardType = new CardType(rsCardType.getInt("id"), rsCardType.getString("name"), rsCardType.getString("description")); 
				} else {
					throw new SQLException("Card type with required id doesn't exist");
				}
				
				listOfClientBookings.add(new BookingForm(rsBookingForm.getInt("id"), rsBookingForm.getInt("user_id"), rsBookingForm.getLong("arrival_date"), 
						rsBookingForm.getLong("leaving_date"), apartmentType, rsBookingForm.getInt("amount_of_guests"), rsBookingForm.getInt("amount_of_apartments"), 
						rsBookingForm.getDouble("total_price"), bedType, rsBookingForm.getString("personal_preferences"), cardType, rsBookingForm.getString("card_number"), 
						rsBookingForm.getLong("card_expire_date"), rsBookingForm.getBoolean("booking_aproval_sms"), rsBookingForm.getLong("booking_creation_date"), 
						rsBookingForm.getString("status"), rsBookingForm.getBoolean("forcibly_closed_flag")));
			}
		} finally {
			closeStatement(pstBedType);
			closeStatement(pstCardType);
		}
		return listOfClientBookings;
	}
	
	/**
	 * Close statement.
	 *
	 * @param st the st
	 */
	private void closeStatement(Statement st) {
		try {
			if (st != null) {
				st.close();
			}
		} catch (SQLException ex) {
			LOG.error("Error in closing statement!");
		}
	}
	
	/**
	 * Release connection.
	 *
	 * @param connection the connection
	 */
	private void releaseConnection(Connection connection) {
		try {
			ConnectionPool.getInstance().releaseConnection(connection);
		} catch (SQLException ex) {
			LOG.error("Error in releasing of connection!");
		}
	}
}
