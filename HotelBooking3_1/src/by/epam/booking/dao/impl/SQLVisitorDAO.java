package by.epam.booking.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import by.epam.booking.dao.DAOException;
import by.epam.booking.dao.VisitorDAO;
import by.epam.booking.pool.ConnectionPool;

/**
 * The Class SQLVisitorDAO.
 */
public class SQLVisitorDAO implements VisitorDAO  {
	
	/** The Constant LOG. */
	private static final Logger LOG = Logger.getLogger(SQLVisitorDAO.class);
	
	/** The Constant INSTANCE. */
	private static final SQLVisitorDAO INSTANCE = new SQLVisitorDAO();
	
	/** The Constant INFORMATION_RESOURCE_LANGUAGE_SPECIFIC_SQL_SELECT. */
	private static final String INFORMATION_RESOURCE_LANGUAGE_SPECIFIC_SQL_SELECT = "SELECT text "
																				  + "FROM hotel.INFORMATION_RESOURCE "
																				  + "WHERE name=? AND language=?;";
	
	/** The Constant INFORMATION_RESOURCE_MULTILANGUAGE_SQL_SELECT. */
	private static final String INFORMATION_RESOURCE_MULTILANGUAGE_SQL_SELECT = "SELECT text, language "
			  																  + "FROM hotel.INFORMATION_RESOURCE "
			  																  + "WHERE name=?;";
	
	/** The Constant CURRENCY_NAME_SQL_SELECT. */
	private static final String CURRENCY_NAME_SQL_SELECT = "SELECT currency_name "
														 + "FROM hotel.CURRENCY "
														 + "WHERE country=?;";
	
	/** The Constant DOLLAR_COEFFICIENT_SQL_SELECT. */
	private static final String DOLLAR_COEFFICIENT_SQL_SELECT = "SELECT dollar_coefficient "
															  + "FROM hotel.CURRENCY "
															  + "WHERE country=?;";
	
	/** The Constant APARTMENT_PRICES_SQL_SELECT. */
	private static final String APARTMENT_PRICES_SQL_SELECT = "SELECT price_in_dollars "
			  												+ "FROM hotel.APARTMENT_TYPE;";
	
	/**
	 * Instantiates a new SQL visitor dao.
	 */
	protected SQLVisitorDAO() {}
	
	/**
	 * Gets the single instance of SQLVisitorDAO.
	 *
	 * @return single instance of SQLVisitorDAO
	 */
	public static SQLVisitorDAO getInstance() {
		return INSTANCE;
	}
	
	/* (non-Javadoc)
	 * @see by.epam.booking.dao.VisitorDAO#selectResource(java.lang.String, java.lang.String)
	 */
	@Override
	public String selectResource(String resourceName, String language) throws DAOException {
		Connection connection = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		String resource = null;
				
		try {
			connection = ConnectionPool.getInstance().takeConnection();
			pst = connection.prepareStatement(INFORMATION_RESOURCE_LANGUAGE_SPECIFIC_SQL_SELECT);
			pst.setString(1, resourceName);
			pst.setString(2, language);
			rs = pst.executeQuery();
			
			if (rs.next()) {
				resource = rs.getString("text");
			}
			
		} catch (InterruptedException ex) {
			throw new DAOException("Error in obtaining connection. ", ex);
		} catch (SQLException ex) {
			throw new DAOException("SQLException in DAO class. ", ex);
		} finally {
			closeStatement(pst);
			releaseConnection(connection);
		}
		return resource;
	}
	
	/* (non-Javadoc)
	 * @see by.epam.booking.dao.VisitorDAO#selectResource(java.lang.String)
	 */
	@Override
	public Map<String, String> selectResource(String resourceName) throws DAOException {
		Connection connection = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		Map<String, String> resource = new HashMap<String, String>();
		
		try {
			connection = ConnectionPool.getInstance().takeConnection();
			pst = connection.prepareStatement(INFORMATION_RESOURCE_MULTILANGUAGE_SQL_SELECT);
			pst.setString(1, resourceName);
			rs = pst.executeQuery();
			
			while (rs.next()) {
				resource.put(rs.getString("language"), rs.getString("text"));
			}
			
		} catch (InterruptedException ex) {
			throw new DAOException("Error in obtaining connection. ", ex);
		} catch (SQLException ex) {
			throw new DAOException("SQLException in DAO class. ", ex);
		} finally {
			closeStatement(pst);
			releaseConnection(connection);
		}		
		return resource;
	}

	/* (non-Javadoc)
	 * @see by.epam.booking.dao.VisitorDAO#selectCurrencyName(java.lang.String)
	 */
	@Override
	public String selectCurrencyName(String countryName) throws DAOException {
		Connection connection = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		String currencyName = null;
				
		try {
			connection = ConnectionPool.getInstance().takeConnection();
			pst = connection.prepareStatement(CURRENCY_NAME_SQL_SELECT);
			pst.setString(1, countryName);
			rs = pst.executeQuery();
			
			if (rs.next()) {
				currencyName = rs.getString("currency_name");
			}
			
		} catch (InterruptedException ex) {
			throw new DAOException("Error in obtaining connection. ", ex);
		} catch (SQLException ex) {
			throw new DAOException("SQLException in DAO class. ", ex);
		} finally {
			closeStatement(pst);
			releaseConnection(connection);
		}
		return currencyName;
	}

	/* (non-Javadoc)
	 * @see by.epam.booking.dao.VisitorDAO#selectDollarCoefficient(java.lang.String)
	 */
	@Override
	public float selectDollarCoefficient(String countryName) throws DAOException {
		Connection connection = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		float dollarCoefficient = 0.0F;
				
		try {
			connection = ConnectionPool.getInstance().takeConnection();
			pst = connection.prepareStatement(DOLLAR_COEFFICIENT_SQL_SELECT);
			pst.setString(1, countryName);
			rs = pst.executeQuery();
			
			if (rs.next()) {
				dollarCoefficient = rs.getFloat("dollar_coefficient");
			}
			
		} catch (InterruptedException ex) {
			throw new DAOException("Error in obtaining connection. ", ex);
		} catch (SQLException ex) {
			throw new DAOException("SQLException in DAO class. ", ex);
		} finally {
			closeStatement(pst);
			releaseConnection(connection);
		}
		return dollarCoefficient;
	}
	
	/* (non-Javadoc)
	 * @see by.epam.booking.dao.VisitorDAO#selectApartmentPrices()
	 */
	@Override
	public List<Double> selectApartmentPrices() throws DAOException {
		Connection connection = null;
		Statement st = null;
		ResultSet rs = null;
		List<Double> prices = new ArrayList<Double>();
		
		try {
			connection = ConnectionPool.getInstance().takeConnection();
			st = connection.createStatement();
			rs = st.executeQuery(APARTMENT_PRICES_SQL_SELECT);
			
			while (rs.next()) {
				prices.add(rs.getDouble("price_in_dollars"));
			}
			
		} catch (InterruptedException ex) {
			throw new DAOException("Error in obtaining connection. ", ex);
		} catch (SQLException ex) {
			throw new DAOException("SQLException in DAO class. ", ex);
		} finally {
			closeStatement(st);
			releaseConnection(connection);
		}		
		return prices;
	}
	
	/**
	 * Close statement.
	 *
	 * @param st the st
	 */
	private void closeStatement(Statement st) {
		try {
			if (st != null) {
				st.close();
			}
		} catch (SQLException ex) {
			LOG.error("Error in closing prepared statement!");
		}
	}
	
	/**
	 * Release connection.
	 *
	 * @param connection the connection
	 */
	private void releaseConnection(Connection connection) {
		try {
			ConnectionPool.getInstance().releaseConnection(connection);
		} catch (SQLException ex) {
			LOG.error("Error in releasing of connection!");
		}
	}	
}
