package by.epam.booking.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;
import java.util.Set;

import org.apache.log4j.Logger;

import by.epam.booking.dao.DAOException;
import by.epam.booking.dao.UserDAO;
import by.epam.booking.domain.User;
import by.epam.booking.domain.UserRole;
import by.epam.booking.pool.ConnectionPool;

/**
 * The Class SQLUserDAO.
 */
public class SQLUserDAO extends SQLVisitorDAO implements UserDAO {
	
	/** The Constant LOG. */
	private final static Logger LOG = Logger.getLogger(SQLUserDAO.class);
	
	/** The Constant INSTANCE. */
	private static final SQLUserDAO INSTANCE = new SQLUserDAO();
	
	/** The Constant LOGIN_SQL_CHECK. */
	private static final String LOGIN_SQL_CHECK = "SELECT login "
												+ "FROM hotel.USER "
												+ "WHERE login=? AND status=?;";
	
	/** The Constant LOGIN_PASSWORD_SQL_CHECK. */
	private static final String LOGIN_PASSWORD_SQL_CHECK = "SELECT login "
														 + "FROM hotel.USER "
														 + "WHERE login=? AND password=? AND status=?;";
		
	/** The Constant USER_ALL_INFORMATION_SQL_SELECT. */
	private static final String USER_ALL_INFORMATION_SQL_SELECT = "SELECT USER.id, USER.login, USER.password, USER.user_role_id, "
															 	+ "USER.user_info_id, USER.key_question, USER.key_answer, USER.email, user.status, USER_INFO.name, USER_INFO.surname, "
															 	+ "USER_INFO.patronymic, USER_INFO.country, USER_INFO.address, USER_INFO.telephone, USER_INFO.mobile "
															 	+ "FROM hotel.USER "
															 	+ "INNER JOIN hotel.USER_INFO "
															 	+ "ON USER.user_info_id = USER_INFO.id "
															 	+ "WHERE login=? AND password=?;";
	
	/** The Constant ROLE_NAME_SQL_SELECT. */
	private static final String ROLE_NAME_SQL_SELECT = "SELECT name "
													 + "FROM hotel.USER_ROLE "
													 + "WHERE id=?";
	
	/** The Constant ROLE_INFORMATION_SELECT_SQL. */
	private static final String ROLE_INFORMATION_SELECT_SQL = "SELECT id, name, description, rights "
															+ "FROM hotel.USER_ROLE "
															+ "WHERE name=?";
	
	/** The Constant USER_ID_INFO_SQL_SELECT. */
	private static final String USER_ID_INFO_SQL_SELECT = "SELECT id "
														+ "FROM hotel.USER_INFO "
														+ "WHERE name=? AND surname=? AND patronymic=? AND country=? AND address=? AND telephone=? AND mobile=?;";
	
	/** The Constant USER_GENERAL_INFORMATION_SQL_INSERT. */
	private static final String USER_GENERAL_INFORMATION_SQL_INSERT = "INSERT INTO hotel.USER_INFO (name, surname, patronymic, country, address, telephone, mobile) "
																 	+ "VALUES(?,?,?,?,?,?,?);";
	
	/** The Constant USER_TECHNICAL_INFORMATION_SQL_INSERT. */
	private static final String USER_TECHNICAL_INFORMATION_SQL_INSERT = "INSERT INTO hotel.USER (login, password, user_role_id, user_info_id, key_question, key_answer, email, status) "
																	  + "VALUES(?,?,?,?,?,?,?,?);";
	
	/** The Constant USER_ACTIVE_STATUS. */
	private static final String USER_ACTIVE_STATUS = "active";
		
	/**
	 * Instantiates a new SQL user dao.
	 */
	protected SQLUserDAO() {}
	
	/**
	 * Gets the single instance of SQLUserDAO.
	 *
	 * @return single instance of SQLUserDAO
	 */
	public static SQLUserDAO getInstance() {
		return INSTANCE;
	}
	
	/* (non-Javadoc)
	 * @see by.epam.booking.dao.UserDAO#checkUser(java.lang.String)
	 */
	@Override
	public boolean checkUser(String login) throws DAOException {
		Connection connection = null;
		PreparedStatement st = null;
		ResultSet rs = null;
		
		try {
			connection = ConnectionPool.getInstance().takeConnection();
			st = connection.prepareStatement(LOGIN_SQL_CHECK);
			st.setString(1, login);
			st.setString(2, USER_ACTIVE_STATUS);
			
			rs = st.executeQuery();
			if (rs.next()) {
				return true;
			}
			
		} catch (InterruptedException ex) {
			throw new DAOException("Error in obtaining connection. ", ex);			
		} catch (SQLException ex) {
			throw new DAOException("SQLException in DAO class. ", ex);
		} finally {
			closeStatement(st);
			releaseConnection(connection);
		}
		return false;
	}
	
	/* (non-Javadoc)
	 * @see by.epam.booking.dao.UserDAO#checkUser(java.lang.String, java.lang.String)
	 */
	@Override
	public boolean checkUser(String login, String password) throws DAOException {
		Connection connection = null;
		PreparedStatement st = null;
		ResultSet rs = null;
		
		try {
			connection = ConnectionPool.getInstance().takeConnection();
			st = connection.prepareStatement(LOGIN_PASSWORD_SQL_CHECK);
			st.setString(1, login);
			st.setString(2, password);
			st.setString(3, USER_ACTIVE_STATUS);
			
			rs = st.executeQuery();
			if (rs.next()) {
				return true;
			}
			
		} catch (InterruptedException ex) {
			throw new DAOException("Error in obtaining connection. ", ex);			
		} catch (SQLException ex) {
			throw new DAOException("SQLException in DAO class. ", ex);
		} finally {
			closeStatement(st);
			releaseConnection(connection);
		}
		return false;
	}
	
	/* (non-Javadoc)
	 * @see by.epam.booking.dao.UserDAO#obtainUser(java.lang.String, java.lang.String)
	 */
	@Override
	public User obtainUser(String login, String password) throws DAOException {
		Connection connection = null;
		PreparedStatement stUserInf = null; 
		PreparedStatement stRoleName = null;
		ResultSet rsUserInf = null;
		ResultSet rsRoleName = null;
		try {
			connection = ConnectionPool.getInstance().takeConnection();
			stUserInf = connection.prepareStatement(USER_ALL_INFORMATION_SQL_SELECT);
			stUserInf.setString(1, login);
			stUserInf.setString(2, password);
			rsUserInf = stUserInf.executeQuery();
			
			if (rsUserInf.next()) {
				stRoleName = connection.prepareStatement(ROLE_NAME_SQL_SELECT);
				stRoleName.setInt(1, rsUserInf.getInt("user_role_id"));
				rsRoleName = stRoleName.executeQuery();
				if (rsRoleName.next()) {					
					User user = new User(rsUserInf.getInt("id"), rsUserInf.getString("login"), rsUserInf.getString("password"), rsUserInf.getInt("user_role_id"), 
							rsRoleName.getString("name"), rsUserInf.getString("status"), rsUserInf.getString("key_question"), rsUserInf.getString("key_answer"), rsUserInf.getString("email"), 
							rsUserInf.getString("name"), rsUserInf.getString("surname"), rsUserInf.getString("patronymic"), rsUserInf.getString("country"), rsUserInf.getString("address"), 
							rsUserInf.getString("telephone"), rsUserInf.getString("mobile"));
					return user;
				}				
			}
			
		} catch (InterruptedException ex) {
			throw new DAOException("Error in obtaining connection. ", ex);			
		} catch (SQLException ex) {
			throw new DAOException("SQLException in DAO class. ", ex);
		} finally {
			closeStatement(stUserInf);
			closeStatement(stRoleName);
			releaseConnection(connection);
		}
		return null;
	}

	/* (non-Javadoc)
	 * @see by.epam.booking.dao.UserDAO#obtainRoleInformation(java.lang.String)
	 */
	@Override
	public UserRole obtainRoleInformation(String name) throws DAOException {
		Connection connection = null;
		PreparedStatement st = null;
		ResultSet rs = null;
		UserRole userRole = null;
		
		try {
			connection = ConnectionPool.getInstance().takeConnection();
			st = connection.prepareStatement(ROLE_INFORMATION_SELECT_SQL);
			st.setString(1, name);
			
			rs = st.executeQuery();
			if (rs.next()) {
				Set<String> rightsSet = new HashSet<String>();
				String[] rightsArray = rs.getObject("rights").toString().split(",");
				for (String right : rightsArray) {
					rightsSet.add(right);
				}
				userRole = new UserRole(rs.getInt("id"), rs.getString("name"), rs.getString("description"), rightsSet);
			}
		} catch (InterruptedException ex) {
			throw new DAOException("Error in obtaining connection. ", ex);			
		} catch (SQLException ex) {
			throw new DAOException("SQLException in DAO class.", ex);
		} finally {
			closeStatement(st);
			releaseConnection(connection);
		}
		return userRole;
	}

	/* (non-Javadoc)
	 * @see by.epam.booking.dao.UserDAO#addUser(by.epam.booking.domain.User)
	 */
	@Override
	public boolean addUser(User user) throws DAOException {
		Connection connection = null;
		PreparedStatement stUserGenInf = null;
		PreparedStatement stUserIdInfo = null;
		PreparedStatement stUserTechInf = null;
		ResultSet rs = null;
		int result = 0;
		int id = 0;
		
		try {
			connection = ConnectionPool.getInstance().takeConnection();
			connection.setAutoCommit(false);
			connection.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
			
			stUserGenInf = connection.prepareStatement(USER_GENERAL_INFORMATION_SQL_INSERT);
			stUserGenInf.setString(1, user.getName());
			stUserGenInf.setString(2, user.getSurname());
			stUserGenInf.setString(3, user.getPatronymic());
			stUserGenInf.setString(4, user.getCountry());
			stUserGenInf.setString(5, user.getAddress());
			stUserGenInf.setString(6, user.getTelephone());
			stUserGenInf.setString(7, user.getMobile());
			result = stUserGenInf.executeUpdate();												// выполняем запрос на добавление общей информации о пользователе
			if (result == 1) {																	// если добавилась, то сразу же выполняем запрос на выборку id вставленной записи
				stUserIdInfo = connection.prepareStatement(USER_ID_INFO_SQL_SELECT);
				stUserIdInfo.setString(1, user.getName());
				stUserIdInfo.setString(2, user.getSurname());
				stUserIdInfo.setString(3, user.getPatronymic());
				stUserIdInfo.setString(4, user.getCountry());
				stUserIdInfo.setString(5, user.getAddress());
				stUserIdInfo.setString(6, user.getTelephone());
				stUserIdInfo.setString(7, user.getMobile());
				rs = stUserIdInfo.executeQuery();
				if (rs.next()) {
					id = rs.getInt("id");
				} else {
					LOG.error("User info hasn't been found");
				}
			} else {																			// если не добавилась, возвращаем откатываем изменения
				connection.rollback();															// и возвращаем общий провал всей операции
				return false;
			}
			
			stUserTechInf = connection.prepareStatement(USER_TECHNICAL_INFORMATION_SQL_INSERT);	// выполняем следующий запрос на добавление технической информации о пользователе
			stUserTechInf.setString(1, user.getLogin());
			stUserTechInf.setString(2, user.getPassword());
			stUserTechInf.setInt(3, user.getRoleID());
			stUserTechInf.setInt(4, id);
			stUserTechInf.setString(5, user.getKeyQuestion());
			stUserTechInf.setString(6, user.getKeyAnswer());
			stUserTechInf.setString(7, user.getEmail());
			stUserTechInf.setString(8, user.getStatus());
			result = stUserTechInf.executeUpdate();
			if (result == 1) {																	// если добавилась, то коммитим всю транзакцию 
				connection.commit();															// и возвращаем общий успех всей операции по добавлению пользователя
				return true;
			} else {
				connection.rollback();
				return false; 																	// иначе, возвращаем общий провал всей операции
			}
		} catch (InterruptedException ex) {
			throw new DAOException("Error in obtaining connection. ", ex);			
		} catch (SQLException ex) {
			throw new DAOException("SQLException in DAO class.", ex);
		} finally {
			closeStatement(stUserGenInf);
			closeStatement(stUserIdInfo);
			closeStatement(stUserTechInf);			
			releaseConnection(connection);
		}
	}
	
	/**
	 * Close statement.
	 *
	 * @param st the st
	 */
	private void closeStatement(Statement st) {
		try {
			if (st != null) {
				st.close();
			}
		} catch (SQLException ex) {
			LOG.error("Error in closing prepared statement!");
		}
	}
	
	/**
	 * Release connection.
	 *
	 * @param connection the connection
	 */
	private void releaseConnection(Connection connection) {
		try {
			ConnectionPool.getInstance().releaseConnection(connection);
		} catch (SQLException ex) {
			LOG.error("Error in releasing of connection!");
		}
	}
}
