package by.epam.booking.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.log4j.Logger;

import by.epam.booking.dao.AdminDAO;
import by.epam.booking.dao.DAOException;
import by.epam.booking.domain.ApartmentType;
import by.epam.booking.domain.BedType;
import by.epam.booking.domain.Bill;
import by.epam.booking.domain.BookingForm;
import by.epam.booking.domain.CardType;
import by.epam.booking.domain.Service;
import by.epam.booking.domain.User;
import by.epam.booking.pool.ConnectionPool;

/**
 * The Class SQLAdminDAO.
 */
public class SQLAdminDAO extends SQLUserDAO implements AdminDAO {
	
	/** The Constant LOG. */
	private final static Logger LOG = Logger.getLogger(SQLAdminDAO.class);
	
	/** The Constant INSTANCE. */
	private static final SQLAdminDAO INSTANCE = new SQLAdminDAO();
	
	/** The Constant USER_DEFAULT_ROLE. */
	private static final String USER_DEFAULT_ROLE = "client";
	
	/** The Constant MYSQL_PREPARED_STATEMENT_CLASS_NAME. */
	private static final String MYSQL_PREPARED_STATEMENT_CLASS_NAME = "class com.mysql.jdbc.PreparedStatement";
	
	/** The Constant NAME_BOOKING_SQL_SELECT. */
	private static final String NAME_BOOKING_SQL_SELECT = "SELECT BOOKING_FORM.id, BOOKING_FORM.user_id, BOOKING_FORM.arrival_date, BOOKING_FORM.leaving_date, BOOKING_FORM.apartment_type_id, "
			 											+ "BOOKING_FORM.amount_of_guests, BOOKING_FORM.amount_of_apartments, BOOKING_FORM.total_price, BOOKING_FORM.bed_type_id, "
			 											+ "BOOKING_FORM.personal_preferences, BOOKING_FORM.card_type_id, BOOKING_FORM.card_number, BOOKING_FORM.card_expire_date, "
			 											+ "BOOKING_FORM.booking_aproval_sms, BOOKING_FORM.booking_creation_date, BOOKING_FORM.status, BOOKING_FORM.forcibly_closed_flag "
			 											+ "FROM hotel.BOOKING_FORM "
			 											+ "INNER JOIN (hotel.USER INNER JOIN hotel.USER_INFO "
			 											+ "ON USER.user_info_id = USER_INFO.id) "
			 											+ "ON BOOKING_FORM.user_id = USER.id "
			 											+ "WHERE USER_INFO.name = ?;";
	
	/** The Constant SURNAME_BOOKING_SQL_SELECT. */
	private static final String SURNAME_BOOKING_SQL_SELECT = "SELECT BOOKING_FORM.id, BOOKING_FORM.user_id, BOOKING_FORM.arrival_date, BOOKING_FORM.leaving_date, BOOKING_FORM.apartment_type_id, "
														   + "BOOKING_FORM.amount_of_guests, BOOKING_FORM.amount_of_apartments, BOOKING_FORM.total_price, BOOKING_FORM.bed_type_id, "
														   + "BOOKING_FORM.personal_preferences, BOOKING_FORM.card_type_id, BOOKING_FORM.card_number, BOOKING_FORM.card_expire_date, "
														   + "BOOKING_FORM.booking_aproval_sms, BOOKING_FORM.booking_creation_date, BOOKING_FORM.status, BOOKING_FORM.forcibly_closed_flag "
														   + "FROM hotel.BOOKING_FORM "
														   + "INNER JOIN (hotel.USER INNER JOIN hotel.USER_INFO "
														   + "ON USER.user_info_id = USER_INFO.id) "
														   + "ON BOOKING_FORM.user_id = USER.id "
														   + "WHERE USER_INFO.surname = ?;";
	
	/** The Constant EMAIL_BOOKING_SQL_SELECT. */
	private static final String EMAIL_BOOKING_SQL_SELECT = "SELECT BOOKING_FORM.id, BOOKING_FORM.user_id, BOOKING_FORM.arrival_date, BOOKING_FORM.leaving_date, BOOKING_FORM.apartment_type_id, "
			 											 + "BOOKING_FORM.amount_of_guests, BOOKING_FORM.amount_of_apartments, BOOKING_FORM.total_price, BOOKING_FORM.bed_type_id, "
			 											 + "BOOKING_FORM.personal_preferences, BOOKING_FORM.card_type_id, BOOKING_FORM.card_number, BOOKING_FORM.card_expire_date, "
			 											 + "BOOKING_FORM.booking_aproval_sms, BOOKING_FORM.booking_creation_date, BOOKING_FORM.status, BOOKING_FORM.forcibly_closed_flag "
			 											 + "FROM hotel.BOOKING_FORM "
			 											 + "INNER JOIN (hotel.USER INNER JOIN hotel.USER_INFO "
			 											 + "ON USER.user_info_id = USER_INFO.id) "
			 											 + "ON BOOKING_FORM.user_id = USER.id "
			 											 + "WHERE USER.email = ?;";
	
	/** The Constant MOBILE_BOOKING_SQL_SELECT. */
	private static final String MOBILE_BOOKING_SQL_SELECT = "SELECT BOOKING_FORM.id, BOOKING_FORM.user_id, BOOKING_FORM.arrival_date, BOOKING_FORM.leaving_date, BOOKING_FORM.apartment_type_id, "
			 											  + "BOOKING_FORM.amount_of_guests, BOOKING_FORM.amount_of_apartments, BOOKING_FORM.total_price, BOOKING_FORM.bed_type_id, "
			 											  + "BOOKING_FORM.personal_preferences, BOOKING_FORM.card_type_id, BOOKING_FORM.card_number, BOOKING_FORM.card_expire_date, "
			 											  + "BOOKING_FORM.booking_aproval_sms, BOOKING_FORM.booking_creation_date, BOOKING_FORM.status, BOOKING_FORM.forcibly_closed_flag "
			 											  + "FROM hotel.BOOKING_FORM "
			 											  + "INNER JOIN (hotel.USER INNER JOIN hotel.USER_INFO "
			 											  + "ON USER.user_info_id = USER_INFO.id) "
			 											  + "ON BOOKING_FORM.user_id = USER.id "
			 											  + "WHERE USER_INFO.mobile = ?;";
	
	/** The Constant NAME_SURNAME_BOOKING_SQL_SELECT. */
	private static final String NAME_SURNAME_BOOKING_SQL_SELECT = "SELECT BOOKING_FORM.id, BOOKING_FORM.user_id, BOOKING_FORM.arrival_date, BOOKING_FORM.leaving_date, BOOKING_FORM.apartment_type_id, "
			 													+ "BOOKING_FORM.amount_of_guests, BOOKING_FORM.amount_of_apartments, BOOKING_FORM.total_price, BOOKING_FORM.bed_type_id, "
			 													+ "BOOKING_FORM.personal_preferences, BOOKING_FORM.card_type_id, BOOKING_FORM.card_number, BOOKING_FORM.card_expire_date, "
			 													+ "BOOKING_FORM.booking_aproval_sms, BOOKING_FORM.booking_creation_date, BOOKING_FORM.status, BOOKING_FORM.forcibly_closed_flag "
			 													+ "FROM hotel.BOOKING_FORM "
			 													+ "INNER JOIN (hotel.USER INNER JOIN hotel.USER_INFO "
			 													+ "ON USER.user_info_id = USER_INFO.id) "
			 													+ "ON BOOKING_FORM.user_id = USER.id "
			 													+ "WHERE USER_INFO.name = ? AND USER_INFO.surname = ?;";
	
	/** The Constant NAME_EMAIL_BOOKING_SQL_SELECT. */
	private static final String NAME_EMAIL_BOOKING_SQL_SELECT = "SELECT BOOKING_FORM.id, BOOKING_FORM.user_id, BOOKING_FORM.arrival_date, BOOKING_FORM.leaving_date, BOOKING_FORM.apartment_type_id, "
			 												  + "BOOKING_FORM.amount_of_guests, BOOKING_FORM.amount_of_apartments, BOOKING_FORM.total_price, BOOKING_FORM.bed_type_id, "
			 												  + "BOOKING_FORM.personal_preferences, BOOKING_FORM.card_type_id, BOOKING_FORM.card_number, BOOKING_FORM.card_expire_date, "
			 												  + "BOOKING_FORM.booking_aproval_sms, BOOKING_FORM.booking_creation_date, BOOKING_FORM.status, BOOKING_FORM.forcibly_closed_flag "
			 												  + "FROM hotel.BOOKING_FORM "
			 												  + "INNER JOIN (hotel.USER INNER JOIN hotel.USER_INFO "
			 												  + "ON USER.user_info_id = USER_INFO.id) "
			 												  + "ON BOOKING_FORM.user_id = USER.id "
			 												  + "WHERE USER_INFO.name = ? AND USER.email = ?;";
	
	/** The Constant NAME_MOBILE_BOOKING_SQL_SELECT. */
	private static final String NAME_MOBILE_BOOKING_SQL_SELECT = "SELECT BOOKING_FORM.id, BOOKING_FORM.user_id, BOOKING_FORM.arrival_date, BOOKING_FORM.leaving_date, BOOKING_FORM.apartment_type_id, "
			 												   + "BOOKING_FORM.amount_of_guests, BOOKING_FORM.amount_of_apartments, BOOKING_FORM.total_price, BOOKING_FORM.bed_type_id, "
			 												   + "BOOKING_FORM.personal_preferences, BOOKING_FORM.card_type_id, BOOKING_FORM.card_number, BOOKING_FORM.card_expire_date, "
			 												   + "BOOKING_FORM.booking_aproval_sms, BOOKING_FORM.booking_creation_date, BOOKING_FORM.status, BOOKING_FORM.forcibly_closed_flag "
			 												   + "FROM hotel.BOOKING_FORM "
			 												   + "INNER JOIN (hotel.USER INNER JOIN hotel.USER_INFO "
			 												   + "ON USER.user_info_id = USER_INFO.id) "
			 												   + "ON BOOKING_FORM.user_id = USER.id "
			 												   + "WHERE USER_INFO.name = ? AND USER_INFO.mobile = ?;";
	
	/** The Constant SURNAME_EMAIL_BOOKING_SQL_SELECT. */
	private static final String SURNAME_EMAIL_BOOKING_SQL_SELECT = "SELECT BOOKING_FORM.id, BOOKING_FORM.user_id, BOOKING_FORM.arrival_date, BOOKING_FORM.leaving_date, BOOKING_FORM.apartment_type_id, "
			 													 + "BOOKING_FORM.amount_of_guests, BOOKING_FORM.amount_of_apartments, BOOKING_FORM.total_price, BOOKING_FORM.bed_type_id, "
			 													 + "BOOKING_FORM.personal_preferences, BOOKING_FORM.card_type_id, BOOKING_FORM.card_number, BOOKING_FORM.card_expire_date, "
			 													 + "BOOKING_FORM.booking_aproval_sms, BOOKING_FORM.booking_creation_date, BOOKING_FORM.status, BOOKING_FORM.forcibly_closed_flag "
			 													 + "FROM hotel.BOOKING_FORM "
			 													 + "INNER JOIN (hotel.USER INNER JOIN hotel.USER_INFO "
			 													 + "ON USER.user_info_id = USER_INFO.id) "
			 													 + "ON BOOKING_FORM.user_id = USER.id "
			 													 + "WHERE USER_INFO.surname = ? AND USER.email = ?;";
	
	/** The Constant SURNAME_MOBILE_BOOKING_SQL_SELECT. */
	private static final String SURNAME_MOBILE_BOOKING_SQL_SELECT = "SELECT BOOKING_FORM.id, BOOKING_FORM.user_id, BOOKING_FORM.arrival_date, BOOKING_FORM.leaving_date, BOOKING_FORM.apartment_type_id, "
			 													  + "BOOKING_FORM.amount_of_guests, BOOKING_FORM.amount_of_apartments, BOOKING_FORM.total_price, BOOKING_FORM.bed_type_id, "
			 													  + "BOOKING_FORM.personal_preferences, BOOKING_FORM.card_type_id, BOOKING_FORM.card_number, BOOKING_FORM.card_expire_date, "
			 													  + "BOOKING_FORM.booking_aproval_sms, BOOKING_FORM.booking_creation_date, BOOKING_FORM.status, BOOKING_FORM.forcibly_closed_flag "
			 													  + "FROM hotel.BOOKING_FORM "
			 													  + "INNER JOIN (hotel.USER INNER JOIN hotel.USER_INFO "
			 													  + "ON USER.user_info_id = USER_INFO.id) "
			 													  + "ON BOOKING_FORM.user_id = USER.id "
			 													  + "WHERE USER_INFO.surname = ? AND USER_INFO.mobile = ?;";
	
	/** The Constant EMAIL_MOBILE_BOOKING_SQL_SELECT. */
	private static final String EMAIL_MOBILE_BOOKING_SQL_SELECT = "SELECT BOOKING_FORM.id, BOOKING_FORM.user_id, BOOKING_FORM.arrival_date, BOOKING_FORM.leaving_date, BOOKING_FORM.apartment_type_id, "
			 													+ "BOOKING_FORM.amount_of_guests, BOOKING_FORM.amount_of_apartments, BOOKING_FORM.total_price, BOOKING_FORM.bed_type_id, "
			 													+ "BOOKING_FORM.personal_preferences, BOOKING_FORM.card_type_id, BOOKING_FORM.card_number, BOOKING_FORM.card_expire_date, "
			 													+ "BOOKING_FORM.booking_aproval_sms, BOOKING_FORM.booking_creation_date, BOOKING_FORM.status, BOOKING_FORM.forcibly_closed_flag "
			 													+ "FROM hotel.BOOKING_FORM "
			 													+ "INNER JOIN (hotel.USER INNER JOIN hotel.USER_INFO "
			 													+ "ON USER.user_info_id = USER_INFO.id) "
			 													+ "ON BOOKING_FORM.user_id = USER.id "
			 													+ "WHERE USER.email = ? AND USER_INFO.mobile = ?;";
	
	/** The Constant NAME_SURNAME_EMAIL_BOOKING_SQL_SELECT. */
	private static final String NAME_SURNAME_EMAIL_BOOKING_SQL_SELECT = "SELECT BOOKING_FORM.id, BOOKING_FORM.user_id, BOOKING_FORM.arrival_date, BOOKING_FORM.leaving_date, BOOKING_FORM.apartment_type_id, "
			 														  + "BOOKING_FORM.amount_of_guests, BOOKING_FORM.amount_of_apartments, BOOKING_FORM.total_price, BOOKING_FORM.bed_type_id, "
			 														  + "BOOKING_FORM.personal_preferences, BOOKING_FORM.card_type_id, BOOKING_FORM.card_number, BOOKING_FORM.card_expire_date, "
			 														  + "BOOKING_FORM.booking_aproval_sms, BOOKING_FORM.booking_creation_date, BOOKING_FORM.status, BOOKING_FORM.forcibly_closed_flag "
			 														  + "FROM hotel.BOOKING_FORM "
			 														  + "INNER JOIN (hotel.USER INNER JOIN hotel.USER_INFO "
			 														  + "ON USER.user_info_id = USER_INFO.id) "
			 														  + "ON BOOKING_FORM.user_id = USER.id "
			 														  + "WHERE USER_INFO.name = ? AND USER_INFO.surname = ? AND USER.email = ?;";
	
	/** The Constant NAME_SURNAME_MOBILE_BOOKING_SQL_SELECT. */
	private static final String NAME_SURNAME_MOBILE_BOOKING_SQL_SELECT = "SELECT BOOKING_FORM.id, BOOKING_FORM.user_id, BOOKING_FORM.arrival_date, BOOKING_FORM.leaving_date, BOOKING_FORM.apartment_type_id, "
			 														   + "BOOKING_FORM.amount_of_guests, BOOKING_FORM.amount_of_apartments, BOOKING_FORM.total_price, BOOKING_FORM.bed_type_id, "
			 														   + "BOOKING_FORM.personal_preferences, BOOKING_FORM.card_type_id, BOOKING_FORM.card_number, BOOKING_FORM.card_expire_date, "
			 														   + "BOOKING_FORM.booking_aproval_sms, BOOKING_FORM.booking_creation_date, BOOKING_FORM.status, BOOKING_FORM.forcibly_closed_flag "
			 														   + "FROM hotel.BOOKING_FORM "
			 														   + "INNER JOIN (hotel.USER INNER JOIN hotel.USER_INFO "
			 														   + "ON USER.user_info_id = USER_INFO.id) "
			 														   + "ON BOOKING_FORM.user_id = USER.id "
			 														   + "WHERE USER_INFO.name = ? AND USER_INFO.surname = ? AND USER_INFO.mobile = ?;";
	
	/** The Constant SURNAME_EMAIL_MOBILE_BOOKING_SQL_SELECT. */
	private static final String SURNAME_EMAIL_MOBILE_BOOKING_SQL_SELECT = "SELECT BOOKING_FORM.id, BOOKING_FORM.user_id, BOOKING_FORM.arrival_date, BOOKING_FORM.leaving_date, BOOKING_FORM.apartment_type_id, "
			 															+ "BOOKING_FORM.amount_of_guests, BOOKING_FORM.amount_of_apartments, BOOKING_FORM.total_price, BOOKING_FORM.bed_type_id, "
			 															+ "BOOKING_FORM.personal_preferences, BOOKING_FORM.card_type_id, BOOKING_FORM.card_number, BOOKING_FORM.card_expire_date, "
			 															+ "BOOKING_FORM.booking_aproval_sms, BOOKING_FORM.booking_c	reation_date, BOOKING_FORM.status, BOOKING_FORM.forcibly_closed_flag "
			 															+ "FROM hotel.BOOKING_FORM "
			 															+ "INNER JOIN (hotel.USER INNER JOIN hotel.USER_INFO "	
			 															+ "ON USER.user_info_id = USER_INFO.id) "
			 															+ "ON BOOKING_FORM.user_id = USER.id "
			 															+ "WHERE USER_INFO.surname = ? AND USER.email = ? AND USER_INFO.mobile = ?;";
	
	/** The Constant NAME_EMAIL_MOBILE_BOOKING_SQL_SELECT. */
	private static final String NAME_EMAIL_MOBILE_BOOKING_SQL_SELECT = "SELECT BOOKING_FORM.id, BOOKING_FORM.user_id, BOOKING_FORM.arrival_date, BOOKING_FORM.leaving_date, BOOKING_FORM.apartment_type_id, "
			 														 + "BOOKING_FORM.amount_of_guests, BOOKING_FORM.amount_of_apartments, BOOKING_FORM.total_price, BOOKING_FORM.bed_type_id, "
			 														 + "BOOKING_FORM.personal_preferences, BOOKING_FORM.card_type_id, BOOKING_FORM.card_number, BOOKING_FORM.card_expire_date, "
			 														 + "BOOKING_FORM.booking_aproval_sms, BOOKING_FORM.booking_creation_date, BOOKING_FORM.status, BOOKING_FORM.forcibly_closed_flag "
			 														 + "FROM hotel.BOOKING_FORM "
			 														 + "INNER JOIN (hotel.USER INNER JOIN hotel.USER_INFO "
			 														 + "ON USER.user_info_id = USER_INFO.id) "
			 														 + "ON BOOKING_FORM.user_id = USER.id "
			 														 + "WHERE USER_INFO.name = ? AND USER.email = ? AND USER_INFO.mobile = ?;";
	
	/** The Constant NAME_SURNAME_EMAIL_MOBILE_BOOKING_SQL_SELECT. */
	private static final String NAME_SURNAME_EMAIL_MOBILE_BOOKING_SQL_SELECT = "SELECT BOOKING_FORM.id, BOOKING_FORM.user_id, BOOKING_FORM.arrival_date, BOOKING_FORM.leaving_date, BOOKING_FORM.apartment_type_id, "
																			 + "BOOKING_FORM.amount_of_guests, BOOKING_FORM.amount_of_apartments, BOOKING_FORM.total_price, BOOKING_FORM.bed_type_id, "
																			 + "BOOKING_FORM.personal_preferences, BOOKING_FORM.card_type_id, BOOKING_FORM.card_number, BOOKING_FORM.card_expire_date, "
																			 + "BOOKING_FORM.booking_aproval_sms, BOOKING_FORM.booking_creation_date, BOOKING_FORM.status, BOOKING_FORM.forcibly_closed_flag "
																			 + "FROM hotel.BOOKING_FORM "
																			 + "INNER JOIN (hotel.USER INNER JOIN hotel.USER_INFO "
																			 + "ON USER.user_info_id = USER_INFO.id) "
																			 + "ON BOOKING_FORM.user_id = USER.id "
																			 + "WHERE USER_INFO.name = ? AND USER_INFO.surname = ? AND USER.email = ? AND USER_INFO.mobile = ?;";
	
	/** The Constant APARTMENT_TYPE_INFORMATION_SQL_SELECT. */
	private static final String APARTMENT_TYPE_INFORMATION_SQL_SELECT = "SELECT id, name, description, square, amount_of_rooms, amount_of_guests_max, price_in_dollars, "
																	  + "discount_price_in_dollars, discount_price_date_from, discount_price_date_until, service_set_id "
																	  + "FROM hotel.APARTMENT_TYPE "
																	  + "WHERE id = ?;";

	/** The Constant ALL_SERVICES_OF_SPECIFIC_SERVICE_SET_SQL_SELECT. */
	private static final String ALL_SERVICES_OF_SPECIFIC_SERVICE_SET_SQL_SELECT = "SELECT SERVICE.id, SERVICE.name, SERVICE.description "
					    														+ "FROM hotel.SERVICE_SET "
					    														+ "INNER JOIN (hotel.SERVICE_MATCHER INNER JOIN hotel.SERVICE "
					    														+ "ON SERVICE_MATCHER.service_id = SERVICE.id) "
					    														+ "ON SERVICE_SET.id = SERVICE_MATCHER.service_set_id "
					    														+ "WHERE SERVICE_SET.id = ?;";
	
	/** The Constant UNCONFIRMED_BOOKING_FORMS_SQL_SELECT. */
	private static final String UNCONFIRMED_BOOKING_FORMS_SQL_SELECT = "SELECT id, user_id, arrival_date, leaving_date, apartment_type_id, amount_of_guests, amount_of_apartments, total_price, bed_type_id, "
			   														 + "personal_preferences, card_type_id, card_number, card_expire_date, booking_aproval_sms, booking_creation_date, status, forcibly_closed_flag "
			   														 + "FROM hotel.BOOKING_FORM "
			   														 + "WHERE arrival_date = ? AND status = ?;";
	
	/** The Constant ALL_BOOKING_FORMS_SQL_SELECT. */
	private static final String ALL_BOOKING_FORMS_SQL_SELECT = "SELECT id, user_id, arrival_date, leaving_date, apartment_type_id, amount_of_guests, amount_of_apartments, total_price, bed_type_id, "
																+ "personal_preferences, card_type_id, card_number, card_expire_date, booking_aproval_sms, booking_creation_date, status, forcibly_closed_flag "
																+ "FROM hotel.BOOKING_FORM;";
	
	/** The Constant BED_TYPE_INFORMATION_SQL_SELECT. */
	private static final String BED_TYPE_INFORMATION_SQL_SELECT = "SELECT id, name, description, amount_of_persons "
			 													+ "FROM hotel.BED_TYPE "
			 													+ "WHERE id = ?;";
	
	/** The Constant CARD_TYPE_INFORMATION_SQL_SELECT. */
	private static final String CARD_TYPE_INFORMATION_SQL_SELECT = "SELECT id, name, description "
		 	 													 + "FROM hotel.CARD_TYPE "
		 	 													 + "WHERE id = ?;";
	
	/** The Constant BOOKING_FORM_CHANGING_STATUS_SQL_UPDATE. */
	private static final String BOOKING_FORM_CHANGING_STATUS_SQL_UPDATE = "UPDATE hotel.BOOKING_FORM "
			  													   		+ "SET status = ? "
			  													   		+ "WHERE id = ?;";
	
	/** The Constant BOOKING_FORM_FROCIBLY_CLOSE_SQL_UPDATE. */
	private static final String BOOKING_FORM_FROCIBLY_CLOSE_SQL_UPDATE = "UPDATE hotel.BOOKING_FORM "
		   															   + "SET status = ?, forcibly_closed_flag = ? "
		   															   + "WHERE id = ?;";
	
	/** The Constant USER_OF_BOOKING_INFORMATION_SQL_SELECT. */
	private static final String USER_OF_BOOKING_INFORMATION_SQL_SELECT = "SELECT USER.id, USER.login, USER.password, USER.user_role_id, USER.user_info_id, USER.status, USER.key_question, USER.key_answer, USER.email, "
																	   + "USER_INFO.name, USER_INFO.surname, USER_INFO.patronymic,USER_INFO.country, USER_INFO.address, USER_INFO.telephone, USER_INFO.mobile "
		 	 														   + "FROM hotel.USER "
		 	 														   + "INNER JOIN hotel.USER_INFO "
		 	 														   + "ON USER.user_info_id = USER_INFO.id "
		 	 														   + "WHERE USER.id = ?;";
	
	/** The Constant BILL_SAVING_SQL_INSERT. */
	private static final String BILL_SAVING_SQL_INSERT = "INSERT INTO hotel.BILL (booking_form_id, total_price)"
			   										   + "VALUES (?,?);";
	
	/** The Constant BILL_EXISTANCE_CHECK_SQL_SELECT. */
	private static final String BILL_EXISTANCE_CHECK_SQL_SELECT = "SELECT booking_form_id, total_price "
				 												+ "FROM hotel.BILL "
				 												+ "WHERE booking_form_id = ?;";
	
	/**
	 * Instantiates a new SQL admin dao.
	 */
	private SQLAdminDAO() {}
	
	/**
	 * Gets the single instance of SQLAdminDAO.
	 *
	 * @return single instance of SQLAdminDAO
	 */
	public static SQLAdminDAO getInstance() {
		return INSTANCE;
	}
	
	/* (non-Javadoc)
	 * @see by.epam.booking.dao.AdminDAO#selectAllTemporaryBookingForms(java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public List<BookingForm> selectAllTemporaryBookingForms(String name, String surname, String email, String mobile, String status) throws DAOException {
		Connection connection = null;
		PreparedStatement pstBookingForm = null;
		List<BookingForm> listOfClientBookings = new ArrayList<BookingForm>();
		List<BookingForm> tempListOfClientBookings = new ArrayList<BookingForm>();
				
		try {
			connection = ConnectionPool.getInstance().takeConnection();
			pstBookingForm = obtainPreparedStatement(name, surname, email, mobile, connection);					// получаем нужный запрос с уже заполненными параметрами
			tempListOfClientBookings = selectBookingForms(pstBookingForm, connection);							// получаем список заявок по бронированию 
			
			for (BookingForm bookingForm : tempListOfClientBookings) {
				if (status.equals(bookingForm.getStatus())) {
					listOfClientBookings.add(bookingForm);
				}
			}
			
		} catch (InterruptedException ex) {
			throw new DAOException("Error in obtaining connection. ", ex);
		} catch (SQLException ex) {
			throw new DAOException("SQLException in DAO class. ", ex);
		} finally {
			closeStatement(pstBookingForm);
			releaseConnection(connection);
		}		
		return listOfClientBookings;
	}

	/* (non-Javadoc)
	 * @see by.epam.booking.dao.AdminDAO#selectAllConfirmedBookingForms(java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public List<BookingForm> selectAllConfirmedBookingForms(String name, String surname, String email, String mobile, String status) throws DAOException {
		Connection connection = null;
		PreparedStatement pstBookingForm = null;
		List<BookingForm> listOfClientBookings = new ArrayList<BookingForm>();
		List<BookingForm> tempListOfClientBookings = new ArrayList<BookingForm>();
				
		try {
			connection = ConnectionPool.getInstance().takeConnection();
			pstBookingForm = obtainPreparedStatement(name, surname, email, mobile, connection);			// получаем нужный запрос с уже заполненными параметрами
			tempListOfClientBookings = selectBookingForms(pstBookingForm, connection);					// получаем список заявок по бронированию 
			
			for (BookingForm bookingForm : tempListOfClientBookings) {
				if (status.equals(bookingForm.getStatus())) {
					listOfClientBookings.add(bookingForm);
				}
			}
			
		} catch (InterruptedException ex) {
			throw new DAOException("Error in obtaining connection. ", ex);
		} catch (SQLException ex) {
			throw new DAOException("SQLException in DAO class. ", ex);
		} finally {
			closeStatement(pstBookingForm);
			releaseConnection(connection);
		}		
		return listOfClientBookings;
	}
	
	/* (non-Javadoc)
	 * @see by.epam.booking.dao.AdminDAO#selectAllClosedBookingForms(java.lang.String, java.lang.String, java.lang.String, java.lang.String, boolean, java.lang.String)
	 */
	@Override
	public List<BookingForm> selectAllClosedBookingForms(String name, String surname, String email, String mobile, boolean forciblyClosedFlag, String status) throws DAOException {
		Connection connection = null;
		Statement stBookingForm = null;
		List<BookingForm> listOfClientBookings = new ArrayList<BookingForm>();
		List<BookingForm> tempListBeforeFlagChoosing = new ArrayList<BookingForm>();
		List<BookingForm> tempListBeforeStatusChoosing = new ArrayList<BookingForm>();
		try {
			connection = ConnectionPool.getInstance().takeConnection();
			if (name.isEmpty() && surname.isEmpty() && email.isEmpty() && mobile.isEmpty()) {
				stBookingForm = connection.createStatement();
				tempListBeforeFlagChoosing = selectBookingForms(stBookingForm, connection);
			} else {
				stBookingForm = obtainPreparedStatement(name, surname, email, mobile, connection);			// получаем нужный запрос с уже заполненными параметрами
				tempListBeforeFlagChoosing = selectBookingForms(stBookingForm, connection);					// получаем список заявок по бронированию
			}
			
			for (BookingForm bookingForm : tempListBeforeFlagChoosing) {									// убираем все бронирования с неподходящим значением флага
				if (bookingForm.getForciblyClosedFlag() == forciblyClosedFlag) {
					tempListBeforeStatusChoosing.add(bookingForm);
				}
			}
			
			for (BookingForm bookingForm : tempListBeforeStatusChoosing) {									// в возвращаемый список кладём только бронирования с нужным статусом
				if (status.equals(bookingForm.getStatus())) {
					listOfClientBookings.add(bookingForm);
				}
			}
			
		} catch (InterruptedException ex) {
			throw new DAOException("Error in obtaining connection. ", ex);
		} catch (SQLException ex) {
			throw new DAOException("SQLException in DAO class. ", ex);
		} finally {
			closeStatement(stBookingForm);
			releaseConnection(connection);
		}		
		return listOfClientBookings;
	}

	/* (non-Javadoc)
	 * @see by.epam.booking.dao.AdminDAO#selectAllBookingForms(java.lang.String, java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public List<BookingForm> selectAllBookingForms(String name, String surname, String email, String mobile) throws DAOException {
		Connection connection = null;
		PreparedStatement pstBookingForm = null;
		List<BookingForm> listOfClientBookings = new ArrayList<BookingForm>();
				
		try {
			connection = ConnectionPool.getInstance().takeConnection();
			pstBookingForm = obtainPreparedStatement(name, surname, email, mobile, connection);			// получаем нужный запрос с уже заполненными параметрами
			listOfClientBookings = selectBookingForms(pstBookingForm, connection);						// получаем список заявок по бронированию 
					
		} catch (InterruptedException ex) {
			throw new DAOException("Error in obtaining connection. ", ex);
		} catch (SQLException ex) {
			throw new DAOException("SQLException in DAO class. ", ex);
		} finally {
			closeStatement(pstBookingForm);
			releaseConnection(connection);
		}		
		return listOfClientBookings;
	}

	/* (non-Javadoc)
	 * @see by.epam.booking.dao.AdminDAO#selectAllUnconfirmedBookingForms(java.lang.String, long)
	 */
	@Override
	public List<BookingForm> selectAllUnconfirmedBookingForms(String status, long arrivalDateTime) throws DAOException {
		Connection connection = null;
		PreparedStatement pstBookingForm = null;
		List<BookingForm> listOfUnconfirmedBookings = new ArrayList<BookingForm>();
		
		try {
			connection = ConnectionPool.getInstance().takeConnection();
			pstBookingForm = connection.prepareStatement(UNCONFIRMED_BOOKING_FORMS_SQL_SELECT);
			pstBookingForm.setLong(1, arrivalDateTime);
			pstBookingForm.setString(2, status);
			listOfUnconfirmedBookings = selectBookingForms(pstBookingForm, connection);
			
		} catch (InterruptedException ex) {
			throw new DAOException("Error in obtaining connection. ", ex);
		} catch (SQLException ex) {
			throw new DAOException("SQLException in DAO class. ", ex);
		} finally {
			closeStatement(pstBookingForm);
			releaseConnection(connection);
		}
		return listOfUnconfirmedBookings;
	}
	
	/* (non-Javadoc)
	 * @see by.epam.booking.dao.AdminDAO#confirmBookingForm(int[], java.lang.String)
	 */
	@Override
	public boolean confirmBookingForm(int[] bookingId, String status) throws DAOException {
		Connection connection = null;
		PreparedStatement pstConfirmBooking = null;
		int countConfirmBooking = 0;
		
		try {
			connection = ConnectionPool.getInstance().takeConnection();
			pstConfirmBooking = connection.prepareStatement(BOOKING_FORM_CHANGING_STATUS_SQL_UPDATE);
			connection.setAutoCommit(false);
			connection.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
			
			for (int id : bookingId) {
				pstConfirmBooking.setString(1, status);
				pstConfirmBooking.setInt(2, id);
				countConfirmBooking = pstConfirmBooking.executeUpdate();
				if (countConfirmBooking != 1) {
					connection.rollback();
					return false;
				}
				LOG.debug(id);
			}
			connection.commit();			
		} catch (InterruptedException ex) {
			throw new DAOException("Error in obtaining connection. ", ex);			
		} catch (SQLException ex) {
			throw new DAOException("SQLException in DAO class.", ex);
		} finally {
			closeStatement(pstConfirmBooking);
			releaseConnection(connection);
		}	
		return true;
	}

	/* (non-Javadoc)
	 * @see by.epam.booking.dao.AdminDAO#closeBookingForm(int[], java.lang.String)
	 */
	@Override
	public boolean closeBookingForm(int[] bookingId, String status) throws DAOException {
		Connection connection = null;
		PreparedStatement pstCloseBooking = null;
		int countCloseBooking = 0;
		
		try {
			connection = ConnectionPool.getInstance().takeConnection();
			pstCloseBooking = connection.prepareStatement(BOOKING_FORM_CHANGING_STATUS_SQL_UPDATE);
			connection.setAutoCommit(false);
			connection.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
			
			for (int id : bookingId) {
				pstCloseBooking.setString(1, status);
				pstCloseBooking.setInt(2, id);
				countCloseBooking = pstCloseBooking.executeUpdate();
				if (countCloseBooking != 1) {
					connection.rollback();
					return false;
				}
				LOG.debug(id);
			}
			connection.commit();			
		} catch (InterruptedException ex) {
			throw new DAOException("Error in obtaining connection. ", ex);
		} catch (SQLException ex) {
			throw new DAOException("SQLException in DAO class.", ex);
		} finally {
			closeStatement(pstCloseBooking);
			releaseConnection(connection);
		}	
		return true;
	}
	
	/* (non-Javadoc)
	 * @see by.epam.booking.dao.AdminDAO#forciblyCloseBookingForm(int[], java.lang.String, boolean)
	 */
	@Override
	public boolean forciblyCloseBookingForm(int[] bookingId, String status, boolean forciblyCloseFlag) throws DAOException {
		Connection connection = null;
		PreparedStatement pstCloseBooking = null;
		int countCloseBooking = 0;
		
		try {
			connection = ConnectionPool.getInstance().takeConnection();
			pstCloseBooking = connection.prepareStatement(BOOKING_FORM_FROCIBLY_CLOSE_SQL_UPDATE);
			connection.setAutoCommit(false);
			connection.setTransactionIsolation(Connection.TRANSACTION_SERIALIZABLE);
			
			for (int id : bookingId) {
				pstCloseBooking.setString(1, status);
				pstCloseBooking.setBoolean(2, forciblyCloseFlag);
				pstCloseBooking.setInt(3, id);
				countCloseBooking = pstCloseBooking.executeUpdate();
				if (countCloseBooking != 1) {
					connection.rollback();
					return false;
				}
				LOG.debug(id);
			}
			connection.commit();			
		} catch (InterruptedException ex) {
			throw new DAOException("Error in obtaining connection. ", ex);
		} catch (SQLException ex) {
			throw new DAOException("SQLException in DAO class.", ex);
		} finally {
			closeStatement(pstCloseBooking);
			releaseConnection(connection);
		}	
		return true;
	}
	
	/* (non-Javadoc)
	 * @see by.epam.booking.dao.AdminDAO#obtainUserOfBookingInformation(int)
	 */
	@Override
	public User obtainUserOfBookingInformation(int userId) throws DAOException {
		User user = null;
		Connection connection = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		
		try {
			connection = ConnectionPool.getInstance().takeConnection();
			pst = connection.prepareStatement(USER_OF_BOOKING_INFORMATION_SQL_SELECT);
			pst.setInt(1, userId);
			
			rs = pst.executeQuery();
			if (rs.next()) {
				user = new User(rs.getInt("id"), rs.getString("login"), rs.getString("password"), rs.getInt("user_role_id"), USER_DEFAULT_ROLE, rs.getString("status"), 
								rs.getString("key_question"), rs.getString("key_answer"), rs.getString("email"), rs.getString("name"), rs.getString("surname"), 
								rs.getString("patronymic"), rs.getString("country"), rs.getString("address"), rs.getString("telephone"), rs.getString("mobile"));
			} else {
				LOG.error("User hasn't been found");
			}
			
		} catch (InterruptedException ex) {
			throw new DAOException("Error in obtaining connection. ", ex);
		} catch (SQLException ex) {
			throw new DAOException("SQLException in DAO class.", ex);
		} finally {
			closeStatement(pst);
			releaseConnection(connection);
		}		
		return user;
	}

	/* (non-Javadoc)
	 * @see by.epam.booking.dao.AdminDAO#saveBill(by.epam.booking.domain.Bill)
	 */
	@Override
	public boolean saveBill(Bill bill) throws DAOException {
		Connection connection = null;
		PreparedStatement pst = null;
		int saveResult = 0;
		
		try {
			connection = ConnectionPool.getInstance().takeConnection();
			pst = connection.prepareStatement(BILL_SAVING_SQL_INSERT);
			pst.setInt(1, bill.getBookingFormId());
			pst.setDouble(2, bill.getTotalPrice());
			
			saveResult = pst.executeUpdate();
			if (saveResult == 1) {
				return true;
			} else {
				return false;
			}
			
		} catch (InterruptedException ex) {
			throw new DAOException("Error in obtaining connection. ", ex);
		} catch (SQLException ex) {
			throw new DAOException("SQLException in DAO class.", ex);
		} finally {
			closeStatement(pst);
			releaseConnection(connection);
		}
	}
	
	/* (non-Javadoc)
	 * @see by.epam.booking.dao.AdminDAO#checkBillExistance(int)
	 */
	@Override
	public boolean checkBillExistance(int bookingFormId) throws DAOException {
		Connection connection = null;
		PreparedStatement pst = null;
		ResultSet rs = null;
		
		try {
			connection = ConnectionPool.getInstance().takeConnection();
			pst = connection.prepareStatement(BILL_EXISTANCE_CHECK_SQL_SELECT);
			pst.setInt(1, bookingFormId);
			rs = pst.executeQuery();
			
			if (rs.next()) {
				return true;
			} else {
				return false;
			}
			
		} catch (InterruptedException ex) {
			throw new DAOException("Error in obtaining connection. ", ex);
		} catch (SQLException ex) {
			throw new DAOException("SQLException in DAO class.", ex);
		} finally {
			closeStatement(pst);
			releaseConnection(connection);
		}
	}
	
	/**
	 * Obtain prepared statement.
	 *
	 * @param name the name
	 * @param surname the surname
	 * @param email the email
	 * @param mobile the mobile
	 * @param connection the connection
	 * @return the prepared statement
	 */
	private PreparedStatement obtainPreparedStatement(String name, String surname, String email, String mobile, Connection connection) {		// метод, определяющий по каким полям искать заявки по бронированию
		PreparedStatement pst = null;
		try {
			if (!name.isEmpty() && surname.isEmpty() && email.isEmpty() && mobile.isEmpty()) {
				pst = connection.prepareStatement(NAME_BOOKING_SQL_SELECT);
				pst.setString(1, name);
			} else if (!surname.isEmpty() && name.isEmpty() && email.isEmpty() && mobile.isEmpty()) {
				pst = connection.prepareStatement(SURNAME_BOOKING_SQL_SELECT);
				pst.setString(1, surname);
			} else if (!email.isEmpty() && name.isEmpty() && surname.isEmpty() && mobile.isEmpty()) {
				pst = connection.prepareStatement(EMAIL_BOOKING_SQL_SELECT);
				pst.setString(1, email);
			} else if (!mobile.isEmpty() && name.isEmpty() && email.isEmpty() && mobile.isEmpty()) {
				pst = connection.prepareStatement(MOBILE_BOOKING_SQL_SELECT);
				pst.setString(1, mobile);
			}
			  else if (!name.isEmpty() && !surname.isEmpty() && email.isEmpty() && mobile.isEmpty()) {
				  pst = connection.prepareStatement(NAME_SURNAME_BOOKING_SQL_SELECT);
				  pst.setString(1, name);
				  pst.setString(2, surname);
			} else if (!name.isEmpty() && !email.isEmpty() && surname.isEmpty() && mobile.isEmpty()) {
				pst = connection.prepareStatement(NAME_EMAIL_BOOKING_SQL_SELECT);
				pst.setString(1, name);
				pst.setString(2, email);
			} else if (!name.isEmpty() && !mobile.isEmpty() && surname.isEmpty() && email.isEmpty()) {
				pst = connection.prepareStatement(NAME_MOBILE_BOOKING_SQL_SELECT);
				pst.setString(1, name);
				pst.setString(2, mobile);
			}  else if (!surname.isEmpty() && !email.isEmpty() && name.isEmpty() && mobile.isEmpty()) {
				pst = connection.prepareStatement(SURNAME_EMAIL_BOOKING_SQL_SELECT);
				pst.setString(1, surname);
				pst.setString(2, email);
			} else if (!surname.isEmpty() && !mobile.isEmpty() && name.isEmpty() && email.isEmpty()) {
				pst = connection.prepareStatement(SURNAME_MOBILE_BOOKING_SQL_SELECT);
				pst.setString(1, surname);
				pst.setString(2, mobile);
			} else if (!email.isEmpty() && !mobile.isEmpty() && name.isEmpty() && surname.isEmpty()) {
				pst = connection.prepareStatement(EMAIL_MOBILE_BOOKING_SQL_SELECT);
				pst.setString(1, email);
				pst.setString(2, mobile);
			} 
			  else if (!name.isEmpty() && !surname.isEmpty() && !email.isEmpty() && mobile.isEmpty()) {
				pst = connection.prepareStatement(NAME_SURNAME_EMAIL_BOOKING_SQL_SELECT);
				pst.setString(1, name);
				pst.setString(2, surname);
				pst.setString(3, email);
			} else if (!name.isEmpty() && !surname.isEmpty() && !mobile.isEmpty() && email.isEmpty()) {
				pst = connection.prepareStatement(NAME_SURNAME_MOBILE_BOOKING_SQL_SELECT);
				pst.setString(1, name);
				pst.setString(2, surname);
				pst.setString(3, mobile);
			} else if (!surname.isEmpty() && !email.isEmpty() && !mobile.isEmpty() && name.isEmpty()) {
				pst = connection.prepareStatement(SURNAME_EMAIL_MOBILE_BOOKING_SQL_SELECT);
				pst.setString(1, surname);
				pst.setString(2, email);
				pst.setString(3, mobile);
			} else if (!name.isEmpty() && !email.isEmpty() && !mobile.isEmpty() && surname.isEmpty()) {
				pst = connection.prepareStatement(NAME_EMAIL_MOBILE_BOOKING_SQL_SELECT);
				pst.setString(1, name);
				pst.setString(2, email);
				pst.setString(3, mobile);
			} 
			  else if (!name.isEmpty() && !surname.isEmpty() && !email.isEmpty() && !mobile.isEmpty()) {
				pst = connection.prepareStatement(NAME_SURNAME_EMAIL_MOBILE_BOOKING_SQL_SELECT);
				pst.setString(1, name);
				pst.setString(2, surname);
				pst.setString(3, email);
				pst.setString(4, mobile);
			}
		} catch (SQLException ex) {
			
		}
		
		try {LOG.debug("Amount of parameters in query: " + pst.getParameterMetaData().getParameterCount());} catch (SQLException ex) {}
		return pst;
	}
	
	/**
	 * Select booking forms.
	 *
	 * @param stBookingForm the st booking form
	 * @param connection the connection
	 * @return the list
	 * @throws SQLException the SQL exception
	 * @throws DAOException the DAO exception
	 */
	private List<BookingForm> selectBookingForms(Statement stBookingForm, Connection connection) throws SQLException, DAOException {
		PreparedStatement pstBedType = null;
		PreparedStatement pstCardType = null;
		ResultSet rsBookingForm = null;
		ResultSet rsBedType = null;
		ResultSet rsCardType = null;
		ApartmentType apartmentType = null;
		BedType bedType = null;
		CardType cardType = null;
		List<BookingForm> listOfClientBookings = new ArrayList<BookingForm>();
		
		if (MYSQL_PREPARED_STATEMENT_CLASS_NAME.equals(stBookingForm.getClass().toString())) {
			PreparedStatement pstBookingForm = (PreparedStatement) stBookingForm;
			rsBookingForm = pstBookingForm.executeQuery();
		} else {
			rsBookingForm = stBookingForm.executeQuery(ALL_BOOKING_FORMS_SQL_SELECT);
		}		
		
		try {
			while (rsBookingForm.next()) {																					// для каждой найденной заявки получаем данные по типу номера
				apartmentType = selectApartmentTypeById(rsBookingForm.getInt("apartment_type_id"), connection);
				LOG.debug(rsBookingForm.getInt("id"));
								
				pstBedType = connection.prepareStatement(BED_TYPE_INFORMATION_SQL_SELECT);									// для каждой найденной заявки получаем данные по типу кроватей
				pstBedType.setInt(1, rsBookingForm.getInt("bed_type_id"));
				rsBedType = pstBedType.executeQuery();
				if (rsBedType.next()) {
					bedType = new BedType(rsBedType.getInt("id"), rsBedType.getString("name"), rsBedType.getString("description"), rsBedType.getInt("amount_of_persons")); 
				} else {
					throw new SQLException("Bed type with required id doesn't exist");
				}
				
				pstCardType = connection.prepareStatement(CARD_TYPE_INFORMATION_SQL_SELECT);								// для каждой найденной заявки получаем данные по типу карты оплаты
				pstCardType.setInt(1, rsBookingForm.getInt("card_type_id"));
				rsCardType = pstCardType.executeQuery();
				if (rsCardType.next()) {
					cardType = new CardType(rsCardType.getInt("id"), rsCardType.getString("name"), rsCardType.getString("description")); 
				} else {
					throw new SQLException("Card type with required id doesn't exist");
				}
				
				listOfClientBookings.add(new BookingForm(rsBookingForm.getInt("id"), rsBookingForm.getInt("user_id"), rsBookingForm.getLong("arrival_date"), 
						rsBookingForm.getLong("leaving_date"), apartmentType, rsBookingForm.getInt("amount_of_guests"), rsBookingForm.getInt("amount_of_apartments"), 
						rsBookingForm.getDouble("total_price"), bedType, rsBookingForm.getString("personal_preferences"), cardType, rsBookingForm.getString("card_number"), 
						rsBookingForm.getLong("card_expire_date"), rsBookingForm.getBoolean("booking_aproval_sms"), rsBookingForm.getLong("booking_creation_date"), 
						rsBookingForm.getString("status"), rsBookingForm.getBoolean("forcibly_closed_flag")));
			}
		} finally {
			closeStatement(pstBedType);
			closeStatement(pstCardType);
		}
		return listOfClientBookings;
	}
	
	/**
	 * Select apartment type by id.
	 *
	 * @param typeId the type id
	 * @param connection the connection
	 * @return the apartment type
	 * @throws DAOException the DAO exception
	 */
	private ApartmentType selectApartmentTypeById(int typeId, Connection connection) throws DAOException {
		PreparedStatement pstApartmentTypeInf = null;
		PreparedStatement pstServicesOfSet = null;
		ResultSet rsApartmentTypeInf = null;
		ResultSet rsServicesOfSet = null;
		ApartmentType apartmentType = null;
		Set<Service> setOfServices = new HashSet<Service>();
		
		try {
			pstApartmentTypeInf = connection.prepareStatement(APARTMENT_TYPE_INFORMATION_SQL_SELECT);
			pstApartmentTypeInf.setInt(1, typeId);
			rsApartmentTypeInf = pstApartmentTypeInf.executeQuery();
			
			if (rsApartmentTypeInf.next()) {
				pstServicesOfSet = connection.prepareStatement(ALL_SERVICES_OF_SPECIFIC_SERVICE_SET_SQL_SELECT);
				pstServicesOfSet.setInt(1, rsApartmentTypeInf.getInt("service_set_id"));
				rsServicesOfSet = pstServicesOfSet.executeQuery();
				while(rsServicesOfSet.next()) {
					Service service = new Service(rsServicesOfSet.getInt("id"), rsServicesOfSet.getString("name"), rsServicesOfSet.getString("description"));
					setOfServices.add(service);
				}
				apartmentType = new ApartmentType(rsApartmentTypeInf.getInt("id"), rsApartmentTypeInf.getString("name"), rsApartmentTypeInf.getString("description"), 
						  rsApartmentTypeInf.getInt("square"), rsApartmentTypeInf.getInt("amount_of_rooms"), rsApartmentTypeInf.getInt("amount_of_guests_max"), 
						  rsApartmentTypeInf.getDouble("price_in_dollars"), rsApartmentTypeInf.getDouble("discount_price_in_dollars"), 
						  rsApartmentTypeInf.getLong("discount_price_date_from"), rsApartmentTypeInf.getLong("discount_price_date_until"), setOfServices);
			}
		} catch (SQLException ex) {
			throw new DAOException("SQLException in DAO class. ", ex);
		} finally {
			closeStatement(pstApartmentTypeInf);
			closeStatement(pstServicesOfSet);
		}	
		return apartmentType;
	}
	
	/**
	 * Close statement.
	 *
	 * @param st the st
	 */
	private void closeStatement(Statement st) {
		try {
			if (st != null) {
				st.close();
			}
		} catch (SQLException ex) {
			LOG.error("Error in closing prepared statement!");
		}
	}
	
	/**
	 * Release connection.
	 *
	 * @param connection the connection
	 */
	private void releaseConnection(Connection connection) {
		try {
			ConnectionPool.getInstance().releaseConnection(connection);
		} catch (SQLException ex) {
			LOG.error("Error in releasing of connection!");
		}
	}
}
