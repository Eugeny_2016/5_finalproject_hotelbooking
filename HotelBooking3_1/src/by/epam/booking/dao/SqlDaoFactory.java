package by.epam.booking.dao;

import by.epam.booking.dao.impl.SQLAdminDAO;
import by.epam.booking.dao.impl.SQLClientDAO;
import by.epam.booking.dao.impl.SQLUserDAO;
import by.epam.booking.dao.impl.SQLVisitorDAO;

/**
 * A factory for creating SqlDao objects.
 */
public class SqlDaoFactory {
	
	/** The Constant INSTANCE. */
	private static final SqlDaoFactory INSTANCE = new SqlDaoFactory();
	
	/**
	 * Instantiates a new sql dao factory.
	 */
	private SqlDaoFactory() {}
	
	/**
	 * Gets the single instance of SqlDaoFactory.
	 *
	 * @return single instance of SqlDaoFactory
	 */
	public static SqlDaoFactory getInstance() {
		return INSTANCE;
	}
	
	/**
	 * Gets the sql dao.
	 *
	 * @param type the type
	 * @return the sql dao
	 * @throws DAOException the DAO exception
	 */
	public VisitorDAO getSqlDao(SqlDaoType type) throws DAOException {
		switch (type) {
		case VISITOR:
			return SQLVisitorDAO.getInstance();
		case USER:
			return SQLUserDAO.getInstance();
		case CLIENT:
			return SQLClientDAO.getInstance();
		case ADMIN:
			return SQLAdminDAO.getInstance();
		default:
			throw new DAOException("No such DAO!");
		}
	}
}
