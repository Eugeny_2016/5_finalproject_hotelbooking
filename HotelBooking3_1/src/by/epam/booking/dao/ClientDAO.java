package by.epam.booking.dao;

import java.util.List;
import java.util.Map;
import java.util.Set;

import by.epam.booking.command.impl.CommandDenialReasonName;
import by.epam.booking.domain.ApartmentType;
import by.epam.booking.domain.BedType;
import by.epam.booking.domain.BookingForm;
import by.epam.booking.domain.CardType;

/**
 * The Interface ClientDAO.
 */
public interface ClientDAO extends UserDAO {
	
	/**
	 * Select apartment on date.
	 *
	 * @param dateFrom the date from
	 * @param dateUntil the date until
	 * @return the map
	 * @throws DAOException the DAO exception
	 */
	Map<ApartmentType, List<Integer>> selectApartmentOnDate(long dateFrom, long dateUntil) throws DAOException;
	
	/**
	 * Save booking form.
	 *
	 * @param dateFrom the date from
	 * @param dateUntil the date until
	 * @param bookingForm the booking form
	 * @return the map
	 * @throws DAOException the DAO exception
	 */
	Map<Boolean, CommandDenialReasonName> saveBookingForm(long dateFrom, long dateUntil, BookingForm bookingForm) throws DAOException;
	
	/**
	 * Cancel booking form.
	 *
	 * @param bookingId the booking id
	 * @param status the status
	 * @return true, if successful
	 * @throws DAOException the DAO exception
	 */
	boolean cancelBookingForm(int[] bookingId, String status) throws DAOException;
	
	/**
	 * Select all booking forms.
	 *
	 * @param userId the user id
	 * @return the list
	 * @throws DAOException the DAO exception
	 */
	List<BookingForm> selectAllBookingForms(int userId) throws DAOException;
	
	/**
	 * Select all temporary booking forms.
	 *
	 * @param userId the user id
	 * @param status the status
	 * @return the list
	 * @throws DAOException the DAO exception
	 */
	List<BookingForm> selectAllTemporaryBookingForms(int userId, String status) throws DAOException;
	
	/**
	 * Select apartment type by id.
	 *
	 * @param typeId the type id
	 * @return the apartment type
	 * @throws DAOException the DAO exception
	 */
	ApartmentType selectApartmentTypeById(int typeId) throws DAOException;
	
	/**
	 * Select all bed types.
	 *
	 * @return the sets the
	 * @throws DAOException the DAO exception
	 */
	Set<BedType> selectAllBedTypes() throws DAOException;
	
	/**
	 * Select all card types.
	 *
	 * @return the sets the
	 * @throws DAOException the DAO exception
	 */
	Set<CardType> selectAllCardTypes() throws DAOException;
}
