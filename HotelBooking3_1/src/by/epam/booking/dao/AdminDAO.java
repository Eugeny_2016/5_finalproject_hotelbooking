package by.epam.booking.dao;

import java.util.List;

import by.epam.booking.domain.Bill;
import by.epam.booking.domain.BookingForm;
import by.epam.booking.domain.User;

/**
 * The Interface AdminDAO.
 */
public interface AdminDAO extends UserDAO {
	
	/**
	 * Select all temporary booking forms.
	 *
	 * @param name the name
	 * @param surname the surname
	 * @param email the email
	 * @param mobile the mobile
	 * @param status the status
	 * @return the list
	 * @throws DAOException the DAO exception
	 */
	List<BookingForm> selectAllTemporaryBookingForms(String name, String surname, String email, String mobile, String status) throws DAOException;
	
	/**
	 * Select all confirmed booking forms.
	 *
	 * @param name the name
	 * @param surname the surname
	 * @param email the email
	 * @param mobile the mobile
	 * @param status the status
	 * @return the list
	 * @throws DAOException the DAO exception
	 */
	List<BookingForm> selectAllConfirmedBookingForms(String name, String surname, String email, String mobile, String status) throws DAOException;
	
	/**
	 * Select all closed booking forms.
	 *
	 * @param name the name
	 * @param surname the surname
	 * @param email the email
	 * @param mobile the mobile
	 * @param forciblyClosedFlag the forcibly closed flag
	 * @param status the status
	 * @return the list
	 * @throws DAOException the DAO exception
	 */
	List<BookingForm> selectAllClosedBookingForms(String name, String surname, String email, String mobile, boolean forciblyClosedFlag, String status) throws DAOException;
	
	/**
	 * Select all booking forms.
	 *
	 * @param name the name
	 * @param surname the surname
	 * @param email the email
	 * @param mobile the mobile
	 * @return the list
	 * @throws DAOException the DAO exception
	 */
	List<BookingForm> selectAllBookingForms(String name, String surname, String email, String mobile) throws DAOException;
	
	/**
	 * Select all unconfirmed booking forms.
	 *
	 * @param status the status
	 * @param arrivalDateTime the arrival date time
	 * @return the list
	 * @throws DAOException the DAO exception
	 */
	List<BookingForm> selectAllUnconfirmedBookingForms(String status, long arrivalDateTime) throws DAOException;
	
	/**
	 * Obtain user of booking information.
	 *
	 * @param id the id
	 * @return the user
	 * @throws DAOException the DAO exception
	 */
	User obtainUserOfBookingInformation(int id) throws DAOException;
	
	/**
	 * Confirm booking form.
	 *
	 * @param bookingId the booking id
	 * @param status the status
	 * @return true, if successful
	 * @throws DAOException the DAO exception
	 */
	boolean confirmBookingForm(int[] bookingId, String status) throws DAOException;
	
	/**
	 * Close booking form.
	 *
	 * @param bookingId the booking id
	 * @param status the status
	 * @return true, if successful
	 * @throws DAOException the DAO exception
	 */
	boolean closeBookingForm(int[] bookingId, String status) throws DAOException;
	
	/**
	 * Forcibly close booking form.
	 *
	 * @param bookingId the booking id
	 * @param status the status
	 * @param forciblyCloseFlag the forcibly close flag
	 * @return true, if successful
	 * @throws DAOException the DAO exception
	 */
	boolean forciblyCloseBookingForm(int[] bookingId, String status, boolean forciblyCloseFlag) throws DAOException;	
	
	/**
	 * Save bill.
	 *
	 * @param bill the bill
	 * @return true, if successful
	 * @throws DAOException the DAO exception
	 */
	boolean saveBill(Bill bill) throws DAOException;
	
	/**
	 * Check bill existance.
	 *
	 * @param bookingFormId the booking form id
	 * @return true, if successful
	 * @throws DAOException the DAO exception
	 */
	boolean checkBillExistance(int bookingFormId) throws DAOException;
}
