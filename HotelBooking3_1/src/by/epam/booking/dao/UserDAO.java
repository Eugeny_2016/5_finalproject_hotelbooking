package by.epam.booking.dao;

import by.epam.booking.domain.User;
import by.epam.booking.domain.UserRole;

/**
 * The Interface UserDAO.
 */
public interface UserDAO extends VisitorDAO {
	
	/**
	 * Check user.
	 *
	 * @param login the login
	 * @return true, if successful
	 * @throws DAOException the DAO exception
	 */
	public abstract boolean checkUser(String login) throws DAOException;
	
	/**
	 * Check user.
	 *
	 * @param login the login
	 * @param password the password
	 * @return true, if successful
	 * @throws DAOException the DAO exception
	 */
	public abstract boolean checkUser(String login, String password) throws DAOException;
	
	/**
	 * Obtain user.
	 *
	 * @param login the login
	 * @param password the password
	 * @return the user
	 * @throws DAOException the DAO exception
	 */
	public abstract User obtainUser(String login, String password) throws DAOException;
	
	/**
	 * Obtain role information.
	 *
	 * @param name the name
	 * @return the user role
	 * @throws DAOException the DAO exception
	 */
	public abstract UserRole obtainRoleInformation(String name) throws DAOException;
	
	/**
	 * Adds the user.
	 *
	 * @param user the user
	 * @return true, if successful
	 * @throws DAOException the DAO exception
	 */
	public abstract boolean addUser(User user) throws DAOException;

}
