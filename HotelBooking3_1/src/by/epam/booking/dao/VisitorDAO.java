package by.epam.booking.dao;

import java.util.List;
import java.util.Map;

/**
 * The Interface VisitorDAO.
 */
public interface VisitorDAO {
	
	/**
	 * Select resource.
	 *
	 * @param resourceName the resource name
	 * @param language the language
	 * @return the string
	 * @throws DAOException the DAO exception
	 */
	String selectResource(String resourceName, String language) throws DAOException;
	
	/**
	 * Select resource.
	 *
	 * @param resourceName the resource name
	 * @return the map
	 * @throws DAOException the DAO exception
	 */
	Map<String, String> selectResource(String resourceName) throws DAOException;
	
	/**
	 * Select currency name.
	 *
	 * @param countryName the country name
	 * @return the string
	 * @throws DAOException the DAO exception
	 */
	String selectCurrencyName(String countryName) throws DAOException;
	
	/**
	 * Select dollar coefficient.
	 *
	 * @param countryName the country name
	 * @return the float
	 * @throws DAOException the DAO exception
	 */
	float selectDollarCoefficient(String countryName) throws DAOException;
	
	/**
	 * Select apartment prices.
	 *
	 * @return the list
	 * @throws DAOException the DAO exception
	 */
	List<Double> selectApartmentPrices() throws DAOException;
}
