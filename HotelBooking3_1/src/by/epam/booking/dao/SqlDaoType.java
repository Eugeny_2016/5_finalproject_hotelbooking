package by.epam.booking.dao;

/**
 * The Enum SqlDaoType.
 */
public enum SqlDaoType {
	
	/** The visitor. */
	VISITOR, 
 /** The user. */
 USER, 
 /** The client. */
 CLIENT, 
 /** The admin. */
 ADMIN
}
