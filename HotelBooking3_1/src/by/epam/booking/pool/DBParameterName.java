package by.epam.booking.pool;

/**
 * The Class DBParameterName.
 */
public final class DBParameterName {
	
	/**
	 * Instantiates a new DB parameter name.
	 */
	private DBParameterName() {}
	
	/** The Constant DB_DRIVER. */
	public static final String DB_DRIVER = "db.driver";
	
	/** The Constant DB_URL. */
	public static final String DB_URL = "db.url";
	
	/** The Constant DB_USER. */
	public static final String DB_USER = "db.user";
	
	/** The Constant DB_PASSWORD. */
	public static final String DB_PASSWORD = "db.password";
	
	/** The Constant DB_POOLSIZE. */
	public static final String DB_POOLSIZE = "db.poolsize";
	
}
