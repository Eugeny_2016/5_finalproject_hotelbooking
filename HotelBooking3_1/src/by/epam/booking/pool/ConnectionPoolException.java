package by.epam.booking.pool;

/**
 * The Class ConnectionPoolException.
 */
public class ConnectionPoolException extends Exception {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * Instantiates a new connection pool exception.
	 */
	public ConnectionPoolException() {
		super();
	}
	
	/**
	 * Instantiates a new connection pool exception.
	 *
	 * @param msg the msg
	 * @param ex the ex
	 */
	public ConnectionPoolException(String msg, Throwable ex) {
		super(msg, ex);
	}

	/**
	 * Instantiates a new connection pool exception.
	 *
	 * @param msg the msg
	 */
	public ConnectionPoolException(String msg) {
		super(msg);
	}

	/**
	 * Instantiates a new connection pool exception.
	 *
	 * @param ex the ex
	 */
	public ConnectionPoolException(Throwable ex) {
		super(ex);
	}
}
