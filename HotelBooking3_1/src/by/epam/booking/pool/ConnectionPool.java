package by.epam.booking.pool;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.ReentrantLock;

import org.apache.log4j.Logger;

/**
 * The Class ConnectionPool.
 */
public final class ConnectionPool {
	
	/** The Constant LOG. */
	private final static Logger LOG = Logger.getRootLogger();
	
	/** The Constant DEFAULT_POOL_SIZE. */
	private static final int DEFAULT_POOL_SIZE = 5;								// размер пула по умолчанию
	
	/** The Constant MINIMAL_POOL_SIZE. */
	private static final int MINIMAL_POOL_SIZE = 2;								// минимально допустимый размер пула
		
	/** The instance. */
	private static ConnectionPool instance = null;
	
	/** The lock. */
	private static ReentrantLock lock = new ReentrantLock();
	
	/** The is created flag. */
	private static AtomicBoolean isCreatedFlag = new AtomicBoolean(false);
	
	/** The free connection queue. */
	private BlockingQueue<Connection> freeConnectionQueue;
	
	/** The occupied connection queue. */
	private BlockingQueue<Connection> occupiedConnectionQueue;

	/** The resource. */
	private ResourceBundle resource;
	
	/** The driver name. */
	private String driverName;
	
	/** The url. */
	private String url;
	
	/** The user. */
	private String user;
	
	/** The password. */
	private String password;
	
	/** The pool size. */
	private int poolSize;
	
	/**
	 * Instantiates a new connection pool. Gets the DB parameters 
	 * from resource bundle file and creates queues for connections
	 * to be filled afterwards
	 */
	private ConnectionPool() {
		resource = ResourceBundle.getBundle("resources.db");
		driverName = resource.getString(DBParameterName.DB_DRIVER);
		url = resource.getString(DBParameterName.DB_URL);
		user = resource.getString(DBParameterName.DB_USER);
		password = resource.getString(DBParameterName.DB_PASSWORD);
		
		try {
			poolSize = Integer.parseInt(resource.getString(DBParameterName.DB_POOLSIZE));
			freeConnectionQueue = new ArrayBlockingQueue<Connection>(poolSize);
			occupiedConnectionQueue = new ArrayBlockingQueue<Connection>(poolSize);
		} catch (NumberFormatException ex) {
			poolSize = DEFAULT_POOL_SIZE;
			freeConnectionQueue = new ArrayBlockingQueue<Connection>(poolSize);
			occupiedConnectionQueue = new ArrayBlockingQueue<Connection>(poolSize);
		}
	}
	
	/**
	 * Gets the single instance of ConnectionPool.
	 *
	 * @return single instance of ConnectionPool
	 */
	public static ConnectionPool getInstance() {
		if (!isCreatedFlag.get()) {
			try {
				lock.lock();
				if (!isCreatedFlag.get()) {
					instance = new ConnectionPool();
					isCreatedFlag.set(true);
				}
			} catch (MissingResourceException ex) {
				LOG.error("File with DB properties hasn't been found or the required parameter is absent. " + ex.getMessage());
			} finally {
				lock.unlock();
			}
		}
		return instance;
	}
	
	/**
	 * Inits the pool data.
	 *
	 * @throws ConnectionPoolException the connection pool exception
	 */
	public final void initPoolData() throws ConnectionPoolException {
		try {
			Class.forName(driverName);
			
			if (freeConnectionQueue.isEmpty()) {																// если очередь свободных соединений ещё пуста
				for(int i = 0; i < poolSize; i++) {
					Connection connection = DriverManager.getConnection(url, user, password);
					freeConnectionQueue.add(connection);
				}
			} else {																							// если очередь свободных соединений уже наполнена, то пытаемся её дозаполнить
				int startingValue = freeConnectionQueue.size();
				for(int i = startingValue; i < poolSize; i++) {
					Connection connection = DriverManager.getConnection(url, user, password);
					freeConnectionQueue.add(connection);
				}
			}			
		} catch (ClassNotFoundException ex) {
			throw new ConnectionPoolException("Can't find database driver class.", ex);
		} catch (SQLException ex) {
			LOG.error("An error occured while getting connection from Driver manager. " + ex.getMessage());		// гасим это исключение, чтобы в дальнейшем снова попробовать
		}
	}
	
	/**
	 * Take connection.
	 *
	 * @return the connection
	 * @throws InterruptedException the interrupted exception
	 */
	public Connection takeConnection() throws InterruptedException {
		Connection connection = null;
		connection = freeConnectionQueue.take();
		occupiedConnectionQueue.add(connection);
		return connection;
	}
	
	/**
	 * Release connection.
	 *
	 * @param connection the connection
	 * @throws SQLException the SQL exception
	 */
	public void releaseConnection(Connection connection) throws SQLException {
		if (connection.isReadOnly()) {
			connection.setReadOnly(false);
		}
		
		if (!connection.getAutoCommit()) {
			connection.setAutoCommit(true);
		}
		
		if (!connection.isClosed()) {
			if (occupiedConnectionQueue.remove(connection)) {
				if (!freeConnectionQueue.offer(connection)) {
					LOG.error("Error in adding connection to freeConnectionQueue!");				
				}
			} else {
				LOG.error("Error in removing connection from occupiedConnectionQueue!");
			}
		} else {
			LOG.error("Error in closing connection. Connection is already closed!");
		}
	}
	
	/**
	 * Close connection queues.
	 */
	public final void closeConnectionQueues() {
		try {
			Connection connection;
			while ((connection = freeConnectionQueue.poll()) != null) {
				if (!connection.getAutoCommit()) {
					connection.commit();
				}
				connection.close();
			}
			while ((connection = occupiedConnectionQueue.poll()) != null) {
				if (!connection.getAutoCommit()) {
					connection.commit();
				}
				connection.close();
			}
		} catch (SQLException ex) {
			LOG.error("Error in closing the connection", ex);
		}
	}
	
	/**
	 * Checks if is filled.
	 *
	 * @return true, if is filled
	 */
	public final boolean isFilled() {
		return (freeConnectionQueue.size() == poolSize) ? true : false;
	}
	
	/**
	 * Checks if is minimally filled.
	 *
	 * @return true, if is minimally filled
	 */
	public final boolean isMinimallyFilled() {
		return (freeConnectionQueue.size() >= MINIMAL_POOL_SIZE) ? true : false;
	}
	
	/**
	 * Gets the pool size.
	 *
	 * @return the pool size
	 */
	public final int getPoolSize() {
		return freeConnectionQueue.size();
	}
}
