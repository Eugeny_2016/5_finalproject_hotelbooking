package by.epam.booking.domain;

/**
 * The Class User.
 */
public class User {
	
	/** The id. */
	private int id;
	
	/** The login. */
	private String login;
	
	/** The password. */
	private String password;
	
	/** The role id. */
	private int roleID;
	
	/** The role name. */
	private String roleName;
	
	/** The status. */
	private String status;
	
	/** The key question. */
	private String keyQuestion;
	
	/** The key answer. */
	private String keyAnswer;
	
	/** The email. */
	private String email;
	
	/** The name. */
	private String name;
	
	/** The surname. */
	private String surname;
	
	/** The patronymic. */
	private String patronymic;
	
	/** The country. */
	private String country;
	
	/** The address. */
	private String address;
	
	/** The telephone. */
	private String telephone;
	
	/** The mobile. */
	private String mobile;
	
	/**
	 * Instantiates a new user.
	 */
	public User() {}
	
	/**
	 * Instantiates a new user.
	 *
	 * @param id the id
	 * @param login the login
	 * @param password the password
	 * @param roleID the role id
	 * @param roleName the role name
	 * @param status the status
	 * @param keyQuestion the key question
	 * @param keyAnswer the key answer
	 * @param email the email
	 * @param name the name
	 * @param surname the surname
	 * @param patronymic the patronymic
	 * @param country the country
	 * @param address the address
	 * @param telephone the telephone
	 * @param mobile the mobile
	 */
	public User(int id, String login, String password, int roleID, String roleName, String status, String keyQuestion, 
			String keyAnswer, String email, String name, String surname, String patronymic, String country, String address, String telephone, String mobile) {
		this.id = id;
		this.login = login;
		this.password = password;
		this.roleID = roleID;
		this.roleName = roleName;
		this.status = status;
		this.keyQuestion = keyQuestion;
		this.keyAnswer = keyAnswer;
		this.email = email;
		this.name = name;
		this.surname = surname;
		this.patronymic = patronymic;
		this.country = country;
		this.address = address;
		this.telephone = telephone;
		this.mobile = mobile;
	}
	
	/**
	 * Instantiates a new user without setting id-field.
	 *
	 * @param login the login
	 * @param password the password
	 * @param roleID the role id
	 * @param roleName the role name
	 * @param status the status
	 * @param keyQuestion the key question
	 * @param keyAnswer the key answer
	 * @param email the email
	 * @param name the name
	 * @param surname the surname
	 * @param patronymic the patronymic
	 * @param country the country
	 * @param address the address
	 * @param telephone the telephone
	 * @param mobile the mobile
	 */
	public User(String login, String password, int roleID, String roleName, String status, String keyQuestion, 
			String keyAnswer, String email, String name, String surname, String patronymic, String country, String address, String telephone, String mobile) {
		this.login = login;
		this.password = password;
		this.roleID = roleID;
		this.roleName = roleName;
		this.status = status;
		this.keyQuestion = keyQuestion;
		this.keyAnswer = keyAnswer;
		this.email = email;
		this.name = name;
		this.surname = surname;
		this.patronymic = patronymic;
		this.country = country;
		this.address = address;
		this.telephone = telephone;
		this.mobile = mobile;
	}
	
	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * Gets the login.
	 *
	 * @return the login
	 */
	public String getLogin() {
		return login;
	}

	/**
	 * Gets the password.
	 *
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * Gets the role id.
	 *
	 * @return the role id
	 */
	public int getRoleID() {
		return roleID;
	}

	/**
	 * Gets the role name.
	 *
	 * @return the role name
	 */
	public String getRoleName() {
		return roleName;
	}

	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * Gets the key question.
	 *
	 * @return the key question
	 */
	public String getKeyQuestion() {
		return keyQuestion;
	}

	/**
	 * Gets the key answer.
	 *
	 * @return the key answer
	 */
	public String getKeyAnswer() {
		return keyAnswer;
	}

	/**
	 * Gets the email.
	 *
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Gets the surname.
	 *
	 * @return the surname
	 */
	public String getSurname() {
		return surname;
	}
	
	/**
	 * Gets the patronymic.
	 *
	 * @return the patronymic
	 */
	public String getPatronymic() {
		return patronymic;
	}

	/**
	 * Gets the country.
	 *
	 * @return the country
	 */
	public String getCountry() {
		return country;
	}

	/**
	 * Gets the address.
	 *
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * Gets the telephone.
	 *
	 * @return the telephone
	 */
	public String getTelephone() {
		return telephone;
	}

	/**
	 * Gets the mobile.
	 *
	 * @return the mobile
	 */
	public String getMobile() {
		return mobile;
	}

	/**
	 * Sets the login.
	 *
	 * @param login the new login
	 */
	public void setLogin(String login) {
		this.login = login;
	}

	/**
	 * Sets the password.
	 *
	 * @param password the new password
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * Sets the role id.
	 *
	 * @param roleID the new role id
	 */
	public void setRoleID(int roleID) {
		this.roleID = roleID;
	}

	/**
	 * Sets the role name.
	 *
	 * @param roleName the new role name
	 */
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	/**
	 * Sets the status.
	 *
	 * @param status the new status
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * Sets the key question.
	 *
	 * @param keyQuestion the new key question
	 */
	public void setKeyQuestion(String keyQuestion) {
		this.keyQuestion = keyQuestion;
	}

	/**
	 * Sets the key answer.
	 *
	 * @param keyAnswer the new key answer
	 */
	public void setKeyAnswer(String keyAnswer) {
		this.keyAnswer = keyAnswer;
	}

	/**
	 * Sets the email.
	 *
	 * @param email the new email
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Sets the surname.
	 *
	 * @param surname the new surname
	 */
	public void setSurname(String surname) {
		this.surname = surname;
	}
	
	/**
	 * Sets the patronymic.
	 *
	 * @param patronymic the new patronymic
	 */
	public void setPatronymic(String patronymic) {
		this.patronymic = patronymic;
	}

	/**
	 * Sets the country.
	 *
	 * @param country the new country
	 */
	public void setCountry(String country) {
		this.country = country;
	}

	/**
	 * Sets the address.
	 *
	 * @param address the new address
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * Sets the telephone.
	 *
	 * @param telephone the new telephone
	 */
	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	/**
	 * Sets the mobile.
	 *
	 * @param mobile the new mobile
	 */
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("User id: " + id);
		sb.append(" Login: " + login);
		sb.append(" Password: " + password);
		sb.append(" Role id: " + roleID);
		sb.append(" Role name: " + roleName);
		sb.append(" Status: " + status);
		sb.append(" Question: " + keyQuestion);
		sb.append(" Answer: " + keyAnswer);
		sb.append(" Email: " + email);
		sb.append(" Name: " + name);
		sb.append(" Surname: " + surname);
		sb.append(" Country: " + country);
		sb.append(" Address: " + address);
		sb.append(" Telephone: " + telephone);
		sb.append(" Mobile: " + mobile);
		return sb.toString();
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		
		result = prime * result + id;
		result = prime * result + ((login == null) ? 0 : login.hashCode());
		
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		
		if (this == obj) {
			return true;
		}
		
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		
		User other = (User) obj;
		
		if (id != other.id) {
			return false;
		}
		
		if (login == null) {
			if (other.login != null) {
				return false;
			}
		} else if (!login.equals(other.login)) {
			return false;
		}
		return true;
	}	
}
