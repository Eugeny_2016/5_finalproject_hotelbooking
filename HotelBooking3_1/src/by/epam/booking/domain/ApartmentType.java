package by.epam.booking.domain;

import java.util.Set;

/**
 * The Class ApartmentType.
 */
public class ApartmentType {
	
	/** The id. */
	private int id;
	
	/** The name. */
	private String name;
	
	/** The description. */
	private String description;
	
	/** The square. */
	private int square;
	
	/** The amount of rooms. */
	private int amountOfRooms;
	
	/** The amount of guests max. */
	private int amountOfGuestsMax;
	
	/** The price in dollars. */
	private double priceInDollars;
	
	/** The discount price in dollars. */
	private double discountPriceInDollars;
	
	/** The discount price date from. */
	private long discountPriceDateFrom;
	
	/** The discount price date until. */
	private long discountPriceDateUntil;
	
	/** The service set. */
	private Set<Service> serviceSet;
	
	/**
	 * Instantiates a new apartment type.
	 *
	 * @param id the id
	 * @param name the name
	 * @param description the description
	 * @param square the square
	 * @param amountOfRooms the amount of rooms
	 * @param amountOfGuestsMax the amount of guests max
	 * @param priceInDollars the price in dollars
	 * @param discountPriceInDollars the discount price in dollars
	 * @param discountPriceDateFrom the discount price date from
	 * @param discountPriceDateUntil the discount price date until
	 * @param serviceSet the service set
	 */
	public ApartmentType(int id, String name, String description, int square, int amountOfRooms, int amountOfGuestsMax,
			double priceInDollars, double discountPriceInDollars, long discountPriceDateFrom,
			long discountPriceDateUntil, Set<Service> serviceSet) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
		this.square = square;
		this.amountOfRooms = amountOfRooms;
		this.amountOfGuestsMax = amountOfGuestsMax;
		this.priceInDollars = priceInDollars;
		this.discountPriceInDollars = discountPriceInDollars;
		this.discountPriceDateFrom = discountPriceDateFrom;
		this.discountPriceDateUntil = discountPriceDateUntil;
		this.serviceSet = serviceSet;
	}
	
	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Gets the square.
	 *
	 * @return the square
	 */
	public int getSquare() {
		return square;
	}

	/**
	 * Gets the amount of rooms.
	 *
	 * @return the amount of rooms
	 */
	public int getAmountOfRooms() {
		return amountOfRooms;
	}

	/**
	 * Gets the amount of guests max.
	 *
	 * @return the amount of guests max
	 */
	public int getAmountOfGuestsMax() {
		return amountOfGuestsMax;
	}

	/**
	 * Gets the price in dollars.
	 *
	 * @return the price in dollars
	 */
	public double getPriceInDollars() {
		return priceInDollars;
	}

	/**
	 * Gets the discount price in dollars.
	 *
	 * @return the discount price in dollars
	 */
	public double getDiscountPriceInDollars() {
		return discountPriceInDollars;
	}

	/**
	 * Gets the discount price date from.
	 *
	 * @return the discount price date from
	 */
	public long getDiscountPriceDateFrom() {
		return discountPriceDateFrom;
	}

	/**
	 * Gets the discount price date until.
	 *
	 * @return the discount price date until
	 */
	public long getDiscountPriceDateUntil() {
		return discountPriceDateUntil;
	}

	/**
	 * Gets the service set.
	 *
	 * @return the service set
	 */
	public Set<Service> getServiceSet() {
		return serviceSet;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Sets the description.
	 *
	 * @param description the new description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Sets the square.
	 *
	 * @param square the new square
	 */
	public void setSquare(int square) {
		this.square = square;
	}

	/**
	 * Sets the amount of rooms.
	 *
	 * @param amountOfRooms the new amount of rooms
	 */
	public void setAmountOfRooms(int amountOfRooms) {
		this.amountOfRooms = amountOfRooms;
	}

	/**
	 * Sets the amount of guests max.
	 *
	 * @param amountOfGuestsMax the new amount of guests max
	 */
	public void setAmountOfGuestsMax(int amountOfGuestsMax) {
		this.amountOfGuestsMax = amountOfGuestsMax;
	}

	/**
	 * Sets the price in dollars.
	 *
	 * @param priceInDollars the new price in dollars
	 */
	public void setPriceInDollars(double priceInDollars) {
		this.priceInDollars = priceInDollars;
	}

	/**
	 * Sets the discount price in dollars.
	 *
	 * @param discountPriceInDollars the new discount price in dollars
	 */
	public void setDiscountPriceInDollars(double discountPriceInDollars) {
		this.discountPriceInDollars = discountPriceInDollars;
	}

	/**
	 * Sets the discount price date from.
	 *
	 * @param discountPriceDateFrom the new discount price date from
	 */
	public void setDiscountPriceDateFrom(long discountPriceDateFrom) {
		this.discountPriceDateFrom = discountPriceDateFrom;
	}

	/**
	 * Sets the discount price date until.
	 *
	 * @param discountPriceDateUntil the new discount price date until
	 */
	public void setDiscountPriceDateUntil(long discountPriceDateUntil) {
		this.discountPriceDateUntil = discountPriceDateUntil;
	}

	/**
	 * Sets the service set.
	 *
	 * @param serviceSet the new service set
	 */
	public void setServiceSet(Set<Service> serviceSet) {
		this.serviceSet = serviceSet;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Apartment type id: " + id);
		sb.append(" Apartment type name: " + name);
		sb.append(" Description: " + description);
		sb.append(" Square: " + square);
		sb.append(" Amount of rooms: " + amountOfRooms);
		sb.append(" Amount of guests: " + amountOfGuestsMax);
		sb.append(" Price in dollars: " + priceInDollars);
		sb.append(" Discount price in dollars: " + discountPriceInDollars);
		sb.append(" Discount price date from: " + discountPriceDateFrom);
		sb.append(" Discount price date until: " + discountPriceDateUntil);
		sb.append(" Service set: " + serviceSet);
		return sb.toString();
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		
		result = prime * result + id;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		long temp;
		temp = Double.doubleToLongBits(priceInDollars);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		
		if (this == obj) {
			return true;
		}
		
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		
		ApartmentType other = (ApartmentType) obj;
		
		if (id != other.id) {
			return false;
		}
		
		if (name == null) {
			if (other.name != null) {
				return false;
			}
		} else if (!name.equals(other.name)) {
			return false;
		}
		
		if (Double.doubleToLongBits(priceInDollars) != Double.doubleToLongBits(other.priceInDollars)) {
			return false;
		}
		return true;
	}
}
