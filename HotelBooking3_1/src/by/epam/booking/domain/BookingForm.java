package by.epam.booking.domain;

/**
 * The Class BookingForm.
 */
public class BookingForm {
	
	/** The id. */
	private int id;
	
	/** The user id. */
	private int userId;
	
	/** The arrival date. */
	private long arrivalDate;
	
	/** The leaving date. */
	private long leavingDate;
	
	/** The apartment type. */
	private ApartmentType apartmentType;
	
	/** The amount of guests. */
	private int amountOfGuests;
	
	/** The amount of apartments. */
	private int amountOfApartments;
	
	/** The total price. */
	private double totalPrice;
	
	/** The bed type. */
	private BedType bedType;
	
	/** The personal preferences. */
	private String personalPreferences;
	
	/** The card type. */
	private CardType cardType;
	
	/** The card number. */
	private String cardNumber;
	
	/** The card expire date. */
	private long cardExpireDate;
	
	/** The booking aproval sms. */
	private boolean bookingAprovalSms;
	
	/** The booking creation date. */
	private long bookingCreationDate;
	
	/** The status. */
	private String status;
	
	/** The forcibly closed flag. */
	private boolean forciblyClosedFlag;
	
	/**
	 * Instantiates a new booking form.
	 *
	 * @param id the id
	 * @param userId the user id
	 * @param arrivalDate the arrival date
	 * @param leavingDate the leaving date
	 * @param apartmentType the apartment type
	 * @param amountOfGuests the amount of guests
	 * @param amountOfApartments the amount of apartments
	 * @param totalPrice the total price
	 * @param bedType the bed type
	 * @param personalPreferences the personal preferences
	 * @param cardType the card type
	 * @param cardNumber the card number
	 * @param cardExpireDate the card expire date
	 * @param bookingAprovalSms the booking aproval sms
	 * @param bookingCreationDate the booking creation date
	 * @param status the status
	 * @param forciblyClosedFlag the forcibly closed flag
	 */
	public BookingForm(int id, int userId, long arrivalDate, long leavingDate,
			ApartmentType apartmentType, int amountOfGuests, int amountOfApartments, double totalPrice, 
			BedType bedType, String personalPreferences, CardType cardType, String cardNumber, long cardExpireDate, 
			boolean bookingAprovalSms, long bookingCreationDate, String status, boolean forciblyClosedFlag) {
		
		this.id = id;
		this.userId = userId;
		this.arrivalDate = arrivalDate;
		this.leavingDate = leavingDate;
		this.apartmentType = apartmentType;
		this.amountOfGuests = amountOfGuests;
		this.amountOfApartments = amountOfApartments;
		this.totalPrice = totalPrice;
		this.bedType = bedType;
		this.personalPreferences = personalPreferences;
		this.cardType = cardType;
		this.cardNumber = cardNumber;
		this.cardExpireDate = cardExpireDate;
		this.bookingAprovalSms = bookingAprovalSms;
		this.bookingCreationDate = bookingCreationDate;
		this.status = status;
		this.forciblyClosedFlag = forciblyClosedFlag;
	}
	
	/**
	 * Instantiates a new booking form.
	 *
	 * @param userId the user id
	 * @param arrivalDate the arrival date
	 * @param leavingDate the leaving date
	 * @param apartmentType the apartment type
	 * @param amountOfGuests the amount of guests
	 * @param amountOfApartments the amount of apartments
	 * @param totalPrice the total price
	 * @param bedType the bed type
	 * @param personalPreferences the personal preferences
	 * @param cardType the card type
	 * @param cardNumber the card number
	 * @param cardExpireDate the card expire date
	 * @param bookingAprovalSms the booking aproval sms
	 * @param bookingCreationDate the booking creation date
	 * @param status the status
	 * @param forciblyClosedFlag the forcibly closed flag
	 */
	public BookingForm(int userId, long arrivalDate, long leavingDate,
			ApartmentType apartmentType, int amountOfGuests, int amountOfApartments, double totalPrice, 
			BedType bedType, String personalPreferences, CardType cardType, String cardNumber, long cardExpireDate, 
			boolean bookingAprovalSms, long bookingCreationDate, String status, boolean forciblyClosedFlag) {
		
		this.userId = userId;
		this.arrivalDate = arrivalDate;
		this.leavingDate = leavingDate;
		this.apartmentType = apartmentType;
		this.amountOfGuests = amountOfGuests;
		this.amountOfApartments = amountOfApartments;
		this.totalPrice = totalPrice;
		this.bedType = bedType;
		this.personalPreferences = personalPreferences;
		this.cardType = cardType;
		this.cardNumber = cardNumber;
		this.cardExpireDate = cardExpireDate;
		this.bookingAprovalSms = bookingAprovalSms;
		this.bookingCreationDate = bookingCreationDate;
		this.status = status;
		this.forciblyClosedFlag = forciblyClosedFlag;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * Gets the user id.
	 *
	 * @return the user id
	 */
	public int getUserId() {
		return userId;
	}

	/**
	 * Gets the arrival date.
	 *
	 * @return the arrival date
	 */
	public long getArrivalDate() {
		return arrivalDate;
	}

	/**
	 * Gets the leaving date.
	 *
	 * @return the leaving date
	 */
	public long getLeavingDate() {
		return leavingDate;
	}

	/**
	 * Gets the apartment type.
	 *
	 * @return the apartment type
	 */
	public ApartmentType getApartmentType() {
		return apartmentType;
	}

	/**
	 * Gets the amount of guests.
	 *
	 * @return the amount of guests
	 */
	public int getAmountOfGuests() {
		return amountOfGuests;
	}

	/**
	 * Gets the amount of apartments.
	 *
	 * @return the amount of apartments
	 */
	public int getAmountOfApartments() {
		return amountOfApartments;
	}
	
	/**
	 * Gets the total price.
	 *
	 * @return the total price
	 */
	public double getTotalPrice() {
		return totalPrice;
	}

	/**
	 * Gets the bed type.
	 *
	 * @return the bed type
	 */
	public BedType getBedType() {
		return bedType;
	}

	/**
	 * Gets the personal preferences.
	 *
	 * @return the personal preferences
	 */
	public String getPersonalPreferences() {
		return personalPreferences;
	}

	/**
	 * Gets the card type.
	 *
	 * @return the card type
	 */
	public CardType getCardType() {
		return cardType;
	}

	/**
	 * Gets the card number.
	 *
	 * @return the card number
	 */
	public String getCardNumber() {
		return cardNumber;
	}

	/**
	 * Gets the card expire date.
	 *
	 * @return the card expire date
	 */
	public long getCardExpireDate() {
		return cardExpireDate;
	}

	/**
	 * Gets the booking aproval sms.
	 *
	 * @return the booking aproval sms
	 */
	public boolean getBookingAprovalSms() {
		return bookingAprovalSms;
	}

	/**
	 * Gets the booking creation date.
	 *
	 * @return the booking creation date
	 */
	public long getBookingCreationDate() {
		return bookingCreationDate;
	}
	
	/**
	 * Gets the status.
	 *
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}
	
	/**
	 * Gets the forcibly closed flag.
	 *
	 * @return the forcibly closed flag
	 */
	public boolean getForciblyClosedFlag() {
		return forciblyClosedFlag;
	}

	/**
	 * Sets the arrival date.
	 *
	 * @param arrivalDate the new arrival date
	 */
	public void setArrivalDate(long arrivalDate) {
		this.arrivalDate = arrivalDate;
	}

	/**
	 * Sets the leaving date.
	 *
	 * @param leavingDate the new leaving date
	 */
	public void setLeavingDate(long leavingDate) {
		this.leavingDate = leavingDate;
	}

	/**
	 * Sets the apartment type.
	 *
	 * @param apartmentType the new apartment type
	 */
	public void setApartmentType(ApartmentType apartmentType) {
		this.apartmentType = apartmentType;
	}

	/**
	 * Sets the amount of guests.
	 *
	 * @param amountOfGuests the new amount of guests
	 */
	public void setAmountOfGuests(int amountOfGuests) {
		this.amountOfGuests = amountOfGuests;
	}

	/**
	 * Sets the amount of apartments.
	 *
	 * @param amountOfApartments the new amount of apartments
	 */
	public void setAmountOfApartments(int amountOfApartments) {
		this.amountOfApartments = amountOfApartments;
	}
	
	/**
	 * Sets the total price.
	 *
	 * @param totalPrice the new total price
	 */
	public void setTotalPrice(double totalPrice) {
		this.totalPrice = totalPrice;
	}

	/**
	 * Sets the bed type.
	 *
	 * @param bedType the new bed type
	 */
	public void setBedType(BedType bedType) {
		this.bedType = bedType;
	}

	/**
	 * Sets the personal preferences.
	 *
	 * @param personalPreferences the new personal preferences
	 */
	public void setPersonalPreferences(String personalPreferences) {
		this.personalPreferences = personalPreferences;
	}

	/**
	 * Sets the card type.
	 *
	 * @param cardType the new card type
	 */
	public void setCardType(CardType cardType) {
		this.cardType = cardType;
	}

	/**
	 * Sets the card number.
	 *
	 * @param cardNumber the new card number
	 */
	public void setCardNumber(String cardNumber) {
		this.cardNumber = cardNumber;
	}

	/**
	 * Sets the card expire date.
	 *
	 * @param cardExpireDate the new card expire date
	 */
	public void setCardExpireDate(long cardExpireDate) {
		this.cardExpireDate = cardExpireDate;
	}

	/**
	 * Sets the booking aproval sms.
	 *
	 * @param bookingAprovalSms the new booking aproval sms
	 */
	public void setBookingAprovalSms(boolean bookingAprovalSms) {
		this.bookingAprovalSms = bookingAprovalSms;
	}

	/**
	 * Sets the booking creation date.
	 *
	 * @param bookingCreationDate the new booking creation date
	 */
	public void setBookingCreationDate(long bookingCreationDate) {
		this.bookingCreationDate = bookingCreationDate;
	}
	
	/**
	 * Sets the status.
	 *
	 * @param status the new status
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	
	/**
	 * Sets the forcibly closed flag.
	 *
	 * @param forciblyClosedFlag the new forcibly closed flag
	 */
	public void setForciblyClosedFlag(boolean forciblyClosedFlag) {
		this.forciblyClosedFlag = forciblyClosedFlag;
	}
	
	@Override
	public String toString() {
		StringBuilder str = new StringBuilder();
		str.append("Booking form id: ").append(id);
		str.append(" User id: ").append(userId);
		str.append(" Arrival date: ").append(arrivalDate);
		str.append(" Leaving date: ").append(leavingDate);
		str.append(" Apartment type: ").append(apartmentType);
		str.append(" Amount of guests: ").append(amountOfGuests);
		str.append(" Amount of apartments: ").append(amountOfApartments);
		str.append(" Total price: ").append(totalPrice);
		str.append(" Bed type: ").append(bedType);
		str.append(" Personal preferences: ").append(personalPreferences);
		str.append(" Card type: ").append(cardType);
		str.append(" Card number: ").append(cardNumber);
		str.append(" Card expire date: ").append(cardExpireDate);
		str.append(" Booking approval sms: ").append(bookingAprovalSms);
		str.append(" Booking creation date: ").append(bookingCreationDate);
		str.append(" Status: ").append(status);
		str.append(" Forcibly closed flag: ").append(forciblyClosedFlag);
		return str.toString();
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		
		result = prime * result + id;
		result = prime * result + (int) (bookingCreationDate ^ (bookingCreationDate >>> 32));
								
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		
		if (this == obj) {
			return true;
		}
		
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		
		BookingForm other = (BookingForm) obj;
		
		if (id != other.id) {
			return false;
		}
		
		if (bookingCreationDate != other.bookingCreationDate) {
			return false;
		}	
		
		return true;
	}
}
