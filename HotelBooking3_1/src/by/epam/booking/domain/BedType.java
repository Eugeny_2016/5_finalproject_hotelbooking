package by.epam.booking.domain;

/**
 * The Class BedType.
 */
public class BedType {
	
	/** The id. */
	private int id;
	
	/** The name. */
	private String name;
	
	/** The description. */
	private String description;
	
	/** The amount of persons. */
	private int amountOfPersons;
	
	/**
	 * Instantiates a new bed type.
	 *
	 * @param id the id
	 * @param name the name
	 * @param description the description
	 * @param amountOfPersons the amount of persons
	 */
	public BedType(int id, String name, String description, int amountOfPersons) {
		this.id = id;
		this.name = name;
		this.description = description;
		this.amountOfPersons = amountOfPersons;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Gets the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	
	/**
	 * Gets the amount of persons.
	 *
	 * @return the amount of persons
	 */
	public int getAmountOfPersons() {
		return amountOfPersons;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Sets the description.
	 *
	 * @param description the new description
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	
	/**
	 * Sets the amount of persons.
	 *
	 * @param amountOfPersons the new amount of persons
	 */
	public void setAmountOfPersons(int amountOfPersons) {
		this.amountOfPersons = amountOfPersons;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Bed type id: " + id);
		sb.append(" Bed type name: " + name);
		sb.append(" Description: " + description);
		sb.append(" Amount of guests: " + amountOfPersons);
		return sb.toString();
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		
		result = prime * result + id;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + amountOfPersons;	
		
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		
		if (this == obj) {
			return true;
		}
		
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		
		BedType other = (BedType) obj;
		
		if (id != other.id) {
			return false;
		}
		
		if (name == null) {
			if (other.name != null) {
				return false;
			}
		} else if (!name.equals(other.name)) {
			return false;
		}
		
		if (amountOfPersons != other.amountOfPersons) {
			return false;
		}		
		return true;
	}	
}
