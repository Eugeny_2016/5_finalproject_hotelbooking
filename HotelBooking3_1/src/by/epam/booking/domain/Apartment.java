package by.epam.booking.domain;

/**
 * The Class Apartment.
 */
public class Apartment {
	
	/** The id. */
	private int id;
	
	/** The number. */
	private int number;
	
	/** The apartment type. */
	private ApartmentType apartmentType;
	
	/** The floor. */
	private int floor;
	
	/** The wing. */
	private int wing;
	
	/**
	 * Instantiates a new apartment.
	 *
	 * @param id the id
	 * @param number the number
	 * @param apartmentType the apartment type
	 * @param floor the floor
	 * @param wing the wing
	 */
	public Apartment(int id, int number, ApartmentType apartmentType, int floor, int wing) {
		
		this.id = id;
		this.number = number;
		this.apartmentType = apartmentType;
		this.floor = floor;
		this.wing = wing;
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * Gets the number.
	 *
	 * @return the number
	 */
	public int getNumber() {
		return number;
	}

	/**
	 * Gets the apartment type.
	 *
	 * @return the apartment type
	 */
	public ApartmentType getApartmentType() {
		return apartmentType;
	}

	/**
	 * Gets the floor.
	 *
	 * @return the floor
	 */
	public int getFloor() {
		return floor;
	}

	/**
	 * Gets the wing.
	 *
	 * @return the wing
	 */
	public int getWing() {
		return wing;
	}

	/**
	 * Sets the number.
	 *
	 * @param number the new number
	 */
	public void setNumber(int number) {
		this.number = number;
	}

	/**
	 * Sets the apartment type.
	 *
	 * @param apartmentType the new apartment type
	 */
	public void setApartmentType(ApartmentType apartmentType) {
		this.apartmentType = apartmentType;
	}

	/**
	 * Sets the floor.
	 *
	 * @param floor the new floor
	 */
	public void setFloor(int floor) {
		this.floor = floor;
	}

	/**
	 * Sets the wing.
	 *
	 * @param wing the new wing
	 */
	public void setWing(int wing) {
		this.wing = wing;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Apartment id: " + id);
		sb.append(" Apartment number: " + number);
		sb.append(" Apartment type: " + apartmentType);
		sb.append(" Floor: " + floor);
		sb.append(" Wing: " + wing);
		return sb.toString();
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		
		result = prime * result + id;
		result = prime * result + number;
		
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		
		if (this == obj) {
			return true;
		}
		
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		
		Apartment other = (Apartment) obj;
		
		if (id != other.id) {
			return false;
		}
		
		if (number != other.number) {
			return false;
		}
		return true;
	}	
}
