package by.epam.booking.domain;

/**
 * The Class Bill.
 */
public class Bill {
	
	/** The id. */
	private int id;
	
	/** The booking form id. */
	private int bookingFormId;
	
	/** The total price. */
	private double totalPrice;
	
	/**
	 * Instantiates a new bill.
	 *
	 * @param bookingFormId the booking form id
	 * @param totalPrice the total price
	 */
	public Bill(int bookingFormId, double totalPrice) {
		this.bookingFormId = bookingFormId;
		this.totalPrice = totalPrice;		
	}
	
	/**
	 * Instantiates a new bill.
	 *
	 * @param id the id
	 * @param bookingFormId the booking form id
	 * @param totalSum the total sum
	 */
	public Bill(int id, int bookingFormId, double totalSum) {
		this.id = id;
		this.bookingFormId = bookingFormId;
		this.totalPrice = totalSum;		
	}

	/**
	 * Gets the id.
	 *
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * Gets the booking form id.
	 *
	 * @return the booking form id
	 */
	public int getBookingFormId() {
		return bookingFormId;
	}

	/**
	 * Gets the total price.
	 *
	 * @return the total price
	 */
	public double getTotalPrice() {
		return totalPrice;
	}

	/**
	 * Sets the booking form id.
	 *
	 * @param bookingFormId the new booking form id
	 */
	public void setBookingFormId(int bookingFormId) {
		this.bookingFormId = bookingFormId;
	}

	/**
	 * Sets the total price.
	 *
	 * @param totalPrice the new total price
	 */
	public void setTotalPrice(double totalPrice) {
		this.totalPrice = totalPrice;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Bill id: " + id);
		sb.append(" Booking form id: " + bookingFormId);
		sb.append(" Total price: " + totalPrice);
		return sb.toString();
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		
		result = prime * result + id;
		result = prime * result + bookingFormId;
		long temp;
		temp = Double.doubleToLongBits(totalPrice);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		
		if (this == obj) {
			return true;
		}
		
		if (this.getClass() != obj.getClass()) {
			return false;
		}
		
		Bill other = (Bill) obj;
		
		if (id != other.id) {
			return false;
		}
		
		if (bookingFormId != other.bookingFormId) {
			return false;
		}
		
		if (Double.doubleToLongBits(totalPrice) != Double.doubleToLongBits(other.totalPrice)) {
			return false;
		}
		return true;
	}
}
