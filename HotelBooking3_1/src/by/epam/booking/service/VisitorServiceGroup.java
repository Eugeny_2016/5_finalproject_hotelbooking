package by.epam.booking.service;

import java.util.List;
import java.util.Map;

import by.epam.booking.dao.DAOException;
import by.epam.booking.dao.SqlDaoFactory;
import by.epam.booking.dao.SqlDaoType;
import by.epam.booking.dao.VisitorDAO;

/**
 * The Class VisitorServiceGroup.
 */
public class VisitorServiceGroup {
	
	/** The Constant INSTANCE. */
	private static final VisitorServiceGroup INSTANCE = new VisitorServiceGroup();
	
	/**
	 * Instantiates a new visitor service group.
	 */
	private VisitorServiceGroup() {} 

	/**
	 * Gets the single instance of VisitorServiceGroup.
	 *
	 * @return single instance of VisitorServiceGroup
	 */
	public static VisitorServiceGroup getInstance() {
		return INSTANCE;
	}
	
	/**
	 * Gets the resource.
	 *
	 * @param resourceName the resource name
	 * @param language the language
	 * @return the resource
	 * @throws ServiceException the service exception
	 */
	public String getResource(String resourceName, String language) throws ServiceException {
		VisitorDAO dao = null;
		SqlDaoType type = SqlDaoType.VISITOR;
		try {
			dao = (VisitorDAO) SqlDaoFactory.getInstance().getSqlDao(type);
			return dao.selectResource(resourceName, language);
		} catch (DAOException ex) {
			throw new ServiceException("Error in obtaining dao ", ex);
		}
	}
	
	/**
	 * Gets the resource.
	 *
	 * @param resourceName the resource name
	 * @return the resource
	 * @throws ServiceException the service exception
	 */
	public Map<String, String> getResource(String resourceName) throws ServiceException {
		VisitorDAO dao = null;
		SqlDaoType type = SqlDaoType.VISITOR;
		try {
			dao = (VisitorDAO) SqlDaoFactory.getInstance().getSqlDao(type);
			return dao.selectResource(resourceName);
		} catch (DAOException ex) {
			throw new ServiceException("Error in obtaining dao ", ex);
		}
	}
	
	/**
	 * Gets the currency name.
	 *
	 * @param countryName the country name
	 * @return the currency name
	 * @throws ServiceException the service exception
	 */
	public String getCurrencyName(String countryName) throws ServiceException {
		VisitorDAO dao = null;
		SqlDaoType type = SqlDaoType.VISITOR;
		try {
			dao = (VisitorDAO) SqlDaoFactory.getInstance().getSqlDao(type);
			return dao.selectCurrencyName(countryName);
		} catch (DAOException ex) {
			throw new ServiceException("Error in obtaining dao ", ex);
		}
	}

	/**
	 * Gets the dollar coefficient.
	 *
	 * @param countryName the country name
	 * @return the dollar coefficient
	 * @throws ServiceException the service exception
	 */
	public float getDollarCoefficient(String countryName) throws ServiceException {
		VisitorDAO dao = null;
		SqlDaoType type = SqlDaoType.VISITOR;
		try {
			dao = (VisitorDAO) SqlDaoFactory.getInstance().getSqlDao(type);
			return dao.selectDollarCoefficient(countryName);
		} catch (DAOException ex) {
			throw new ServiceException("Error in obtaining dao ", ex);
		}
	}
		
	/**
	 * Gets the apartment prices.
	 *
	 * @return the apartment prices
	 * @throws ServiceException the service exception
	 */
	public List<Double> getApartmentPrices() throws ServiceException {
		VisitorDAO dao = null;
		SqlDaoType type = SqlDaoType.VISITOR;
		try {
			dao = (VisitorDAO) SqlDaoFactory.getInstance().getSqlDao(type);
			return dao.selectApartmentPrices();
		} catch (DAOException ex) {
			throw new ServiceException("Error in obtaining dao ", ex);
		}
	}
	
}
