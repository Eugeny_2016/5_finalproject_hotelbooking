package by.epam.booking.service;

import by.epam.booking.dao.DAOException;
import by.epam.booking.dao.SqlDaoFactory;
import by.epam.booking.dao.UserDAO;
import by.epam.booking.dao.SqlDaoType;
import by.epam.booking.domain.User;
import by.epam.booking.domain.UserRole;

/**
 * The Class UserServiceGroup.
 */
public class UserServiceGroup {
	
	/** The Constant INSTANCE. */
	private static final UserServiceGroup INSTANCE = new UserServiceGroup();
	
	/**
	 * Instantiates a new user service group.
	 */
	private UserServiceGroup() {} 

	/**
	 * Gets the single instance of UserServiceGroup.
	 *
	 * @return single instance of UserServiceGroup
	 */
	public static UserServiceGroup getInstance() {
		return INSTANCE;
	}
	
	/**
	 * Check user.
	 *
	 * @param login the login
	 * @param password the password
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	public boolean checkUser(String login, String password) throws ServiceException {
		UserDAO dao = null;
		SqlDaoType type = SqlDaoType.USER;
		try {
			dao = (UserDAO) SqlDaoFactory.getInstance().getSqlDao(type);
			return dao.checkUser(login, password);
		} catch (DAOException ex) {
			throw new ServiceException("Error in obtaining dao ", ex);
		}
	}
	
	/**
	 * Check user.
	 *
	 * @param login the login
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	public boolean checkUser(String login) throws ServiceException {
		UserDAO dao = null;
		SqlDaoType type = SqlDaoType.USER;
		try {
			dao = (UserDAO) SqlDaoFactory.getInstance().getSqlDao(type);
			return dao.checkUser(login);
		} catch (DAOException ex) {
			throw new ServiceException("Error in obtaining dao ", ex);
		}
	}
	
	/**
	 * Gets the user.
	 *
	 * @param login the login
	 * @param password the password
	 * @return the user
	 * @throws ServiceException the service exception
	 */
	public User getUser(String login, String password) throws ServiceException {
		UserDAO dao = null;
		SqlDaoType type = SqlDaoType.USER;
		try {
			dao = (UserDAO) SqlDaoFactory.getInstance().getSqlDao(type);
			return dao.obtainUser(login, password);			
		} catch (DAOException ex) {
			throw new ServiceException("Error in obtaining dao ", ex);
		}
	}
	
	/**
	 * Gets the role information.
	 *
	 * @param name the name
	 * @return the role information
	 * @throws ServiceException the service exception
	 */
	public UserRole getRoleInformation(String name) throws ServiceException {
		UserDAO dao = null;
		SqlDaoType type = SqlDaoType.USER;
		try {
			dao = (UserDAO) SqlDaoFactory.getInstance().getSqlDao(type);
			return dao.obtainRoleInformation(name);
		} catch (DAOException ex) {
			throw new ServiceException("Error in obtaining dao ", ex);
		}
	}
	
	/**
	 * Adds the user.
	 *
	 * @param user the user
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	public boolean addUser(User user) throws ServiceException {
		UserDAO dao = null;
		try {
			dao = (UserDAO) SqlDaoFactory.getInstance().getSqlDao(SqlDaoType.USER);
			return dao.addUser(user);
		} catch (DAOException ex) {
			throw new ServiceException("Error in obtaining dao ", ex);
		}
	}

}
