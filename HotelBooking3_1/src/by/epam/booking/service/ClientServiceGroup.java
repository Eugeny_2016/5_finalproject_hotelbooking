package by.epam.booking.service;

import java.util.List;
import java.util.Map;
import java.util.Set;

import by.epam.booking.command.impl.CommandDenialReasonName;
import by.epam.booking.dao.ClientDAO;
import by.epam.booking.dao.DAOException;
import by.epam.booking.dao.SqlDaoFactory;
import by.epam.booking.dao.SqlDaoType;
import by.epam.booking.domain.ApartmentType;
import by.epam.booking.domain.BedType;
import by.epam.booking.domain.BookingForm;
import by.epam.booking.domain.CardType;

/**
 * The Class ClientServiceGroup.
 */
public class ClientServiceGroup {
	
	/** The Constant INSTANCE. */
	private static final ClientServiceGroup INSTANCE = new ClientServiceGroup();
	
	/**
	 * Instantiates a new client service group.
	 */
	private ClientServiceGroup() {} 

	/**
	 * Gets the single instance of ClientServiceGroup.
	 *
	 * @return single instance of ClientServiceGroup
	 */
	public static ClientServiceGroup getInstance() {
		return INSTANCE;
	}
	
	/**
	 * Check apartment on date.
	 *
	 * @param dateFrom the date from
	 * @param dateUntil the date until
	 * @return the map
	 * @throws ServiceException the service exception
	 */
	public Map<ApartmentType, List<Integer>> checkApartmentOnDate(long dateFrom, long dateUntil) throws ServiceException {
		ClientDAO dao = null;
		SqlDaoType type = SqlDaoType.CLIENT;
		try {
			dao = (ClientDAO) SqlDaoFactory.getInstance().getSqlDao(type);
			return dao.selectApartmentOnDate(dateFrom, dateUntil);			
		} catch (DAOException ex) {
			throw new ServiceException("Error in obtaining dao. ", ex);
		}
	}
	
	/**
	 * Save booking form.
	 *
	 * @param dateFrom the date from
	 * @param dateUntil the date until
	 * @param bookingForm the booking form
	 * @return the map
	 * @throws ServiceException the service exception
	 */
	public Map<Boolean, CommandDenialReasonName> saveBookingForm(long dateFrom, long dateUntil, BookingForm bookingForm) throws ServiceException {
		ClientDAO dao = null;
		SqlDaoType type = SqlDaoType.CLIENT;
		try {
			dao = (ClientDAO) SqlDaoFactory.getInstance().getSqlDao(type);
			return dao.saveBookingForm(dateFrom, dateUntil, bookingForm);
		} catch (DAOException ex) {
			throw new ServiceException("Error in obtaining dao. ", ex);
		}
	}
	
	/**
	 * View all bookings.
	 *
	 * @param userId the user id
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<BookingForm> viewAllBookings(int userId) throws ServiceException {
		ClientDAO dao = null;
		SqlDaoType type = SqlDaoType.CLIENT;
		try {
			dao = (ClientDAO) SqlDaoFactory.getInstance().getSqlDao(type);
			return dao.selectAllBookingForms(userId);
		} catch (DAOException ex) {
			throw new ServiceException("Error in obtaining dao. ", ex);
		}
	}
	
	/**
	 * View all temporary bookings.
	 *
	 * @param userId the user id
	 * @param status the status
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<BookingForm> viewAllTemporaryBookings(int userId, String status) throws ServiceException {
		ClientDAO dao = null;
		SqlDaoType type = SqlDaoType.CLIENT;
		try {
			dao = (ClientDAO) SqlDaoFactory.getInstance().getSqlDao(type);
			return dao.selectAllTemporaryBookingForms(userId, status);
		} catch (DAOException ex) {
			throw new ServiceException("Error in obtaining dao. ", ex);
		}
	}
	
	/**
	 * Cancel bookings.
	 *
	 * @param bookingId the booking id
	 * @param status the status
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	public boolean cancelBookings(int[] bookingId, String status) throws ServiceException {
		ClientDAO dao = null;
		SqlDaoType type = SqlDaoType.CLIENT;
		try {
			dao = (ClientDAO) SqlDaoFactory.getInstance().getSqlDao(type);
			return dao.cancelBookingForm(bookingId, status);
		} catch (DAOException ex) {
			throw new ServiceException("Error in obtaining dao. ", ex);
		}
	}
	
	/**
	 * Gets the apartment type.
	 *
	 * @param typeId the type id
	 * @return the apartment type
	 * @throws ServiceException the service exception
	 */
	public ApartmentType getApartmentType(int typeId) throws ServiceException {
		ClientDAO dao = null;
		SqlDaoType type = SqlDaoType.CLIENT;
		
		try {
			dao = (ClientDAO) SqlDaoFactory.getInstance().getSqlDao(type);
			return dao.selectApartmentTypeById(typeId);
		} catch (DAOException ex) {
			throw new ServiceException("Error in obtaining dao. ", ex);
		} 
	}
	
	/**
	 * Gets the card types.
	 *
	 * @return the card types
	 * @throws ServiceException the service exception
	 */
	public Set<CardType> getCardTypes() throws ServiceException {
		ClientDAO dao = null;
		SqlDaoType type = SqlDaoType.CLIENT;
		
		try {
			dao = (ClientDAO) SqlDaoFactory.getInstance().getSqlDao(type);
			return dao.selectAllCardTypes();
		} catch (DAOException ex) {
			throw new ServiceException("Error in obtaining dao. ", ex);
		} 
	}
	
	/**
	 * Gets the bed types.
	 *
	 * @return the bed types
	 * @throws ServiceException the service exception
	 */
	public Set<BedType> getBedTypes() throws ServiceException {
		ClientDAO dao = null;
		SqlDaoType type = SqlDaoType.CLIENT;
		
		try {
			dao = (ClientDAO) SqlDaoFactory.getInstance().getSqlDao(type);
			return dao.selectAllBedTypes();
		} catch (DAOException ex) {
			throw new ServiceException("Error in obtaining dao. ", ex);
		} 
	}
}
