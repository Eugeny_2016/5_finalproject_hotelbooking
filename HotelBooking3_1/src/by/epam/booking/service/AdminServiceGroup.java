package by.epam.booking.service;

import java.util.List;

import by.epam.booking.dao.AdminDAO;
import by.epam.booking.dao.DAOException;
import by.epam.booking.dao.SqlDaoFactory;
import by.epam.booking.dao.SqlDaoType;
import by.epam.booking.domain.Bill;
import by.epam.booking.domain.BookingForm;
import by.epam.booking.domain.User;

/**
 * The Class AdminServiceGroup.
 */
public class AdminServiceGroup {
	
	/** The Constant INSTANCE. */
	private static final AdminServiceGroup INSTANCE = new AdminServiceGroup();
	
	/**
	 * Instantiates a new admin service group.
	 */
	private AdminServiceGroup() {} 

	/**
	 * Gets the single instance of AdminServiceGroup.
	 *
	 * @return single instance of AdminServiceGroup
	 */
	public static AdminServiceGroup getInstance() {
		return INSTANCE;
	}
	
	/**
	 * View client temporary bookings.
	 *
	 * @param name the name
	 * @param surname the surname
	 * @param email the email
	 * @param mobile the mobile
	 * @param status the status
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<BookingForm> viewClientTemporaryBookings(String name, String surname, String email, String mobile, String status) throws ServiceException {
		AdminDAO dao = null;
		SqlDaoType type = SqlDaoType.ADMIN;
		try {
			dao = (AdminDAO) SqlDaoFactory.getInstance().getSqlDao(type);
			return dao.selectAllTemporaryBookingForms(name, surname, email, mobile, status);
		} catch (DAOException ex) {
			throw new ServiceException("Error in obtaining dao. ", ex);
		}
	}
	
	/**
	 * View client confirmed bookings.
	 *
	 * @param name the name
	 * @param surname the surname
	 * @param email the email
	 * @param mobile the mobile
	 * @param status the status
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<BookingForm> viewClientConfirmedBookings(String name, String surname, String email, String mobile, String status) throws ServiceException {
		AdminDAO dao = null;
		SqlDaoType type = SqlDaoType.ADMIN;
		try {
			dao = (AdminDAO) SqlDaoFactory.getInstance().getSqlDao(type);
			return dao.selectAllConfirmedBookingForms(name, surname, email, mobile, status);
		} catch (DAOException ex) {
			throw new ServiceException("Error in obtaining dao. ", ex);
		}
	}
	
	/**
	 * View client closed bookings.
	 *
	 * @param name the name
	 * @param surname the surname
	 * @param email the email
	 * @param mobile the mobile
	 * @param forciblyClosedFlag the forcibly closed flag
	 * @param status the status
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<BookingForm> viewClientClosedBookings(String name, String surname, String email, String mobile, boolean forciblyClosedFlag, String status) throws ServiceException {
		AdminDAO dao = null;
		SqlDaoType type = SqlDaoType.ADMIN;
		try {
			dao = (AdminDAO) SqlDaoFactory.getInstance().getSqlDao(type);
			return dao.selectAllClosedBookingForms(name, surname, email, mobile, forciblyClosedFlag, status);
		} catch (DAOException ex) {
			throw new ServiceException("Error in obtaining dao. ", ex);
		}
	}
	
	/**
	 * View all client bookings.
	 *
	 * @param name the name
	 * @param surname the surname
	 * @param email the email
	 * @param mobile the mobile
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<BookingForm> viewAllClientBookings(String name, String surname, String email, String mobile) throws ServiceException {
		AdminDAO dao = null;
		SqlDaoType type = SqlDaoType.ADMIN;
		try {
			dao = (AdminDAO) SqlDaoFactory.getInstance().getSqlDao(type);
			return dao.selectAllBookingForms(name, surname, email, mobile);
		} catch (DAOException ex) {
			throw new ServiceException("Error in obtaining dao. ", ex);
		}
	}
	
	/**
	 * View all unconfirmed bookings.
	 *
	 * @param status the status
	 * @param arrivalDateTime the arrival date time
	 * @return the list
	 * @throws ServiceException the service exception
	 */
	public List<BookingForm> viewAllUnconfirmedBookings(String status, long arrivalDateTime) throws ServiceException {
		AdminDAO dao = null;
		SqlDaoType type = SqlDaoType.ADMIN;
		try {
			dao = (AdminDAO) SqlDaoFactory.getInstance().getSqlDao(type);			
			return dao.selectAllUnconfirmedBookingForms(status, arrivalDateTime);
		} catch (DAOException ex) {
			throw new ServiceException("Error in obtaining dao. ", ex);
		}
	}
	
	/**
	 * Confirm bookings.
	 *
	 * @param bookingId the booking id
	 * @param status the status
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	public boolean confirmBookings(int[] bookingId, String status) throws ServiceException {
		AdminDAO dao = null;
		SqlDaoType type = SqlDaoType.ADMIN;
		try {
			dao = (AdminDAO) SqlDaoFactory.getInstance().getSqlDao(type);
			return dao.confirmBookingForm(bookingId, status);
		} catch (DAOException ex) {
			throw new ServiceException("Error in obtaining dao. ", ex);
		}
	}
	
	/**
	 * Close bookings.
	 *
	 * @param bookingId the booking id
	 * @param status the status
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	public boolean closeBookings(int[] bookingId, String status) throws ServiceException {
		AdminDAO dao = null;
		SqlDaoType type = SqlDaoType.ADMIN;
		try {
			dao = (AdminDAO) SqlDaoFactory.getInstance().getSqlDao(type);
			return dao.closeBookingForm(bookingId, status);
		} catch (DAOException ex) {
			throw new ServiceException("Error in obtaining dao. ", ex);
		}
	}
	
	/**
	 * Forcibly close bookings.
	 *
	 * @param bookingId the booking id
	 * @param status the status
	 * @param forciblyCloseFlag the forcibly close flag
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	public boolean forciblyCloseBookings(int[] bookingId, String status, boolean forciblyCloseFlag) throws ServiceException {
		AdminDAO dao = null;
		SqlDaoType type = SqlDaoType.ADMIN;
		try {
			dao = (AdminDAO) SqlDaoFactory.getInstance().getSqlDao(type);
			return dao.forciblyCloseBookingForm(bookingId, status, forciblyCloseFlag);
		} catch (DAOException ex) {
			throw new ServiceException("Error in obtaining dao. ", ex);
		}
	}
	
	/**
	 * Gets the user of booking.
	 *
	 * @param userId the user id
	 * @return the user of booking
	 * @throws ServiceException the service exception
	 */
	public User getUserOfBooking(int userId) throws ServiceException {
		AdminDAO dao = null;
		SqlDaoType type = SqlDaoType.ADMIN;
		try {
			dao = (AdminDAO) SqlDaoFactory.getInstance().getSqlDao(type);
			return dao.obtainUserOfBookingInformation(userId);
		} catch (DAOException ex) {
			throw new ServiceException("Error in obtaining dao. ", ex);
		}
	}
	
	/**
	 * Save bill.
	 *
	 * @param bill the bill
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	public boolean saveBill(Bill bill) throws ServiceException {
		AdminDAO dao = null;
		SqlDaoType type = SqlDaoType.ADMIN;
		try {
			dao = (AdminDAO) SqlDaoFactory.getInstance().getSqlDao(type);
			return dao.saveBill(bill);
		} catch (DAOException ex) {
			throw new ServiceException("Error in obtaining dao. ", ex);
		}
	}
	
	/**
	 * Check bill existance.
	 *
	 * @param bookingFormId the booking form id
	 * @return true, if successful
	 * @throws ServiceException the service exception
	 */
	public boolean checkBillExistance(int bookingFormId) throws ServiceException {
		AdminDAO dao = null;
		SqlDaoType type = SqlDaoType.ADMIN;
		try {
			dao = (AdminDAO) SqlDaoFactory.getInstance().getSqlDao(type);
			return dao.checkBillExistance(bookingFormId);
		} catch (DAOException ex) {
			throw new ServiceException("Error in obtaining dao. ", ex);
		}
	}
}
