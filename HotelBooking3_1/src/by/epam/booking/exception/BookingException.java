package by.epam.booking.exception;

import javax.servlet.ServletException;

/**
 * The Class BookingException.
 */
public class BookingException extends ServletException {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * Instantiates a new booking exception.
	 */
	public BookingException() {
		super();
	}

	/**
	 * Instantiates a new booking exception.
	 *
	 * @param msg the msg
	 * @param ex the ex
	 */
	public BookingException(String msg, Throwable ex) {
		super(msg, ex);
	}

	/**
	 * Instantiates a new booking exception.
	 *
	 * @param msg the msg
	 */
	public BookingException(String msg) {
		super(msg);
	}

	/**
	 * Instantiates a new booking exception.
	 *
	 * @param ex the ex
	 */
	public BookingException(Throwable ex) {
		super(ex);
	}

}
