package by.epam.booking.utility;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.apache.log4j.Logger;

/**
 * The Class EncryptionUtility.
 */
public class EncryptionUtility {
	
	/** The Constant LOG. */
	private final static Logger LOG = Logger.getLogger(EncryptionUtility.class);
	
	/** The Constant MD5_ALGORITHM. */
	private static final String MD5_ALGORITHM = "MD5";
	
	/**
	 * Encrypt password.
	 *
	 * @param password the password
	 * @return the string
	 */
	public static String encryptPasswordWithMD5(String password) {
		String encryptedPassword = null;
		try {
		
			MessageDigest messageDigest = MessageDigest.getInstance(MD5_ALGORITHM);
			byte[] bytesArray = password.getBytes();
			
			messageDigest.update(bytesArray, 0, password.length());
			
			BigInteger  bigInteger = new BigInteger(1, messageDigest.digest());
			encryptedPassword = bigInteger.toString(16);
		
			} catch (NoSuchAlgorithmException ex) {
				LOG.error("Password hasn't been encrypted. " + ex.getMessage());
			}		
		return encryptedPassword;
	}
}
