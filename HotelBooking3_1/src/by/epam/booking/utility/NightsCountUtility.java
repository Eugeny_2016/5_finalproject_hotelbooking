package by.epam.booking.utility;

/**
 * The Class NightsCountUtility.
 */
public class NightsCountUtility {
	
	/** The Constant MINIMAL_NIGHT_AMOUNT. */
	private static final int MINIMAL_NIGHT_AMOUNT = 1;
	
	/** The Constant DAY_MILLISECONDS_AMOUNT. */
	private static final int DAY_MILLISECONDS_AMOUNT = 86_400_000;
	
	/** The Constant DAY_TIME_DIFFERENCE_MILLISECONDS_AMOUNT. */
	private static final int DAY_TIME_DIFFERENCE_MILLISECONDS_AMOUNT = 7_200_000;
	
	/**
	 * Count nights.
	 *
	 * @param fristDay the frist day
	 * @param lastDay the last day
	 * @return the int
	 */
	public static int countNights(long fristDay, long lastDay) {
		
		int result = (int) ((lastDay - fristDay) + DAY_TIME_DIFFERENCE_MILLISECONDS_AMOUNT) / DAY_MILLISECONDS_AMOUNT;
		
		if (result > 0) {
			return result;
		} else {
			return MINIMAL_NIGHT_AMOUNT;
		}
	}
}
