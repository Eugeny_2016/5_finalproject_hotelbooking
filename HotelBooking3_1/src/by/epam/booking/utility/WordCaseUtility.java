package by.epam.booking.utility;

/**
 * The Class WordCaseUtility.
 */
public class WordCaseUtility {
	
	/** The Constant NIGHT_WORD_SINGULAR_NOMINATIVE_CASE. */
	private static final String NIGHT_WORD_SINGULAR_NOMINATIVE_CASE = "singular_nom";
	
	/** The Constant NIGHT_WORD_SINGULAR_PARENT_CASE. */
	private static final String NIGHT_WORD_SINGULAR_PARENT_CASE = "singular_par";
	
	/** The Constant NIGHT_WORD_PLURAL_PARENT_CASE. */
	private static final String NIGHT_WORD_PLURAL_PARENT_CASE = "plural_par";
	
	/**
	 * Gets the night clause.
	 *
	 * @param amountOfNights the amount of nights
	 * @return the night clause
	 */
	public static String getNightClause(int amountOfNights) {
		String nightCase = null;
		int ostatok100 = amountOfNights % 100;
		int ostatok10 = amountOfNights % 10;
		
		if (ostatok100 == 11 || ostatok100 == 12 || ostatok100 == 13 || ostatok100 == 14) {
			nightCase = NIGHT_WORD_PLURAL_PARENT_CASE;
		} else {
			switch (ostatok10) {
			case 1:
				nightCase = NIGHT_WORD_SINGULAR_NOMINATIVE_CASE;
				break;
			case 2:
			case 3:
			case 4:
				nightCase = NIGHT_WORD_SINGULAR_PARENT_CASE;
				break;
			default:
				nightCase = NIGHT_WORD_PLURAL_PARENT_CASE;
			}
		}
		return nightCase;
	}
}
