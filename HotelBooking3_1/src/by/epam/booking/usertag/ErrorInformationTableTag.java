package by.epam.booking.usertag;

import java.io.IOException;
import java.util.Queue;

import javax.servlet.jsp.JspTagException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

/**
 * The Class ErrorInformationTableTag.
 */
@SuppressWarnings("rawtypes")
public class ErrorInformationTableTag extends TagSupport {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	/** The headers. */
	private Queue headers;
	
	/** The parameters. */
	private Queue parameters;
	
	/** The size. */
	private int size;

	/**
	 * Sets the headers.
	 *
	 * @param headers the new headers
	 */
	public void setHeaders (Queue headers) {
		this.headers = headers;
	}
	
	/**
	 * Sets the parameters.
	 *
	 * @param parameters the new parameters
	 */
	public void setParameters (Queue parameters) {
		this.parameters = parameters;
	}
	
	/**
	 * Sets the size.
	 *
	 * @param size the new size
	 */
	public void setSize (int size) {
		this.size = size;
	}
	
	/* (non-Javadoc)
	 * @see javax.servlet.jsp.tagext.TagSupport#doStartTag()
	 */
	@Override
	public int doStartTag() throws JspTagException {
		try {
			JspWriter out = pageContext.getOut();
			out.write("<table><tr>");
			for (int i = 0; i < 2; i++) {
				out.write("<th>" + headers.poll() + "</th>");
			}
			out.write("</tr><tr><td>");			
		} catch (IOException ex) {
			throw new JspTagException(ex.getMessage(), ex);
		}		
		return EVAL_BODY_INCLUDE;
	}

	/* (non-Javadoc)
	 * @see javax.servlet.jsp.tagext.TagSupport#doAfterBody()
	 */
	@Override
	public int doAfterBody() throws JspTagException {
		if (size > 1) {
			try {
				pageContext.getOut().write("</td><td>" + parameters.poll().toString() + "</td></tr><tr><td>");
				size--;
			} catch (IOException ex) {
				throw new JspTagException(ex.getMessage(), ex);
			}
			return EVAL_BODY_AGAIN;
		} else if (size == 1) {
			try {
				pageContext.getOut().write("</td><td>" + parameters.poll().toString() + "</td></tr>");
			} catch (IOException ex) {
				throw new JspTagException(ex.getMessage(), ex);
			}
			return SKIP_BODY;
		} else {
			return SKIP_BODY;
		}				
	}

	/* (non-Javadoc)
	 * @see javax.servlet.jsp.tagext.TagSupport#doEndTag()
	 */
	@Override
	public int doEndTag() throws JspTagException {
		try {
			pageContext.getOut().write("</table>");
		} catch (IOException ex) {
			throw new JspTagException(ex.getMessage(), ex);
		}
		return EVAL_PAGE;
	}
}
