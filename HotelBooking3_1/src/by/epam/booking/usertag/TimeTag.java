package by.epam.booking.usertag;

import java.io.IOException;
import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.log4j.Logger;

/**
 * The Class TimeTag.
 */
public class TimeTag extends TagSupport {
	
	/** The Constant LOG. */
	@SuppressWarnings("unused")
	private final static Logger LOG = Logger.getLogger(TimeTag.class);
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/* (non-Javadoc)
	 * @see javax.servlet.jsp.tagext.TagSupport#doStartTag()
	 */
	@Override
	public int doStartTag() throws JspException {
		Locale locale = (Locale) pageContext.getSession().getAttribute("locale");
		DateFormat df = DateFormat.getDateTimeInstance(DateFormat.FULL, DateFormat.SHORT, locale);
		Date localTimeDate = new Date();
				
		try {
			JspWriter out = pageContext.getOut();
			out.write("<b id=\"time\">" + df.format(localTimeDate) + "</b>");
		} catch (IOException ex) {
			throw new JspException(ex.getMessage());
		}		
		return SKIP_BODY;
	}

	/* (non-Javadoc)
	 * @see javax.servlet.jsp.tagext.TagSupport#doEndTag()
	 */
	@Override
	public int doEndTag() throws JspException {
		return EVAL_BODY_AGAIN;
	}
}
