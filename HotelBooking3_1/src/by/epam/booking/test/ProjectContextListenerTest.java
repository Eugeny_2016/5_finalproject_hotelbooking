package by.epam.booking.test;

import org.junit.Test;

import java.sql.Connection;

import javax.servlet.ServletContextEvent;

import org.junit.Assert;
import org.junit.BeforeClass;

import by.epam.booking.listener.ProjectContextListener;
import by.epam.booking.pool.ConnectionPool;
import by.epam.booking.pool.ConnectionPoolException;

/**
 * The Class ProjectContextListenerTest.
 */
public class ProjectContextListenerTest {
	
	/** The project context listener. */
	private static ProjectContextListener projectContextListener;
	
	/**
	 * Inits the project context listener.
	 */
	@BeforeClass
	public static void initProjectContextListener() {
		projectContextListener = new ProjectContextListener();
	}
	
	/**
	 * Minimally filled connection pool test.
	 */
	@SuppressWarnings("unused")
	@Test															//  (expected = RuntimeException.class) убрав последний блок try/catch и прописав ожидаемое исключение
	public void minimallyFilledConnectionPoolTest() {				//  можно проверить генерацию исключения при неуспешной инициализации пула
		ConnectionPool pool = ConnectionPool.getInstance();
		
		try {
			pool.initPoolData();
			if (!pool.isFilled()) {
				Assert.fail("Pool hasn't been filled enough");
			}
		} catch (ConnectionPoolException ex) {
			Assert.fail("Pool hasn't been initialized");
		}
		
		try {
			Connection conection1 = pool.takeConnection();			// берём ровно столько соединений из пула, чтобы там осталось минимальное достаточное количество соединений
			Connection conection2 = pool.takeConnection();
			Connection conection3 = pool.takeConnection();
		  //Connection conection4 = pool.takeConnection();			// раскомментировав эту строку, можно проверить сценарий недостаочного наполнения пула
		} catch (InterruptedException ex) {
			Assert.fail("Error in taking connection");
		}
		
		ServletContextEvent contextEvent = null;
		try {
			projectContextListener.contextInitialized(contextEvent);
		} catch (RuntimeException ex) {
			Assert.fail("Context listener didn't accept a half filled pool");
		}
	}
}
