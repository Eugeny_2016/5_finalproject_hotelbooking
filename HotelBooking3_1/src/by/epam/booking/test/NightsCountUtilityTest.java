package by.epam.booking.test;

import org.junit.Test;

import java.util.Calendar;

import org.junit.Assert;

import by.epam.booking.utility.NightsCountUtility;

/**
 * The Class NightsCountUtilityTest.
 */
public class NightsCountUtilityTest {
	
	/** The Constant PM1400_TIME_MILLISECONDS_AMOUNT. */
	private static final int PM1400_TIME_MILLISECONDS_AMOUNT = 50_400_000;
	
	/** The Constant AM1200_TIME_MILLISECONDS_AMOUNT. */
	private static final int AM1200_TIME_MILLISECONDS_AMOUNT = 43_200_000;
	
	/** The Constant DAY_MILLISECONDS_AMOUNT. */
	private static final int DAY_MILLISECONDS_AMOUNT = 86_400_000;
	
	/**
	 * Count one night test.
	 */
	@Test
	public void countOneNightTest() {
		Calendar today = Calendar.getInstance();
		today.set(Calendar.HOUR_OF_DAY, 0);
		today.set(Calendar.MINUTE, 0);
		today.set(Calendar.SECOND, 0);
		today.set(Calendar.MILLISECOND, 0);
				
		Calendar yesterday = Calendar.getInstance();
		yesterday.setTimeInMillis(today.getTimeInMillis() - DAY_MILLISECONDS_AMOUNT);
				
		int expected = 1;
		int actual = NightsCountUtility.countNights(yesterday.getTimeInMillis() + PM1400_TIME_MILLISECONDS_AMOUNT, today.getTimeInMillis() + AM1200_TIME_MILLISECONDS_AMOUNT);
		Assert.assertEquals("Amount of nights is wrong", expected, actual, 0.01);
	}
	
	/**
	 * Count two nights test.
	 */
	@Test
	public void countTwoNightsTest() {
		Calendar today = Calendar.getInstance();
		today.set(Calendar.HOUR_OF_DAY, 0);
		today.set(Calendar.MINUTE, 0);
		today.set(Calendar.SECOND, 0);
		today.set(Calendar.MILLISECOND, 0);
				
		Calendar yesterday = Calendar.getInstance();
		yesterday.setTimeInMillis(today.getTimeInMillis() - DAY_MILLISECONDS_AMOUNT * 2);
				
		int expected = 2;
		int actual = NightsCountUtility.countNights(yesterday.getTimeInMillis() + PM1400_TIME_MILLISECONDS_AMOUNT, today.getTimeInMillis() + AM1200_TIME_MILLISECONDS_AMOUNT);
		Assert.assertEquals("Amount of nights is wrong", expected, actual, 0.01);
	}
	
	/**
	 * Count zero nights test.
	 */
	@Test
	public void countZeroNightsTest() {
		Calendar today = Calendar.getInstance();
		today.set(Calendar.HOUR_OF_DAY, 0);
		today.set(Calendar.MINUTE, 0);
		today.set(Calendar.SECOND, 0);
		today.set(Calendar.MILLISECOND, 0);
						
		int expected = 1;
		int actual = NightsCountUtility.countNights(today.getTimeInMillis() + PM1400_TIME_MILLISECONDS_AMOUNT, today.getTimeInMillis() + AM1200_TIME_MILLISECONDS_AMOUNT);
		Assert.assertEquals("Amount of nights is wrong", expected, actual, 0.01);
	}
}