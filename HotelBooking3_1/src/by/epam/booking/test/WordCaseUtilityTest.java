package by.epam.booking.test;

import org.junit.Test;

import org.junit.Assert;

import by.epam.booking.utility.WordCaseUtility;

/**
 * The Class WordCaseUtilityTest.
 */
public class WordCaseUtilityTest {
		
	/** The Constant NIGHT_WORD_SINGULAR_NOMINATIVE_CASE. */
	private static final String NIGHT_WORD_SINGULAR_NOMINATIVE_CASE = "singular_nom";
	
	/** The Constant NIGHT_WORD_SINGULAR_PARENT_CASE. */
	private static final String NIGHT_WORD_SINGULAR_PARENT_CASE = "singular_par";
	
	/** The Constant NIGHT_WORD_PLURAL_PARENT_CASE. */
	private static final String NIGHT_WORD_PLURAL_PARENT_CASE = "plural_par";
	
	/**
	 * Gets the night clause for12 test.
	 * 
	 */
	@Test
	public void getNightClauseFor12Test() {
		String expected = NIGHT_WORD_PLURAL_PARENT_CASE;
		String actual = WordCaseUtility.getNightClause(12);
		Assert.assertEquals("Night clause for 10 is wrong", expected, actual);
	}
	
	/**
	 * Gets the night clause for3 test.
	 *
	 */
	@Test
	public void getNightClauseFor3Test() {
		String expected = NIGHT_WORD_SINGULAR_PARENT_CASE;
		String actual = WordCaseUtility.getNightClause(3);
		Assert.assertEquals("Night clause for 10 is wrong", expected, actual);
	}
	
	/**
	 * Gets the night clause for1 test.
	 *
	 */
	@Test
	public void getNightClauseFor1Test() {
		String expected = NIGHT_WORD_SINGULAR_NOMINATIVE_CASE;
		String actual = WordCaseUtility.getNightClause(1);
		Assert.assertEquals("Night clause for 10 is wrong", expected, actual);
	}
	
	/**
	 * Gets the night clause for126 test.
	 *
	 */
	@Test
	public void getNightClauseFor126Test() {
		String expected = NIGHT_WORD_PLURAL_PARENT_CASE;
		String actual = WordCaseUtility.getNightClause(126);
		Assert.assertEquals("Night clause for 10 is wrong", expected, actual);
	}
}
