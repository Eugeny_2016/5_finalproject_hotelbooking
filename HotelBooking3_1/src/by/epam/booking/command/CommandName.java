package by.epam.booking.command;

/**
 * The Enum CommandName.
 */
public enum CommandName {
	
	/** The authorization. */
	AUTHORIZATION,
	
	/** The registration. */
	REGISTRATION, 
	
	/** The booking. */
	BOOKING,
	
	/** The log out. */
	LOG_OUT,
	
	/** The confirm booking. */
	CONFIRM_BOOKING,
	
	/** The confirm registration. */
	CONFIRM_REGISTRATION,
	
	/** The localization. */
	LOCALIZATION,
	
	/** The check on date. */
	CHECK_ON_DATE,
	
	/** The view client bookings. */
	VIEW_CLIENT_BOOKINGS,
	
	/** The cancel client booking. */
	CANCEL_CLIENT_BOOKING,
	
	/** The confirm bookings canceling. */
	CONFIRM_BOOKINGS_CANCELING, 
	
	/** The view booking agreement. */
	VIEW_BOOKING_AGREEMENT, 
	
	/** The view contacts. */
	VIEW_CONTACTS, 
	
	/** The home. */
	HOME, 
	
	/** The search page. */
	SEARCH_PAGE,
	
	/** The confirm status booking. */
	CONFIRM_STATUS_BOOKING, 
	
	/** The close status booking. */
	CLOSE_STATUS_BOOKING, 
	
	/** The view all unconfirmed bookings. */
	VIEW_ALL_UNCONFIRMED_BOOKINGS, 
	
	/** The forcibly close bookings. */
	FORCIBLY_CLOSE_BOOKINGS, 
	
	/** The create bill. */
	CREATE_BILL, 
	
	/** The save bill. */
	SAVE_BILL, 
	
	/** The search for bookings. */
	SEARCH_FOR_BOOKINGS
}
