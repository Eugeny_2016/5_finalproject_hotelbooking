package by.epam.booking.command;

/**
 * The Enum CommandType.
 */
public enum CommandType {
	
	/** The requires db changing. */
	REQUIRES_DB_CHANGING,
	
	/** The without db changing. */
	WITHOUT_DB_CHANGING	
	
}
