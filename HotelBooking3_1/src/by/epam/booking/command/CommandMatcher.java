package by.epam.booking.command;

import java.util.HashMap;
import java.util.Map;

import by.epam.booking.command.impl.AuthorizationCommand;
import by.epam.booking.command.impl.BookingCommand;
import by.epam.booking.command.impl.CancelClientBookingCommand;
import by.epam.booking.command.impl.CheckOnDateCommand;
import by.epam.booking.command.impl.CloseStatusBookingCommand;
import by.epam.booking.command.impl.ConfirmBookingCommand;
import by.epam.booking.command.impl.ConfirmBookingsCancelingCommand;
import by.epam.booking.command.impl.ConfirmRegistrationCommand;
import by.epam.booking.command.impl.ConfirmStatusBookingCommand;
import by.epam.booking.command.impl.CreateBillCommand;
import by.epam.booking.command.impl.ForciblyCloseBookingsCommand;
import by.epam.booking.command.impl.HomePageCommand;
import by.epam.booking.command.impl.LocalizationCommand;
import by.epam.booking.command.impl.LogOutCommand;
import by.epam.booking.command.impl.RegistrationCommand;
import by.epam.booking.command.impl.SaveBillCommand;
import by.epam.booking.command.impl.SearchForBookingsCommand;
import by.epam.booking.command.impl.SearchPageCommand;
import by.epam.booking.command.impl.ViewAllUnconfirmedBookingsCommand;
import by.epam.booking.command.impl.ViewBookingAgreementCommand;
import by.epam.booking.command.impl.ViewClientBookingsCommand;
import by.epam.booking.command.impl.ViewContactsCommand;

/**
 * The Class CommandMatcher. This singleton class encapsulates a table for matching command names and instances of each command class
 */
public final class CommandMatcher {
	
	/** The Constant INSTANCE. */
	private static final CommandMatcher INSTANCE = new CommandMatcher(); 
	
	/** The table. */
	private Map<CommandName, Command> table = new HashMap<CommandName, Command>();
	
	/**
	 * Instantiates a new command matcher. Fills the table with entries for all existing command classes 
	 */
	private CommandMatcher() {
		table.put(CommandName.AUTHORIZATION, AuthorizationCommand.getInstance());
		table.put(CommandName.LOG_OUT, LogOutCommand.getInstance());
		table.put(CommandName.REGISTRATION, RegistrationCommand.getInstance());
		table.put(CommandName.CONFIRM_REGISTRATION, ConfirmRegistrationCommand.getInstance());
		table.put(CommandName.LOCALIZATION, LocalizationCommand.getInstance());
		table.put(CommandName.CHECK_ON_DATE, CheckOnDateCommand.getInstance());
		table.put(CommandName.BOOKING, BookingCommand.getInstance());
		table.put(CommandName.CONFIRM_BOOKING, ConfirmBookingCommand.getInstance());
		table.put(CommandName.VIEW_CLIENT_BOOKINGS, ViewClientBookingsCommand.getInstance());
		table.put(CommandName.CANCEL_CLIENT_BOOKING, CancelClientBookingCommand.getInstance());
		table.put(CommandName.CONFIRM_BOOKINGS_CANCELING, ConfirmBookingsCancelingCommand.getInstance());
		table.put(CommandName.VIEW_BOOKING_AGREEMENT, ViewBookingAgreementCommand.getInstance());
		table.put(CommandName.VIEW_CONTACTS, ViewContactsCommand.getInstance());
		table.put(CommandName.HOME, HomePageCommand.getInstance());
		table.put(CommandName.SEARCH_PAGE, SearchPageCommand.getInstance());
		table.put(CommandName.CONFIRM_STATUS_BOOKING, ConfirmStatusBookingCommand.getInstance());
		table.put(CommandName.CLOSE_STATUS_BOOKING, CloseStatusBookingCommand.getInstance());
		table.put(CommandName.VIEW_ALL_UNCONFIRMED_BOOKINGS, ViewAllUnconfirmedBookingsCommand.getInstance());
		table.put(CommandName.FORCIBLY_CLOSE_BOOKINGS, ForciblyCloseBookingsCommand.getInstance());
		table.put(CommandName.CREATE_BILL, CreateBillCommand.getInstance());
		table.put(CommandName.SAVE_BILL, SaveBillCommand.getInstance());
		table.put(CommandName.SEARCH_FOR_BOOKINGS, SearchForBookingsCommand.getInstance());
	}
	
	/**
	 * Gets the single instance of CommandMatcher.
	 *
	 * @return single instance of CommandMatcher
	 */
	public static CommandMatcher getInstance() {
		return INSTANCE;
	}
	
	/**
	 * Gets the command instance by the received command type.
	 *
	 * @param key the key
	 * @return the command
	 */
	public Command getCommand(CommandName key) {
		return table.get(key);
	}

}
