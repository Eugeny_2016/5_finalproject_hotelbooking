package by.epam.booking.command;

public enum RequiredRoleName {
	
	ADMINISTRATOR, CLIENT, NONE 
}
