package by.epam.booking.command.impl;


import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import by.epam.booking.command.Command;
import by.epam.booking.command.CommandException;
import by.epam.booking.command.CommandType;
import by.epam.booking.command.JspPageName;
import by.epam.booking.command.RequiredRoleName;
import by.epam.booking.service.ServiceException;
import by.epam.booking.service.VisitorServiceGroup;

/**
 * The Class HomePageCommand.
 */
public class HomePageCommand implements Command {
	
	/** The Constant LOG. */
	private static final Logger LOG = Logger.getLogger(HomePageCommand.class);
	
	/** The Constant INSTANCE. */
	private static final HomePageCommand INSTANCE = new HomePageCommand();
		
	/** The Constant RESOURCE_NAME_REQUEST_PARAMETER. */
	private static final String RESOURCE_NAME_REQUEST_PARAMETER = "resource-name";
	
	/** The type. */
	private CommandType type;
	
	/** The required role name for the command. */
	private RequiredRoleName requiredRoleName;
		
	/**
	 * Instantiates a new home page command.
	 */
	private HomePageCommand() {
		type = CommandType.WITHOUT_DB_CHANGING;
		requiredRoleName = RequiredRoleName.NONE; 
	}
	
	/**
	 * Gets the single instance of HomePageCommand.
	 *
	 * @return single instance of HomePageCommand
	 */
	public static HomePageCommand getInstance() {
		return INSTANCE;
	}
	
	/* (non-Javadoc)
	 * @see by.epam.booking.command.Command#getType()
	 */
	@Override
	public CommandType getCommandType() {
		return type;
	}
	
	/* (non-Javadoc)
	 * @see by.epam.booking.command.Command#getRequiredRoleName()
	 */
	@Override
	public RequiredRoleName getRequiredRoleName() {
		return requiredRoleName;
	}
	
	/* (non-Javadoc)
	 * @see by.epam.booking.command.Command#execute(javax.servlet.http.HttpServletRequest)
	 */
	@Override
	public String execute(HttpServletRequest request) throws CommandException {
		String page = null;
		String resource = null;
		String resourceName = null;
		
		resourceName = request.getParameter(RESOURCE_NAME_REQUEST_PARAMETER);
		Locale locale = (Locale) request.getSession().getAttribute("locale");
		String language = locale.getLanguage();
		LOG.debug(resourceName);
		
		try {
			resource = VisitorServiceGroup.getInstance().getResource(resourceName, language);

			page = JspPageName.INDEX_PAGE;
			request.getSession().setAttribute("lastAccessedPage", page);
			request.getSession().setAttribute("lastRequiredResource", resource);
			request.getSession().setAttribute("lastRequiredResourceName", resourceName);
		} catch (ServiceException ex) {
			page = JspPageName.INDEX_PAGE;
			LOG.error("Error in getting information about the hotel from DB on chosen language");
		} finally {
			request.getSession(false).setAttribute("bookingMapToView", null);								// обнуляем все важные атрибуты в сессии
			request.getSession(false).setAttribute("bookingMapToCancel", null);
			request.getSession(false).setAttribute("bookingMapToClose", null);
			request.getSession(false).setAttribute("bookingMapToConfirm", null);
			request.getSession(false).setAttribute("bookingMapToCancel", null);
			request.getSession(false).setAttribute("bookingMapToConfirm", null);
			request.getSession(false).setAttribute("bookingMapToClose", null);
			request.getSession(false).setAttribute("bookingMapToView", null);
			request.getSession(false).setAttribute("bookingMapToCreateBill", null);
			request.getSession(false).setAttribute("forciblyClosedFlag", null);
			request.getSession(false).setAttribute("tableOfAvailableApartmentsAndTypes", null);
			request.getSession(false).setAttribute("regLogin", null);
			request.getSession(false).setAttribute("regPassword", null);
			request.getSession(false).setAttribute("regPasswordAgain", null);
			request.getSession(false).setAttribute("keyQuestion", null);
			request.getSession(false).setAttribute("keyAnswer", null);
			request.getSession(false).setAttribute("regEmail", null);
			request.getSession(false).setAttribute("regName", null);
	        request.getSession(false).setAttribute("regSurname", null);
	        request.getSession(false).setAttribute("regPatronymic", null);
	        request.getSession(false).setAttribute("regCountry", null);
	        request.getSession(false).setAttribute("regAddress", null);
	        request.getSession(false).setAttribute("regPhone", null);
	        request.getSession(false).setAttribute("regMobile", null);
	        request.getSession(false).setAttribute("enteredArrivalDateAsString", null);
	        request.getSession(false).setAttribute("enteredLeavingDateAsString", null);
		}
		return page;
	}
}
