package by.epam.booking.command.impl;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import by.epam.booking.command.Command;
import by.epam.booking.command.CommandException;
import by.epam.booking.command.CommandType;
import by.epam.booking.command.JspPageName;
import by.epam.booking.command.RequiredRoleName;
import by.epam.booking.domain.User;
import by.epam.booking.service.ServiceException;
import by.epam.booking.service.UserServiceGroup;
import by.epam.booking.utility.EncryptionUtility;

/**
 * The Class AuthorizationCommand.
 */
public class AuthorizationCommand implements Command {
	
	/** The Constant LOG. */
	private final static Logger LOG = Logger.getLogger(AuthorizationCommand.class);
	
	/** The Constant INSTANCE. */
	private static final AuthorizationCommand INSTANCE = new AuthorizationCommand();
	
	/** The Constant LOGIN_REQUEST_PARAMETER. */
	private static final String LOGIN_REQUEST_PARAMETER = "login";
	
	/** The Constant PASSWORD_REQUEST_PARAMETER. */
	private static final String PASSWORD_REQUEST_PARAMETER = "password";
	
	/** The command type. */
	private CommandType commandType;
	
	/** The required role name for the command. */
	private RequiredRoleName requiredRoleName;
	
	/**
	 * Instantiates a new authorization command.
	 */
	private AuthorizationCommand() {
		commandType = CommandType.WITHOUT_DB_CHANGING;
		requiredRoleName = RequiredRoleName.NONE;
	}
	
	/**
	 * Gets the single instance of AuthorizationCommand.
	 *
	 * @return single instance of AuthorizationCommand
	 */
	public static AuthorizationCommand getInstance() {
		return INSTANCE;
	}
	
	/* (non-Javadoc)
	 * @see by.epam.booking.command.Command#getCommandType()
	 */
	@Override
	public CommandType getCommandType() {
		return commandType;
	}
	
	/* (non-Javadoc)
	 * @see by.epam.booking.command.Command#getRequiredRoleName()
	 */
	@Override
	public RequiredRoleName getRequiredRoleName() {
		return requiredRoleName;
	}
	
	/* (non-Javadoc)
	 * @see by.epam.booking.command.Command#execute(javax.servlet.http.HttpServletRequest)
	 */
	@Override
	public String execute(HttpServletRequest request) throws CommandException {
		boolean result = false;
		String login = null;
		String password = null;
		String encryptedPassword = null;
		String page = null;
		User user = null;
		
		if (request.getAttribute("pageFromLoggingFilter") != null) {									// если фильтр на входные пользовательские данные сработал, то не ищем пользователя,
			page = request.getAttribute("pageFromLoggingFilter").toString();							// а оставляем его на главной странице с выдачей сообщения
			request.setAttribute("incorrectDataFlag", "true");
			request.getSession().setAttribute("lastAccessedPage", page);
			return page;
		}
		
		login = request.getParameter(LOGIN_REQUEST_PARAMETER);
		password = request.getParameter(PASSWORD_REQUEST_PARAMETER);
		encryptedPassword = EncryptionUtility.encryptPasswordWithMD5(password);
		
		try {
			result = UserServiceGroup.getInstance().checkUser(login, encryptedPassword);
			if (result) {
				user = UserServiceGroup.getInstance().getUser(login, encryptedPassword);
				if (user != null) {
					request.getSession().setAttribute("user", user);
					request.getSession().setAttribute("userRoleId", user.getRoleID());
					request.getSession().setAttribute("userRoleName", RequiredRoleName.valueOf(user.getRoleName().toUpperCase()));
				} else {
					LOG.error("Error in obtaining user from DB");
				}
				page = JspPageName.INDEX_PAGE;
				request.getSession().setAttribute("lastAccessedPage", page);
			} else {
				page = JspPageName.INDEX_PAGE;
				request.setAttribute("incorrectDataFlag", "true");
				request.getSession().setAttribute("lastAccessedPage", page);
			}
		} catch (ServiceException ex) {
			throw new CommandException("Unable to check user's existance. ", ex);  
		}
		return page;
	}
}
