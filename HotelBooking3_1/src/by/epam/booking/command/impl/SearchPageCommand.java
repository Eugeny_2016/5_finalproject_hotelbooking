package by.epam.booking.command.impl;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import by.epam.booking.command.Command;
import by.epam.booking.command.CommandException;
import by.epam.booking.command.CommandType;
import by.epam.booking.command.JspPageName;
import by.epam.booking.command.RequiredRoleName;

/**
 * The Class SearchPageCommand.
 */
public class SearchPageCommand implements Command {
	
	/** The Constant LOG. */
	private static final Logger LOG = Logger.getLogger(SearchPageCommand.class);
	
	/** The Constant INSTANCE. */
	private static final SearchPageCommand INSTANCE = new SearchPageCommand();
	
	/** The Constant FUTURE_COMMAND_REQUEST_PARAMETER. */
	private static final String FUTURE_COMMAND_REQUEST_PARAMETER = "future-command";
	
	/** The type. */
	private CommandType type;
	
	/** The required role name for the command. */
	private RequiredRoleName requiredRoleName;
		
	/**
	 * Instantiates a new search page command.
	 */
	private SearchPageCommand() {
		type = CommandType.WITHOUT_DB_CHANGING;
		requiredRoleName = RequiredRoleName.ADMINISTRATOR;
	}
	
	/**
	 * Gets the single instance of SearchPageCommand.
	 *
	 * @return single instance of SearchPageCommand
	 */
	public static SearchPageCommand getInstance() {
		return INSTANCE;
	}
	
	/* (non-Javadoc)
	 * @see by.epam.booking.command.Command#getType()
	 */
	@Override
	public CommandType getCommandType() {
		return type;
	}
	
	/* (non-Javadoc)
	 * @see by.epam.booking.command.Command#getRequiredRoleName()
	 */
	@Override
	public RequiredRoleName getRequiredRoleName() {
		return requiredRoleName;
	}
	
	/* (non-Javadoc)
	 * @see by.epam.booking.command.Command#execute(javax.servlet.http.HttpServletRequest)
	 */
	@Override
	public String execute(HttpServletRequest request) throws CommandException {
		String page = null;
		
		if (request.getSession(false) == null) {														// проверка, не закончилась ли сессия
			page = JspPageName.INDEX_PAGE;
			request.setAttribute("mustAuthorizeAgainFlag", "true");
			return page;
		}
		
		if (!requiredRoleName.equals(request.getSession(false).getAttribute("userRoleName"))) {			// если пользователь вручную набирает в адресной строке команду не для своей роли
			LOG.warn("User is trying to use a command not of his role");
			page = JspPageName.INDEX_PAGE;
			return page;
		}
		
		String futureCommandName = request.getParameter(FUTURE_COMMAND_REQUEST_PARAMETER);
		request.getSession(false).setAttribute("futureCommandName", futureCommandName);
		
		return JspPageName.SEARCH_PAGE;
	}
}
