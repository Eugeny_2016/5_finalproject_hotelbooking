package by.epam.booking.command.impl;

import java.text.NumberFormat;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import by.epam.booking.command.Command;
import by.epam.booking.command.CommandException;
import by.epam.booking.command.CommandType;
import by.epam.booking.command.JspPageName;
import by.epam.booking.command.RequiredRoleName;
import by.epam.booking.domain.ApartmentType;
import by.epam.booking.service.ClientServiceGroup;
import by.epam.booking.service.ServiceException;
import by.epam.booking.utility.NightsCountUtility;
import by.epam.booking.utility.WordCaseUtility;

/**
 * The Class CheckOnDateCommand.
 */
public class CheckOnDateCommand implements Command {

	/** The Constant LOG. */
	@SuppressWarnings("unused")
	private final static Logger LOG = Logger.getLogger(CheckOnDateCommand.class);
	
	/** The Constant INSTANCE. */
	private static final CheckOnDateCommand INSTANCE = new CheckOnDateCommand();
			
	/** The type. */
	private CommandType type;
	
	/** The required role name for the command. */
	private RequiredRoleName requiredRoleName;
		
	/**
	 * Instantiates a new check on date command.
	 */
	private CheckOnDateCommand() {
		type = CommandType.WITHOUT_DB_CHANGING;
		requiredRoleName = RequiredRoleName.NONE;
	}
	
	/**
	 * Gets the single instance of CheckOnDateCommand.
	 *
	 * @return single instance of CheckOnDateCommand
	 */
	public static CheckOnDateCommand getInstance() {
		return INSTANCE;
	}
	
	/* (non-Javadoc)
	 * @see by.epam.booking.command.Command#getType()
	 */
	public CommandType getCommandType() {
		return type;
	}
	
	/* (non-Javadoc)
	 * @see by.epam.booking.command.Command#getRequiredRoleName()
	 */
	@Override
	public RequiredRoleName getRequiredRoleName() {
		return requiredRoleName;
	}
	
	/* (non-Javadoc)
	 * @see by.epam.booking.command.Command#execute(javax.servlet.http.HttpServletRequest)
	 */
	@Override
	public String execute(HttpServletRequest request) throws CommandException {
		Map<ApartmentType, List<Integer>> tableOfAvailableApartmentsAndTypes = null;
		int amountOfNights = 0;
		Date dateFrom = null;
		Date dateUntil = null;
		String page = null;
		String nightCase = null;
			
		if (request.getSession(false).getAttribute("user") == null) {									// проверка, не закончилась ли сессия (нетрадиционным способом)
			page = JspPageName.INDEX_PAGE;																// провевряем именно через наличие пользователя в сессии
			request.getSession(false).invalidate();														// т.к. сессия будет в любом случае создана в фильтре. По этой же причине делаем invalidate()
			request.setAttribute("notAuthorizedFlag", "true");
			request.setAttribute("emptyDateValuesFlag", null);
			request.setAttribute("incorrectDateValuesFlag", null);
			request.setAttribute("incorrectDateFormatFlag", null);
			return page;
		} 
		
		if (request.getAttribute("pageFromDateFilter") != null) {										// не забраковал ли фильтр даты, введённые клиентом
			page = request.getAttribute("pageFromDateFilter").toString();
			request.getSession(false).setAttribute("lastAccessedPage", page);
			return page;
		}
		
		dateFrom = (Date) request.getSession(false).getAttribute("enteredArrivalDate");
		dateUntil = (Date) request.getSession(false).getAttribute("enteredLeavingDate");
				
		tableOfAvailableApartmentsAndTypes = new TreeMap<ApartmentType, List<Integer>>(new Comparator<ApartmentType>() {		// создаём таблицу с анонимным компаратором, который расположит все типы номеров по возрастанию
			@Override
			public int compare(ApartmentType type1, ApartmentType type2) {
				return type1.getId() - type2.getId();
			}
		});
		
		try {
			tableOfAvailableApartmentsAndTypes.putAll(ClientServiceGroup.getInstance().checkApartmentOnDate(dateFrom.getTime(), dateUntil.getTime()));	// добавляем результат работы сервиса в нашу карту
		} catch (ServiceException ex) {
			throw new CommandException("Unable to get available apartments on the requested dates.", ex);
		}
		
		if (!tableOfAvailableApartmentsAndTypes.isEmpty()) {
			amountOfNights = NightsCountUtility.countNights(dateFrom.getTime(), dateUntil.getTime());
			request.getSession(false).setAttribute("amountOfNights", amountOfNights);
			
			Locale locale = (Locale) request.getSession(false).getAttribute("locale");
			NumberFormat currencyFormat = NumberFormat.getCurrencyInstance(locale);
			
			nightCase = WordCaseUtility.getNightClause(amountOfNights);
			request.getSession(false).setAttribute("nightCase", nightCase);
			
			page = JspPageName.INDEX_PAGE;
			request.getSession(false).setAttribute("tableOfAvailableApartmentsAndTypes", tableOfAvailableApartmentsAndTypes);
			request.getSession(false).setAttribute("currencyFormat", currencyFormat);
			request.getSession().setAttribute("lastAccessedPage", page);
		} else {
			page = JspPageName.INDEX_PAGE;
			request.setAttribute("noAvailableApartmentsFlag", "true");
			request.getSession().setAttribute("lastAccessedPage", page);
		}
		return page;
	}
}
