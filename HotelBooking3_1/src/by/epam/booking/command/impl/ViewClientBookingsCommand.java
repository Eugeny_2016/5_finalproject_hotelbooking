package by.epam.booking.command.impl;

import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import by.epam.booking.command.Command;
import by.epam.booking.command.CommandException;
import by.epam.booking.command.CommandType;
import by.epam.booking.command.JspPageName;
import by.epam.booking.command.RequiredRoleName;
import by.epam.booking.domain.BookingForm;
import by.epam.booking.domain.User;
import by.epam.booking.service.ClientServiceGroup;
import by.epam.booking.service.ServiceException;
import by.epam.booking.utility.NightsCountUtility;
import by.epam.booking.utility.WordCaseUtility;

/**
 * The Class ViewClientBookingsCommand.
 */
public class ViewClientBookingsCommand implements Command {
	
	/** The Constant LOG. */
	private static final Logger LOG = Logger.getLogger(ViewClientBookingsCommand.class);
	
	/** The Constant INSTANCE. */
	private static final ViewClientBookingsCommand INSTANCE = new ViewClientBookingsCommand();
		
	/** The Constant COMMAND_REQUEST_PARAMETER. */
	private static final String COMMAND_REQUEST_PARAMETER = "command";
	
	/** The type. */
	private CommandType type;
	
	/** The required role name for the command. */
	private RequiredRoleName requiredRoleName;
		
	/**
	 * Instantiates a new view client bookings command.
	 */
	private ViewClientBookingsCommand() {
		type = CommandType.WITHOUT_DB_CHANGING;
		requiredRoleName = RequiredRoleName.CLIENT;
	}
	
	/**
	 * Gets the single instance of ViewClientBookingsCommand.
	 *
	 * @return single instance of ViewClientBookingsCommand
	 */
	public static ViewClientBookingsCommand getInstance() {
		return INSTANCE;
	}
	
	/* (non-Javadoc)
	 * @see by.epam.booking.command.Command#getType()
	 */
	@Override
	public CommandType getCommandType() {
		return type;
	}
	
	/* (non-Javadoc)
	 * @see by.epam.booking.command.Command#getRequiredRoleName()
	 */
	@Override
	public RequiredRoleName getRequiredRoleName() {
		return requiredRoleName;
	}
	
	/* (non-Javadoc)
	 * @see by.epam.booking.command.Command#execute(javax.servlet.http.HttpServletRequest)
	 */
	@Override
	public String execute(HttpServletRequest request) throws CommandException {
		int userId = 0;
		User user = null;
		String page = null;
		String commandName = null;
		List<BookingForm> listOfBookingForms = null;
		Queue<String> queue = new LinkedList<String>();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Map<BookingForm, Queue<String>> bookingMapToView = null;
		
		if (request.getSession(false) == null) {																	// проверка, не закончилась ли сессия
			page = JspPageName.INDEX_PAGE;
			request.setAttribute("mustAuthorizeAgainFlag", "true");
			return page;
		}
		
		if (!requiredRoleName.equals(request.getSession(false).getAttribute("userRoleName"))) {						// если пользователь вручную набирает в адресной строке команду не для своей роли
			LOG.warn("User is trying to use a command not of his role");
			page = JspPageName.INDEX_PAGE;
			return page;
		}
		
		user = (User) request.getSession(false).getAttribute("user");
		userId = user.getId();
		commandName = request.getParameter(COMMAND_REQUEST_PARAMETER);
		
		try {
			listOfBookingForms = ClientServiceGroup.getInstance().viewAllBookings(userId);
			LOG.debug(listOfBookingForms);
			if (!listOfBookingForms.isEmpty()) {																	// если список заявок по бронированию не пустой, то для каждой заявки получаем даты заезда/отъезда, число ночей и слово "ночь" в нужном падеже
				bookingMapToView = new TreeMap<BookingForm, Queue<String>>(new Comparator<BookingForm>() {			// создаём таблицу с анонимным компаратором, который расположит все заявки по убыванию даты создания
					@Override
					public int compare(BookingForm form1, BookingForm form2) {
						return (int) (form2.getBookingCreationDate() - form1.getBookingCreationDate());
					}
				});
				for (BookingForm bookingForm : listOfBookingForms) {
					Date arrivalDate = new Date(bookingForm.getArrivalDate());
					Date leavingDate = new Date(bookingForm.getLeavingDate());
					String dateFrom = formatter.format(arrivalDate);
	            	String dateUntil = formatter.format(leavingDate);

	            	int amountOfNights = NightsCountUtility.countNights(arrivalDate.getTime(), leavingDate.getTime());
	            	String nightCase = WordCaseUtility.getNightClause(amountOfNights);
	            	
	    			queue.add(dateFrom);
	    			queue.add(dateUntil);
	    			queue.add(nightCase);
	    			queue.add(String.valueOf(amountOfNights));
	    			LOG.debug(queue);
	    			bookingMapToView.put(bookingForm, new LinkedList<String>(queue));
	    			queue.clear();
				}
				page = JspPageName.CLIENT_BOOKINGS_PAGE;
				request.setAttribute("commandName", commandName);
				request.setAttribute("bookingMapToView", bookingMapToView);
			} else {
				page = JspPageName.CLIENT_BOOKINGS_PAGE;
				request.setAttribute("NoBookingsFoundFlag", "true");
			}
		} catch (ServiceException ex) {
			throw new CommandException("Unable to get all bookings for client. ", ex);
		}	
		return page;
	}
}
