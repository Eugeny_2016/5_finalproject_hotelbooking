package by.epam.booking.command.impl;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import by.epam.booking.command.Command;
import by.epam.booking.command.CommandException;
import by.epam.booking.command.CommandType;
import by.epam.booking.command.JspPageName;
import by.epam.booking.command.RequiredRoleName;
import by.epam.booking.service.AdminServiceGroup;
import by.epam.booking.service.ServiceException;

/**
 * The Class CloseStatusBookingCommand.
 */
public class CloseStatusBookingCommand implements Command {
	
	/** The Constant LOG. */
	private static final Logger LOG = Logger.getLogger(CloseStatusBookingCommand.class);
	
	/** The Constant INSTANCE. */
	private static final CloseStatusBookingCommand INSTANCE = new CloseStatusBookingCommand();
		
	/** The Constant BOOKING_FORM_CLOSED_STATUS. */
	private static final String BOOKING_FORM_CLOSED_STATUS = "closed";
	
	/** The Constant CHOSEN_BOOKINGS_REQUEST_PARAMETER. */
	private static final String CHOSEN_BOOKINGS_REQUEST_PARAMETER = "chosen-bookigs";
	
	/** The type. */
	private CommandType type;
	
	/** The required role name for the command. */
	private RequiredRoleName requiredRoleName;
		
	/**
	 * Instantiates a new close status booking command.
	 */
	private CloseStatusBookingCommand() {
		type = CommandType.REQUIRES_DB_CHANGING;
		requiredRoleName = RequiredRoleName.ADMINISTRATOR;
	}
	
	/**
	 * Gets the single instance of CloseStatusBookingCommand.
	 *
	 * @return single instance of CloseStatusBookingCommand
	 */
	public static CloseStatusBookingCommand getInstance() {
		return INSTANCE;
	}
	
	/* (non-Javadoc)
	 * @see by.epam.booking.command.Command#getType()
	 */
	@Override
	public CommandType getCommandType() {
		return type;
	}
	
	/* (non-Javadoc)
	 * @see by.epam.booking.command.Command#getRequiredRoleName()
	 */
	@Override
	public RequiredRoleName getRequiredRoleName() {
		return requiredRoleName;
	}
	
	/* (non-Javadoc)
	 * @see by.epam.booking.command.Command#execute(javax.servlet.http.HttpServletRequest)
	 */
	@Override
	public String execute(HttpServletRequest request) throws CommandException {
		String page = null;
		String[] chosenBookings = null;
		boolean closeResult = false;
		type = CommandType.REQUIRES_DB_CHANGING;													// если при истечении сессии (хотя бы раз) тип менялся, нужно возвращаться всегда обратно!
		
		if (request.getSession(false) == null) {													// проверка, не закончилась ли сессия
			page = JspPageName.INDEX_PAGE;
			type = CommandType.WITHOUT_DB_CHANGING;
			request.setAttribute("mustAuthorizeAgainFlag", "true");
			return page;
		}
		
		if (!requiredRoleName.equals(request.getSession(false).getAttribute("userRoleName"))) {		// если пользователь вручную набирает в адресной строке команду не для своей роли
			LOG.warn("User is trying to use a command not of his role");
			page = JspPageName.INDEX_PAGE;
			type = CommandType.WITHOUT_DB_CHANGING;
			return page;
		}
		
		if (request.getSession(false).getAttribute("bookingMapToClose") == null) {					// для ситуации, если админ нажмёт "назад" после успешного/не успешного закрытия заявки по бронированию
			page = JspPageName.INDEX_PAGE;
			type = CommandType.WITHOUT_DB_CHANGING;
			return page;
		}
		
		chosenBookings = request.getParameterValues(CHOSEN_BOOKINGS_REQUEST_PARAMETER);
		if (chosenBookings == null) {
			page = JspPageName.SEARCH_PAGE;
			type = CommandType.WITHOUT_DB_CHANGING;
			request.setAttribute("noBookingsChosenFlag", "true");									// если админ ничего не выбрал, нам нужно отобразить тот же список заявок, но предупредить его
			return page;
		}
		
		int[] bookingId = new int[chosenBookings.length];
		for (int i = 0; i < chosenBookings.length; i++) {
			bookingId[i] = Integer.parseInt(chosenBookings[i]);
		}
		
		try {
			closeResult = AdminServiceGroup.getInstance().closeBookings(bookingId, BOOKING_FORM_CLOSED_STATUS);
			if (closeResult) {
				page = JspPageName.AFTER_CLOSING_PAGE;
				request.getSession(false).setAttribute("closingFlag", "true");
				LOG.info("Booking forms have been closed successfully");
			} else {
				page = JspPageName.AFTER_CLOSING_PAGE;
				request.getSession(false).setAttribute("closingFlag", "false");
				LOG.error("Error in closing booking forms");
			}
		} catch (ServiceException ex) {
			throw new CommandException("Unable to close booking forms. ", ex);
		} finally {
			request.getSession(false).setAttribute("bookingMapToClose", null);						// обнуляем список заявок для закрытия, чтобы больше им нельзя было воспользоваться
		}
		
		return page;
	}
}
