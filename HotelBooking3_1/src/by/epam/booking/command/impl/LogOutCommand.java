package by.epam.booking.command.impl;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import by.epam.booking.command.Command;
import by.epam.booking.command.CommandType;
import by.epam.booking.command.JspPageName;
import by.epam.booking.command.RequiredRoleName;

// TODO: Auto-generated Javadoc
/**
 * The Class LogOutCommand.
 */
public class LogOutCommand implements Command {
	
	/** The Constant LOG. */
	private static final Logger LOG = Logger.getLogger(LogOutCommand.class);
	
	/** The Constant INSTANCE. */
	private static final LogOutCommand INSTANCE = new LogOutCommand();
	
	/** The type. */
	private CommandType type;
	
	/** The required role name for the command. */
	private RequiredRoleName requiredRoleName;
	
	/**
	 * Instantiates a new log out command.
	 */
	private LogOutCommand() {
		type = CommandType.WITHOUT_DB_CHANGING;
		requiredRoleName = RequiredRoleName.NONE;
	}
	
	/**
	 * Gets the single instance of LogOutCommand.
	 *
	 * @return single instance of LogOutCommand
	 */
	public static LogOutCommand getInstance() {
		return INSTANCE;
	}
	
	/* (non-Javadoc)
	 * @see by.epam.booking.command.Command#getType()
	 */
	@Override
	public CommandType getCommandType() {
		return type;
	}
	
	/* (non-Javadoc)
	 * @see by.epam.booking.command.Command#getRequiredRoleName()
	 */
	@Override
	public RequiredRoleName getRequiredRoleName() {
		return requiredRoleName;
	}
	
	/* (non-Javadoc)
	 * @see by.epam.booking.command.Command#execute(javax.servlet.http.HttpServletRequest)
	 */
	@Override
	public String execute(HttpServletRequest request) {
		if (request.getSession(false) == null) {												// проверка, не закончилась ли сессия уже сама
			LOG.debug("session is already null");
			return JspPageName.INDEX_PAGE;
		} else {
			LOG.debug("session is not null");
			request.getSession().invalidate();
			return JspPageName.INDEX_PAGE;
		}
	}

}
