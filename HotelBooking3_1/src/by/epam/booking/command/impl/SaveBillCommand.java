package by.epam.booking.command.impl;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import by.epam.booking.command.Command;
import by.epam.booking.command.CommandException;
import by.epam.booking.command.CommandType;
import by.epam.booking.command.JspPageName;
import by.epam.booking.command.RequiredRoleName;
import by.epam.booking.domain.Bill;
import by.epam.booking.service.AdminServiceGroup;
import by.epam.booking.service.ServiceException;

/**
 * The Class SaveBillCommand.
 */
public class SaveBillCommand implements Command {
	
	/** The Constant LOG. */
	private static final Logger LOG = Logger.getLogger(SaveBillCommand.class);
	
	/** The Constant INSTANCE. */
	private static final SaveBillCommand INSTANCE = new SaveBillCommand();
		
	/** The Constant BOOKING_ID_REQUEST_PARAMETER. */
	private static final String BOOKING_ID_REQUEST_PARAMETER = "booking-id";
	
	/** The Constant BOOKING_TOTAL_PRICE_REQUEST_PARAMETER. */
	private static final String BOOKING_TOTAL_PRICE_REQUEST_PARAMETER = "total-price";
	
	/** The type. */
	private CommandType type;
	
	/** The required role name for the command. */
	private RequiredRoleName requiredRoleName;
		
	/**
	 * Instantiates a new save bill command.
	 */
	private SaveBillCommand() {
		type = CommandType.REQUIRES_DB_CHANGING;
		requiredRoleName = RequiredRoleName.ADMINISTRATOR;
	}
	
	/**
	 * Gets the single instance of SaveBillCommand.
	 *
	 * @return single instance of SaveBillCommand
	 */
	public static SaveBillCommand getInstance() {
		return INSTANCE;
	}
	
	/* (non-Javadoc)
	 * @see by.epam.booking.command.Command#getType()
	 */
	@Override
	public CommandType getCommandType() {
		return type;
	}
	
	/* (non-Javadoc)
	 * @see by.epam.booking.command.Command#getRequiredRoleName()
	 */
	@Override
	public RequiredRoleName getRequiredRoleName() {
		return requiredRoleName;
	}
	
	/* (non-Javadoc)
	 * @see by.epam.booking.command.Command#execute(javax.servlet.http.HttpServletRequest)
	 */
	@Override
	public String execute(HttpServletRequest request) throws CommandException {
		String page = null;
		boolean saveBillFlag = false;
		type = CommandType.REQUIRES_DB_CHANGING;														// если при истечении сессии (хотя бы раз) тип менялся, нужно возвращаться всегда обратно!
		
		if (request.getSession(false) == null) {														// проверка, не закончилась ли сессия
			page = JspPageName.INDEX_PAGE;
			type = CommandType.WITHOUT_DB_CHANGING;
			request.setAttribute("mustAuthorizeAgainFlag", "true");
			return page;
		}
		
		if (!requiredRoleName.equals(request.getSession(false).getAttribute("userRoleName"))) {			// если пользователь вручную набирает в адресной строке команду не для своей роли
			LOG.warn("User is trying to use a command not of his role");
			page = JspPageName.INDEX_PAGE;
			type = CommandType.WITHOUT_DB_CHANGING;
			return page;
		}
		
		if (request.getSession(false).getAttribute("forSavingFlag") == null) {							// для ситуации, если админ нажмёт "назад" после успешного/не успешного сохранения счёта
			page = JspPageName.INDEX_PAGE;
			type = CommandType.WITHOUT_DB_CHANGING;
			return page;
		}
		
		int bookingId = Integer.parseInt(request.getParameter(BOOKING_ID_REQUEST_PARAMETER));
		double totalPrice = Double.parseDouble(request.getParameter(BOOKING_TOTAL_PRICE_REQUEST_PARAMETER));
		Bill bill = new Bill(bookingId, totalPrice);
						
		try {
			saveBillFlag = AdminServiceGroup.getInstance().saveBill(bill);
			LOG.debug(saveBillFlag);
			if (saveBillFlag) {
				page = JspPageName.AFTER_BILL_SAVING_PAGE;
				request.getSession(false).setAttribute("saveBillFlag", "true");
				LOG.info("Bill has been successfully saved");
			} else {
				page = JspPageName.AFTER_BILL_SAVING_PAGE;
				request.getSession(false).setAttribute("saveBillFlag", "false");
				LOG.error("Error in saving bill");
			}
		} catch (ServiceException ex) {
			throw new CommandException("Unable to get information about the user of booking.", ex);
		} finally {
			request.getSession(false).setAttribute("forSavingFlag", null);								// обнуляем атрибут в сессии, чтобы счёт нельзя было сохранить повторно
			request.getSession(false).setAttribute("bookingMapToCreateBill", null);						// а также все таблицы с результатами прошлых поисков			
		}
		return page;
	}
}
