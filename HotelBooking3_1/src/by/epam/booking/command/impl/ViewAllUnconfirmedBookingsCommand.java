package by.epam.booking.command.impl;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import by.epam.booking.command.Command;
import by.epam.booking.command.CommandException;
import by.epam.booking.command.CommandType;
import by.epam.booking.command.JspPageName;
import by.epam.booking.command.RequiredRoleName;
import by.epam.booking.domain.BookingForm;
import by.epam.booking.service.AdminServiceGroup;
import by.epam.booking.service.ServiceException;
import by.epam.booking.utility.NightsCountUtility;
import by.epam.booking.utility.WordCaseUtility;

/**
 * The Class ViewAllUnconfirmedBookingsCommand.
 */
public class ViewAllUnconfirmedBookingsCommand implements Command {
	
	/** The Constant LOG. */
	private static final Logger LOG = Logger.getLogger(ViewAllUnconfirmedBookingsCommand.class);
	
	/** The Constant INSTANCE. */
	private static final ViewAllUnconfirmedBookingsCommand INSTANCE = new ViewAllUnconfirmedBookingsCommand();
		
	/** The Constant BOOKING_FORM_TEMPORARY_STATUS. */
	private static final String BOOKING_FORM_TEMPORARY_STATUS = "temporary";
	
	/** The Constant TODAY_STRING_VALUE. */
	private static final String TODAY_STRING_VALUE = "today";
	
	/** The Constant PM1400_TIME_MILLISECONDS_AMOUNT. */
	private static final int PM1400_TIME_MILLISECONDS_AMOUNT = 50_400_000;
	
	/** The Constant PM1800_TIME_MILLISECONDS_AMOUNT. */
	private static final int PM1800_TIME_MILLISECONDS_AMOUNT = 64_800_000;
	
	/** The type. */
	private CommandType type;
	
	/** The required role name for the command. */
	private RequiredRoleName requiredRoleName;
		
	/**
	 * Instantiates a new view all unconfirmed bookings command.
	 */
	private ViewAllUnconfirmedBookingsCommand() {
		type = CommandType.WITHOUT_DB_CHANGING;
		requiredRoleName = RequiredRoleName.ADMINISTRATOR;
	}
	
	/**
	 * Gets the single instance of ViewAllUnconfirmedBookingsCommand.
	 *
	 * @return single instance of ViewAllUnconfirmedBookingsCommand
	 */
	public static ViewAllUnconfirmedBookingsCommand getInstance() {
		return INSTANCE;
	}
	
	/* (non-Javadoc)
	 * @see by.epam.booking.command.Command#getType()
	 */
	@Override
	public CommandType getCommandType() {
		return type;
	}
	
	/* (non-Javadoc)
	 * @see by.epam.booking.command.Command#getRequiredRoleName()
	 */
	@Override
	public RequiredRoleName getRequiredRoleName() {
		return requiredRoleName;
	}
	
	/* (non-Javadoc)
	 * @see by.epam.booking.command.Command#execute(javax.servlet.http.HttpServletRequest)
	 */
	@Override
	public String execute(HttpServletRequest request) throws CommandException {
		String page = null;
		Date currentDateTime = new Date();																				// текущее время
		List<BookingForm> listOfBookingForms = null;
		Queue<String> queue = new LinkedList<String>();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Map<BookingForm, Queue<String>> unconfirmedBookingMap = null; 

		if (request.getSession(false) == null) {																		// проверка, не закончилась ли сессия
			page = JspPageName.INDEX_PAGE;
			request.setAttribute("mustAuthorizeAgainFlag", "true");
			return page;
		}
		
		if (!requiredRoleName.equals(request.getSession(false).getAttribute("userRoleName"))) {							// если пользователь вручную набирает в адресной строке команду не для своей роли
			LOG.warn("User is trying to use a command not of his role");
			page = JspPageName.INDEX_PAGE;
			return page;
		}
		
		Date todayAtMidnight = getDateAtMidnight(new Date());
		long arrivalDateTime = todayAtMidnight.getTime() + PM1400_TIME_MILLISECONDS_AMOUNT;								// получаем кол-во миллисекунд для сегодня 14.00
		long timeToClose = todayAtMidnight.getTime() + PM1800_TIME_MILLISECONDS_AMOUNT;									// получаем кол-во миллисекунд для сегодня 18.00
		
		if (currentDateTime.getTime() < arrivalDateTime) {																// заявки с сегодняшним заездом становятся неподтверждёнными, только если после 14.00 они ещё не подтвердились
			page = JspPageName.UNCONFIRMED_BOOKINGS_PAGE;																// но принудительно закрыть их администратор сможет только после 18.00, т.е. даём время время на опоздание клиенту
			request.setAttribute("NoUnconfirmedBookingsFoundFlag", "true");
			LOG.debug("Too early to look for unconfirmed bookings");
			return page;
		} else if (currentDateTime.getTime() > arrivalDateTime && currentDateTime.getTime() < timeToClose) {			// с 14.00 до 18.00 админ может только просматривать еще неподтверждённые заявки
			request.setAttribute("OnlyViewFlag", "true");
			LOG.debug("OnlyViewFlag is true");
		} else {
			request.setAttribute("CanCloseFlag", "true");																// после 18.00 админ может все неподтверждённые заявки принудительно закрыть
			LOG.debug("CanCloseFlag is true");
		}
		
		try {
			listOfBookingForms = AdminServiceGroup.getInstance().viewAllUnconfirmedBookings(BOOKING_FORM_TEMPORARY_STATUS, arrivalDateTime);
			LOG.debug(listOfBookingForms);
			if (!listOfBookingForms.isEmpty()) {																		// если список заявок по бронированию не пустой, то для каждой заявки получаем даты заезда/отъезда, число ночей и слово "ночь" в нужном падеже
				unconfirmedBookingMap = new TreeMap<BookingForm, Queue<String>>(new Comparator<BookingForm>() {			// создаём таблицу с анонимным компаратором, который расположит все типы номеров по возрастанию
					@Override
					public int compare(BookingForm form1, BookingForm form2) {
						return (int) (form2.getBookingCreationDate() - form1.getBookingCreationDate());
					}
				});
				for (BookingForm bookingForm : listOfBookingForms) {
					Date createdAt = getDateAtMidnight(new Date(bookingForm.getBookingCreationDate()));
					currentDateTime = getDateAtMidnight(currentDateTime);
					Date arrivalDate = new Date(bookingForm.getArrivalDate());
					Date leavingDate = new Date(bookingForm.getLeavingDate());
					String bookingCreationDate = (createdAt.before(currentDateTime)) ? formatter.format(createdAt) : TODAY_STRING_VALUE;
					String dateFrom = formatter.format(arrivalDate);
	            	String dateUntil = formatter.format(leavingDate);

	            	int amountOfNights = NightsCountUtility.countNights(arrivalDate.getTime(), leavingDate.getTime());
	            	String nightCase = WordCaseUtility.getNightClause(amountOfNights);
	        		
	    			queue.add(bookingCreationDate);
	    			queue.add(dateFrom);
	    			queue.add(dateUntil);
	    			queue.add(nightCase);
	    			queue.add(String.valueOf(amountOfNights));
	    			LOG.debug(queue);
	    			unconfirmedBookingMap.put(bookingForm, new LinkedList<String>(queue));
	    			queue.clear();
				}
				page = JspPageName.UNCONFIRMED_BOOKINGS_PAGE;
				request.getSession(false).setAttribute("unconfirmedBookingMap", unconfirmedBookingMap);
			} else {
				page = JspPageName.UNCONFIRMED_BOOKINGS_PAGE;
				request.setAttribute("NoUnconfirmedBookingsFoundFlag", "true");
			}
		} catch (ServiceException ex) {
			throw new CommandException("Unable to get all unconfirmed bookings. ", ex);
		}	
		return page;
	}
	
	/**
	 * Gets the date at midnight.
	 *
	 * @param date the date
	 * @return the date at midnight
	 */
	private Date getDateAtMidnight(Date date) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.HOUR_OF_DAY, 00);
		calendar.set(Calendar.MINUTE, 00);
		calendar.set(Calendar.SECOND, 00);
		calendar.set(Calendar.MILLISECOND, 000);
		
		return calendar.getTime();
	}
}
