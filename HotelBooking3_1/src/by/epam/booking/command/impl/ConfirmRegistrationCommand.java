package by.epam.booking.command.impl;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import by.epam.booking.command.Command;
import by.epam.booking.command.CommandException;
import by.epam.booking.command.CommandType;
import by.epam.booking.command.JspPageName;
import by.epam.booking.command.RequiredRoleName;
import by.epam.booking.domain.User;
import by.epam.booking.domain.UserRole;
import by.epam.booking.service.ServiceException;
import by.epam.booking.service.UserServiceGroup;
import by.epam.booking.utility.EncryptionUtility;

/**
 * The Class ConfirmRegistrationCommand.
 */
public class ConfirmRegistrationCommand implements Command {
	
	/** The Constant LOG. */
	private final static Logger LOG = Logger.getLogger(ConfirmRegistrationCommand.class);
	
	/** The Constant INSTANCE. */
	private static final ConfirmRegistrationCommand INSTANCE = new ConfirmRegistrationCommand();
		
	/** The Constant LOGIN_REQUEST_PARAMETER. */
	private static final String LOGIN_REQUEST_PARAMETER = "registration-login";
	
	/** The Constant PASSWORD_REQUEST_PARAMETER. */
	private static final String PASSWORD_REQUEST_PARAMETER = "registration-password";
	
	/** The Constant QUESTION_REQUEST_PARAMETER. */
	private static final String QUESTION_REQUEST_PARAMETER = "question";
	
	/** The Constant ANSWER_REQUEST_PARAMETER. */
	private static final String ANSWER_REQUEST_PARAMETER = "answer";
	
	/** The Constant EMAIL_REQUEST_PARAMETER. */
	private static final String EMAIL_REQUEST_PARAMETER = "email";
	
	/** The Constant NAME_REQUEST_PARAMETER. */
	private static final String NAME_REQUEST_PARAMETER = "name";
	
	/** The Constant SURNAME_REQUEST_PARAMETER. */
	private static final String SURNAME_REQUEST_PARAMETER = "surname";
	
	/** The Constant PATRONYMIC_REQUEST_PARAMETER. */
	private static final String PATRONYMIC_REQUEST_PARAMETER = "patronymic";
	
	/** The Constant COUNTRY_REQUEST_PARAMETER. */
	private static final String COUNTRY_REQUEST_PARAMETER = "country";
	
	/** The Constant ADDRESS_REQUEST_PARAMETER. */
	private static final String ADDRESS_REQUEST_PARAMETER = "address";
	
	/** The Constant PHONE_REQUEST_PARAMETER. */
	private static final String PHONE_REQUEST_PARAMETER = "phone";
	
	/** The Constant MOBILE_REQUEST_PARAMETER. */
	private static final String MOBILE_REQUEST_PARAMETER = "mobile";
			
	/** The Constant CLIENT_ROLE_NAME. */
	private static final String CLIENT_ROLE_NAME = "client";
	
	/** The Constant DEFAULT_ROLE_ID. */
	private static final int DEFAULT_ROLE_ID = 2;
	
	/** The Constant USER_ACTIVE_STATUS. */
	private static final String USER_ACTIVE_STATUS = "active";
	
	/** The type. */
	private CommandType type;
	
	/** The required role name for the command. */
	private RequiredRoleName requiredRoleName;
	
	/**
	 * Instantiates a new confirm registration command.
	 */
	private ConfirmRegistrationCommand() {
		type = CommandType.REQUIRES_DB_CHANGING;
		requiredRoleName = RequiredRoleName.NONE;
	}
	
	/**
	 * Gets the single instance of ConfirmRegistrationCommand.
	 *
	 * @return single instance of ConfirmRegistrationCommand
	 */
	public static ConfirmRegistrationCommand getInstance() {
		return INSTANCE;
	}
	
	/* (non-Javadoc)
	 * @see by.epam.booking.command.Command#getType()
	 */
	public CommandType getCommandType() {
		return type;
	}
	
	/* (non-Javadoc)
	 * @see by.epam.booking.command.Command#getRequiredRoleName()
	 */
	@Override
	public RequiredRoleName getRequiredRoleName() {
		return requiredRoleName;
	}
				
	/* (non-Javadoc)
	 * @see by.epam.booking.command.Command#execute(javax.servlet.http.HttpServletRequest)
	 */
	@Override
	public String execute(HttpServletRequest request) throws CommandException {
		String page = null;
		boolean checkResult = false;
		boolean addResult = false;
		type = CommandType.REQUIRES_DB_CHANGING;															// если фильтр хоть раз браковал данные, нужно возвращать тип команды всегда обратно!
		
		if (request.getAttribute("pageFromConfirmRegFilter") != null) {										// смотрим, не забраковал ли фильтр ввёденную информацию
			page = request.getAttribute("pageFromConfirmRegFilter").toString();
			request.getSession().setAttribute("lastAccessedPage", page);
			type = CommandType.WITHOUT_DB_CHANGING;
			return page;
		}
		
		if (request.getSession().getAttribute("regLogin") == null) {										// для ситуации, если клиент нажмёт "назад" после успешной/не успешной регистрации
			page = JspPageName.INDEX_PAGE;
			request.getSession().setAttribute("lastAccessedPage", page);
			type = CommandType.WITHOUT_DB_CHANGING;
			return page;
		}
		
		String regLogin = request.getParameter(LOGIN_REQUEST_PARAMETER);									// проверяем, существует ли уже пользователь с таким же логином
   		try {
   			checkResult = UserServiceGroup.getInstance().checkUser(regLogin);
   		} catch (ServiceException ex) {
   			throw new CommandException("User hasn't been added because of inability of checking user existance. ", ex);
   		}
   		
		if (checkResult) {																					// если уже сущестует, то оповещаем пользователя об этом
			request.setAttribute("userAlreadyExistsFlag", "true");
			type = CommandType.WITHOUT_DB_CHANGING;
			page = JspPageName.REGISTRATION_PAGE;
		} else {																							// если отсутствует, то создаём нового пользователя и добавляем его в БД
			String regPassword = request.getParameter(PASSWORD_REQUEST_PARAMETER);
			String encryptedPassword = EncryptionUtility.encryptPasswordWithMD5(regPassword);
	  		String keyQuestion = request.getParameter(QUESTION_REQUEST_PARAMETER);
	   		String keyAnswer = request.getParameter(ANSWER_REQUEST_PARAMETER);
	   		String email = request.getParameter(EMAIL_REQUEST_PARAMETER);
	   		String name = request.getParameter(NAME_REQUEST_PARAMETER);
	   		String surname = request.getParameter(SURNAME_REQUEST_PARAMETER);
	   		String patronymic = request.getParameter(PATRONYMIC_REQUEST_PARAMETER);
	   		String country = request.getParameter(COUNTRY_REQUEST_PARAMETER);
	   		String address = request.getParameter(ADDRESS_REQUEST_PARAMETER);
	   		String phone = request.getParameter(PHONE_REQUEST_PARAMETER);
	   		String mobile = request.getParameter(MOBILE_REQUEST_PARAMETER);
	   		UserRole userRole = null;
	   		User user = null;
	   		int roleId = 0;
	   		
	   		try {
	   			userRole = UserServiceGroup.getInstance().getRoleInformation(CLIENT_ROLE_NAME);
	   		} catch (ServiceException ex) {
	   			throw new CommandException("Unable to add user to DB because of inability of getting role information. ", ex);
	   		}
	   		if (userRole != null) {
				roleId = userRole.getId();
			} else {																					// если информация о роли не вернулась,
				roleId = DEFAULT_ROLE_ID;																// для создания пользователя используем id роли по умолчанию
				LOG.error("Error in obtaining information about role");
			}
	
	   		user = new User(regLogin, encryptedPassword, roleId, CLIENT_ROLE_NAME, USER_ACTIVE_STATUS, keyQuestion, keyAnswer, email,		 
	   						name, surname, patronymic, country, address, phone, mobile);
			try {
				addResult = UserServiceGroup.getInstance().addUser(user);
				if (addResult) {																		// если пользователь успешно зарегистрировался
					user = UserServiceGroup.getInstance().getUser(regLogin, encryptedPassword);
					if (user != null) {
						page = JspPageName.AFTER_REGISTRATION_PAGE;
						request.getSession().setAttribute("user", user);
						request.getSession().setAttribute("userRoleId", user.getRoleID());
						request.getSession().setAttribute("userRoleName", RequiredRoleName.valueOf(user.getRoleName().toUpperCase()));
						request.getSession().setAttribute("registrationFlag", "true");
						request.getSession().setAttribute("lastAccessedPage", page);
						LOG.info(user + " has been added to DB");
					} else {
						page = JspPageName.AFTER_REGISTRATION_PAGE;										// если не удалось получить только что зарегистрированного пользователя из БД
						request.getSession().invalidate();												// то уничтожаем сессию, т.к. нечего в неё положить. команда по авторизации положит информацию о пользователе заново
					}
				} else {
					page = JspPageName.AFTER_REGISTRATION_PAGE;
					request.getSession().setAttribute("registrationFlag", "false");
					request.getSession().setAttribute("lastAccessedPage", page);
					LOG.error("Error in adding user to DB");
				}
			} catch (ServiceException ex) {
				throw new CommandException("Unable to add user to DB. ", ex);
			} finally {
				request.getSession().setAttribute("regLogin", null);									// обнуляем все атрибуты, положенные в сессию фильтром по регистрации
				request.getSession().setAttribute("regPassword", null);									// чтобы больше ими нельзя было воспользоваться
				request.getSession().setAttribute("regPasswordAgain", null);
				request.getSession().setAttribute("keyQuestion", null);
				request.getSession().setAttribute("keyAnswer", null);
				request.getSession().setAttribute("regEmail", null);
				request.getSession().setAttribute("regName", null);
		        request.getSession().setAttribute("regSurname", null);
		        request.getSession().setAttribute("regPatronymic", null);
		        request.getSession().setAttribute("regCountry", null);
		        request.getSession().setAttribute("regAddress", null);
		        request.getSession().setAttribute("regPhone", null);
		        request.getSession().setAttribute("regMobile", null);
			}
		}
		return page;
	}
}
