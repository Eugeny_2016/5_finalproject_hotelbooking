package by.epam.booking.command.impl;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import by.epam.booking.command.Command;
import by.epam.booking.command.CommandType;
import by.epam.booking.command.JspPageName;
import by.epam.booking.command.RequiredRoleName;
import by.epam.booking.service.ServiceException;
import by.epam.booking.service.VisitorServiceGroup;

/**
 * The Class LocalizationCommand.
 */
public class LocalizationCommand implements Command {
	
	/** The Constant LOG. */
	private static final Logger LOG = Logger.getLogger(LocalizationCommand.class);
	
	/** The Constant INSTANCE. */
	private static final LocalizationCommand INSTANCE = new LocalizationCommand();
	
	/** The Constant LOCALE_REQUEST_PARAMETER. */
	private static final String LOCALE_REQUEST_PARAMETER = "locale";
	
	/** The Constant COUNTRY_DEFAULT_NAME. */
	private static final String COUNTRY_DEFAULT_NAME = "BY";
	
	/** The Constant CURRENCY_DEFAULT_COEFFICIENT. */
	private static final float CURRENCY_DEFAULT_COEFFICIENT = 1.0F;
	
	/** The type. */
	private CommandType type;
	
	/** The required role name for the command. */
	private RequiredRoleName requiredRoleName;
	
	/**
	 * Instantiates a new localization command.
	 */
	private LocalizationCommand() {
		type = CommandType.WITHOUT_DB_CHANGING;
		requiredRoleName = RequiredRoleName.NONE;
	}
	
	/**
	 * Gets the single instance of LocalizationCommand.
	 *
	 * @return single instance of LocalizationCommand
	 */
	public static LocalizationCommand getInstance() {
		return INSTANCE;
	}
	
	/* (non-Javadoc)
	 * @see by.epam.booking.command.Command#getType()
	 */
	@Override
	public CommandType getCommandType() {
		return type;
	}
	
	/* (non-Javadoc)
	 * @see by.epam.booking.command.Command#getRequiredRoleName()
	 */
	@Override
	public RequiredRoleName getRequiredRoleName() {
		return requiredRoleName;
	}
	
	/* (non-Javadoc)
	 * @see by.epam.booking.command.Command#execute(javax.servlet.http.HttpServletRequest)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public String execute(HttpServletRequest request) {
		String page = null;
		String lastAccessedPage = null;
		String lastRequiredResource = null;
		String lastRequiredResourceName = null;
		String loc = null;
		Locale locale = null;
				
		loc = request.getParameter(LOCALE_REQUEST_PARAMETER);
		String[] localeParts = loc.split("-");
		LOG.debug("New locale to set: " + localeParts[0] + " " + localeParts[1]);
		locale = new Locale(localeParts[0], localeParts[1]);
		request.getSession().setAttribute("locale", locale);
		
		String countryName = "RU".equals(locale.getCountry()) ? COUNTRY_DEFAULT_NAME : locale.getCountry();    	
    	float dollarCoefficient = 0.0F;
    	try {																														// получаем курс доллара для страны из новой локали
    		dollarCoefficient = VisitorServiceGroup.getInstance().getDollarCoefficient(countryName);
			if (dollarCoefficient == 0 ) {
				dollarCoefficient = CURRENCY_DEFAULT_COEFFICIENT;
				LOG.info("Currency coefficient hasn't been found. Set to default: " + dollarCoefficient);
			}
		} catch (ServiceException ex) {
			LOG.error("Error in obtaining dollar coefficient information");
		}
    	request.getSession().setAttribute("dollarCoefficient", dollarCoefficient);
    	request.getSession(false).setAttribute("currencyFormat", NumberFormat.getCurrencyInstance(locale));							// устанавливаем форматировщик валюты для новой локали 
    	
    	List<Double> apartmentPricesInDouble = (List<Double>) request.getSession().getAttribute("apartmentPricesInDouble");			// получаем список цен на номера для умножения их на курс доллара в стране из новой локали
    	List<String> apartmentPricesInString = new ArrayList<String>();
		NumberFormat nf = NumberFormat.getCurrencyInstance(locale);
		
		for (int i = 0; i < apartmentPricesInDouble.size(); i++) {
			String price = nf.format(apartmentPricesInDouble.get(i) * dollarCoefficient);
			apartmentPricesInString.add(price);
		}
		request.getSession().setAttribute("apartmentPricesInString", apartmentPricesInString);
    	
		lastAccessedPage = (String) request.getSession().getAttribute("lastAccessedPage");
		lastRequiredResourceName = (String) request.getSession().getAttribute("lastRequiredResourceName");
		LOG.debug("Last accessed page: " + lastAccessedPage);
		LOG.debug("Last required resource name: " + lastRequiredResourceName);
		
		if (lastRequiredResourceName != null) {
			try {
				lastRequiredResource = VisitorServiceGroup.getInstance().getResource(lastRequiredResourceName, locale.getLanguage());	// получаем последний запрошенный ресурс на новом языке
			} catch (ServiceException ex) {
				LOG.error("Error in obtaining information resource in the required language");
			}
		}
		request.getSession().setAttribute("lastRequiredResource", lastRequiredResource);
				
		page = (lastAccessedPage == null) ? JspPageName.INDEX_PAGE : lastAccessedPage;
		LOG.debug("target page: " + page);
		
		return page;
	}
}
