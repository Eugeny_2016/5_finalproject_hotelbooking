package by.epam.booking.command.impl;

/**
 * The Enum CommandDenialReasonName.
 */
public enum CommandDenialReasonName {
	
	/** The no apartments of required amount. */
	NO_APARTMENTS_OF_REQUIRED_AMOUNT("No available apartments of required amount for client now"),
	
	/** The no apartments of type. */
	NO_APARTMENTS_OF_TYPE("No available apartments of required type for client now"), 
	
	/** The no apartments at all. */
	NO_APARTMENTS_AT_ALL("No available apartments at all for client now"), 
	
	/** The none. */
	NONE("None");																			// для ситуации, когда причина отказа отсутствует
	
	/** The reason. */
																			private String reason;
	
	/**
	 * Instantiates a new command denial reason name.
	 *
	 * @param reason the reason
	 */
	private CommandDenialReasonName(String reason) {
		this.reason = reason;
	}
	
	/**
	 * Gets the reason.
	 *
	 * @return the reason
	 */
	public String getReason() {
		return reason;
	}
}
