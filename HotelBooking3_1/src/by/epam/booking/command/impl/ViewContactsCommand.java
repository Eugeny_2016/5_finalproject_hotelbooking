package by.epam.booking.command.impl;


import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import by.epam.booking.command.Command;
import by.epam.booking.command.CommandException;
import by.epam.booking.command.CommandType;
import by.epam.booking.command.JspPageName;
import by.epam.booking.command.RequiredRoleName;
import by.epam.booking.service.ServiceException;
import by.epam.booking.service.VisitorServiceGroup;

/**
 * The Class ViewContactsCommand.
 */
public class ViewContactsCommand implements Command {
	
	/** The Constant LOG. */
	@SuppressWarnings("unused")
	private static final Logger LOG = Logger.getLogger(ViewContactsCommand.class);
	
	/** The Constant INSTANCE. */
	private static final ViewContactsCommand INSTANCE = new ViewContactsCommand();
		
	/** The Constant RESOURCE_NAME_REQUEST_PARAMETER. */
	private static final String RESOURCE_NAME_REQUEST_PARAMETER = "resource-name";
	
	/** The type. */
	private CommandType type;
	
	/** The required role name for the command. */
	private RequiredRoleName requiredRoleName;
		
	/**
	 * Instantiates a new view contacts command.
	 */
	private ViewContactsCommand() {
		type = CommandType.WITHOUT_DB_CHANGING;
		requiredRoleName = RequiredRoleName.NONE;
	}
	
	/**
	 * Gets the single instance of ViewContactsCommand.
	 *
	 * @return single instance of ViewContactsCommand
	 */
	public static ViewContactsCommand getInstance() {
		return INSTANCE;
	}
	
	/* (non-Javadoc)
	 * @see by.epam.booking.command.Command#getType()
	 */
	@Override
	public CommandType getCommandType() {
		return type;
	}
	
	/* (non-Javadoc)
	 * @see by.epam.booking.command.Command#getRequiredRoleName()
	 */
	@Override
	public RequiredRoleName getRequiredRoleName() {
		return requiredRoleName;
	}
	
	/* (non-Javadoc)
	 * @see by.epam.booking.command.Command#execute(javax.servlet.http.HttpServletRequest)
	 */
	@Override
	public String execute(HttpServletRequest request) throws CommandException {
		String page = null;
		String resource = null;
		String resourceName = null;

		resourceName = request.getParameter(RESOURCE_NAME_REQUEST_PARAMETER);
		Locale locale = (Locale) request.getSession().getAttribute("locale");
		String language = locale.getLanguage();
		
		try {
			resource = VisitorServiceGroup.getInstance().getResource(resourceName, language);

			page = JspPageName.CONTACTS_PAGE;
			request.getSession().setAttribute("lastAccessedPage", page);
			request.getSession().setAttribute("lastRequiredResource", resource);
			request.getSession().setAttribute("lastRequiredResourceName", resourceName);			
		} catch (ServiceException ex) {
			throw new CommandException("Unable to get contact information from DB. ", ex);
		}	
		return page;
	}
}
