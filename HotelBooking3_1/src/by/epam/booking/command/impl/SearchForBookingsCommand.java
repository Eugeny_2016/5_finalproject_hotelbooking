package by.epam.booking.command.impl;

import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import by.epam.booking.command.Command;
import by.epam.booking.command.CommandException;
import by.epam.booking.command.CommandType;
import by.epam.booking.command.JspPageName;
import by.epam.booking.command.RequiredRoleName;
import by.epam.booking.domain.BookingForm;
import by.epam.booking.service.AdminServiceGroup;
import by.epam.booking.service.ServiceException;
import by.epam.booking.utility.NightsCountUtility;
import by.epam.booking.utility.WordCaseUtility;

/**
 * The Class SearchForBookingsCommand.
 */
public class SearchForBookingsCommand implements Command {
	
	/** The Constant LOG. */
	private static final Logger LOG = Logger.getLogger(SearchForBookingsCommand.class);
	
	/** The Constant INSTANCE. */
	private static final SearchForBookingsCommand INSTANCE = new SearchForBookingsCommand();
	
	/** The Constant CONFIRM_STATUS_BOOKING. */
	private static final String CONFIRM_STATUS_BOOKING = "confirm_status_booking";
	
	/** The Constant CLOSE_STATUS_BOOKING. */
	private static final String CLOSE_STATUS_BOOKING = "close_status_booking";
	
	/** The Constant VIEW_ALL_BOOKING_OF_CLIENT. */
	private static final String VIEW_ALL_BOOKING_OF_CLIENT = "view_all_bookings_of_client";
	
	/** The Constant CREATE_BILL. */
	private static final String CREATE_BILL = "create_bill";
	
	/** The Constant TEMPORARY_STATUS_NAME. */
	private static final String TEMPORARY_STATUS_NAME = "temporary";
	
	/** The Constant CONFIRMED_STATUS_NAME. */
	private static final String CONFIRMED_STATUS_NAME = "confirmed";
	
	/** The Constant CLOSED_STATUS_NAME. */
	private static final String CLOSED_STATUS_NAME = "closed";
	
	/** The type. */
	private CommandType type;
	
	/** The required role name for the command. */
	private RequiredRoleName requiredRoleName;
		
	/**
	 * Instantiates a new search for bookings command.
	 */
	private SearchForBookingsCommand() {
		type = CommandType.WITHOUT_DB_CHANGING;
		requiredRoleName = RequiredRoleName.ADMINISTRATOR;
	}
	
	/**
	 * Gets the single instance of SearchForBookingsCommand.
	 *
	 * @return single instance of SearchForBookingsCommand
	 */
	public static SearchForBookingsCommand getInstance() {
		return INSTANCE;
	}
	
	/* (non-Javadoc)
	 * @see by.epam.booking.command.Command#getType()
	 */
	@Override
	public CommandType getCommandType() {
		return type;
	}
	
	/* (non-Javadoc)
	 * @see by.epam.booking.command.Command#getRequiredRoleName()
	 */
	@Override
	public RequiredRoleName getRequiredRoleName() {
		return requiredRoleName;
	}
	
	/* (non-Javadoc)
	 * @see by.epam.booking.command.Command#execute(javax.servlet.http.HttpServletRequest)
	 */
	@Override
	public String execute(HttpServletRequest request) throws CommandException {
		boolean forciblyClosedFlag = false;
		String name = null;
		String surname = null;
		String email = null;
		String mobile = null;
		String page = null;
		String status = null;
		String futureCommandName = null;
		List<BookingForm> listOfBookingForms = null;
		Queue<String> queue = new LinkedList<String>();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Map<BookingForm, Queue<String>> bookingMap = null;
		
		if (request.getSession(false).getAttribute("user") == null) {												// проверка, не закончилась ли сессия (нетрадиционным способом)
			page = JspPageName.INDEX_PAGE;																			// провевряем именно через наличие пользователя в сессии
			request.getSession(false).invalidate();																	// т.к. сессия будет в любом случае создана в фильтре. По этой же причине делаем invalidate()
			request.setAttribute("mustAuthorizeAgainFlag", "true");
			return page;
		}
		
		if (!requiredRoleName.equals(request.getSession(false).getAttribute("userRoleName"))) {						// если пользователь вручную набирает в адресной строке команду не для своей роли
			LOG.warn("User is trying to use a command not of his role");
			page = JspPageName.INDEX_PAGE;
			return page;
		}
		
		if (request.getAttribute("pageFromSearchFilter") != null) {													// не забраковал ли фильтр данные от клиента
			page = request.getAttribute("pageFromSearchFilter").toString();
			request.getSession(false).setAttribute("bookingMapToConfirm", null);									// обнуляем все данные в сессии, чтобы результаты поиска исчезли
			request.getSession(false).setAttribute("bookingMapToClose", null);
			request.getSession(false).setAttribute("bookingMapToView", null);
			request.getSession(false).setAttribute("bookingMapToCreateBill", null);
			request.getSession(false).setAttribute("forciblyClosedFlag", null);
			return page;
		}
		
		futureCommandName = request.getSession(false).getAttribute("futureCommandName").toString();
		name = request.getSession(false).getAttribute("name").toString();
		surname = request.getSession(false).getAttribute("surname").toString();
		email = request.getSession(false).getAttribute("email").toString();
		mobile = request.getSession(false).getAttribute("mobile").toString();
		forciblyClosedFlag = Boolean.parseBoolean(request.getSession(false).getAttribute("forciblyClosedFlag").toString());
		LOG.debug(forciblyClosedFlag);
		
		switch (futureCommandName) {
		case CONFIRM_STATUS_BOOKING:
			status = TEMPORARY_STATUS_NAME;
			try {
				listOfBookingForms = AdminServiceGroup.getInstance().viewClientTemporaryBookings(name, surname, email, mobile, status);
				LOG.debug(listOfBookingForms);
			} catch (ServiceException ex) {
				throw new CommandException("Unable to get all bookings for client. ", ex);
			}			
			break;
		case CLOSE_STATUS_BOOKING:
			status = CONFIRMED_STATUS_NAME;
			try {
				listOfBookingForms = AdminServiceGroup.getInstance().viewClientConfirmedBookings(name, surname, email, mobile, status);
				LOG.debug(listOfBookingForms);
			} catch (ServiceException ex) {
				throw new CommandException("Unable to get all bookings for client. ", ex);
			}			
			break;
		case VIEW_ALL_BOOKING_OF_CLIENT:
			try {
				listOfBookingForms = AdminServiceGroup.getInstance().viewAllClientBookings(name, surname, email, mobile);
				LOG.debug(listOfBookingForms);
			} catch (ServiceException ex) {
				throw new CommandException("Unable to get all bookings for client. ", ex);
			}
			break;
		case CREATE_BILL:
			status = CLOSED_STATUS_NAME;
			try {
				listOfBookingForms = AdminServiceGroup.getInstance().viewClientClosedBookings(name, surname, email, mobile, forciblyClosedFlag, status);
				LOG.debug(listOfBookingForms);
			} catch (ServiceException ex) {
				throw new CommandException("Unable to get all bookings for client. ", ex);
			}
			break;
		}
		
		if (!listOfBookingForms.isEmpty()) {																	// если список заявок по бронированию не пустой, то для каждой заявки получаем даты заезда/отъезда, число ночей и слово "ночь" в нужном падеже
			request.setAttribute("userId", listOfBookingForms.get(0).getUserId());								// кладём в запрос id клиента, чей список заявок к нам вернулся из dao
			bookingMap = new TreeMap<BookingForm, Queue<String>>(new Comparator<BookingForm>() {				// создаём таблицу с анонимным компаратором, который расположит все заявки по убыванию даты создания заявки
							@Override
							public int compare(BookingForm form1, BookingForm form2) {
								return (int) (form2.getBookingCreationDate() - form1.getBookingCreationDate());
							}
							});
			for (BookingForm bookingForm : listOfBookingForms) {
				Date arrivalDate = new Date(bookingForm.getArrivalDate());
				Date leavingDate = new Date(bookingForm.getLeavingDate());
				String dateFrom = formatter.format(arrivalDate);
            	String dateUntil = formatter.format(leavingDate);
            	
            	int amountOfNights = NightsCountUtility.countNights(arrivalDate.getTime(), leavingDate.getTime());
            	String nightCase = WordCaseUtility.getNightClause(amountOfNights);
            	            	
    			queue.add(dateFrom);
    			queue.add(dateUntil);
    			queue.add(nightCase);
    			queue.add(String.valueOf(amountOfNights));
    			LOG.debug(queue);
    			bookingMap.put(bookingForm, new LinkedList<String>(queue));
    			queue.clear();
			}
			page = JspPageName.SEARCH_PAGE;
			LOG.debug("Future command: " + futureCommandName);
			
			switch(futureCommandName) {
			case CONFIRM_STATUS_BOOKING:
				request.getSession(false).setAttribute("bookingMapToConfirm", bookingMap);
				break;
			case CLOSE_STATUS_BOOKING:
				request.getSession(false).setAttribute("bookingMapToClose", bookingMap);
				break;
			case VIEW_ALL_BOOKING_OF_CLIENT:
				request.setAttribute("bookingMapToView", bookingMap);
				break;
			case CREATE_BILL:
				request.getSession(false).setAttribute("bookingMapToCreateBill", bookingMap);
				break;
			}
			
		} else {
			page = JspPageName.SEARCH_PAGE;
			switch(futureCommandName) {
			case CONFIRM_STATUS_BOOKING:
				request.setAttribute("NoTemporaryBookingsFoundFlag", "true");
				break;
			case CLOSE_STATUS_BOOKING:
				request.setAttribute("NoConfirmedBookingsFoundFlag", "true");
				break;
			case VIEW_ALL_BOOKING_OF_CLIENT:
				request.setAttribute("NoBookingsFoundFlag", "true");
				break;
			case CREATE_BILL:
				if (name.isEmpty() && surname.isEmpty() && email.isEmpty() && mobile.isEmpty()) {
					request.setAttribute("NoForciblyClosedBookingsFoundFlag", "true");
				} else {
					request.setAttribute("NoClosedBookingsFoundFlag", "true");
				}
				break;				
			}
		}
		return page;
	}
}