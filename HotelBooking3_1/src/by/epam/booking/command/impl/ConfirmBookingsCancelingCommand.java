package by.epam.booking.command.impl;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import by.epam.booking.command.Command;
import by.epam.booking.command.CommandException;
import by.epam.booking.command.CommandType;
import by.epam.booking.command.JspPageName;
import by.epam.booking.command.RequiredRoleName;
import by.epam.booking.service.ClientServiceGroup;
import by.epam.booking.service.ServiceException;

/**
 * The Class ConfirmBookingsCancelingCommand.
 */
public class ConfirmBookingsCancelingCommand implements Command {
	
	/** The Constant LOG. */
	private static final Logger LOG = Logger.getLogger(ConfirmBookingsCancelingCommand.class);
	
	/** The Constant INSTANCE. */
	private static final ConfirmBookingsCancelingCommand INSTANCE = new ConfirmBookingsCancelingCommand();
	
	/** The Constant BOOKING_FORM_CANCELED_STATUS. */
	private static final String BOOKING_FORM_CANCELED_STATUS = "canceled";
	
	/** The Constant CHOSEN_BOOKINGS_REQUEST_PARAMETER. */
	private static final String CHOSEN_BOOKINGS_REQUEST_PARAMETER = "chosen-bookigs";
	
	/** The Constant CANCEL_CLIENT_BOOKING_COMMAND_NAME. */
	private static final String CANCEL_CLIENT_BOOKING_COMMAND_NAME = "cancel_client_booking";
	
	/** The type. */
	private CommandType type;
	
	/** The required role name for the command. */
	private RequiredRoleName requiredRoleName;
	
	/**
	 * Instantiates a new confirm bookings canceling command.
	 */
	private ConfirmBookingsCancelingCommand() {
		type = CommandType.REQUIRES_DB_CHANGING;
		requiredRoleName = RequiredRoleName.CLIENT;
	}
	
	/**
	 * Gets the single instance of ConfirmBookingsCancelingCommand.
	 *
	 * @return single instance of ConfirmBookingsCancelingCommand
	 */
	public static ConfirmBookingsCancelingCommand getInstance() {
		return INSTANCE;
	}
	
	/* (non-Javadoc)
	 * @see by.epam.booking.command.Command#getType()
	 */
	@Override
	public CommandType getCommandType() {
		return type;
	}
	
	/* (non-Javadoc)
	 * @see by.epam.booking.command.Command#getRequiredRoleName()
	 */
	@Override
	public RequiredRoleName getRequiredRoleName() {
		return requiredRoleName;
	}
	
	/* (non-Javadoc)
	 * @see by.epam.booking.command.Command#execute(javax.servlet.http.HttpServletRequest)
	 */
	@Override
	public String execute(HttpServletRequest request) throws CommandException {
		String page = null;
		String[] chosenBookings = null;
		boolean cancelResult = false;
		type = CommandType.REQUIRES_DB_CHANGING;												// если при истечении сессии (хотя бы раз) тип менялся, нужно возвращаться всегда обратно!
		
		if (request.getSession(false) == null) {												// проверка, не закончилась ли сессия
			page = JspPageName.INDEX_PAGE;
			type = CommandType.WITHOUT_DB_CHANGING;
			request.setAttribute("mustAuthorizeAgainFlag", "true");
			return page;
		}
		
		if (!requiredRoleName.equals(request.getSession(false).getAttribute("userRoleName"))) {	// если пользователь вручную набирает в адресной строке команду не для своей роли
			LOG.warn("User is trying to use a command not of his role");
			page = JspPageName.INDEX_PAGE;
			type = CommandType.WITHOUT_DB_CHANGING;
			return page;
		}
		
		if (request.getSession(false).getAttribute("bookingMapToCancel") == null) {				// для ситуации, если клиент нажмёт "назад" после успешной/не успешной отмены заявки по бронированию
			page = JspPageName.INDEX_PAGE;
			type = CommandType.WITHOUT_DB_CHANGING;
			return page;
		}
		
		chosenBookings = request.getParameterValues(CHOSEN_BOOKINGS_REQUEST_PARAMETER);
		if (chosenBookings == null) {
			page = JspPageName.CLIENT_BOOKINGS_PAGE;
			type = CommandType.WITHOUT_DB_CHANGING;
			request.setAttribute("noBookingsChosenFlag", "true");
			request.setAttribute("commandName", CANCEL_CLIENT_BOOKING_COMMAND_NAME);			// если клиент ничего не выбрал, нам нужно отобразить тот же список заявок, но предупредить его
			return page;
		}
		
		int[] bookingId = new int[chosenBookings.length];
		for (int i = 0; i < chosenBookings.length; i++) {
			bookingId[i] = Integer.parseInt(chosenBookings[i]);
		}
		
		try {
			cancelResult = ClientServiceGroup.getInstance().cancelBookings(bookingId, BOOKING_FORM_CANCELED_STATUS);
			if (cancelResult) {
				page = JspPageName.AFTER_CANCELING_PAGE;
				request.getSession(false).setAttribute("cancelingFlag", "true");
				LOG.info("Booking forms have been canceled successfully");
			} else {
				page = JspPageName.AFTER_CANCELING_PAGE;
				request.getSession(false).setAttribute("cancelingFlag", "false");
				LOG.error("Error in canceling booking forms");
			}
		} catch (ServiceException ex) {
			throw new CommandException("Unable to cancel booking forms. ", ex);
		} finally {
			request.getSession(false).setAttribute("bookingMapToCancel", null);					// обнуляем список заявок для отмены, чтобы больше им нельзя было воспользоваться
		}
		
		return page;
	}
}
