package by.epam.booking.command.impl;

import java.util.Calendar;
import java.util.Date;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import by.epam.booking.command.Command;
import by.epam.booking.command.CommandException;
import by.epam.booking.command.CommandType;
import by.epam.booking.command.JspPageName;
import by.epam.booking.command.RequiredRoleName;
import by.epam.booking.domain.ApartmentType;
import by.epam.booking.domain.BedType;
import by.epam.booking.domain.BookingForm;
import by.epam.booking.domain.CardType;
import by.epam.booking.domain.User;
import by.epam.booking.service.ClientServiceGroup;
import by.epam.booking.service.ServiceException;

/**
 * The Class ConfirmBookingCommand.
 */
public class ConfirmBookingCommand implements Command {
	
	/** The Constant LOG. */
	private static final Logger LOG = Logger.getLogger(ConfirmBookingCommand.class);
	
	/** The Constant INSTANCE. */
	private static final ConfirmBookingCommand INSTANCE = new ConfirmBookingCommand();
	
	/** The Constant BOOKING_INITIAL_STATUS. */
	private static final String BOOKING_INITIAL_STATUS = "temporary";
	
	/** The Constant BOOKING_FORCIBLY_CLOSED_FLAG. */
	private static final boolean BOOKING_FORCIBLY_CLOSED_FLAG = false;
	
	/** The Constant AMOUNT_OF_GUESTS_REQUEST_PARAMETER. */
	private static final String AMOUNT_OF_GUESTS_REQUEST_PARAMETER = "amount-of-guests";
	
	/** The Constant AMOUNT_OF_APARTMENTS_REQUEST_PARAMETER. */
	private static final String AMOUNT_OF_APARTMENTS_REQUEST_PARAMETER = "amount-of-apartments";
	
	/** The Constant BED_TYPE_REQUEST_PARAMETER. */
	private static final String BED_TYPE_REQUEST_PARAMETER = "bed-type";
	
	/** The Constant SMS_APPROVAL_REQUEST_PARAMETER. */
	private static final String SMS_APPROVAL_REQUEST_PARAMETER = "sms-approval-flag";
	
	/** The Constant CARD_TYPE_REQUEST_PARAMETER. */
	private static final String CARD_TYPE_REQUEST_PARAMETER = "card-type";
	
	/** The Constant CARD_NUMBER_REQUEST_PARAMETER. */
	private static final String CARD_NUMBER_REQUEST_PARAMETER = "card-number";
	
	/** The Constant CARD_EXPIRE_MONTH_REQUEST_PARAMETER. */
	private static final String CARD_EXPIRE_MONTH_REQUEST_PARAMETER = "month-value";
	
	/** The Constant CARD_EXPIRE_YEAR_REQUEST_PARAMETER. */
	private static final String CARD_EXPIRE_YEAR_REQUEST_PARAMETER = "year-value";
	
	/** The Constant PERSONAL_PREFS_REQUEST_PARAMETER. */
	private static final String PERSONAL_PREFS_REQUEST_PARAMETER = "personal-prefs";
	
	/** The type. */
	private CommandType type;
	
	/** The required role name for the command. */
	private RequiredRoleName requiredRoleName;
	
	/**
	 * Instantiates a new confirm booking command.
	 */
	private ConfirmBookingCommand() {
		type = CommandType.REQUIRES_DB_CHANGING;
		requiredRoleName = RequiredRoleName.CLIENT;
	}
	
	/**
	 * Gets the single instance of ConfirmBookingCommand.
	 *
	 * @return single instance of ConfirmBookingCommand
	 */
	public static ConfirmBookingCommand getInstance() {
		return INSTANCE;
	}
	
	/* (non-Javadoc)
	 * @see by.epam.booking.command.Command#getType()
	 */
	@Override
	public CommandType getCommandType() {
		return type;
	}
	
	/* (non-Javadoc)
	 * @see by.epam.booking.command.Command#getRequiredRoleName()
	 */
	@Override
	public RequiredRoleName getRequiredRoleName() {
		return requiredRoleName;
	}
	
	/* (non-Javadoc)
	 * @see by.epam.booking.command.Command#execute(javax.servlet.http.HttpServletRequest)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public String execute(HttpServletRequest request) throws CommandException {
		String page = null;
		BedType bedType = null;
		CardType cardType = null;
		Map<Boolean, CommandDenialReasonName> saveResultMap = null;
		type = CommandType.REQUIRES_DB_CHANGING;														// если при истечении сессии или после работы фильтра тип менялся (хотя бы раз), нужно возвращаться всегда обратно!
		
		if (request.getSession(false) == null) {														// проверка, не закончилась ли сессия
			page = JspPageName.INDEX_PAGE;
			type = CommandType.WITHOUT_DB_CHANGING;
			request.setAttribute("mustAuthorizeAgainFlag", "true");
			return page;
		}
		
		if (!requiredRoleName.equals(request.getSession(false).getAttribute("userRoleName"))) {			// если пользователь вручную набирает в адресной строке команду не для своей роли
			LOG.warn("User is trying to use a command not of his role");
			page = JspPageName.INDEX_PAGE;
			type = CommandType.WITHOUT_DB_CHANGING;
			return page;
		}
		
		if (request.getAttribute("pageFromConfirmBookingFilter") != null) {								// не забраковал ли фильтр дданые от клиента
			page = request.getAttribute("pageFromConfirmBookingFilter").toString();
			type = CommandType.WITHOUT_DB_CHANGING;
			return page;
		}
		
		if (request.getSession(false).getAttribute("chosenApartmentType") == null) {					// для ситуации, если клиент нажмёт "назад" после сохранения/не сохранения заявки по бронированию
			page = JspPageName.INDEX_PAGE;
			type = CommandType.WITHOUT_DB_CHANGING;
			return page;
		}
		
		Set<BedType> setOfBedTypes = (Set<BedType>) request.getSession(false).getAttribute("bedTypes");
		Set<CardType> setOfCardTypes = (Set<CardType>) request.getSession(false).getAttribute("cardTypes");
		
		User user = (User) request.getSession(false).getAttribute("user");
		int userId = user.getId();
		LOG.debug("userId: " + userId);
		
		Date dateFrom = (Date) request.getSession(false).getAttribute("enteredArrivalDate");
		Date dateUntil = (Date) request.getSession(false).getAttribute("enteredLeavingDate");
		long arrivalDate = dateFrom.getTime();
		long leavingDate = dateUntil.getTime();
		
		ApartmentType chosenApartmentType = (ApartmentType) request.getSession(false).getAttribute("chosenApartmentType");
		int amountOfGuests = Integer.parseInt(request.getParameter(AMOUNT_OF_GUESTS_REQUEST_PARAMETER).toString());
		int amountOfApartments = Integer.parseInt(request.getParameter(AMOUNT_OF_APARTMENTS_REQUEST_PARAMETER).toString());
		double totalPrice = (Double) (request.getSession(false).getAttribute("apartmentTotalPrice"));
		
		String chosenBedTypeDescription = request.getParameter(BED_TYPE_REQUEST_PARAMETER);
		String chosenCardTypeName = request.getParameter(CARD_TYPE_REQUEST_PARAMETER);
		LOG.debug(chosenBedTypeDescription);
		LOG.debug(chosenCardTypeName);
		
		for(BedType bedTypeTmp : setOfBedTypes) {
			if (bedTypeTmp.getDescription().equals(chosenBedTypeDescription)) {
				bedType = bedTypeTmp;
			}
		}
		
		for(CardType cardTypeTmp : setOfCardTypes) {
			if (cardTypeTmp.getName().equals(chosenCardTypeName)) {
				cardType = cardTypeTmp;
			}
		}
		LOG.debug(bedType);
		LOG.debug(cardType);
		
		String cardNumber = request.getParameter(CARD_NUMBER_REQUEST_PARAMETER);
		Calendar expireDate = Calendar.getInstance();
		expireDate.set(Calendar.MONTH, Integer.parseInt(request.getParameter(CARD_EXPIRE_MONTH_REQUEST_PARAMETER)));
		expireDate.set(Calendar.YEAR, Integer.parseInt(request.getParameter(CARD_EXPIRE_YEAR_REQUEST_PARAMETER)));
		expireDate.set(Calendar.DATE, 1);
		long cardExpireDate = expireDate.getTimeInMillis();
		
		long bookingCreationDate = Calendar.getInstance().getTimeInMillis();
		boolean bookingAprovalSms = (Boolean.parseBoolean(request.getParameter(SMS_APPROVAL_REQUEST_PARAMETER )) == true) ? true : false;
		LOG.debug(bookingAprovalSms);
		
		String personalPrefs = request.getParameter(PERSONAL_PREFS_REQUEST_PARAMETER);
		String status = BOOKING_INITIAL_STATUS;
		boolean forciblyClosedFlag = BOOKING_FORCIBLY_CLOSED_FLAG;
		
		BookingForm bookingForm = new BookingForm(userId, arrivalDate, leavingDate, chosenApartmentType, amountOfGuests, amountOfApartments, 
												  totalPrice, bedType, personalPrefs, cardType, cardNumber, cardExpireDate, bookingAprovalSms, 
												  bookingCreationDate, status, forciblyClosedFlag);
		
		try {
			saveResultMap = ClientServiceGroup.getInstance().saveBookingForm(dateFrom.getTime(), dateUntil.getTime(), bookingForm);
			if (saveResultMap.containsKey(true)) {
				page = JspPageName.AFTER_BOOKING_PAGE;
				request.getSession(false).setAttribute("saveFlag", "true");
				LOG.info("Booking form has been successfully saved");
			} else {
				page = JspPageName.AFTER_BOOKING_PAGE;
				request.getSession(false).setAttribute("denialReason", saveResultMap.get(false));
				LOG.error("Error in saving booking form. Denial reason: " + saveResultMap.get(false));
			}
		} catch (ServiceException ex) {
			throw new CommandException("Unable to save booking form. ", ex);
		} finally {
			request.getSession(false).setAttribute("tableOfAvailableApartmentsAndTypes", null);							// обнуляем лежащую в сессии таблицу доступных номеров с типами, т.к. она уже не актуальна
			request.getSession(false).setAttribute("chosenApartmentId", null);											// и все остальные атрибуты, положенные в сессию командой booking
			request.getSession(false).setAttribute("apartmentDescription", null);										// чтобы больше ими нельзя было воспользоваться
			request.getSession(false).setAttribute("apartmentGuestsMax", null);
			request.getSession(false).setAttribute("apartmentTotalPrice", null);
			request.getSession(false).setAttribute("amountOfApartments", null);
			request.getSession(false).setAttribute("fullEnteredArrivalDateAsString", null);
			request.getSession(false).setAttribute("fullEnteredLeavingDateAsString", null);
			request.getSession(false).setAttribute("arrivalTimeSince", null);
			request.getSession(false).setAttribute("leavingTimeUntil", null);
			request.getSession(false).setAttribute("currentYear", null);
			request.getSession(false).setAttribute("chosenApartmentType", null);
			request.getSession(false).setAttribute("bedTypes", null);
			request.getSession(false).setAttribute("cardTypes", null);
			request.getSession(false).setAttribute("enteredArrivalDateAsString", null);
			request.getSession(false).setAttribute("enteredLeavingDateAsString", null);
			request.getSession(false).setAttribute("enteredArrivalDate", null);
			request.getSession(false).setAttribute("enteredLeavingDate", null);
			request.getSession(false).setAttribute("amountOfNights", null);
			request.getSession(false).setAttribute("nightCase", null);			
		}
		return page;
	}
}
