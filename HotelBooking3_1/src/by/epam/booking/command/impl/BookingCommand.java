package by.epam.booking.command.impl;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import by.epam.booking.command.Command;
import by.epam.booking.command.CommandException;
import by.epam.booking.command.CommandType;
import by.epam.booking.command.JspPageName;
import by.epam.booking.command.RequiredRoleName;
import by.epam.booking.domain.ApartmentType;
import by.epam.booking.domain.BedType;
import by.epam.booking.domain.CardType;
import by.epam.booking.service.ClientServiceGroup;
import by.epam.booking.service.ServiceException;

/**
 * The Class BookingCommand.
 */
public class BookingCommand implements Command {
	
	/** The Constant LOG. */
	private static final Logger LOG = Logger.getLogger(BookingCommand.class);
	
	/** The Constant INSTANCE. */
	private static final BookingCommand INSTANCE = new BookingCommand();
		
	/** The Constant APARTMENT_ID_REQUEST_PARAMETER. */
	private static final String APARTMENT_ID_REQUEST_PARAMETER = "apartment-id";
	
	/** The Constant APARTMENT_DESCRIPTION_REQUEST_PARAMETER. */
	private static final String APARTMENT_DESCRIPTION_REQUEST_PARAMETER = "apartment-description";
	
	/** The Constant APARTMENT_GUESTS_MAX_AMOUNT_REQUEST_PARAMETER. */
	private static final String APARTMENT_GUESTS_MAX_AMOUNT_REQUEST_PARAMETER = "apartment-guests-max";
	
	/** The Constant APARTMENT_TOTAL_PRICE_REQUEST_PARAMETER. */
	private static final String APARTMENT_TOTAL_PRICE_REQUEST_PARAMETER = "apartment-total-price";
	
	/** The Constant AMOUNT_OF_APARTMENTS_REQUEST_PARAMETER. */
	private static final String AMOUNT_OF_APARTMENTS_REQUEST_PARAMETER = "amount-of-apartments";
	
	/** The type. */
	private CommandType type;
	
	/** The required role name for the command. */
	private RequiredRoleName requiredRoleName;
	
	/**
	 * Instantiates a new booking command.
	 */
	private BookingCommand() {
		type = CommandType.WITHOUT_DB_CHANGING;
		requiredRoleName = RequiredRoleName.CLIENT;
	}
	
	/**
	 * Gets the single instance of BookingCommand.
	 *
	 * @return single instance of BookingCommand
	 */
	public static BookingCommand getInstance() {
		return INSTANCE;
	}
	
	/* (non-Javadoc)
	 * @see by.epam.booking.command.Command#getType()
	 */
	@Override
	public CommandType getCommandType() {
		return type;
	}
	
	/* (non-Javadoc)
	 * @see by.epam.booking.command.Command#getRequiredRoleName()
	 */
	@Override
	public RequiredRoleName getRequiredRoleName() {
		return requiredRoleName;
	}
	
	/* (non-Javadoc)
	 * @see by.epam.booking.command.Command#execute(javax.servlet.http.HttpServletRequest)
	 */
	@Override
	public String execute(HttpServletRequest request) throws CommandException {
		int chosenAparmentId = 0;
		int amountOfApartments = 0;
		int apartmentGuestsMax = 0;
		String page = null;
		String apartmentDescription = null;
		ApartmentType chosenApartmentType = null;
		double apartmentTotalPrice = 0.0;
		Date fullEnteredArrivalDate = null;
		Date fullEnteredLeavingDate = null;
		Set<CardType> setOfCardTypes = null;
		Set<BedType> setOfBedTypes = null;
		Set<BedType> setOfBedTypesTmp = new HashSet<BedType>();
		
		if (request.getSession(false) == null) {														// проверка, не закончилась ли сессия
			page = JspPageName.INDEX_PAGE;
			request.setAttribute("mustAuthorizeAgainFlag", "true");
			return page;
		}
		
		if (!requiredRoleName.equals(request.getSession(false).getAttribute("userRoleName"))) {			// если пользователь вручную набирает в адресной строке команду не для своей роли
			LOG.warn("User is trying to use a command not of his role");
			page = JspPageName.INDEX_PAGE;
			return page;
		}
		
		if (Integer.parseInt(request.getParameter(AMOUNT_OF_APARTMENTS_REQUEST_PARAMETER)) == 0) {		// если клиент не выбрал ни одного номера
			page = JspPageName.INDEX_PAGE;
			request.setAttribute("noApartmensChosenFlag", "true");
			return page;
		}
		
		Locale locale = (Locale) request.getSession(false).getAttribute("locale");
		DateFormat dateFormatter = DateFormat.getDateInstance(DateFormat.FULL, locale);
		DateFormat timeFormatter = DateFormat.getTimeInstance(DateFormat.SHORT, locale);
		
		chosenAparmentId = Integer.parseInt(request.getParameter(APARTMENT_ID_REQUEST_PARAMETER));
		amountOfApartments = Integer.parseInt(request.getParameter(AMOUNT_OF_APARTMENTS_REQUEST_PARAMETER));
		apartmentDescription = request.getParameter(APARTMENT_DESCRIPTION_REQUEST_PARAMETER);
		apartmentGuestsMax = Integer.parseInt(request.getParameter(APARTMENT_GUESTS_MAX_AMOUNT_REQUEST_PARAMETER));
		apartmentTotalPrice = Double.parseDouble(request.getParameter(APARTMENT_TOTAL_PRICE_REQUEST_PARAMETER)) * amountOfApartments;
		fullEnteredArrivalDate = (Date) request.getSession().getAttribute("enteredArrivalDate");
		fullEnteredLeavingDate = (Date) request.getSession().getAttribute("enteredLeavingDate");
		LOG.debug(fullEnteredArrivalDate);
		LOG.debug(fullEnteredLeavingDate);
		
		request.getSession(false).setAttribute("chosenApartmentId", chosenAparmentId);
		request.getSession(false).setAttribute("apartmentDescription", apartmentDescription);
		request.getSession(false).setAttribute("apartmentGuestsMax", apartmentGuestsMax);
		request.getSession(false).setAttribute("apartmentTotalPrice", apartmentTotalPrice);
		request.getSession(false).setAttribute("amountOfApartments", amountOfApartments);
		request.getSession(false).setAttribute("fullEnteredArrivalDateAsString", dateFormatter.format(fullEnteredArrivalDate));
		request.getSession(false).setAttribute("fullEnteredLeavingDateAsString", dateFormatter.format(fullEnteredLeavingDate));
		request.getSession(false).setAttribute("arrivalTimeSince", timeFormatter.format(fullEnteredArrivalDate));
		request.getSession(false).setAttribute("leavingTimeUntil", timeFormatter.format(fullEnteredLeavingDate));
		request.getSession(false).setAttribute("currentYear", Calendar.getInstance().get(Calendar.YEAR));
		request.getSession(false).setAttribute("currentMonth", Calendar.getInstance().get(Calendar.MONTH) + 1);
		LOG.debug("arrivalTimeSince: " + timeFormatter.format(fullEnteredArrivalDate) + " leavingTimeUntil: " + timeFormatter.format(fullEnteredLeavingDate));
		
		try {
			chosenApartmentType = ClientServiceGroup.getInstance().getApartmentType(chosenAparmentId);
		} catch (ServiceException ex) {
			throw new CommandException("Unable to get information about apartment type.", ex);
		}
		
		try {
			setOfCardTypes = ClientServiceGroup.getInstance().getCardTypes();
		} catch (ServiceException ex) {
			throw new CommandException("Unable to get information about card types.", ex);
		}
		
		try {
			setOfBedTypes = ClientServiceGroup.getInstance().getBedTypes();
		} catch (ServiceException ex) {
			throw new CommandException("Unable to get information about bed types.", ex);
		}
				
		for (BedType bedType : setOfBedTypes) {																							// отбираем типы кроватей согласно выбранному номеру
			if (bedType.getAmountOfPersons() == apartmentGuestsMax || (bedType.getAmountOfPersons() - 1) == apartmentGuestsMax) {
				setOfBedTypesTmp.add(bedType);
			}
		}
		setOfBedTypes.clear();
		setOfBedTypes.addAll(setOfBedTypesTmp);
		
		request.getSession(false).setAttribute("chosenApartmentType", chosenApartmentType);
		request.getSession(false).setAttribute("bedTypes", setOfBedTypes);
		request.getSession(false).setAttribute("cardTypes", setOfCardTypes);		

		page = JspPageName.BOOKING_PAGE;
		return page;
	}
}
