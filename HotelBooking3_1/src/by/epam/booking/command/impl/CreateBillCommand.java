package by.epam.booking.command.impl;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.Queue;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import by.epam.booking.command.Command;
import by.epam.booking.command.CommandException;
import by.epam.booking.command.CommandType;
import by.epam.booking.command.JspPageName;
import by.epam.booking.command.RequiredRoleName;
import by.epam.booking.domain.BookingForm;
import by.epam.booking.domain.User;
import by.epam.booking.service.AdminServiceGroup;
import by.epam.booking.service.ServiceException;
import by.epam.booking.utility.NightsCountUtility;
import by.epam.booking.utility.WordCaseUtility;

/**
 * The Class CreateBillCommand.
 */
public class CreateBillCommand implements Command {
	
	/** The Constant LOG. */
	private static final Logger LOG = Logger.getLogger(CreateBillCommand.class);
	
	/** The Constant INSTANCE. */
	private static final CreateBillCommand INSTANCE = new CreateBillCommand();
		
	/** The Constant CHOSEN_BOOKING_REQUEST_PARAMETER. */
	private static final String CHOSEN_BOOKING_REQUEST_PARAMETER = "chosen-booking";
	
	/** The type. */
	private CommandType type;
	
	/** The required role name for the command. */
	private RequiredRoleName requiredRoleName;
		
	/**
	 * Instantiates a new creates the bill command.
	 */
	private CreateBillCommand() {
		type = CommandType.WITHOUT_DB_CHANGING;
		requiredRoleName = RequiredRoleName.ADMINISTRATOR;
	}
	
	/**
	 * Gets the single instance of CreateBillCommand.
	 *
	 * @return single instance of CreateBillCommand
	 */
	public static CreateBillCommand getInstance() {
		return INSTANCE;
	}
	
	/* (non-Javadoc)
	 * @see by.epam.booking.command.Command#getCommandType()
	 */
	@Override
	public CommandType getCommandType() {
		return type;
	}
	
	/* (non-Javadoc)
	 * @see by.epam.booking.command.Command#getRequiredRoleName()
	 */
	@Override
	public RequiredRoleName getRequiredRoleName() {
		return requiredRoleName;
	}
	
	/* (non-Javadoc)
	 * @see by.epam.booking.command.Command#execute(javax.servlet.http.HttpServletRequest)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public String execute(HttpServletRequest request) throws CommandException {
		String page = null;
		User userOfBooking = null;
		boolean billExistanceFlag = false;
		BookingForm bookingFormInBill = null;
		Map<BookingForm, Queue<String>> bookingMapToCreateBill = null;
		
		if (request.getSession(false) == null) {																// проверка, не закончилась ли сессия
			page = JspPageName.INDEX_PAGE;
			type = CommandType.WITHOUT_DB_CHANGING;
			request.setAttribute("mustAuthorizeAgainFlag", "true");
			return page;
		}
		
		if (!requiredRoleName.equals(request.getSession(false).getAttribute("userRoleName"))) {					// если пользователь вручную набирает в адресной строке команду не для своей роли
			LOG.warn("User is trying to use a command not of his role");
			page = JspPageName.INDEX_PAGE;
			return page;
		}
		
		String chosenBooking = request.getParameter(CHOSEN_BOOKING_REQUEST_PARAMETER);
		LOG.debug("Booking form ID to form bill for: " + chosenBooking);
		if (chosenBooking == null) {
			page = JspPageName.SEARCH_PAGE;
			request.setAttribute("noBookingsChosenFlag", "true");												// если админ ничего не выбрал, нам нужно отобразить тот же список заявок, но предупредить его
			return page;
		}
		
		int bookingId = Integer.parseInt(chosenBooking);
		bookingMapToCreateBill = (TreeMap<BookingForm, Queue<String>>) request.getSession(false).getAttribute("bookingMapToCreateBill");
		for (BookingForm bookingForm : bookingMapToCreateBill.keySet()) {
			if (bookingForm.getId() == bookingId) {
				bookingFormInBill = bookingForm;
				LOG.debug(bookingFormInBill);
			}
		}
		
		Locale locale = (Locale) request.getSession(false).getAttribute("locale");
		DateFormat dateFormatter = DateFormat.getDateInstance(DateFormat.FULL, locale);
		long fullArrivalDate = bookingFormInBill.getArrivalDate();
		long fullLeavingDate = bookingFormInBill.getLeavingDate();
		long todayDate = Calendar.getInstance(locale).getTimeInMillis();
		long cardExpireDate = bookingFormInBill.getCardExpireDate();
		int amountOfNights = NightsCountUtility.countNights(fullArrivalDate, fullLeavingDate);
		String nightCase = WordCaseUtility.getNightClause(amountOfNights);
				
		try {
			userOfBooking = AdminServiceGroup.getInstance().getUserOfBooking(bookingFormInBill.getUserId());
			LOG.debug(userOfBooking);
		} catch (ServiceException ex) {
			throw new CommandException("Unable to get information about the user of booking.", ex);
		}
		
		if (userOfBooking == null) {
			throw new CommandException("Unable to get information about the user of booking.");
		}
		
		try {
			billExistanceFlag = AdminServiceGroup.getInstance().checkBillExistance(bookingId);
			LOG.debug("Bill existance: " + billExistanceFlag);
			if (billExistanceFlag) {
				request.setAttribute("alreadySavedBillFlag", "true");
			}
		} catch (ServiceException ex) {
			throw new CommandException("Unable to check bill existance.", ex);
		}
		
		request.setAttribute("userOfBooking", userOfBooking);
		request.setAttribute("bookingFormInBill", bookingFormInBill);
		request.setAttribute("amountOfNights", amountOfNights);
		request.setAttribute("nightCase", nightCase);
		request.setAttribute("cardExpireDate",  dateFormatter.format(new Date(cardExpireDate)));
		request.setAttribute("todayDate", dateFormatter.format(new Date(todayDate)));
		request.setAttribute("fullArrivalDate", dateFormatter.format(new Date(fullArrivalDate)));
		request.setAttribute("fullLeavingDate", dateFormatter.format(new Date(fullLeavingDate)));
		request.getSession(false).setAttribute("forSavingFlag", "true");
		
		page = JspPageName.BILL_PAGE;
		return page;
	}
}
