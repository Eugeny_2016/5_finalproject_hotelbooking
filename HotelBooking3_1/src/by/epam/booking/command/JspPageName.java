package by.epam.booking.command;

/**
 * The Class JspPageName.
 */
public class JspPageName {
	
	/**
	 * Instantiates a new jsp page name.
	 */
	private JspPageName() {}
	
	/** The Constant INDEX_PAGE. */
	public static final String INDEX_PAGE = "index.jsp";
	
	/** The Constant ERROR_PAGE. */
	public static final String ERROR_PAGE = "jsp/error/error.jsp";
	
	/** The Constant REGISTRATION_PAGE. */
	public static final String REGISTRATION_PAGE = "jsp/main/registration.jsp";
	
	/** The Constant SEARCH_PAGE. */
	public static final String SEARCH_PAGE = "jsp/main/clientBookingSearch.jsp";
	
	/** The Constant BOOKING_PAGE. */
	public static final String BOOKING_PAGE = "jsp/main/booking.jsp";
	
	/** The Constant CLIENT_BOOKINGS_PAGE. */
	public static final String CLIENT_BOOKINGS_PAGE = "jsp/main/clientBookings.jsp";
	
	/** The Constant UNCONFIRMED_BOOKINGS_PAGE. */
	public static final String UNCONFIRMED_BOOKINGS_PAGE = "jsp/main/unconfirmedBookings.jsp";
	
	/** The Constant BILL_PAGE. */
	public static final String BILL_PAGE = "jsp/main/bill.jsp";
	
	/** The Constant AFTER_REGISTRATION_PAGE. */
	public static final String AFTER_REGISTRATION_PAGE = "jsp/result/afterRegistration.jsp";
	
	/** The Constant AFTER_BOOKING_PAGE. */
	public static final String AFTER_BOOKING_PAGE = "jsp/result/afterBooking.jsp";
	
	/** The Constant AFTER_VIEWING_PAGE. */
	public static final String AFTER_VIEWING_PAGE = "jsp/result/afterViewing.jsp";
	
	/** The Constant AFTER_CANCELING_PAGE. */
	public static final String AFTER_CANCELING_PAGE = "jsp/result/afterCanceling.jsp";
	
	/** The Constant AFTER_CONFIRMING_PAGE. */
	public static final String AFTER_CONFIRMING_PAGE = "jsp/result/afterConfirming.jsp";
	
	/** The Constant AFTER_CLOSING_PAGE. */
	public static final String AFTER_CLOSING_PAGE = "jsp/result/afterClosing.jsp";
	
	/** The Constant AFTER_FORCIBLE_CLOSING_PAGE. */
	public static final String AFTER_FORCIBLE_CLOSING_PAGE = "jsp/result/afterForcibleClosing.jsp";
	
	/** The Constant AFTER_BILL_SAVING_PAGE. */
	public static final String AFTER_BILL_SAVING_PAGE = "jsp/result/afterBillSaving.jsp";	
	
	/** The Constant BOOKING_AGREEMENT_PAGE. */
	public static final String BOOKING_AGREEMENT_PAGE = "jsp/main/bookingAgreement.jsp";
	
	/** The Constant CONTACTS_PAGE. */
	public static final String CONTACTS_PAGE = "jsp/main/contacts.jsp";

}