package by.epam.booking.command;

import javax.servlet.http.HttpServletRequest;

/**
 * The Interface Command.
 */
public interface Command {
	
	/**
	 * Executes the command's logic.
	 *
	 * @param request the request
	 * @return the string
	 * @throws CommandException if some exception arose in it's proceeding
	 */
	String execute(HttpServletRequest request) throws CommandException;
	
	/**
	 * Gets the type of the command instance.
	 *
	 * @return the type of the command
	 */
	CommandType getCommandType();
	RequiredRoleName getRequiredRoleName();

}
