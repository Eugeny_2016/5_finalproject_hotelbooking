package by.epam.booking.command;

/**
 * The Class CommandException.
 */
public class CommandException extends Exception {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;

	/**
	 * Instantiates a new command exception.
	 */
	public CommandException() {
		super();
	}

	/**
	 * Instantiates a new command exception.
	 *
	 * @param msg the msg
	 * @param ex the ex
	 */
	public CommandException(String msg, Throwable ex) {
		super(msg, ex);
	}

	/**
	 * Instantiates a new command exception.
	 *
	 * @param msg the msg
	 */
	public CommandException(String msg) {
		super(msg);
	}

	/**
	 * Instantiates a new command exception.
	 *
	 * @param ex the ex
	 */
	public CommandException(Throwable ex) {
		super(ex);
	}	
}
